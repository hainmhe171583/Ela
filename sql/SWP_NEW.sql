
Create database SWP_NEW;
use SWP_NEW;

create table `User` (
	id int auto_increment primary key,
    username nvarchar(200) not null,
    `password` nvarchar(200),
    email nvarchar(200) not null,
    balance decimal,
    phone nvarchar(200),
    actived bit,
    banned bit,
    notification_code varchar(200),
	`role` nvarchar(50),
	created_at timestamp,
    created_by int,
    updated_at TIMESTAMP,
    updated_by int,
    deleted_at TIMESTAMP,
    deleted_by int,
    isDeleted bit
);

INSERT INTO `User` (username, `password`, email, balance, banned, phone, actived , `role`)
VALUES
('user1', 'password1', 'user1@example.com', 0, 0, 'Phone1',1, 'user'),
('user2', 'password2', 'user2@example.com', 0, 0,'Phone2', 1,'user'),
('user3', 'password3', 'user3@example.com', 0, 0, 'Phone3', 1,'user'),
('user4', 'password4', 'user4@example.com', 0, 0, 'Phone4', 1,'user'),
('user5', 'password5', 'user5@example.com', 0, 0, 'Phone5', 1,'user'),
('user6', 'password6', 'user6@example.com', 0, 0, 'Phone6', 0,'user');

create table Transac(
	id int auto_increment primary key not null,
    `type` nvarchar(20)
);

insert into transac (type)
values ('NẠP TIỀN'), 
       ('PHÍ TẠO ĐƠN HÀNG'),
       ('MUA HÀNG'),
       ('BÁN HÀNG'),
       ('RÚT TIỀN'),
       ('HOÀN TRẢ'),
       ('PHÍ GIAO DỊCH');

create table Payment (
	id int auto_increment primary key not null,
    vnp_TxnRef nvarchar(20),
    payment_code nvarchar(100),
    amount double,
    link text,
    `status` bit,
    transId int not null,
    foreign key (transId) references `transac`(id),
    userID int not null,
    foreign key (userID) references `User`(id),
    created_at timestamp,
    created_by int
);

create table Category (
	id int auto_increment primary key,
    categoryName nvarchar(200) not null,
    `describe` nvarchar(200),
    created_at timestamp,
    created_by int,
    updated_at TIMESTAMP,
    updated_by int,
    deleted_at TIMESTAMP,
    deleted_by int,
    isDeleted bit
);

INSERT INTO Category (categoryName, `describe`)
VALUES
('Tài khoản', '/ElaTrading/assets/image/taikhoan.png'),
('Phần Mềm', '/ElaTrading/assets/image/phanmem.png'),
('Khác', '/ElaTrading/assets/image/more.png');

create table `Status` (
	id int auto_increment primary key,
    statusName nvarchar(200) not null
);

INSERT INTO `Status` (statusName)
VALUES
('Upload'),
('Processing'),
('Rejected'),
('Accepted'),
('Reported'),
('other');

create table Product (
	id int auto_increment primary key,
    `code` nvarchar(200) not null,
    productName nvarchar(200) not null,
    `describe` text not null,
    price decimal,
    stock int,
    contact nvarchar(200) not null,
    is_Private bit,
    dataProduct text not null,
    link nvarchar(200) not null,
    statusID int,
    foreign key (statusID) references `Status`(id),
    categoryID int,
    foreign key (categoryID) references `Category`(id),
    userID int,
    foreign key (userID) references `User`(id),
    created_at timestamp,
    created_by int,
    updated_at TIMESTAMP,
    updated_by int,
    deleted_at TIMESTAMP,
    deleted_by int,
    isDeleted bit
);

INSERT INTO Product (productName, `code`,`describe`, price, stock, link, contact, is_Private, dataProduct, statusID, categoryID, userID, isDeleted)
VALUES
('Product 1', '88c175da-6a51-4767-9cfb-3de5d15cccd2', 'Description for Product 1', 10, 1,'link1', 'Zalo',0,'text1', 1, 1, 1, 0),
('Product 2', '696a03a5-689d-44ad-ab32-7f92bb1e8f23', 'Description for Product 2', 10, 1,'link2','Zalo', 0,'text2', 1, 2, 2, 0),
('Product 3', 'e0fc44ce-774d-4b6d-9b48-ebee73fbbbbe', 'Description for Product 3', 20, 0,'link3','Zalo', 0,'text3', 3, 1, 3, 0),
('Product 4', 'a28920c3-4aa3-4ba3-b14a-f6271bdeb43a', 'Description for Product 4', 10, 1,'link4','Zalo', 0,'text4', 1, 2, 1, 1),
('Product 5', '25f26656-6cc4-45b1-82e4-0ef2596ca194', 'Description for Product 5', 50, 1,'link5','Zalo', 1,'text5', 1, 3, 5, 0),
('Product 6', 'e550a2e0-67b9-4e7f-b4ad-1d57f25de2c8', 'Description for Product 6', 150, 1,'link6', 'Zalo', 0,'text6', 1, 1, 4, 0),
('Product 7', 'e46cf0a5-66b5-49b4-8a2f-4e4d2cde6143', 'Description for Product 7', 100, 1,'link7', 'Zalo', 0,'text7', 1, 1, 1, 1),
('Product 8', 'e0da345f0-958c-4329-8bfc-2c4d3b1a2a80', 'Description for Product 8', 150, 1,'link8', 'Zalo', 1,'text8', 1, 3, 1, 0);

create table Rating (
	id int auto_increment primary key,
    `code` nvarchar(200) not null,
    rate int not null,
    `comment` text,
    userID int,
    foreign key (userID) references `User`(id),
    created_at timestamp,
    created_by int,
    updated_at TIMESTAMP,
    updated_by int,
    deleted_at TIMESTAMP,
    deleted_by int,
    isDeleted bit
);
INSERT INTO Rating (rate, `code`, `comment`, isDeleted, userID, created_by )
VALUES
(4, 'e479357d-b569-447c-9748-3e907f9719cz', 'Good product!', 0, 3, 3),
(5, 'a27ab97a-9806-4593-9476-c7c682e931ez', 'Excellent service!', 0, 1, 1),
(5, 'a27ab97a-9806-4593-9476-c7c68fssaf1ez', 'Excellent hoho!', 0, 2, 2),
(3, '4abb9060-3803-4cdb-a4fc-c60f51d99652', 'Average experience.', 0, 4, 4);

create table Trading (
	id int auto_increment primary key,
    `code` nvarchar(200) not null,
    total decimal not null,
    statusID int,
    foreign key (statusID) references `Status`(id),
    productID int,
    foreign key (productID) references Product(id),
    userID int,
    foreign key (userID) references `User`(id),
    created_at timestamp,
    created_by int,
    updated_at TIMESTAMP,
    updated_by int,
    deleted_at TIMESTAMP,
    deleted_by int,
    isDeleted bit
);

create table Report (
	id int auto_increment primary key,
    `code` nvarchar(200) not null,
    `title` text not null,
    `describe` text not null,
    call_admin boolean not null,
    statusID int, 
    foreign key (statusID) references `Status`(id),
    tradingID int,
    foreign key (tradingID) references Trading(id),
    userID int,
	foreign key (userID) references `User`(id),
    created_at timestamp,
    created_by int,
    updated_at TIMESTAMP,
    updated_by int,
    deleted_at TIMESTAMP,
    deleted_by int,               
    isDeleted bit
);

create table token (
	id int auto_increment primary key,
    `token` nvarchar(200) not null,
    email nvarchar(200) not null,
    expiryDate TIMESTAMP
);

create table `entityType`
(
	id int auto_increment primary key,
    entity nvarchar(25) not null,
    description nvarchar(100) not null
);

INSERT INTO `entitytype` (`id`, `entity`, `description`) VALUES ('1', 'trading', 'đã thực hiện giao dịch cho sản phẩm_của bạn.');
INSERT INTO `entitytype` (`id`, `entity`, `description`) VALUES ('2', 'trading', 'đã chấp nhận giao dịch cho sản phẩm_của bạn.');
INSERT INTO `entitytype` (`id`, `entity`, `description`) VALUES ('3', 'report', 'đã khiếu nại về sản phẩm_của bạn.');
INSERT INTO `entitytype` (`id`, `entity`, `description`) VALUES ('4', 'report', 'đã khiếu nại về sản phẩm');
INSERT INTO `entitytype` (`id`, `entity`, `description`) VALUES ('5', 'report', 'Yêu cầu khiếu nại về sản phẩm_đã được chấp nhận.');
INSERT INTO `entitytype` (`id`, `entity`, `description`) VALUES ('6', 'report', 'Yêu cầu khiếu nại về sản phẩm_đã bị từ chối.');
INSERT INTO `entitytype` (`id`, `entity`, `description`) VALUES ('7', 'banking', 'Yêu cầu nạp tiền của bạn đã được chấp nhận.');

create table `notifications`
(
	id int auto_increment primary key, 
    code varchar(200),
    url nvarchar(255),   
    `status` bit,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    entityTypeId int,
    foreign key (entityTypeId) references `entityType`(id)
);

create table `subject`
(
	id int auto_increment primary key,
    actorId int,
    foreign key (actorId) references `user`(id),
    notificationsId int,
    foreign key (notificationsId) references `notifications`(id)
);

create table `in_subject`
(
	id int auto_increment primary key, 
    notificationsId int,
    receiverId int,
    foreign key (receiverId) references `user`(id),
    foreign key (notificationsId) references `notifications`(id)
);

create table `di_subject`
(
	id int auto_increment primary key,
    productId int,
    foreign key (productId) references `product`(id),
    notificationsId int,
    foreign key (notificationsId) references `notifications`(id)
);

create table chat (
	id int auto_increment primary key,
    `code` varchar(200),
    `textcontent` text,
    `imagecontent` text,
    senderid int,
    foreign key (senderid) references `user`(id),
    created_at timestamp,
    created_by int,
    updated_at TIMESTAMP,
    updated_by int,
    deleted_at TIMESTAMP,
    deleted_by int,               
    isDeleted bit
);

INSERT INTO chat (`code`,textcontent, imagecontent, senderid, created_at, isDeleted)
VALUES  ('a34fd51d-e7f3-4fab-9905-1de029a62360', 'Chào bạn!', null, 1, now(), 0),
		('7876d6e2-ace1-4121-9bdb-9551fd340401', 'Chào shop!', null, 2, now(), 0),
        ('d8cf7bbf-c06a-4ac4-88df-411bd35c14ac', 'Chào mọi người !', null, 3, now(), 0),
        ('c32fc372-408c-4934-b1b8-35afd507ec76', 'Vấn đề ....', null, 1, now(), 0),
        ('db508b91-39cf-4c44-8d16-6627ccd515e8', 'Vấn đề ....', null, 2, now(), 0),
        ('4abb9060-3803-4cdb-a4fc-c60f51d99652', 'Vấn đề ....', null, 1, now(), 0),
        ('a27ab97a-9806-4593-9476-c7c68fssaf12', 'Vấn đề ....', null, 3, now(), 0),
        ('e479357d-b569-447c-9748-3e907f9719cf', 'Vấn đề ....', null, 1, now(), 0),
        ('25f26656-6cc4-45b1-82e4-0ef2596ca194', 'Vấn đề ....', null, 2, now(), 0);
        
create table withdraw(
	id int auto_increment primary key,
    `code` nvarchar(200) not null,
    total decimal not null,
	userinfo text,
    respond text,
    statusID int,
    foreign key (statusID) references `Status`(id),
    userId int not null,
    foreign key (userID) references `User`(id),
    created_at timestamp,
    updated_at timestamp
);


delimiter |
CREATE EVENT update_old_data_event
ON SCHEDULE EVERY 1 DAY
DO
BEGIN
insert into `swp_new`.`payment`(`payment_code`,`amount`,`userID`,`status`,`transId`,`created_at`)
select t.`code`,t.`total`,p.`userID`,1,4,now() from `swp_new`.`trading` as t 
inner join `swp_new`.`product` as p on p.id=t.productID
where t.`created_at` < NOW() - INTERVAL 3 DAY and t.`statusID`=1;

update `swp_new`.`user` as u
join (
select t.`total` as `total`,p.`userID` as uid from `swp_new`.`trading` as t 
inner join `swp_new`.`product` as p on p.id=t.productID
where t.`created_at` < NOW() - INTERVAL 3 DAY and `statusID`=1
) as s on u.id=s.uid
set u.balance=u.balance+s.`total`;

UPDATE `swp_new`.`trading`
SET
`statusID` = 3,
`updated_at` = now()
WHERE `created_at` < NOW() - INTERVAL 3 DAY and `statusID`=1;
END |
DELIMITER ;

ALTER EVENT update_old_data_event ENABLE;

show events;




