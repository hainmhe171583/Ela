<%-- 
    Document   : productList
    Created on : Feb 25, 2024, 9:58:07 PM
    Author     : admin
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="vn">
        <head >
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="robots" content="NoIndex, NoFollow">
                <title>Đơn bán của tôi</title>
        </head>
        <body>
                <jsp:include page="/views/base/system_base/header.jsp"/>
                <style>
                        tr:hover {
                                background-color: #f0f0f0;
                        }
                        tr .btn {
                                font-size: 10px;
                        }
                        td {
                                vertical-align: middle !important;
                        }
                </style>
                <c:if test="${requestScope.listP != null}">
                        <div class="container">
                                <div class="row">
                                        <div class="col-md-12">
                                                <div class="panel panel-default">

                                                        <div class="panel-heading">
                                                                Đơn hàng của tôi
                                                        </div>
                                                        <p class="text-success" style="font-family: sans-serif; font-size: medium;">${mess}</p>
                                                        <div class="panel-body">
                                                                <div style="float: right; margin-bottom: 20px">
                                                                        <form action="/ElaTrading/action/products/excel" method="get"> 
                                                                                <select name="fileType" style="width: 100px; height: 30px; border-radius: 5px">
                                                                                        <option value="excel">Excel</option>
                                                                                        <option value="txt">Text</option>
                                                                                </select>
                                                                                <input type="submit"  class="mb-0 text-center btn btn-primary" value="Tải về file">
                                                                        </form>  
                                                                </div>
                                                                <div style="float: left; margin-bottom: 20px">
                                                                        <form action="/ElaTrading/action/products/deleted" method="get"> 
                                                                                <button type="submit" class="mb-0 text-center btn btn-primary"> Danh sách sản phẩm đã xoá</button> 
                                                                        </form>  
                                                                </div>

                                                                <table class="table table-striped table-hover"  id="productTable">
                                                                        <thead>
                                                                                <tr style="font-size: 13px">
                                                                                        <th>Tên sản phẩm</th>
                                                                                        <th>Giá</th>
                                                                                        <th >Loại sản phẩm </th>
                                                                                        <th >Người mua </th>
                                                                                        <th>Trạng thái</th>
                                                                                </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                                <tr>
                                                                                        <c:forEach items="${listP}" var="o">
                                                                                                <td>${o.productName}</td>
                                                                                                <td>${o.price}</td>
                                                                                                <td>${o._getCategory().categoryName}</td>
                                                                                                <td>
                                                                                                        <c:if test="${o.getStatusID() == 4 }">   
                                                                                                                ${o._getUser().username}
                                                                                                        </c:if>
                                                                                                </td>
                                                                                                <td>
                                                                                                        <c:if test="${o.isIsDeleted() == true}">
                                                                                                                <div class="text-danger" style="color: black">Đã xoá</div>
                                                                                                        </c:if>
                                                                                                        <c:if test="${o.isIsDeleted() == false}">
                                                                                                                <c:if test="${o.getStatusID() == 1}">
                                                                                                                        <div class="text-success" style="color: green">Chưa bán</div>
                                                                                                                </c:if>
                                                                                                                <c:if test="${o.getStatusID() == 2 }">
                                                                                                                        <div class="text-danger" style="color: blueviolet">Đang bán</div>
                                                                                                                </c:if>
                                                                                                                <c:if test="${o.getStatusID() == 4 }">                                                  
                                                                                                                        <div class="text-danger" style="color: red">Đã bán</div>
                                                                                                                </c:if>
                                                                                                                <c:if test="${o.getStatusID() == 5 }">
                                                                                                                        <div class="text-danger" style="color: blue">Đang bị khiếu nại</div>
                                                                                                                </c:if>
                                                                                                                <c:if test="${o.getStatusID() == 3 }">
                                                                                                                        <div class="text-danger" style="color: orange">Bị Huỷ</div>
                                                                                                                </c:if>                                                        
                                                                                                        </c:if>
                                                                                                </td>
                                                                                                <td >
                                                                                                        <a  href="product?id=${o.id}"><button type="button" class="btn btn-success"><i class="material-icons" data-toggle="tooltip" title="Details">Chi Tiết</i></button></a>
                                                                                                        <c:if test="${o.getStatusID() == 1}">
                                                                                                                <a  href="update?code=${o.code}"><button type="button" class="btn btn-warning"><i class="material-icons" data-toggle="tooltip" title="Edit">Chỉnh Sửa</i></button></a>
                                                                                                                <a href="delete?code=${o.code}"><button type="button" class="btn btn-danger"><i class="material-icons" data-toggle="tooltip" title="Delete" style="font-size: 10px">Xoá</i></button></a>
                                                                                                        </c:if>
                                                                                                        <c:if test="${o.getStatusID() == 3  || o.getStatusID() == 5}">
                                                                                                                <a href="#"><button type="button" class="btn btn-success" style="background-color: red"><i style="font-size: 10px">Chi tiết khiếu nại</i></button></a>          
                                                                                                        </c:if> 
                                                                                                </td>    
                                                                                        </tr>
                                                                                </c:forEach>
                                                                        </tbody>
                                                                </table>
                                                                <form action="/ElaTrading/action/products" method="get"style="text-align: right; margin-top: 10px">
                                                                        <select name="status" id="status" style="height: 30px;border-radius: 5px;width: 100px;">
                                                                                <option value="All">Tất cả</option>
                                                                                <option value="DangBan">Đang bán</option>
                                                                                <option value="ChuaBan">Chưa bán</option>
                                                                                <option value="DaBan">Đã bán</option>
                                                                                <option value="BiHuy">Bị Huỷ</option>
                                                                                <option value="KhieuNai">Khiếu Nại</option>
                                                                        </select>
                                                                        <input type="submit"  class="mb-0 text-center btn btn-primary" value="Lọc">
                                                                </form>
                                                                <c:if test="${countListProduct > 0}">
                                                                        <ul class="pagination">
                                                                                <c:set var="currentPage" value="${param.index}" />
                                                                                <c:if test="${empty currentPage}">
                                                                                        <c:set var="currentPage" value="1" />
                                                                                </c:if>

                                                                                <c:forEach begin="1" end="${endP}" var="i">     
                                                                                        <li class="page-item text-white ${currentPage == i ? 'active' : ''}">
                                                                                                <a href="/ElaTrading/action/products?status=${status}&index=${i}" class="page-link">${i}</a>
                                                                                        </li>
                                                                                </c:forEach>
                                                                        </ul>
                                                                </c:if>
                                                                <!--                                                                <a href="statisticproductseller" class="list-group-item list-group-item-action py-2 ripple">
                                                                                                                                        <i class="fas fa-chart-bar fa-fw me-3"></i><span>Thống kê</span>
                                                                                                                                </a>-->
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </c:if>
                <c:if test="${requestScope.listP == null}">
                        <c:if test="${countListProductDeleted == 0}">
                                <div class="container-xl">
                                        <div class="table-responsive">                    
                                                <div class="col-md-12">
                                                        <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                        <div class="panel-body">
                                                                                <p>Danh sách trống.</p>
                                                                                <a href="/ElaTrading/action/products?status=">Quay lại</a>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </c:if>
                        <c:if test="${countListProductDeleted != 0}">
                                <c:if test="${requestScope.listD != null}">
                                        <div class="container">
                                                <div class="container">
                                                        <div class="row">
                                                                <div class="col-md-12">
                                                                        <div class="panel panel-default">
                                                                                <div class="panel-heading">
                                                                                        Danh sách sản phẩm đã xoá
                                                                                </div>
                                                                                <div class="panel-body">
                                                                                        <table class="table table-striped table-hover"  id="productTable">
                                                                                                <thead>
                                                                                                        <tr style="font-size: 13px">
                                                                                                                <th>Tên sản phẩm</th>
                                                                                                                <th>Giá</th>
                                                                                                                <th >Loại sản phẩm </th>
                                                                                                                <th>
                                                                                                                        Ngày xoá
                                                                                                                </th>
                                                                                                        </tr>
                                                                                                </thead>
                                                                                                <tbody>

                                                                                                        <c:forEach items="${listD}" var="o">
                                                                                                                <tr >
                                                                                                                        <td>${o.productName}</td>
                                                                                                                        <td>${o.price}</td>
                                                                                                                        <td>${o._getCategory().categoryName}</td>
                                                                                                                        <td>${o.deleted_at}</td>
                                                                                                                        <td >                                                           
                                                                                                                                <a  href="/ElaTrading/action/product?id=${o.id}"><button type="button" class="btn btn-warning"><i class="material-icons" data-toggle="tooltip" title="Details">Chi Tiết</i></button></a>
                                                                                                                        </td>  
                                                                                                                </tr>
                                                                                                        </c:forEach>

                                                                                                </tbody>
                                                                                        </table>
                                                                                        <c:if test="${countListProductDeleted != 0}">
                                                                                                <ul class="pagination">
                                                                                                        <c:forEach begin="1" end="${endPageRestore}" var="i">
                                                                                                                <li class="page-item text-white ${tagRestore == i ? "active": ""}"><a href="/ElaTrading/action/listdeletedproduct?index=${i}" class="page-link">${i}</a></li>
                                                                                                                </c:forEach>
                                                                                                </ul>                                   
                                                                                        </c:if>
                                                                                        <div class="text-right">
                                                                                                <a class="page-item" href="/ElaTrading/action/products?status">
                                                                                                        <button type="button" class="mb-0 text-center btn btn-primary">Back</button>
                                                                                                </a>
                                                                                        </div>
                                                                                </c:if>
                                                                        </c:if>
                                                                </c:if>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </body>
        <jsp:include page="/views/base/system_base/footer.jsp"/>
</html>


