<%-- 
    Document   : addproduct
    Created on : Jan 17, 2024, 10:19:31 PM
    Author     : hai20
--%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>Cập nhật đơn hàng</title>
                <style>
                        .info
                        {
                                margin: 5px 20px 5px;
                                font-weight: bold;
                        }
                        .style_date
                        {
                                margin: 5px 20px 5px 50px;
                                font-style: italic;
                        }
                </style>
        </head>
        <body>
                <jsp:include page="/views/base/system_base/header.jsp"/>
                <div class="container">
                        <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                        <div class="panel panel-default">
                                                <div class="panel-heading">CẬP NHẬT ĐƠN HÀNG</div>
                                                <div class="panel-body">
                                                        <div class="alert alert-info">
                                                                <div class="info">
                                                                        Đơn hàng của bạn đã được đăng bán.
                                                                </div>
                                                                <div class="style_date">
                                                                        Ngày tạo đơn hàng: <fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${pro.created_at}" />
                                                                </div>
                                                                <div class="style_date">
                                                                        Ngày cập nhật mới nhất: <fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${pro.updated_at}" />
                                                                </div> 
                                                        </div>
                                                        <form action="update" role="form" method="post">
                                                                <input type="hidden" value="${code}" readonly="true" name="code">
                                                                <div class="form-group clearfix required">
                                                                        <label class="col-md-2 control-label pt-7 text-right" >Tên sản phẩm</label>
                                                                        <div class="col-md-8">
                                                                                <input type="text" class="form-control"  name="pTitle" value="${pro.getProductName()}">
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix required">
                                                                        <label class="col-md-2 control-label pt-7 text-right">Phân loại</label>
                                                                        <div class="col-md-8">
                                                                                <select id="category" class="input-sm" name="category">
                                                                                        <option value="none"> Phân loại </option>
                                                                                        <c:forEach items="${sessionScope.clist}" var="t">
                                                                                                <c:choose>
                                                                                                        <c:when test="${t.id==pro.getCategoryID()}" >
                                                                                                                <option selected value="${t.id}"> ${t.categoryName}</option>
                                                                                                        </c:when>
                                                                                                        <c:otherwise>
                                                                                                                <option value="${t.id}"> ${t.categoryName}</option>
                                                                                                        </c:otherwise>
                                                                                                </c:choose>
                                                                                        </c:forEach>
                                                                                </select>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Giá sản phẩm</label>
                                                                                <div class="col-md-8">
                                                                                        <input type="text" class="form-control" id="formattedInput" name="price" value="${price}">
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Liên hệ</label>
                                                                                <div class="col-md-8">
                                                                                        <input type="text" class="form-control" name="cont" value="${pro.getContact()}">
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Mô tả</label>
                                                                                <div class="col-md-8">
                                                                                        <textarea id="LN_Series_Summary" name="pDes">${pro.getDescribe()}</textarea>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Thông tin ẩn</label>
                                                                                <div class="col-md-8">
                                                                                        <textarea id="LN_Series_Summary" name="data" >${pro.getDataproduct()}</textarea>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Công khai</label>
                                                                                <div class="col-md-8">
                                                                                        <c:choose>
                                                                                                <c:when test="${pro.isIsPrivate()==true}" >
                                                                                                        <input type="radio"  name="priv" value="0">Có 
                                                                                                        <input type="radio" checked="true" name="priv" value="1" style="margin-left: 50px">Không  
                                                                                                </c:when>
                                                                                                <c:otherwise>
                                                                                                        <input type="radio" checked="true" name="priv" value="0">Có
                                                                                                        <input type="radio"  name="priv" value="1" style="margin-left: 50px">Không  
                                                                                                </c:otherwise>
                                                                                        </c:choose>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Link đơn hàng</label>
                                                                                <div class="col-md-8" style="margin-top: 8px;">
                                                                                        <a href="${link}" target="_blank">${link}</a>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.2.1/tinymce.min.js" referrerpolicy="origin" type="227dddff75eef862ba087f1e-text/javascript"></script>
                                                                <script type="227dddff75eef862ba087f1e-text/javascript">
                                                                        tinymce.init({
                                                                        selector: 'textarea',
                                                                        inline: false,
                                                                        height: 300,
                                                                        skin: 'oxide-dark',
                                                                        content_css: 'dark',
                                                                        branding: false,
                                                                        menubar: false,
                                                                        contextmenu: false,
                                                                        entities: '160,nbsp,38,amp,60,lt,62,gt',
                                                                        paste_word_valid_elements: 'b,strong,i,em,u,s,a,p,br,img',
                                                                        element_format: 'html',
                                                                        formats: {
                                                                        strikethrough: { inline: 's', remove: 'all' },
                                                                        underline: { inline: 'u', remove: 'all' },
                                                                        },
                                                                        plugins: 'wordcount link image code fullscreen paste emoticons',
                                                                        toolbar: 'undo redo | bold italic underline strikethrough forecolor | link image | removeformat | fullscreen'
                                                                        });
                                                                </script>
                                                                <c:if test="${pro.status != 1}">
                                                                        <div class="form-group">
                                                                                <div class="col-md-10 col-md-offset-2">
                                                                                        <c:if test="${pro.getStatusID()==1}">
                                                                                                <button type="submit" class="btn btn-primary">
                                                                                                        Cập nhật đơn hàng
                                                                                                </button>
                                                                                                <a href="delete?code=${pro.getCode()}" >
                                                                                                        <button type="button" class="btn btn-primary" style="margin-left: 100px; background-color: red;">Xóa đơn hàng</button>
                                                                                                </a>
                                                                                        </c:if>
                                                                                </div>
                                                                        </div>
                                                                </c:if>
                                                        </form>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
                <script>
                        document.getElementById('formattedInput').addEventListener('input', function (event)
                        {
                                // Lấy giá trị từ trường input
                                let inputValue = event.target.value;
                                // Loại bỏ dấu phẩy hiện có (nếu có)
                                inputValue = inputValue.replace(/,/g, '');
                                // Chuyển đổi thành số và định dạng lại chuỗi
                                let formattedValue = Number(inputValue).toLocaleString();
                                // Gán lại giá trị vào trường input
                                event.target.value = formattedValue;
                        });
                </script>
                <jsp:include page="/views/base/system_base/footer.jsp"/>
        </body>
</html>



