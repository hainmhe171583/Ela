<%-- 
    Document   : addproduct
    Created on : Jan 17, 2024, 10:19:31 PM
    Author     : hai20
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>Đơn hàng</title>
                <style>
                        .info
                        {
                                margin: 5px 20px 5px;
                                font-weight: bold;
                        }
                        .style_date
                        {
                                margin: 5px 20px 5px 50px;
                                font-style: italic;
                        }
                </style>
        </head>
        <body>
                <jsp:include page="/views/base/system_base/header.jsp"/>
                <c:if test="${tradingDetails == null}">
                        <c:if test="${productDetails != null}">
                                <div class="container">
                                        <div class="row">
                                                <div class="col-md-10 col-md-offset-1">
                                                        <div class="alert alert-info">
                                                                <div class="info">
                                                                        <c:if test="${productDetails.isDeleted == true}">
                                                                                Đơn hàng của bạn đã bị xóa.
                                                                        </c:if>
                                                                </div>
                                                                <c:if test="${productDetails.isDeleted == false}">
                                                                        <div class="info">
                                                                                <c:if test="${productDetails.statusID == 1}">
                                                                                        Đơn hàng của bạn đã được đăng bán.
                                                                                </c:if>
                                                                        </div>
                                                                        <div class="info">
                                                                                <c:if test="${productDetails.statusID == 2}">
                                                                                        Đơn hàng của bạn đã thực hiện giao dịch.
                                                                                </c:if>
                                                                        </div>                                                
                                                                        <div class="info">
                                                                                <c:if test="${productDetails.statusID == 3}">
                                                                                        Đơn hàng của bạn đã bị hủy.
                                                                                </c:if>
                                                                        </div>
                                                                        <div class="info">
                                                                                <c:if test="${productDetails.statusID == 4}">
                                                                                        Đơn hàng của bạn đã được bán.
                                                                                </c:if>
                                                                        </div>
                                                                        <div class="info">
                                                                                <c:if test="${productDetails.statusID == 5}">
                                                                                        Đơn hàng của bạn đã bị khiếu nại.
                                                                                </c:if>
                                                                        </div>
                                                                        <div class="info">
                                                                                <c:if test="${productDetails.statusID == 6}">
                                                                                        Đơn hàng của bạn đã được hệ thống xử lý vì vấn đề khiếu nại.
                                                                                </c:if>
                                                                        </div>
                                                                </c:if>
                                                                <div class="style_date">
                                                                        Ngày tạo đơn hàng: <fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${productDetails.created_at}" />
                                                                </div>
                                                                <div class="style_date">
                                                                        Ngày cập nhật mới nhất: <fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${productDetails.updated_at}" />
                                                                </div> 
                                                        </div>
                                                        <div class="panel panel-default">
                                                                <div class="panel-heading">ĐƠN HÀNG</div>
                                                                <div class="panel-body">
                                                                        <div class="form-group clearfix">
                                                                                <div class="form-group clearfix">
                                                                                        <c:if test="${productDetails.isPrivate == false}">
                                                                                                <label class=" control-label pt-7 text-center" style="font-style: italic; font-weight: bold; color: green">                                              
                                                                                                        Đơn hàng đang ở trạng thái công Khai
                                                                                                </label>
                                                                                        </c:if>
                                                                                        <c:if test="${productDetails.isPrivate == true}">
                                                                                                <label class=" control-label pt-7 text-center" style="font-style: italic; color: #28e1e8; font-weight: bold; color: red">
                                                                                                        Đơn hàng đang ở trạng thái riêng tư
                                                                                                </label>
                                                                                        </c:if>
                                                                                </div>
                                                                        </div>
                                                                        <c:if test="${productDetails.statusID == 4}">
                                                                                <div class="form-group clearfix">
                                                                                        <label class="col-md-2 control-label pt-7 text-right" >Người mua</label>
                                                                                        <div class="col-md-8">
                                                                                                <p class="form-control"  name="buyer">${productDetails.user.username}</p>
                                                                                        </div>
                                                                                </div>
                                                                        </c:if>
                                                                        <div class="form-group clearfix">
                                                                                <label class="col-md-2 control-label pt-7 text-right" >Tên sản phẩm</label>
                                                                                <div class="col-md-8">
                                                                                        <p class="form-control"  name="pTitle">${productDetails.productName}</p>
                                                                                </div>
                                                                        </div>
                                                                        <div class="form-group clearfix">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Phân loại</label>
                                                                                <div class="col-md-8">
                                                                                        <p class="form-control" name="pCategory">${productDetails.category.categoryName}</p>
                                                                                </div>
                                                                        </div>
                                                                        <div class="form-group clearfix">
                                                                                <div class="form-group clearfix">
                                                                                        <label class="col-md-2 control-label pt-7 text-right">Giá sản phẩm</label>
                                                                                        <div class="col-md-8">
                                                                                                <p class="form-control" name="price">${productDetails.price}</p>
                                                                                        </div>
                                                                                </div>
                                                                        </div>
                                                                        <div class="form-group clearfix">
                                                                                <div class="form-group clearfix">
                                                                                        <label class="col-md-2 control-label pt-7 text-right">Liên hệ</label>
                                                                                        <div class="col-md-8">
                                                                                                <p class="form-control" name="contact">
                                                                                                        <a href="https://www.google.com/" target="_blank">${productDetails.contact}</a>
                                                                                                </p>
                                                                                        </div>
                                                                                </div>
                                                                        </div>
                                                                        <div class="form-group clearfix">
                                                                                <div class="form-group clearfix">
                                                                                        <label class="col-md-2 control-label pt-7 text-right">Mô tả</label>
                                                                                        <div class="col-md-8">
                                                                                                <textarea id="LN_Series_Summary" name="pDescribe">${productDetails.describe}</textarea>
                                                                                        </div>
                                                                                </div>
                                                                        </div>
                                                                        <div class="form-group clearfix">
                                                                                <div class="form-group clearfix">
                                                                                        <label class="col-md-2 control-label pt-7 text-right">Thông tin ẩn</label>
                                                                                        <div class="col-md-8">
                                                                                                <textarea id="LN_Series_Summary" name="data">${productDetails.dataproduct}</textarea>
                                                                                        </div>
                                                                                </div>
                                                                        </div>
                                                                        <div class="form-group clearfix">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Link đơn hàng</label>
                                                                                <div class="col-md-8">
                                                                                        <p class="form-control" name="url">
                                                                                                <a href="https://www.google.com/" target="_blank">${productDetails.link}</a>
                                                                                        </p>
                                                                                </div>
                                                                        </div>
                                                                        <a href="/ElaTrading/action/products" type="button" class="mb-0 text-center btn btn-primary" >Đóng</a>             
                                                                </div>
                                                        </div>
                                                </c:if>
                                        </c:if>
                                        <c:if test="${productDetails == null}">
                                                <c:if test="${tradingDetails != null}">
                                                        <div class="container">
                                                                <div class="row">
                                                                        <div class="col-md-10 col-md-offset-1">
                                                                                <div class="alert alert-info">
                                                                                        <div class="info">
                                                                                                <c:if test="${tradingDetails.statusID == 2}">
                                                                                                        Đơn hàng của bạn đang xử lí.
                                                                                                </c:if>
                                                                                        </div>
                                                                                        <div class="info">
                                                                                                <c:if test="${tradingDetails.statusID == 4}">
                                                                                                        Đơn hàng của bạn đã mua thành công.
                                                                                                </c:if>
                                                                                        </div>                                                
                                                                                        <div class="info">
                                                                                                <c:if test="${tradingDetails.statusID == 3}">
                                                                                                        Đơn hàng của bạn đã bị hủy.
                                                                                                </c:if>
                                                                                        </div>
                                                                                        <div class="info">
                                                                                                <c:if test="${tradingDetails.statusID == 5}">
                                                                                                        Đơn hàng của bạn đang khiếu nại.
                                                                                                </c:if>
                                                                                        </div>

                                                                                        <div class="style_date">
                                                                                                Ngày tạo đơn hàng: <fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${tradingDetails.created_at}" />
                                                                                        </div>
                                                                                        <div class="style_date">
                                                                                                Ngày cập nhật mới nhất: <fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${tradingDetails.updated_at}" />
                                                                                        </div> 
                                                                                </div>
                                                                                <div class="panel panel-default">
                                                                                        <div class="panel-heading">ĐƠN HÀNG</div>
                                                                                        <div class="panel-body">
                                                                                                <div class="form-group clearfix">
                                                                                                        <div class="form-group clearfix">
                                                                                                                <c:if test="${tradingDetails.product.isPrivate == false}">
                                                                                                                        <label class=" control-label pt-7 text-center" style="font-style: italic; font-weight: bold; color: red">                                              
                                                                                                                                Đơn hàng đang ở trạng thái riêng tư
                                                                                                                        </label>
                                                                                                                </c:if>
                                                                                                                <c:if test="${tradingDetails.product.isPrivate == true}">
                                                                                                                        <label class=" control-label pt-7 text-center" style="font-style: italic; color: #28e1e8; font-weight: bold; color: green">
                                                                                                                                Đơn hàng đang ở trạng thái công Khai
                                                                                                                        </label>
                                                                                                                </c:if>
                                                                                                        </div>
                                                                                                </div>
                                                                                                <c:if test="${tradingDetails.statusID == 4}">
                                                                                                        <div class="form-group clearfix">
                                                                                                                <label class="col-md-2 control-label pt-7 text-right" >Người bán</label>
                                                                                                                <div class="col-md-8">
                                                                                                                        <p class="form-control"  name="buyer">${tradingDetails.user.username}</p>
                                                                                                                </div>       
                                                                                                        </div>
                                                                                                </c:if>
                                                                                                <div class="form-group clearfix">
                                                                                                        <label class="col-md-2 control-label pt-7 text-right" >Tên sản phẩm</label>
                                                                                                        <div class="col-md-8">
                                                                                                                <p class="form-control"  name="pTitle">${tradingDetails.product.productName}</p>
                                                                                                        </div>
                                                                                                </div>
                                                                                                <div class="form-group clearfix">
                                                                                                        <div class="form-group clearfix">
                                                                                                                <label class="col-md-2 control-label pt-7 text-right">Giá sản phẩm</label>
                                                                                                                <div class="col-md-8">
                                                                                                                        <p class="form-control" name="price">${tradingDetails.product.price}</p>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                </div>
                                                                                                <div class="form-group clearfix">
                                                                                                        <div class="form-group clearfix">
                                                                                                                <label class="col-md-2 control-label pt-7 text-right">Liên hệ</label>
                                                                                                                <div class="col-md-8">
                                                                                                                        <p class="form-control" name="contact">
                                                                                                                                <a href="${tradingDetails.product.contact}" target="_blank">${tradingDetails.product.contact}</a>
                                                                                                                        </p>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                </div>
                                                                                                <div class="form-group clearfix">
                                                                                                        <div class="form-group clearfix">
                                                                                                                <label class="col-md-2 control-label pt-7 text-right">Mô tả</label>
                                                                                                                <div class="col-md-8">
                                                                                                                        <textarea id="LN_Series_Summary" name="pDescribe">${tradingDetails.product.describe}</textarea>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                </div>
                                                                                                <div class="form-group clearfix">
                                                                                                        <div class="form-group clearfix">
                                                                                                                <label class="col-md-2 control-label pt-7 text-right">Thông tin ẩn</label>
                                                                                                                <div class="col-md-8">
                                                                                                                        <textarea id="LN_Series_Summary" name="data">${tradingDetails.product.dataproduct}</textarea>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                </div>
                                                                                                <div class="form-group clearfix">
                                                                                                        <label class="col-md-2 control-label pt-7 text-right">Link đơn hàng</label>
                                                                                                        <div class="col-md-8">
                                                                                                                <p class="form-control" name="url">
                                                                                                                        <a href="https://www.google.com/" target="_blank">${tradingDetails.product.link}</a>
                                                                                                                </p>
                                                                                                        </div>
                                                                                                </div>   
                                                                                        </div>
                                                                                </div>

                                                                        </c:if>
                                                                </c:if>
                                                                <script src="https://cdn.tiny.cloud/1/5c19zuq7jx1haedv7bv6hju4wfvxld356joddpdqnn0hnk5i/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
                                                                <script type="227dddff75eef862ba087f1e-text/javascript">
                                                                        tinymce.init({
                                                                        selector: 'textarea',
                                                                        readonly : 1,
                                                                        inline: false,
                                                                        height: 300,
                                                                        skin: 'oxide-dark',
                                                                        content_css: 'dark',
                                                                        branding: false,
                                                                        menubar: false,
                                                                        contextmenu: false,
                                                                        entities: '160,nbsp,38,amp,60,lt,62,gt',
                                                                        paste_word_valid_elements: 'b,strong,i,em,u,s,a,p,br,img',
                                                                        element_format: 'html',
                                                                        formats: {
                                                                        strikethrough: { inline: 's', remove: 'all' },
                                                                        underline: { inline: 'u', remove: 'all' },
                                                                        },
                                                                        plugins: 'wordcount link image code fullscreen paste emoticons',
                                                                        toolbar: 'undo redo | bold italic underline strikethrough forecolor | link image | removeformat | fullscreen'
                                                                        });
                                                                </script>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <jsp:include page="/views/base/system_base/footer.jsp"/>
        </body>
</html>




