<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="vi" class="light">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>Feedback</title>
                <link rel="stylesheet" href="/ElaTrading/assets/css/interface.css">
                <link rel="stylesheet" href="/ElaTrading/assets/css/tailwind.css">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css" integrity="sha256-BtbhCIbtfeVWGsqxk1vOHEYXS6qcvQvLMZqjtpWUEx8=" crossorigin="anonymous" />
                <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
                <style>
                        .screen-reader{
                                width: var(--screenReaderWidth, 1px) !important;
                                height: var(--screenReaderHeight, 1px) !important;
                                padding: var(--screenReaderPadding, 0) !important;
                                border: var(--screenReaderBorder, none) !important;

                                position: var(--screenReaderPosition, absolute) !important;
                                clip: var(--screenReaderClip, rect(1px, 1px, 1px, 1px)) !important;
                                overflow: var(--screenReaderOverflow, hidden) !important;
                        }
                        .rating{
                                --uiRatingColor: var(--ratingColor, #eee);
                                --uiRatingColorActive: var(--ratingColorActive, #ffcc00);

                                display: var(--ratingDisplay, flex);
                                font-size: var(--ratingSize, 1rem);
                                color: var(--uiRatingColor);
                        }

                        .rating__item{
                                cursor: pointer;
                                position: relative;
                        }

                        .rating__item{
                                padding-left: .25em;
                                padding-right: .25em;
                        }

                        .rating__star{
                                display: block;
                                width: 1em;
                                height: 1em;

                                fill: currentColor;
                                stroke: var(--ratingStroke, #222);
                                stroke-width: var(--ratingStrokeWidth, 1px);
                        }

                        .rating:hover,
                        .rating__control:nth-of-type(1):checked ~ .rating__item:nth-of-type(1),
                        .rating__control:nth-of-type(2):checked ~ .rating__item:nth-of-type(-n+2),
                        .rating__control:nth-of-type(3):checked ~ .rating__item:nth-of-type(-n+3),
                        .rating__control:nth-of-type(4):checked ~ .rating__item:nth-of-type(-n+4),
                        .rating__control:nth-of-type(5):checked ~ .rating__item:nth-of-type(-n+5){
                                color: var(--uiRatingColorActive);
                        }

                        .rating__item:hover ~ .rating__item{
                                color: var(--uiRatingColor);
                        }
                        .rating{
                                --ratingSize: 2rem;
                                --ratingColor: #eee;
                                --ratingColorActive: #ffcc00;
                        }
                        .select-wrapper.selected {
                                background-color: #ffcc00;
                        }
                </style>
        </head>
        <body>
                <jsp:include page="/views/base/home_base/header.jsp"/>
                <c:set var="u" value="${user}"/>
                <main wire:snapshot="{&quot;data&quot;:{&quot;series&quot;:[null,{&quot;class&quot;:&quot;App\\Models\\Series&quot;,&quot;key&quot;:16934,&quot;s&quot;:&quot;mdl&quot;}],&quot;content&quot;:&quot;&quot;,&quot;rating&quot;:0,&quot;isSpoiler&quot;:false,&quot;averageRating&quot;:5,&quot;sort&quot;:&quot;id&quot;,&quot;paginators&quot;:[{&quot;page&quot;:1},{&quot;s&quot;:&quot;arr&quot;}]},&quot;memo&quot;:{&quot;id&quot;:&quot;wlbTuHgkseEE9MDpRpwU&quot;,&quot;name&quot;:&quot;pub.series.member.review&quot;,&quot;path&quot;:&quot;truyen\/16934\/danh-gia&quot;,&quot;method&quot;:&quot;GET&quot;,&quot;children&quot;:{&quot;3411&quot;:[&quot;div&quot;,&quot;KDTsLHHJ87WbxYjXx1lw&quot;],&quot;3370&quot;:[&quot;div&quot;,&quot;lAzYS7IFk0lbXi7xszF3&quot;],&quot;3342&quot;:[&quot;div&quot;,&quot;pj4zzCSt1skw2YcFdEGi&quot;],&quot;3291&quot;:[&quot;div&quot;,&quot;bN91BcmuLbwX6CgBrDPj&quot;],&quot;3263&quot;:[&quot;div&quot;,&quot;gDnCSU6mHGHULjhxL9Jk&quot;],&quot;3254&quot;:[&quot;div&quot;,&quot;OuEALCScTpUKQWMQ4H23&quot;],&quot;3249&quot;:[&quot;div&quot;,&quot;m39nRjWSNnB3b1Reh1JY&quot;],&quot;3231&quot;:[&quot;div&quot;,&quot;H7szTc88swh8iN64XhVA&quot;],&quot;3197&quot;:[&quot;div&quot;,&quot;MEuw5AeTq8m8WvTET70T&quot;],&quot;3194&quot;:[&quot;div&quot;,&quot;sDxlaIUOlb3mJhWQftht&quot;]},&quot;scripts&quot;:[],&quot;assets&quot;:[],&quot;errors&quot;:[],&quot;locale&quot;:&quot;vi&quot;},&quot;checksum&quot;:&quot;008d60ce200a1541ce40be7217a6af027d96ee0ea506c14f43cb815fe523f968&quot;}" wire:effects="{&quot;url&quot;:{&quot;paginators.page&quot;:{&quot;as&quot;:&quot;page&quot;,&quot;use&quot;:&quot;push&quot;,&quot;alwaysShow&quot;:false,&quot;except&quot;:null}},&quot;listeners&quot;:[&quot;reviewDeleted&quot;]}" wire:id="wlbTuHgkseEE9MDpRpwU" id="mainpart" class="browserpage">
                        <div class="container">
                                <div class="grid grid-cols-4">
                                        <div class="col-span-4-X md:col-span-3-X">
                                                <header class="page-title">
                                                        <div class="page-name_wrapper">
                                                                <div class="container">
                                                                        <span class="page-name">
                                                                                <i class="fas fa-circle"></i> Phản hồi từ người dùng
                                                                                <c:if test="${u == null}">
                                                                                        <div style="color: red; margin-top: 10px; margin-left: 30px;">
                                                                                                Bạn cần đăng nhập để có thể phản hồi
                                                                                        </div>
                                                                                </c:if>
                                                                        </span>
                                                                </div>
                                                        </div>
                                                </header>
                                                <section class="browse-section">
                                                        <main class="sect-body col">
                                                                <c:if test="${u != null}">
                                                                        <div class="mb-2" wire:submit="add" id="ratingForm">
                                                                                <div class="d-flex align-items-center mb-1">
                                                                                        <div class="mr-2">Số điểm của bạn:
                                                                                        </div>
                                                                                        <div class="rating">
                                                                                                <input type="radio" name="rating-star2" class="rating__control screen-reader" value="1" id="rc6" onclick="handleStarClick(1)">
                                                                                                <input type="radio" name="rating-star2" class="rating__control screen-reader" value="2" id="rc7" onclick="handleStarClick(2)">
                                                                                                <input type="radio" name="rating-star2" class="rating__control screen-reader" value="3" id="rc8" onclick="handleStarClick(3)">
                                                                                                <input type="radio" name="rating-star2" class="rating__control screen-reader" value="4" id="rc9" onclick="handleStarClick(4)">
                                                                                                <input type="radio" name="rating-star2" class="rating__control screen-reader" value="5" id="rc10" onclick="handleStarClick(5)">
                                                                                                <label for="rc6" class="rating__item">
                                                                                                        <svg class="rating__star">
                                                                                                        <use xlink:href="#star"></use>
                                                                                                        </svg>
                                                                                                        <span class="screen-reader">1</span>
                                                                                                </label>
                                                                                                <label for="rc7" class="rating__item">
                                                                                                        <svg class="rating__star">
                                                                                                        <use xlink:href="#star"></use>
                                                                                                        </svg>
                                                                                                        <span class="screen-reader">2</span>
                                                                                                </label>
                                                                                                <label for="rc8" class="rating__item">
                                                                                                        <svg class="rating__star">
                                                                                                        <use xlink:href="#star"></use>
                                                                                                        </svg>
                                                                                                        <span class="screen-reader">3</span>
                                                                                                </label>
                                                                                                <label for="rc9" class="rating__item">
                                                                                                        <svg class="rating__star">
                                                                                                        <use xlink:href="#star"></use>
                                                                                                        </svg>
                                                                                                        <span class="screen-reader">4</span>
                                                                                                </label>
                                                                                                <label for="rc10" class="rating__item">
                                                                                                        <svg class="rating__star">
                                                                                                        <use xlink:href="#star"></use>
                                                                                                        </svg>
                                                                                                        <span class="screen-reader">5</span>
                                                                                                </label>	
                                                                                        </div> 
                                                                                </div>
                                                                                <svg xmlns="http://www.w3.org/2000/svg" style="display: none">
                                                                                <symbol id="star" viewBox="0 0 26 28">
                                                                                        <path d="M26 10.109c0 .281-.203.547-.406.75l-5.672 5.531 1.344 7.812c.016.109.016.203.016.313 0 .406-.187.781-.641.781a1.27 1.27 0 0 1-.625-.187L13 21.422l-7.016 3.687c-.203.109-.406.187-.625.187-.453 0-.656-.375-.656-.781 0-.109.016-.203.031-.313l1.344-7.812L.39 10.859c-.187-.203-.391-.469-.391-.75 0-.469.484-.656.875-.719l7.844-1.141 3.516-7.109c.141-.297.406-.641.766-.641s.625.344.766.641l3.516 7.109 7.844 1.141c.375.063.875.25.875.719z"/>
                                                                                </symbol>
                                                                                <div wire:ignore>
                                                                                        <textarea id="review_content" style="width: 100%; min-height: 150px"></textarea>
                                                                                </div>
                                                                                <div class="comment_toolkit">
                                                                                        <input class="text-white-X bg-gray-800-X hover:bg-gray-900-X focus:outline-none-X focus:ring-4-X focus:ring-gray-300-X font-medium-X rounded-full-X text-sm-X px-5-X py-2.5-X mr-2-X" type="submit" onclick="AddRating()" value="Đăng đánh giá">
                                                                                </div>
                                                                        </div>
                                                                </c:if>

                                                                <script>
                                                                        let selectedStars = 0;
                                                                        function handleStarClick(value) {
                                                                                selectedStars = value;
                                                                                const stars = document.querySelectorAll('.rating__item');
                                                                                stars.forEach((star, index) => {
                                                                                        if (index <= value - 1) {
                                                                                                star.classList.add('selected');
                                                                                        } else {
                                                                                                star.classList.remove('selected');
                                                                                        }
                                                                                });
                                                                        }
                                                                        function AddRating() {
                                                                                event.preventDefault();
                                                                                let rating = selectedStars;
                                                                                let comment = $("#review_content").val();
                                                                                if (rating === 0) {
                                                                                        alert("Vui lòng chọn số sao trước khi đăng đánh giá.");
                                                                                        return;
                                                                                }
                                                                                if (!comment.trim()) {
                                                                                        alert("Vui lòng nhập nội dung đánh giá trước khi đăng.");
                                                                                        return;
                                                                                }
                                                                                $.ajax({
                                                                                        type: "POST",
                                                                                        url: "feadback/add",
                                                                                        data: {
                                                                                                rating: rating,
                                                                                                comment: comment
                                                                                        },
                                                                                        success: function (response) {
                                                                                                if (response.success) {
                                                                                                        console.log(response.data);
                                                                                                        prependNewRating(response.data);
                                                                                                        $('input[name="rating-star2"]').prop('checked', false);
                                                                                                        $('#review_content').val('');
                                                                                                } else {
                                                                                                        alert("Error: " + response.message);
                                                                                                }
                                                                                        },
                                                                                        error: function () {
                                                                                                alert("Error: Unable to add rating.");
                                                                                        }
                                                                                });
                                                                        }

                                                                        function prependNewRating(newRating) {
                                                                                const container = document.getElementById('newRatingsContainer');
                                                                                const ratingItem = document.createElement('div');
                                                                                ratingItem.className = 'mt-5-X';
                                                                                ratingItem.style.boxShadow = '#60ff00 0px 0px 10px';
                                                                                const flexContainer = document.createElement('div');
                                                                                flexContainer.className = 'flex-X gap-1-X sm:gap-0-X';
                                                                                const avatarDiv = document.createElement('div');
                                                                                avatarDiv.className = 'mt-1-X';
                                                                                const avatarImg = document.createElement('img');
                                                                                avatarImg.style.marginLeft = '5px';
                                                                                avatarImg.style.border = 'solid 1px black';
                                                                                avatarImg.className = 'rounded-full-X me-2-X w-[60px]-X';
                                                                                avatarImg.src = '/ElaTrading/assets/image/avatar.jpg';
                                                                                avatarDiv.appendChild(avatarImg);
                                                                                const userInfoDiv = document.createElement('div');
                                                                                userInfoDiv.className = 'mr-2-X';
                                                                                const flexWrap = document.createElement('div');
                                                                                flexWrap.className = 'flex-X flex-wrap-X gap-x-2-X gap-y-1-X align-middle-X pt-1-X';
                                                                                const usernameDiv = document.createElement('div');
                                                                                usernameDiv.className = 'self-center-X';
                                                                                const usernameLink = document.createElement('div');
                                                                                usernameLink.className = 'font-bold-X leading-6-X md:leading-7-X ln-username';
                                                                                usernameLink.textContent = newRating.user ? newRating.user.username : 'Unknown User';
                                                                                usernameDiv.appendChild(usernameLink);
                                                                                flexWrap.appendChild(usernameDiv);
                                                                                const ratingDiv = document.createElement('div');
                                                                                ratingDiv.style.color = '#d5d512e8';
                                                                                ratingDiv.style.fontSize = '30px';
                                                                                for (let i = 1; i <= 5; i++) {
                                                                                        ratingDiv.textContent += i <= newRating.rate ? '★' : '☆';
                                                                                }
                                                                                const dateDiv = document.createElement('div');
                                                                                dateDiv.className = 'text-gray-400-X';
                                                                                //                                                                        const dateLink = document.createElement('a');
                                                                                //                                                                        dateLink.href = '#';
                                                                                const timeElement = document.createElement('time');
                                                                                timeElement.className = 'timeago';
                                                                                timeElement.setAttribute('datetime', newRating.created_at);
                                                                                timeElement.style.color = "#21bf73";
                                                                                timeElement.textContent = newRating.created_at;
                                                                                //                                                                        dateLink.appendChild(timeElement);
                                                                                dateDiv.appendChild(timeElement);
                                                                                userInfoDiv.appendChild(flexWrap);
                                                                                userInfoDiv.appendChild(ratingDiv);
                                                                                userInfoDiv.appendChild(dateDiv);
                                                                                const commentGroupDiv = document.createElement('div');
                                                                                commentGroupDiv.className = 'ln-comment-group mt-1-X';
                                                                                const commentContainer = document.createElement('div');
                                                                                commentContainer.className = 'rounded-md bg-slate-100 p-2';
                                                                                const commentWrapper = document.createElement('div');
                                                                                commentWrapper.className = 'ln-comment-wrapper';
                                                                                const commentContent = document.createElement('div');
                                                                                commentContent.className = 'ln-comment-content long-text';
                                                                                commentContent.textContent = newRating.comment;
                                                                                commentWrapper.appendChild(commentContent);
                                                                                commentContainer.appendChild(commentWrapper);
                                                                                commentGroupDiv.appendChild(commentContainer);
                                                                                flexContainer.appendChild(avatarDiv);
                                                                                flexContainer.appendChild(userInfoDiv);
                                                                                if (newRating.userID === ${u.id  != null ? u.id : -1}) {
                                                                                        const deleteNewDiv = document.createElement("div");
                                                                                        deleteNewDiv.style.width = "70%";
                                                                                        const deleteNewImg = document.createElement("img");
                                                                                        deleteNewImg.onclick = function () {
                                                                                                confirmDelete(newRating.code);
                                                                                        };
                                                                                        deleteNewImg.style.float = "right";
                                                                                        deleteNewImg.style.width = "30px";
                                                                                        deleteNewImg.style.heigh = "auto";
                                                                                        deleteNewImg.src = '/ElaTrading/assets/image/trashcan.png';
                                                                                        deleteNewDiv.appendChild(deleteNewImg);
                                                                                        flexContainer.appendChild(deleteNewDiv);
                                                                                }
                                                                                ratingItem.appendChild(flexContainer);
                                                                                ratingItem.appendChild(commentGroupDiv);
                                                                                container.insertBefore(ratingItem, container.firstChild);
                                                                                if (container.children.length > visibleRatings) {
                                                                                        container.lastChild.style.display = 'none';
                                                                                }
                                                                        }
                                                                </script>
                                                                <script>
                                                                        function confirmDelete(rateCode) {
                                                                                var result = confirm("Bạn có muốn xóa không?");
                                                                                if (result) {
                                                                                        window.location.href = "feadback/delete?code=" + rateCode;
                                                                                }
                                                                        }
                                                                </script>
                                                                <div id="newRatingsContainer">
                                                                        <!-- NewRatings sẽ được thêm vào đây -->
                                                                </div>
                                                                <div id="ratingsContainer">
                                                                        <!-- Ratings sẽ được thêm vào đây -->
                                                                </div>
                                                                <div style="text-align: center">
                                                                        <button style="padding: 10px" id="loadMore">Hiển thị thêm</button>
                                                                        <button style="padding: 10px" id="hideRatings">Ẩn bớt</button>
                                                                </div>
                                                                <script>
                                                                        const ratings = JSON.parse('${rateJson}');
                                                                        let visibleRatings = 4;
                                                                        function createRatingItem(review) {
                                                                                const container = document.createElement('div');
                                                                                container.className = 'mt-5-X';
                                                                                container.setAttribute('wire:snapshot', '{"data": ... }');
                                                                                const flexContainer = document.createElement('div');
                                                                                flexContainer.className = 'flex-X gap-1-X sm:gap-0-X';
                                                                                const avatarDiv = document.createElement('div');
                                                                                avatarDiv.className = 'mt-1-X';
                                                                                const avatarImg = document.createElement('img');
                                                                                avatarImg.style.border = 'solid 1px black';
                                                                                avatarImg.className = 'rounded-full-X me-2-X w-[60px]-X';
                                                                                avatarImg.src = '/ElaTrading/assets/image/avatar.jpg';
                                                                                avatarDiv.appendChild(avatarImg);
                                                                                const userInfoDiv = document.createElement('div');
                                                                                userInfoDiv.className = 'mr-2-X';
                                                                                const flexWrap = document.createElement('div');
                                                                                flexWrap.className = 'flex-X flex-wrap-X gap-x-2-X gap-y-1-X align-middle-X pt-1-X';
                                                                                const usernameDiv = document.createElement('div');
                                                                                usernameDiv.className = 'self-center-X';
                                                                                const usernameLink = document.createElement('div');
                                                                                usernameLink.className = 'font-bold-X leading-6-X md:leading-7-X ln-username';
                                                                                usernameLink.textContent = review.user.username;
                                                                                usernameDiv.appendChild(usernameLink);
                                                                                flexWrap.appendChild(usernameDiv);
                                                                                const ratingDiv = document.createElement('div');
                                                                                ratingDiv.style.color = '#d5d512e8';
                                                                                ratingDiv.style.fontSize = '30px';
                                                                                for (let i = 1; i <= 5; i++) {
                                                                                        ratingDiv.textContent += i <= review.rate ? '★' : '☆';
                                                                                }
                                                                                const dateDiv = document.createElement('div');
                                                                                dateDiv.className = 'text-gray-400-X';
                                                                                const timeElement = document.createElement('time');
                                                                                timeElement.className = 'timeago';
                                                                                timeElement.setAttribute('datetime', review.created_at);
                                                                                timeElement.style.color = "#21bf73";
                                                                                timeElement.textContent = review.created_at;

                                                                                //dateLink.appendChild(timeElement);
                                                                                dateDiv.appendChild(timeElement);
                                                                                userInfoDiv.appendChild(flexWrap);
                                                                                userInfoDiv.appendChild(ratingDiv);
                                                                                userInfoDiv.appendChild(dateDiv);
                                                                                const commentGroupDiv = document.createElement('div');
                                                                                commentGroupDiv.className = 'ln-comment-group mt-1-X';
                                                                                const commentContainer = document.createElement('div');
                                                                                commentContainer.className = 'rounded-md-X bg-slate-100-X p-2-X';
                                                                                const commentWrapper = document.createElement('div');
                                                                                commentWrapper.className = 'ln-comment-wrapper';
                                                                                const commentContent = document.createElement('div');
                                                                                commentContent.className = 'ln-comment-content long-text';
                                                                                commentContent.textContent = review.comment;
                                                                                commentWrapper.appendChild(commentContent);
                                                                                commentContainer.appendChild(commentWrapper);
                                                                                commentGroupDiv.appendChild(commentContainer);
                                                                                flexContainer.appendChild(avatarDiv);
                                                                                flexContainer.appendChild(userInfoDiv);
                                                                                if (review.userID === ${u.id != null ? u.id : -1}) {
                                                                                        const deleteDiv = document.createElement("div");
                                                                                        deleteDiv.style.width = "70%";
                                                                                        const deleteimg = document.createElement("img");
                                                                                        deleteimg.onclick = function () {
                                                                                                confirmDelete(review.code);
                                                                                        };
                                                                                        deleteimg.style.float = "right";
                                                                                        deleteimg.style.width = "30px";
                                                                                        deleteimg.style.heigh = "auto";
                                                                                        deleteimg.src = '/ElaTrading/assets/image/trashcan.png';
                                                                                        deleteDiv.appendChild(deleteimg);
                                                                                        flexContainer.appendChild(deleteDiv);
                                                                                }
                                                                                container.appendChild(flexContainer);
                                                                                container.appendChild(commentGroupDiv);
                                                                                console.log("created_by/user.id: ", review.created_by);
                                                                                return container;
                                                                        }
                                                                        function updateDisplay() {
                                                                                const container = document.getElementById('ratingsContainer');
                                                                                container.innerHTML = '';
                                                                                for (let i = 0; i < visibleRatings && i < ratings.length; i++) {
                                                                                        container.appendChild(createRatingItem(ratings[i]));
                                                                                }

                                                                                const loadMoreButton = document.getElementById('loadMore');
                                                                                const hideRatingsButton = document.getElementById('hideRatings');

                                                                                if (visibleRatings < ratings.length) {
                                                                                        loadMoreButton.style.display = 'block';
                                                                                        hideRatingsButton.style.display = 'none';
                                                                                } else {
                                                                                        loadMoreButton.style.display = 'none';
                                                                                        hideRatingsButton.style.display = 'block';
                                                                                }
                                                                        }
                                                                        document.getElementById('loadMore').addEventListener('click', function () {
                                                                                visibleRatings += 5;
                                                                                if (visibleRatings > ratings.length) {
                                                                                        visibleRatings = ratings.length;
                                                                                }
                                                                                updateDisplay();
                                                                        });
                                                                        document.getElementById('hideRatings').addEventListener('click', function () {
                                                                                visibleRatings = 5;
                                                                                updateDisplay();
                                                                        }
                                                                        );
                                                                        document.addEventListener('DOMContentLoaded', updateDisplay);
                                                                </script>
                                                                </div>
                                                        </main>
                                                </section>
                                        </div>
                                </div>
                        </div>
                </main>
        </body>
</html>



