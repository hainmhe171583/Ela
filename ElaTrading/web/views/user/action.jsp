<%-- 
    Document   : productList
    Created on : Feb 25, 2024, 9:58:07 PM
    Author     : admin
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="robots" content="NoIndex, NoFollow">
                <title>Lịch sử giao dịch</title>
        </head>
        <body>
                <jsp:include page="/views/base/system_base/header.jsp"/>
                <style>
                        tr:hover {
                                background-color: #f0f0f0;
                        }
                        tr .btn {
                                font-size: 10px;
                        }
                        td {
                                vertical-align: middle !important;
                        }
                        .btn-danger {
                                width: 35.3px;
                        }
                </style>
                <div class="container">
                        <div class="row">             
                                <div class="col-md-12">
                                        <div class="panel panel-default">
                                                <div class="panel-heading">
                                                        Lịch sử giao dịch
                                                </div>
                                                <p class="text-danger" style="font-family: sans-serif; font-size: medium;">${messPay}</p>
                                                <div style="float: right; margin-bottom: 20px">
                                                        <form action="/ElaTrading/action/paymenthistory/excel" method="get" > 
                                                                <select name="fileType" style="width: 100px; height: 30px; border-radius: 5px" >
                                                                        <option value="excel">Excel</option>
                                                                        <option value="txt">Text</option>
                                                                </select>
                                                                <input type="submit" value="Tải về file" class="mb-0 text-center btn btn-primary">
                                                        </form>  
                                                </div>
                                                <div class="panel-body">
                                                        <table class="table table-striped table-hover"  id="productTable">
                                                                <thead>
                                                                        <tr style="font-size: 13px">
                                                                                <th>Tên giao dịch</th>
                                                                                <th>Tiền</th>
                                                                                <th>
                                                                                        Ngày tạo
                                                                                </th>
                                                                                <th>
                                                                                        Trạng thái
                                                                                </th>
                                                                        </tr>
                                                                </thead>
                                                                <tbody>
                                                                        <tr >
                                                                                <c:forEach items="${listPay}" var="o">
                                                                                        <c:if test="${o.transId == 1}">
                                                                                                <td>Nạp tiền</td>
                                                                                        </c:if>
                                                                                        <c:if test="${o.transId == 2}">
                                                                                                <td>Phí tạo đơn hàng</td>
                                                                                        </c:if>
                                                                                        <c:if test="${o.transId == 3}">
                                                                                                <td>Mua hàng</td>
                                                                                        </c:if>
                                                                                        <c:if test="${o.transId == 4}">
                                                                                                <td>Bán Hàng</td>
                                                                                        </c:if>
                                                                                        <c:if test="${o.transId == 5}">
                                                                                                <td>Rút tiền</td>
                                                                                        </c:if>
                                                                                        <c:if test="${o.transId == 6}">
                                                                                                <td>Hoàn trả</td>
                                                                                        </c:if>
                                                                                        <c:if test="${o.transId == 7}">
                                                                                                <td>Phí giao dịch</td>
                                                                                        </c:if>

                                                                                        <td>${o.amount}</td>
                                                                                        <td>${o.created_at}</td>
                                                                                        <c:if test="${o.status == true}">
                                                                                                <td style="color: green"> 
                                                                                                        Thành công
                                                                                                </td>
                                                                                        </c:if>
                                                                                        <c:if test="${o.status == false}">
                                                                                                <td style="color: red">
                                                                                                        Thất bại
                                                                                                </td>
                                                                                        </c:if>
                                                                                        <td >
                                                                                                <a  href="#"><button type="button" class="btn btn-success"><i class="material-icons" data-toggle="tooltip" title="Details">Chi Tiết</i></button></a>
                                                                                        </td>
                                                                                </tr>
                                                                        </c:forEach>
                                                                </tbody>
                                                        </table>
                                                        <form action="/ElaTrading/action" method="get" style="text-align: right; margin-top: 10px">
                                                                <select name="status" id="status">
                                                                        <option value="All">Tất cả</option>
                                                                        <option value="NapTien">Nạp tiền</option>
                                                                        <option value="TaoHang">Phí tạo đơn hàng</option>
                                                                        <option value="MuaHang">Mua hàng</option>
                                                                        <option value="BanHang">Bán Hàng</option>
                                                                        <option value="RutTien">Rút tiền</option>
                                                                        <option value="HoanTien">Hoàn trả</option>
                                                                        <option value="GiaoDich">Phí giao dịch</option>
                                                                </select>
                                                                <input type="submit" value="Lọc">
                                                        </form>
                                                        <c:if test="${countList > 0}">
                                                                <ul class="pagination">
                                                                        <c:set var="currentPage" value="${param.index}" />
                                                                        <c:if test="${empty currentPage}">
                                                                                <c:set var="currentPage" value="1" />
                                                                        </c:if>
                                                                        <c:forEach begin="1" end="${endP}" var="i">     
                                                                                <li class="page-item text-white ${currentPage == i ? 'active' : ''}">
                                                                                        <a href="/ElaTrading/action?status=${status}&index=${i}" class="page-link">${i}</a>
                                                                                </li>
                                                                        </c:forEach>
                                                                </ul>
                                                        </c:if>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </body>
        <jsp:include page="/views/base/system_base/footer.jsp"/>
</html>





