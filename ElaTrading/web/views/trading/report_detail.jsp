<%-- 
    Document   : report_detail
    Created on : Mar 3, 2024, 1:31:53 AM
    Author     : admin
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
        <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Report Form</title>
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
                <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
                <style>
                        body, html {
                                height: 100%;
                                margin: 0;
                        }

                        .container {
                                display: flex;
                                height: 100vh;
                        }

                        .notification, .form-container {
                                height: 900px;
                                flex: 1;
                                padding: 20px;
                                border: 1px solid #000;
                                border-radius: 5px;
                                box-sizing: border-box;
                        }

                        .notification {
                                background-color: #fb7457a3;
                                box-shadow: 0 5px 10px rgb(221 19 19);
                        }

                        .form-button{
                                display: block;
                                width: 100%;
                                padding: 10px;
                                margin-top: 10px;
                                background-color: #3bdb72;
                                color: #fff;
                                border: none;
                                border-radius: 5px;
                        }

                        .form-button:hover {
                                background-color: #fb7457a3;
                        }

                        .back-button:hover {
                                background-color: #fb7457a3;
                        }

                </style>
        </head>
        <body>
                <c:set var="user" value="${user}"/>
                <c:set var="trading" value="${trading}"/>
                <c:set var="report" value="${report}"/>
                <div style="padding-right: 5%">
                        <button class="back-button" style="background-color: #3bdb72; margin-left: 20px; margin-top: 20px; border-radius: 5px"><a style="color: white; text-decoration: none" href="tradings">Back</a></button>
                </div>
                <div class="container">
                        <div class="notification" style="height: 50%">
                                <p style="color: #3b392e">Sau khi gửi đơn khiếu nại, thời gian bên bán xử lí sẽ từ 1 - 2h. Bên bán sẽ chủ động liên lạc để đối chiếu sản phẩm với bạn thông qua thông tin liên hệ đã có sẵn.
                                        Nếu sản phẩm không đúng với mô tả và cam kết thì bên bán sẽ hủy đơn hàng và chịu trách nhiệm với bên mua.
                                </p>
                                <div class="form-group">
                                        <h3>Tạo cuộc họp</h3>
                                        <form action="createmeet" method="post">
                                                <input type="hidden" name="email1" value="${trading.user.email}"/>
                                                <input type="hidden" name="email2" value="${seller.email}"/>
                                                <table>
                                                        <tr>
                                                                <td><label>Link meet</label></td>
                                                                <td><input type="text" name="linkmeet" required placeholder="Nhập link meet..."/><br/></td>
                                                        </tr>
                                                        <tr>
                                                                <td><label for="date">Chọn ngày</label></td>
                                                                <td><input type="date" id="date" name="date" required><br></td>
                                                        </tr>
                                                        <tr>
                                                                <td><label for="time">Chọn giờ</label></td>
                                                                <td><input type="time" id="time" name="time" required><br></td>
                                                        </tr>
                                                        <tr>
                                                        <button class="form-button">Khởi tạo</button>
                                                        </tr>
                                                </table>
                                        </form>
                                </div>
                                <c:if test="${user.id != 0}">
                                        <button style="border-radius: 5px; background-color: #3bdb72" id="callAdmin">Call Admin</button>
                                        <div id="confirmationModal" class="modal" style="display:none; width: 80%; height: auto ;margin-left: 100px; box-shadow: 0 5px 10px rgb(221 19 19);  border: solid 1px black; border-radius: 5px">
                                                <div class="modal-content" style="padding-bottom: 10px;">
                                                        <span class="close" style="text-align: right;">&times;</span>
                                                        <p style="padding-left: 30px">Khi Call Admin , hệ thống sẽ nhận được báo cáo, sau 30p hệ thống sẽ gửi đường dẫn đến phòng họp mặt để bên bán, bên mua và người đại diện của hệ thống có thể xử lí vấn đề trực tiếp.</p>
                                                        <button id="callAdminBtn" data-report-code="${report.code}" style="background-color:#55cb57; color: white; border: solid 1px black; display: block; margin: auto; padding: 3px; border-radius: 5px;" type="button" >Call Admin</button>
                                                </div>
                                        </div>
                                </c:if>
                                <script>
                                        var modal = document.getElementById('confirmationModal');
                                        var btn = document.getElementById("callAdmin");
                                        var callAdminBtn = document.getElementById("callAdminBtn");
                                        var span = document.getElementsByClassName("close")[0];
                                        btn.onclick = function () {
                                                modal.style.display = "block";
                                        };
                                        span.onclick = function () {
                                                modal.style.display = "none";
                                        };
                                        window.onclick = function (event) {
                                                if (event.target === modal) {
                                                        modal.style.display = "none";
                                                }
                                        };
                                        callAdminBtn.onclick = function () {
                                                var reportCode = this.getAttribute("data-report-code");
                                                var formData = new FormData();
                                                formData.append("reportcode", reportCode);

                                                $.ajax({
                                                        type: "POST",
                                                        url: "reporttoadmin",
                                                        data: formData,
                                                        processData: false,
                                                        contentType: false,
                                                        success: function (response) {
                                                                alert("Gửi thành công");
                                                        },
                                                        error: function (xhr, status, error) {
                                                        }
                                                });
                                        };
                                </script>
                        </div>
                        <div class="form-container" >
                                <c:if test="${user.id != 0}">
                                        <div style="height: 90%; margin-bottom: 30px">
                                                <input type="hidden" name="reportcode" value="${report.code}"/>
                                                <div class="form-group">
                                                        <label for="titleReport">Tiêu đề:</label>
                                                        <input type="text" class="form-control" name="titlereport" id="titleReport" placeholder="Nhập tiêu đề ..." value="${report.title}" required>
                                                </div>
                                                <div class="form-group">
                                                        <label for="customerName">Tên khách hàng:</label>
                                                        <input type="text" class="form-control"  name="customername" id="customerName" readonly value="${trading.user.username}" placeholder="Customer name">
                                                </div>
                                                <c:if test="${user.id == trading.product.userID}">
                                                        <div class="form-group">
                                                                <label for="sellercontact">Phương thức liên lạc với người mua:</label>
                                                                <input type="text" class="form-control"  name="sellercontact" id="sellercontact" readonly value="${trading.user.phone}">
                                                        </div>
                                                </c:if>
                                                <div class="form-group">
                                                        <label for="sellerName">Tên người bán:</label>
                                                        <input type="text" class="form-control" name="sellername"  id="sellerName" readonly value="${seller.username}" placeholder="Seller name">
                                                </div>
                                                <c:if test="${user.id == report.userID}">
                                                        <div class="form-group">
                                                                <label for="sellercontact">Phương thức liên lạc với người bán:</label>
                                                                <input type="text" class="form-control"  name="customername" id="customerName" readonly value="${seller.phone}" placeholder="Customer name">
                                                        </div>
                                                </c:if>
                                                <div class="form-group">
                                                        <label for="codeProduct">Tên sản phẩm:</label>
                                                        <input type="text" class="form-control" name="codeproduct"  readonly value="${trading.product.productName}" id="codeProduct" placeholder="Product code">
                                                </div>
                                                <div class="form-group">
                                                        <label for="codeTrading">Mã giao dịch:</label>
                                                        <input type="text" class="form-control" name="codetrading"  readonly value="${trading.code}" id="codeTrading" placeholder="Trading code">
                                                </div>
                                                <div class="form-group">
                                                        <label for="contentReport">Nội dung:</label>
                                                        <textarea class="form-control" name="contentreport" style="min-height: 190px"  id="contentReport" placeholder="Nhập nội dung ..." rows="3"  required>${report.describe}</textarea>
                                                </div>
                                                <c:if test="${report.statusID == 6}">
                                                        <p style="color: red">Ðơn khiếu nại đã được gửi đến admin và đang trong quá trình xử lí, kết quả sẽ được phản hồi qua email.</p>
                                                </c:if>
                                                <c:if test="${report.statusID == 2}">
                                                        <c:if test="${user.id == trading.product.userID}">
                                                                <script>
                                                                        function confirmCancellation() {
                                                                                var result = window.confirm("Bạn có chắc chắn hủy đơn khiếu nại không ?");
                                                                                return result;
                                                                        }
                                                                </script>
                                                                <button class="form-button"><a style="text-decoration: none; color: white" href="report/accept?code=${report.code}" onclick="return confirmCancellation()">Chấp nhận đơn khiếu nại</a></button>
                                                        </c:if>
                                                        <c:if test="${user.id == report.userID}">
                                                                <script>
                                                                        function confirmCancellation() {
                                                                                var result = window.confirm("Bạn có chắc chắn hủy đơn khiếu nại không ?");
                                                                                return result;
                                                                        }
                                                                </script>
                                                                <button class="form-button"><a style="text-decoration: none; color: white" href="report/withdraw?code=${report.code}" onclick="return confirmCancellation()">Rút lại khiếu nại</a></button>
                                                        </c:if>
                                                </c:if>
                                                <c:if test="${report.statusID == 3}">
                                                        <p style="color: red">Sản phẩm đã đảm bảo tính minh bạch và đúng với mô tả ban đầu</p>
                                                        <button class="form-button">Đơn khiếu nại đã bị từ chối</button>
                                                </c:if>
                                                <c:if test="${report.statusID == 4}">
                                                        <p style="color: red">Sản phẩm không đúng với mô tả, đơn hàng sẽ bị hủy và bạn sẽ được hoàn lại tiền !</p>
                                                        <button class="form-button">Đơn khiếu nại đã được chấp nhận</button>
                                                </c:if>
                                        </div>
                                </div>
                        </c:if>

                        <c:if test="${user.id == 0}">
                                <div style="height: 90%; margin-bottom: 30px">
                                        <input type="hidden" name="reportcode" value="${report.code}"/>
                                        <div class="form-group">
                                                <label for="titleReport">Tiêu đề:</label>
                                                <input type="text" class="form-control" name="titlereport" id="titleReport" placeholder="Nhập tiêu đề ..." value="${report.title}" required>
                                        </div>
                                        <div class="form-group">
                                                <label for="customerName">Tên khách hàng:</label>
                                                <input type="text" class="form-control"  name="customername" id="customerName" readonly value="${trading.user.username}" placeholder="Customer name">
                                        </div>
                                        <div class="form-group">
                                                <label for="sellercontact">Phương thức liên lạc với người mua:</label>
                                                <input type="text" class="form-control"  name="sellercontact" id="sellercontact" readonly value="${trading.user.phone}">
                                        </div>
                                        <div class="form-group">
                                                <label for="sellerName">Tên người bán:</label>
                                                <input type="text" class="form-control" name="sellername"  id="sellerName" readonly value="${seller.username}" placeholder="Seller name">
                                        </div>
                                        <div class="form-group">
                                                <label for="sellercontact">Phương thức liên lạc với người bán:</label>
                                                <input type="text" class="form-control"  name="customername" id="customerName" readonly value="${seller.phone}" placeholder="Customer name">
                                        </div>
                                        <div class="form-group">
                                                <label for="codeProduct">Tên sản phẩm:</label>
                                                <input type="text" class="form-control" name="codeproduct"  readonly value="${trading.product.productName}" id="codeProduct" placeholder="Product code">
                                        </div>
                                        <div class="form-group">
                                                <label for="codeTrading">Mã giao dịch:</label>
                                                <input type="text" class="form-control" name="codetrading"  readonly value="${trading.code}" id="codeTrading" placeholder="Trading code">
                                        </div>
                                        <div class="form-group">
                                                <label for="contentReport">Nội dung:</label>
                                                <textarea class="form-control" name="contentreport" style="min-height: 190px"  id="contentReport" placeholder="Nhập nội dung ..." rows="3"  required>${report.describe}</textarea>
                                        </div>
                                </div>
                        </div>
                </c:if>
        </div>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>

</body>
</html>
