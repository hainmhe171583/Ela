<%-- 
    Document   : productList
    Created on : Feb 25, 2024, 9:58:07 PM
    Author     : admin
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="robots" content="NoIndex, NoFollow">
                <title>Đơn mua của tôi</title>
        </head>
        <body>
                <jsp:include page="/views/base/system_base/header.jsp"/>
                <style>
                        tr:hover {
                                background-color: #f0f0f0;
                        }
                        tr .btn {
                                font-size: 10px;
                        }
                        td {
                                vertical-align: middle !important;
                        }
                        .btn-danger {
                                width: 35.3px;
                        }
                </style>
                <div class="container">
                        <div class="row">
                                <div class="col-md-12">
                                        <div class="panel panel-default">
                                                <div class="panel-heading">
                                                        Đơn mua của tôi
                                                </div>
                                                <p class="text-success" style="font-family: sans-serif; font-size: medium;">${messBuy}</p>
                                                <div style="float: right; margin-bottom: 20px">
                                                        <form action="/ElaTrading/action/tradings/excel" method="get"> 
                                                                <select name="fileType" >
                                                                        <option value="excel">Excel</option>
                                                                        <option value="txt">Text</option>
                                                                </select>
                                                                <input type="submit" value="Tải về file">
                                                        </form>  
                                                </div>
                                                <div class="panel-body">
                                                        <table class="table table-striped table-hover"  id="productTable">
                                                                <thead>
                                                                        <tr style="font-size: 13px">

                                                                                <th>Tên sản phẩm</th>
                                                                                <th>Giá</th>
                                                                                <th>
                                                                                        Người bán
                                                                                </th>
                                                                                <th>
                                                                                        Trạng thái
                                                                                </th>
                                                                        </tr>
                                                                </thead>
                                                                <tbody>
                                                                        <tr >
                                                                                <c:forEach items="${listP}" var="o">
                                                                                        <td>${o.product.productName}</td>
                                                                                        <td>${o.product.price}</td>
                                                                                        <td>${o.user.username}</td>
                                                                                        <td>
                                                                                                <c:if test="${o.getStatusID() == 2}">
                                                                                                        <div class="text-success" style="color: green">Đang xử lí </div>
                                                                                                </c:if>
                                                                                                <c:if test="${o.getStatusID() == 4}">
                                                                                                        <div class="text-success" style="color: red">Đã mua</div>
                                                                                                </c:if>
                                                                                                <c:if test="${o.getStatusID() == 3}">
                                                                                                        <div class="text-success" style="color: orange">Bị huỷ</div>
                                                                                                </c:if>
                                                                                                <c:if test="${o.getStatusID() == 5}">
                                                                                                        <div class="text-success" style="color: blue">Đang khiếu nại</div>
                                                                                                </c:if>
                                                                                        </td>
                                                                                        <td >
                                                                                                <a  href="/ElaTrading/action/trading?code=${o.getCode()}"><button type="button" class="btn btn-success"><i class="material-icons" data-toggle="tooltip" title="Details">Chi Tiết</i></button></a>
                                                                                                <c:if test="${ o.getStatusID() ==  3 || o.getStatusID() == 5}">
                                                                                                        <a href="/ElaTrading/action/report?code=${o.code}"><button type="button" class="btn btn-success" style="background-color: red"><i style="font-size: 10px">Chi tiết khiếu nại</i></button></a>          
                                                                                                </c:if>
                                                                                        </td>
                                                                                </tr>
                                                                        </c:forEach>
                                                                </tbody>
                                                        </table>
                                                        <form action="/ElaTrading/action/tradings" method="get" style="text-align: right; margin-top: 10px">
                                                                <label for="status">Lọc theo trạng thái:</label>
                                                                <select name="status" id="status">
                                                                        <option value="All">Tất cả</option>
                                                                        <option value="DangMua">Đang xử lí</option>
                                                                        <option value="DaMua">Đã mua</option>
                                                                        <option value="BiHuy">Bị huỷ</option>
                                                                        <option value="DangKhieuNai">Đang khiếu nại</option>
                                                                        <option selected=""> </option>
                                                                </select>
                                                                <input type="submit" value="Lọc">
                                                        </form>
                                                        <c:if test="${countList > 0}">
                                                                <ul class="pagination">
                                                                        <c:forEach begin="1" end="${endP}" var="i">     
                                                                                <li class="page-item text-white ${tag == i ? 'active': ''}"> <a href="/ElaTrading/action/tradings?status=${status}&index=${i}" class="page-link">${i}</a></li>
                                                                                </c:forEach>
                                                                </ul>
                                                        </c:if>
                                                        <div class="text-right">
                                                                <a class="page-item" href="#">
                                                                        <button type="button" class="mb-0 text-center btn btn-primary">Back</button>
                                                                </a>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </body>
        <jsp:include page="/views/base/system_base/footer.jsp"/>
</html>



