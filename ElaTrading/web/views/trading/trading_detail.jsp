<%-- 
    Document   : addproduct
    Created on : Jan 17, 2024, 10:19:31 PM
    Author     : hai20
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>Đơn mua</title>
                <script>
                        window.onload = function () {
                                var mssValue = '<%= request.getAttribute("mss") %>';
                                if (mssValue != "null")
                                        alert(mssValue);
                                //document.getElementById('input-file').addEventListener('change', getFile);
                        }
                </script>
        </head>
        <body>
                <jsp:include page="../base/system_base/header.jsp"/>
                <div class="container">
                        <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                        <div class="panel panel-default">
                                                <div class="panel-heading">Chi tiết hóa đơn</div>
                                                <div class="panel-body">
                                                        <c:if test="${trade.getStatusID()==2}">
                                                                <div class="alert alert-info">
                                                                        Xin hãy kiểm tra giao dịch!
                                                                </div>
                                                        </c:if>
                                                        <c:if test="${trade.getStatusID()==3}">
                                                                <div class="alert alert-info">
                                                                        Giao dịch thất bại.
                                                                </div>
                                                        </c:if>
                                                        <c:if test="${trade.getStatusID()==4}">
                                                                <div class="alert alert-info">
                                                                        Giao dịch thành công.
                                                                </div>
                                                        </c:if>
                                                        <c:if test="${trade.getStatusID()==5}">
                                                                <div  id="show-error" class="alert alert-danger">
                                                                        <strong>Đơn khiếu nại của bạn đang được xử lý!</strong>
                                                                </div>
                                                        </c:if>
                                                        <form action="trading?code=${code}" role="form" method="post" onsubmit="return checkEmpty()">
                                                                <div class="form-group clearfix required">
                                                                        <label class="col-md-2 control-label pt-7 text-right" >ID đơn hàng:</label>
                                                                        <div class="col-md-8">
                                                                                <input type="text" class="form-control" readonly="true" name="pTitle" value="${code}">
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Tên sản phẩm</label>
                                                                                <div class="col-md-8">
                                                                                        <input type="text" class="form-control" readonly="true" name="price" value="${proName}">
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Giá tiền</label>
                                                                                <div class="col-md-8">
                                                                                        <input type="text" class="form-control" readonly="true" name="cont" value="${total}">
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Thông tin ẩn</label>
                                                                                <div class="col-md-8">
                                                                                        <textarea id="LN_Series_Summary" readonly="true" name="pDes">${data}</textarea>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <!--                                                                <div class="form-group clearfix">
                                                                                                                                        <div class="form-group clearfix required">
                                                                                                                                                <label class="col-md-2 control-label pt-7 text-right"> Loại đơn </label>
                                                                                                                                                <div class="col-md-8">
                                                                                                                                                        <input type="text" class="form-control" readonly="true" name="cont" value="">
                                                                                                                                                </div>
                                                                                                                                        </div>
                                                                                                                                </div>        
                                                                                                                                <div class="form-group clearfix">
                                                                                                                                        <div class="form-group clearfix required">
                                                                                                                                                <label class="col-md-2 control-label pt-7 text-right"> Ngày tạo yêu cầu </label>
                                                                                                                                                <div class="col-md-8">
                                                                                                                                                        <input type="text" class="form-control" readonly="true" name="cont" value="">
                                                                                                                                                </div>
                                                                                                                                        </div>
                                                                                                                                </div>
                                                                                                                                <div class="form-group clearfix">
                                                                                                                                        <div class="form-group clearfix required">
                                                                                                                                                <label class="col-md-2 control-label pt-7 text-right"> Ngày admin phản hồi </label>
                                                                                                                                                <div class="col-md-8">
                                                                                                                                                        <input type="text" class="form-control" readonly="true" name="cont" value="">
                                                                                                                                                </div>
                                                                                                                                        </div>
                                                                                                                                </div>-->
                                                                <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.2.1/tinymce.min.js" referrerpolicy="origin" type="227dddff75eef862ba087f1e-text/javascript"></script>
                                                                <script type="227dddff75eef862ba087f1e-text/javascript">
                                                                        tinymce.init({
                                                                        selector: 'textarea',
                                                                        inline: false,
                                                                        height: 300,
                                                                        skin: 'oxide-dark',
                                                                        content_css: 'dark',
                                                                        branding: false,
                                                                        menubar: false,
                                                                        contextmenu: false,
                                                                        entities: '160,nbsp,38,amp,60,lt,62,gt',
                                                                        paste_word_valid_elements: 'b,strong,i,em,u,s,a,p,br,img',
                                                                        element_format: 'html',
                                                                        formats: {
                                                                        strikethrough: { inline: 's', remove: 'all' },
                                                                        underline: { inline: 'u', remove: 'all' },
                                                                        },
                                                                        plugins: 'wordcount link image code fullscreen paste emoticons',
                                                                        toolbar: 'undo redo | bold italic underline strikethrough forecolor | link image | removeformat | fullscreen'
                                                                        });
                                                                </script>
                                                                <script>
                                                                function confirmComplain() {
                                                                        var result = confirm("Hãy đảm bảo rằng bạn đã kiểm tra đơn hàng một cách kĩ càng trước khi khiếu nại !");
                                                                        if (result) {
                                                                                window.location.href = "report/create?code=${code}";
                                                                        }
                                                                }
                                                                </script>
                                                                <div class="form-group">
                                                                        <div class="col-md-10 col-md-offset-2">
                                                                                <c:choose>
                                                                                        <c:when test="${trade.getStatusID()==2}">
                                                                                                <button type="submit" class="btn btn-primary">Chấp nhận đơn hàng</button>                                                                            
                                                                                                <button type="button" class="btn btn-primary" style="margin-left:100px; background-color:red;" onclick="confirmComplain()">Khiếu nại</button>
                                                                                        </c:when>
                                                                                        <c:otherwise>
                                                                                                <a href="https://www.google.com/" >
                                                                                                        <button class="btn btn-primary" href="/ElaTrading">Trở về trang chủ</button>
                                                                                                </a> 
                                                                                        </c:otherwise>
                                                                                </c:choose>
                                                                        </div>
                                                                </div>
                                                        </form>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
        <jsp:include page="../base/system_base/footer.jsp"/>

</body>
</html>



