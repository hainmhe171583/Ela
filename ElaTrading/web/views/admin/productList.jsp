<%-- 
    Document   : productList
    Created on : Feb 25, 2024, 9:58:07 PM
    Author     : admin
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="robots" content="NoIndex, NoFollow">
                <title>Bảng điều khiển</title>
                <style>
                        tr:hover {
                                background-color: #f0f0f0;
                        }

                        tr .btn {
                                font-size: 10px;
                        }

                        td {
                                vertical-align: middle !important;
                        }

                        .btn-danger {
                                width: 35.3px;
                        }
                </style>
        </head>
        <body>
                <jsp:include page="/views/base/admin_base/header.jsp"/>
                <div class="container">
                        <div class="row">
                                <div class="col-md-12">
                                        <div class="panel panel-default">
                                                <div class="panel-heading">
                                                        Quản lý sản phẩm
                                                </div>
                                                <div class="panel-body">
                                                        <div class="d-flex justify-content-between">
                                                                <div class="filter-contain">
                                                                        <form id="filter-form" class="filter-form" action="/ElaTrading/admin/products" method="get">
                                                                                <label class="filter-label" for="filter">Tùy chọn: </label>
                                                                                <select onchange="document.getElementById('filter-form').submit()" class="filter-select" name="status" id="filter-form">
                                                                                        <option <c:if test="${status=='0'}">selected</c:if> value="0">Tất cả</option>
                                                                                        <option <c:if test="${status=='2'}">selected</c:if> value="2">Đã bán</option>
                                                                                        <option <c:if test="${status=='1'}">selected</c:if> value="1">Chưa bán</option>
                                                                                        <option <c:if test="${status=='3'}">selected</c:if> value="3">Đã xóa</option>
                                                                                        </select>
                                                                                </form>
                                                                        </div>
                                                                </div>
                                                                <table class="table">
                                                                        <thead>
                                                                                <tr>
                                                                                        <th class="col-xs-7 col-sm-6 col-md-5 col-lg-6">Tên sản phẩm</th>
                                                                                        <th class="hidden-xs col-sm-3 col-md-2 col-lg-2">Giá</th>
                                                                                        <th class="hidden-xs hidden-sm col-md-2 col-lg-2">Trạng thái</th>
                                                                                        <th class="hidden-xs hidden-sm col-md-2 col-lg-2">Tình trạng</th>
                                                                                        <th class="col-xs-5 col-sm-3 col-md-3 col-lg-3 text-center">Quản lý</th>
                                                                                </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <c:forEach items="${products}" var="p">
                                                                                <tr>
                                                                                        <td>
                                                                                                <a href="" title="">
                                                                                                        <b>${p.getProductName()}</b>
                                                                                                </a>
                                                                                        </td>
                                                                                        <td class="hidden-xs">${p.getPrice()}</td>
                                                                                        <td class="hidden-xs hidden-sm">
                                                                                                <c:if test="${!p.isIsPrivate()}">
                                                                                                        <div class="text-success">Công khai</div>
                                                                                                </c:if>
                                                                                                <c:if test="${p.isIsPrivate()}">
                                                                                                        <div class="text-danger">Riêng tư</div>
                                                                                                </c:if>
                                                                                        </td>
                                                                                        <td class="hidden-xs">
                                                                                                <c:if test="${p.isIsDeleted() == true}">
                                                                                                        <div class="text-danger">Đã xóa</div>
                                                                                                </c:if>
                                                                                                <c:if test="${p.isIsDeleted() == false}">
                                                                                                        <c:if test="${p.getStock() == 0}">
                                                                                                                <div class="text-primary">Đã bán</div>
                                                                                                        </c:if>
                                                                                                        <c:if test="${p.getStock() != 0}">
                                                                                                                <div class="text-success">Chưa bán</div>
                                                                                                        </c:if>
                                                                                                </c:if> 
                                                                                        </td>
                                                                                        <td class="">
                                                                                                <c:if test="${p.isIsDeleted() != true}">
                                                                                                        <div class="d-flex">
                                                                                                                <a href="/ElaTrading/admin/products/delete?id=${p.getId()}" class="btn btn-danger mx-3">
                                                                                                                        <i class="fa-solid fa-xmark"></i>
                                                                                                                </a>
                                                                                                        </div>
                                                                                                </c:if>
                                                                                        </td>
                                                                                </tr>
                                                                        </c:forEach>
                                                                </tbody>
                                                        </table>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
                <jsp:include page="/views/base/system_base/footer.jsp"/>
        </body>
</html>
