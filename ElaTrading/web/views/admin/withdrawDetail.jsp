<%-- 
    Document   : addproduct
    Created on : Jan 17, 2024, 10:19:31 PM
    Author     : hai20
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="robots" content="NoIndex, NoFollow">
                <script src="/ElaTrading/assets/hai/add.js"></script>
                <script>window.onload = function () {
                        var mssValue = '<%= request.getAttribute("mss") %>';
                        if (mssValue != "null")
                                alert(mssValue);
                        //document.getElementById('input-file').addEventListener('change', getFile);
                        }</script>
                <title>Rút tiền</title>
        </head>
        <body>
                <jsp:include page="/views/base/admin_base/header.jsp"/>
                <div class="container">
                        <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                        <div class="panel panel-default">
                                                <div class="panel-heading">Thông tin</div>
                                                <div class="panel-body">
                                                        <div class="alert alert-info">
                                                                <c:if test="${withdraw.getStatusID()==2}">
                                                                        Đon đang được duyệt!!<br>    
                                                                </c:if>
                                                                <c:if test="${withdraw.getStatusID()==3}">
                                                                        Đon đã  bị hủy!!<br>
                                                                </c:if>
                                                                <c:if test="${withdraw.getStatusID()==4}">
                                                                        Đon đã được duyệt!!    

                                                                </c:if>
                                                        </div>
                                                        <form action="/ElaTrading/admin/withdraw" role="form" method="post" onsubmit="return checkEmpty()">
                                                                <div class="form-group clearfix required">
                                                                        <label class="col-md-2 control-label pt-7 text-right" >ID rút tiền:</label>
                                                                        <div class="col-md-8">
                                                                                <input type="text" class="title intext" readonly="true" name="code" value="${withdraw.getCode()}">
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right">số tiền muốn rút</label>
                                                                                <div class="col-md-8">
                                                                                        <input type="text" class="form-control" readonly="true" id="formattedInput" name="total" value="${total}">
                                                                                </div>
                                                                        </div>
                                                                </div>

                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Thông tin khách hàng</label>
                                                                                <div class="col-md-8">
                                                                                        <textarea id="LN_Series_Summary" readonly="true" disabled name="info" >${withdraw.getUserInfo()}</textarea>
                                                                                </div>
                                                                        </div>
                                                                </div>

                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Phản hồi của Admin</label>
                                                                                <div class="col-md-8">
                                                                                        <textarea id="LN_Series_Summary" <c:if test="${role==1||withdraw.getStatusID()!=1}"> readonly="true"</c:if> name="respond">${withdraw.getRespond()}</textarea>
                                                                                        </div>
                                                                                </div>
                                                                        </div>

                                                                <c:if test="${role==2 && withdraw.getStatusID()==2}">
                                                                        <div class="form-group clearfix">
                                                                                <div class="form-group clearfix required">
                                                                                        <label class="col-md-2 control-label pt-7 text-right">Trạng thái chuyển khoản:</label>
                                                                                        <div class="col-md-8">
                                                                                                <input type="radio" name="status" value="0" >Không thành công  <input type="radio" name="status" value="1" checked="true">Thành công<br><br>
                                                                                        </div>
                                                                                </div>
                                                                        </div></c:if>

                                                                        <div class="form-group clearfix">
                                                                                <div class="form-group clearfix required">
                                                                                        <label class="col-md-2 control-label pt-7 text-right"> Ngày tạo yêu cầu </label>
                                                                                        <div class="col-md-8">
                                                                                                <input type="text" class="form-control" readonly="true" name="cont" value="${withdraw.getCreated_at()}">
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right"> Cập nhật gần nhất </label>
                                                                                <div class="col-md-8">
                                                                                        <input type="text" class="form-control" readonly="true" name="cont" value="${withdraw.getUpdated_at()}">
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <script src="https://cdn.tiny.cloud/1/5c19zuq7jx1haedv7bv6hju4wfvxld356joddpdqnn0hnk5i/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
                                                                <script type="227dddff75eef862ba087f1e-text/javascript">
                                                                        tinymce.init({
                                                                        selector: 'textarea',
                                                                        readonly : 1,
                                                                        inline: false,
                                                                        height: 300,
                                                                        skin: 'oxide-dark',
                                                                        content_css: 'dark',
                                                                        branding: false,
                                                                        menubar: false,
                                                                        contextmenu: false,
                                                                        entities: '160,nbsp,38,amp,60,lt,62,gt',
                                                                        paste_word_valid_elements: 'b,strong,i,em,u,s,a,p,br,img',
                                                                        element_format: 'html',
                                                                        formats: {
                                                                        strikethrough: { inline: 's', remove: 'all' },
                                                                        underline: { inline: 'u', remove: 'all' },
                                                                        },
                                                                        plugins: 'wordcount link image code fullscreen paste emoticons',
                                                                        toolbar: 'undo redo | bold italic underline strikethrough forecolor | link image | removeformat | fullscreen'
                                                                        });
                                                                </script>
                                                                <div class="form-group">
                                                                        <div class="col-md-10 col-md-offset-2">

                                                                                <c:if test="${role==1 && withdraw.getStatusID()==1}">
                                                                                        <button type="submit" class="btn btn-primary">
                                                                                                Cancel
                                                                                        </button>
                                                                                </c:if>
                                                                                <c:if test="${role==2 && withdraw.getStatusID()==2}">
                                                                                        <button type="submit" class="btn btn-primary">
                                                                                                Phản hồi
                                                                                        </button>
                                                                                </c:if>
                                                                                <a href="/ElaTrading">Go to home page</a>
                                                                        </div>
                                                                </div>
                                                        </form>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>

                <script>
                                                                document.getElementById('formattedInput').addEventListener('input', function (e                                                                                        vent) {
                                                                // Lấy giá trị từ trườn                                                                                        g input
                                                                let inputValue = even                                                    t.target.value;
                                                                // Loại bỏ dấu phẩy hiện có (                                                                                        nếu có)
                                                                inputValue = inputValue.rep                                                    lace(/,/g, '');
                                                                // Chuyển đổi thành số và định dạng lạ                                                                                        i chuỗi
                                                                let formattedValue = Number(inputValue).to                                                    LocaleString();
                                                                // Gán lại giá trị vào trườn                                                                                        g input
                                                                event.target.value = formattedValue;
                                                                });
                </script>
                <jsp:include page="/views/base/system_base/footer.jsp"/>
        </body>
</html>





