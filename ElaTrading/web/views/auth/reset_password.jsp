<%-- 
    Document   : reset_password
    Created on : Jan 24, 2024, 2:16:36 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>JSP Page</title>
        </head>
        <body>
                <jsp:include page="../base/system_base/header.jsp"/>
                <div class="container">
                        <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                        <div class="panel panel-default">
                                                <div class="panel-heading">Đặt Lại Mật Khẩu</div>
                                                <div class="panel-body">
                                                        <form id="reset-password" class="form-horizontal" role="form" method="POST" action="/ElaTrading/resetpassword">
                                                                <div id="email-form" class="form-group">
                                                                        <label for="email" class="col-md-4 control-label">Địa Chỉ Email</label>
                                                                        <div class="col-md-6" style="margin-bottom: 10px">
                                                                                <input id="email" type="email" class="form-control" name="email" value>
                                                                        </div>
                                                                        <label class="col-md-4 control-label">Mã Captcha</label>
                                                                        <div class="col-md-6">
                                                                                <input id="captcha" type="text" class="form-control" name="captcha"> 
                                                                        </div>
                                                                </div>
                                                                <div id="email-form" class="form-group">
                                                                        <label class="col-md-4 control-label">Captcha Code</label>
                                                                        <div class="col-md-6">
                                                                                <img class="captcha" alt="captcha" src="/ElaTrading/captcha">
                                                                                <button class="reload-btn" style="height: 35px; width: 35px;"><i class="fas fa-redo-alt"></i></button>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                        <div class="col-md-6 col-md-offset-4">
                                                                                <button type="submit" class="btn btn-primary">
                                                                                        <i class="fas fa-btn fa-envelope"></i>
                                                                                        Gửi Đường Dẫn Đặt Lại Mật Khẩu
                                                                                </button>
                                                                        </div>
                                                                </div>
                                                        </form>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <script>
                        captcha = document.querySelector(".captcha"),
                        reloadBtn = document.querySelector(".reload-btn");
                        
                        reloadBtn.addEventListener("click", () =>
                        {
                                event.preventDefault();
                                var d = new Date();
                                captcha.src = "/ElaTrading/captcha?" + d.getTime();
                        });
                        
                                $("#reset-password").on("submit", function (e)
                                {
                                        e.preventDefault();
                                        const data = {
                                                email: $("#email").val(),
                                                captcha: $("#captcha").val()
                                        };
                                        $.ajax({
                                                method: "POST",
                                                url: "/ElaTrading/resetpassword",
                                                data: data
                                        }).done(function (data)
                                        {
                                                if (!data.success)
                                                {
                                                        error_Notification(data.message);                                               
                                                        var d = new Date();
                                                        captcha.src = "/ElaTrading/captcha?" + d.getTime();
                                                } 
                                        });
                                });
                        </script>
        </body>
</html>
