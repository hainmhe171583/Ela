<%-- 
    Document   : signup
    Created on : Jan 24, 2024, 2:15:26 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>JSP Page</title>
        </head>

        <body>
                <jsp:include page="../base/system_base/header.jsp"/>
                <div class="container">
                        <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                        <div class="panel panel-default">
                                                <div class="panel-heading">Đăng ký</div>
                                                <div class="panel-body">
                                                        <form class="form-horizontal" id="signup" role="form" method="POST" action="/ElaTrading/signup">
                                                                <div class="form-group">
                                                                        <label for="name" class="col-md-4 control-label">Tên đăng nhập</label>
                                                                        <div class="col-md-6">
                                                                                <input id="name" type="text" class="form-control" name="name" value>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                        <label for="email" class="col-md-4 control-label">Địa chỉ E-Mail</label>
                                                                        <div class="col-md-6">
                                                                                <input id="email" class="form-control" name="email" value>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                        <label for="password" class="col-md-4 control-label">Mật khẩu</label>
                                                                        <div class="col-md-6">
                                                                                <input id="password" type="password" class="form-control" name="password">
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                        <label for="password-confirm" class="col-md-4 control-label">Xác nhận mật khẩu</label>
                                                                        <div class="col-md-6">
                                                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                                                        </div>
                                                                </div>
                                                                <div class="form-group col-6">
                                                                        <label class="col-md-4 control-label">Mã Captcha</label>
                                                                        <div class="col-md-6">
                                                                                <input id="captcha" type="text" class="form-control" name="captcha"> 
                                                                        </div>
                                                                </div>
                                                                <div class="form-group col-6">
                                                                        <label class="col-md-4 control-label">Captcha Code</label>
                                                                        <div class="col-md-6">
                                                                                <img class="captcha" alt="captcha" src="/ElaTrading/captcha">
                                                                                <button class="reload-btn" style="height: 35px; width: 35px;"><i class="fas fa-redo-alt"></i></button>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                        <div class="col-md-6 col-md-offset-4">
                                                                                <button type="submit" class="btn btn-primary">
                                                                                        <i class="fas fa-btn fa-user"></i> Đăng ký
                                                                                </button>
                                                                        </div>
                                                                </div>
                                                        </form>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
                <script>
                        captcha = document.querySelector(".captcha"),
                                reloadBtn = document.querySelector(".reload-btn");

                        reloadBtn.addEventListener("click", () =>
                        {
                                event.preventDefault();
                                var d = new Date();
                                captcha.src = "/ElaTrading/captcha?" + d.getTime();
                        });

                        $("#signup").on("submit", function (e)
                        {
                                e.preventDefault();
                                const data = {
                                        name: $("#name").val(),
                                        email: $("#email").val(),
                                        password: $("#password").val(),
                                        password_confirm: $("#password-confirm").val(),
                                        captcha: $("#captcha").val()
                                };
                                $.ajax({
                                        method: "POST",
                                        url: "/ElaTrading/signup",
                                        data: data
                                }).done(function (data)
                                {
                                        if (!data.success)
                                        {
                                                error_Notification(data.message);
                                                var d = new Date();
                                                captcha.src = "/ElaTrading/captcha?" + d.getTime();

                                        } else {
//                                                console.log(data);
//                                                fetch('/ElaTrading',
//                                                        {
//                                                                method: 'POST',
//                                                                headers: {
//                                                                        'Content-Type': 'application/json;charset=UTF-8'
//                                                                },
//                                                                body: data
//                                                        }).then(response => {
//                                                        if (response.ok) {
//                                                                // Điều hướng đến trang khác
//                                                                window.location.href = '/ElaTrading';
//                                                                console.log("Ekaina");
//                                                                alter(data.message);
//                                                        } else {
//                                                                console.error('Response was not ok.');
//                                                        }
//                                                });
                                                window.location.href = "/ElaTrading";
                                                
                                        }
                                });
                        });
                </script>
                <script src="/livewire/livewire.js?id=f121a5df" data-csrf="KSD2mAeRBgYszHuMUTv1Yw5p6P7XCk2UtKV6xDOQ" data-update-uri="/livewire/update" data-navigate-once="true"></script>
        </body>
</html>
