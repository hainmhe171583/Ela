<%-- 
    Document   : login
    Created on : Jan 24, 2024, 2:15:15 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>JSP Page</title>
        </head>

        <body>
                <jsp:include page="../base/system_base/header.jsp"/>
                <div class="container">
                        <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                        <div class="panel panel-default">
                                                <div class="panel-heading">Đăng nhập</div>
                                                <div class="panel-body">
                                                        <form class="form-horizontal" id="login" role="form" method="POST" action="/ElaTrading/login">
                                                                <div class="form-group">
                                                                        <label for="name" class="col-md-4 control-label">Email hoặc Tên đăng nhập</label>
                                                                        <div class="col-md-6">
                                                                                <input id="name" type="text" class="form-control" name="name" value>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                        <label for="password" class="col-md-4 control-label">Mật khẩu</label>
                                                                        <div class="col-md-6">
                                                                                <input id="password" type="password" class="form-control" name="password">
                                                                        </div>
                                                                </div>
                                                                <div class="form-group col-6">
                                                                        <label class="col-md-4 control-label">Mã Captcha</label>
                                                                        <div class="col-md-6">
                                                                                <input id="captcha" type="text" class="form-control" name="captcha"> 
                                                                        </div>
                                                                </div>
                                                                <div class="form-group col-6">
                                                                        <label class="col-md-4 control-label">Captcha Code</label>
                                                                        <div class="col-md-6">
                                                                                <img class="captcha" alt="captcha" src="/ElaTrading/captcha">
                                                                                <button class="reload-btn" style="height: 35px; width: 35px;"><i class="fas fa-redo-alt"></i></button>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                        <div class="col-md-6 col-md-offset-4" style="text-align: right;">
                                                                                <a class="btn btn-link" href="/ElaTrading/resetpassword">Quên mật khẩu?</a>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                        <div class="col-md-6 col-md-offset-4">
                                                                                <button type="submit" class="btn btn-primary">
                                                                                        <i class="fas fa-btn fa-sign-in"></i> Đăng nhập
                                                                                </button>

                                                                                <a href="/ElaTrading/login/google" class="btn btn-danger ml-2" style="margin-left: 60px">
                                                                                        <i class="fab fa-google"></i> Google
                                                                                </a>
                                                                        </div>
                                                                </div>
                                                        </form>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <script>
                                captcha = document.querySelector(".captcha"),
                                        reloadBtn = document.querySelector(".reload-btn");
                                reloadBtn.addEventListener("click", () =>
                                {
                                        event.preventDefault();
                                        var d = new Date();
                                        captcha.src = "/ElaTrading/captcha?" + d.getTime();
                                });

                                $("#login").on("submit", function (e)
                                {
                                        e.preventDefault();
                                        const data = {
                                                name: $("#name").val(),
                                                password: $("#password").val(),
                                                captcha: $("#captcha").val()
                                        };
                                        $.ajax({
                                                method: "POST",
                                                url: "/ElaTrading/login",
                                                data: data
                                        }).done(function (data)
                                        {
                                                if (!data.success)
                                                {
                                                        error_Notification(data.message);
                                                        var d = new Date();
                                                        captcha.src = "/ElaTrading/captcha?" + d.getTime();
                                                } else {
                                                        location.pathname = "/ElaTrading";
                                                }
                                        });
                                });
                        </script>
                        <jsp:include page="../base/system_base/footer.jsp"/>
        </body>

</html>
