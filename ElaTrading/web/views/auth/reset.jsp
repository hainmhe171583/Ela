<%-- 
    Document   : reset
    Created on : Jan 24, 2024, 2:16:46 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>JSP Page</title>
        </head>
        <body>
                <jsp:include page="../base/system_base/header.jsp"/>
                <div class="container">
                        <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                        <div class="panel panel-default">
                                                <div class="panel-heading">Đặt Lại Mật Khẩu</div>
                                                <div class="panel-body">
                                                        <div class="alert alert-danger hidden" id="show-error">
                                                                <strong>Đã có lỗi xảy ra!</strong><br><br>
                                                                <ul>
                                                                        <li id="content-error"></li>
                                                                </ul>
                                                        </div>
                                                        <form class="form-horizontal" id="reset" role="form" method="POST" action="/ElaTrading/reset">
                                                                <input type="hidden" id="token" name="token" value="${token}" autocomplete="off">
                                                                <div class="form-group">
                                                                        <label for="email" class="col-md-4 control-label">Địa Chỉ Email</label>
                                                                        <div class="col-md-6">
                                                                                <input id="email" type="email" class="form-control" name="email" value="${email}" readonly autofocus>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                        <label for="password" class="col-md-4 control-label">Mật khẩu</label>
                                                                        <div class="col-md-6">
                                                                                <input id="password" type="password" class="form-control" name="password">
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                        <label for="password-confirm" class="col-md-4 control-label">Xác Nhận Mật Khẩu</label>
                                                                        <div class="col-md-6">
                                                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                        <label class="col-md-4 control-label">Mã Captcha</label>
                                                                        <div class="col-md-6">
                                                                                <input id="captcha" type="text" class="form-control" name="captcha"> 
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                        <label class="col-md-4 control-label">Captcha Code</label>
                                                                        <div class="col-md-6">
                                                                                <img class="captcha" alt="captcha" src="/ElaTrading/captcha">
                                                                                <button class="reload-btn" style="height: 35px; width: 35px;"><i class="fas fa-redo-alt"></i></button>
                                                                        </div>
                                                                </div>       
                                                                <div class="form-group">
                                                                        <div class="col-md-6 col-md-offset-4">
                                                                                <button type="submit" class="btn btn-primary">
                                                                                        Đặt Lại Mật Khẩu
                                                                                </button>
                                                                        </div>
                                                                </div>
                                                        </form>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
                <script>                      
                        captcha = document.querySelector(".captcha"),
                        reloadBtn = document.querySelector(".reload-btn");
                        
                        reloadBtn.addEventListener("click", () =>
                        {
                                event.preventDefault();
                                var d = new Date();
                                captcha.src = "/ElaTrading/captcha?" + d.getTime();
                        });
                        
                                $("#reset").on("submit", function (e)
                                {
                                        e.preventDefault();
                                        const data = {
                                                token: $("#token").val(),
                                                email: $("#email").val(),
                                                password: $("#password").val(),
                                                password_confirmation: $("#password-confirm").val(),
                                                captcha: $("#captcha").val()
                                        };
                                        $.ajax({
                                                method: "POST",
                                                url: "/ElaTrading/reset",
                                                data: data
                                        }).done(function (data)
                                        {
                                                if (data != "succeed")
                                                {
                                                        $('#content-error').text(data);
                                                        $("#show-error").removeClass("hidden");
                                                
                                                        var d = new Date();
                                                        captcha.src = "/ElaTrading/captcha?" + d.getTime();
                                                } else {
                                                        location.replace("/ElaTrading")
                                                }
                                        });
                                });
                </script>                                                      
        </body>
</html>
