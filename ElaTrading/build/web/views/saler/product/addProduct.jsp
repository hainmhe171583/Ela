<%-- 
    Document   : addproduct
    Created on : Jan 17, 2024, 10:19:31 PM
    Author     : hai20
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>Tạo đơn hàng</title>
        </head>
        <body>
                <jsp:include page="/views/base/system_base/header.jsp"/>
                <div class="container">
                        <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                        <div class="panel panel-default">
                                                <div class="panel-heading">TẠO ĐƠN HÀNG</div>
                                                <div class="panel-body">
                                                        <form action="create" role="form" method="post">
                                                                <div class="form-group clearfix required">
                                                                        <label class="col-md-2 control-label pt-7 text-right" >Tên sản phẩm</label>
                                                                        <div class="col-md-8">
                                                                                <input type="text" class="form-control" name="pTitle" value>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix required">
                                                                        <label class="col-md-2 control-label pt-7 text-right">Phân loại</label>
                                                                        <div class="col-md-8">
                                                                                <select id="category" class="input-sm" name="category">
                                                                                        <option value="none"> Thể loại </option>
                                                                                        <c:forEach items="${sessionScope.clist}" var="t">
                                                                                                <option value="${t.id}"> ${t.categoryName}</option>
                                                                                        </c:forEach>
                                                                                </select>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Giá sản phẩm</label>
                                                                                <div class="col-md-8">
                                                                                        <input type="text" class="form-control" id="formattedInput" name="price" value>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Liên hệ</label>
                                                                                <div class="col-md-8">
                                                                                        <input type="text" class="form-control" name="cont" value>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Mô tả</label>
                                                                                <div class="col-md-8">
                                                                                        <textarea id="LN_Series_Summary" name="pDes"></textarea>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Thông tin ẩn</label>
                                                                                <div class="col-md-8">
                                                                                        <textarea id="LN_Series_Summary" name="data"></textarea>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Công Khai</label>
                                                                                <div class="col-md-8">
                                                                                        <input type="radio" name="priv" value="0" checked="true">Có
                                                                                        <input type="radio" name="priv" value="1" style="margin-left: 50px">Không  
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <script src="https://cdn.tiny.cloud/1/5c19zuq7jx1haedv7bv6hju4wfvxld356joddpdqnn0hnk5i/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
                                                                <script>
                                                                        tinymce.init({
                                                                        selector: 'textarea',
                                                                        inline: false,
                                                                        height: 300,
                                                                        skin: 'oxide-dark',
                                                                        content_css: 'dark',
                                                                        branding: false,
                                                                        menubar: false,
                                                                        contextmenu: false,
                                                                        entities: '160,nbsp,38,amp,60,lt,62,gt',
                                                                        paste_word_valid_elements: 'b,strong,i,em,u,s,a,p,br,img',
                                                                        element_format: 'html',
                                                                        formats: {
                                                                        strikethrough: { inline: 's', remove: 'all' },
                                                                        underline: { inline: 'u', remove: 'all' },
                                                                        },
                                                                        plugins: 'wordcount link image code fullscreen paste emoticons',
                                                                        toolbar: 'undo redo | bold italic underline strikethrough forecolor | link image | removeformat | fullscreen'
                                                                        });
                                                                </script>
                                                                <div class="form-group">
                                                                        <div class="col-md-10 col-md-offset-2">
                                                                                <button type="submit" class="btn btn-primary">
                                                                                        Tạo đơn hàng
                                                                                </button>
                                                                        </div>
                                                                </div>
                                                        </form>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
                <script>
                          document.getElementById('formattedInput').addEventListener('input', function (event) {
                                        // Lấy giá trị từ trường input
                                        let inputValue = event.target.value;
                                        // Loại bỏ dấu phẩy hiện có (nếu có)
                                        inputValue = inputValue.replace(/,/g, '');
                                        // Chuyển đổi thành số và định dạng lại chuỗi
                                        let formattedValue = Number(inputValue).toLocaleString();
                                        // Gán lại giá trị vào trường input
                                        event.target.value = formattedValue;
                            });

                </script>
                <jsp:include page="/views/base/system_base/footer.jsp"/>
        </body>
</html>


