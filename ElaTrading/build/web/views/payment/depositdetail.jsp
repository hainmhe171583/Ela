<%-- 
    Document   : addproduct
    Created on : Jan 17, 2024, 10:19:31 PM
    Author     : hai20
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="robots" content="NoIndex, NoFollow">
                <script src="/ElaTrading/assets/hai/add.js"></script>
                <script>window.onload = function () {
                        var mssValue = '<%= request.getAttribute("mss") %>';
                        if (mssValue != "null")
                                alert(mssValue);
                        //document.getElementById('input-file').addEventListener('change', getFile);
                        }</script>
                <title> Nạp tiền</title>
        </head>
        <body>
                <jsp:include page="/views/base/system_base/header.jsp"/>
                <div class="container">
                        <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                        <div class="panel panel-default">
                                                <div class="panel-heading">Chi tiết hóa đơn</div>
                                                <div class="panel-body">
                                                        <div class="alert alert-info">
                                                                <c:if test="${pay.isStatus()==true}">
                                                                        Giao dịch thành công.
                                                                </c:if>
                                                                <c:if test="${pay.isStatus()==false}">
                                                                        Giao dịch chưa được thanh toán.
                                                                </c:if>
                                                        </div>
                                                        <form action="" role="form" method="post" onsubmit="return checkEmpty()">
                                                                <input type="hidden" name="_token" value="TfScTidVmsoS5bM9mnghbeXgxFPaDo3qpMxoF5B2">
                                                                <div class="form-group clearfix required">
                                                                        <label class="col-md-2 control-label pt-7 text-right" >ID Thanh toán:</label>
                                                                        <div class="col-md-8">
                                                                                <input type="text" class="title intext" readonly="true" name="pTitle" value="${pay.getPayment_code()}">
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right">Số tiền</label>
                                                                                <div class="col-md-8">
                                                                                        <input type="text" class="form-control" readonly="true" name="cont" value="${amount}">
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <div class="form-group clearfix required">
                                                                                <label class="col-md-2 control-label pt-7 text-right"> Ngày tạo đơn </label>
                                                                                <div class="col-md-8">
                                                                                        <input type="text" class="form-control" readonly="true" name="cont" value="${pay.getCreated_at()}">
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.2.1/tinymce.min.js" referrerpolicy="origin" type="227dddff75eef862ba087f1e-text/javascript"></script>
                                                                <script type="227dddff75eef862ba087f1e-text/javascript">
                                                                        tinymce.init({
                                                                        selector: 'textarea',
                                                                        inline: false,
                                                                        height: 300,
                                                                        skin: 'oxide-dark',
                                                                        content_css: 'dark',
                                                                        branding: false,
                                                                        menubar: false,
                                                                        contextmenu: false,
                                                                        entities: '160,nbsp,38,amp,60,lt,62,gt',
                                                                        paste_word_valid_elements: 'b,strong,i,em,u,s,a,p,br,img',
                                                                        element_format: 'html',
                                                                        formats: {
                                                                        strikethrough: { inline: 's', remove: 'all' },
                                                                        underline: { inline: 'u', remove: 'all' },
                                                                        },
                                                                        plugins: 'wordcount link image code fullscreen paste emoticons',
                                                                        toolbar: 'undo redo | bold italic underline strikethrough forecolor | link image | removeformat | fullscreen'
                                                                        });
                                                                </script>
                                                                <div class="form-group">
                                                                        <div class="col-md-10 col-md-offset-2">
                                                                                <c:choose>
                                                                                        <c:when test="${pay.isStatus()==true}">
                                                                                                <a href="/ElaTrading">Go to home page</a>
                                                                                        </c:when>
                                                                                        <c:otherwise>
                                                                                                <!--<h2>Trading successful</h2>-->
                                                                                                <a href="${pay.getLink()}">Thanh toán ngay</a>
                                                                                        </c:otherwise>
                                                                                </c:choose>
                                                                        </div>
                                                                </div>
                                                        </form>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>

                <script>
                          document.getElementById('formattedInput').addEventListener('input', function (event) {
                                        // Lấy giá trị từ trường input
                                        let inputValue = event.target.value;
                                        // Loại bỏ dấu phẩy hiện có (nếu có)
                                        inputValue = inputValue.replace(/,/g, '');
                                        // Chuyển đổi thành số và định dạng lại chuỗi
                                        let formattedValue = Number(inputValue).toLocaleString();
                                        // Gán lại giá trị vào trường input
                                        event.target.value = formattedValue;
                            });
                </script>
                <jsp:include page="/views/base/system_base/footer.jsp"/>
        </body>
</html>



