<%-- 
    Document   : reportList
    Created on : 06/03/2024, 9:05:49 PM
    Author     : Admin
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="robots" content="NoIndex, NoFollow">
                <title>Bảng điều khiển</title>
                <style>
                        tr:hover {
                                background-color: #f0f0f0;
                        }

                        tr .btn {
                                font-size: 10px;
                        }

                        td {
                                vertical-align: middle !important;
                        }

                        .btn-danger {
                                width: 35.3px;
                        }
                        .state {
                                width: 20px;
                                height: 20px;
                                border-radius: 50%;
                                display: inline-block;
                        }

                        .active {
                                background-color: red;
                        }

                        .inactive {
                                background-color: green;
                        }

                        .filter-contain{
                                display: flex;
                                justify-content: start;
                                width: 100%;
                        }

                        .filter-form {
                                background-color: #fff;
                                padding: 20px;
                                border-radius: 5px;
                                box-shadow: 0 2px 5px rgba(0,0,0,0.1);
                                width: 300px;
                        }

                        .filter-label {
                                font-weight: bold;
                                display: block;
                                margin-bottom: 10px;
                        }

                        .filter-select {
                                width: 100%;
                                padding: 10px;
                                border: 1px solid #ccc;
                                border-radius: 5px;
                                font-size: 16px;
                        }

                        .filter-submit {
                                display: block;
                                width: 100%;
                                padding: 10px;
                                margin-top: 10px;
                                background-color: #007bff;
                                color: #fff;
                                border: none;
                                border-radius: 5px;
                                cursor: pointer;
                        }

                        .filter-submit:hover {
                                background-color: #0056b3;
                        }
                </style>
        </head>
        <body>
                <jsp:include page="/views/base/admin_base/header.jsp"/>
                <div class="container">
                        <div class="row">
                                <div class="col-md-12">
                                        <div class="panel panel-default">
                                                <div class="panel-heading">
                                                        Quản lý khiếu nại
                                                </div>
                                                <div class="filter-contain">
                                                        <form id="filter-form" class="filter-form" action="/ElaTrading/admin/reports" method="post">
                                                                <label class="filter-label" for="filter">Tùy chọn:</label>
                                                                <select onchange="document.getElementById('filter-form').submit()" class="filter-select" name="filter" id="filter">
                                                                        <option <c:if test="${filter=='all'}">selected</c:if> value="all">Tất cả</option>
                                                                        <option <c:if test="${filter=='processing'}">selected</c:if> value="processing">Đang xử lý</option>
                                                                        <option <c:if test="${filter=='accepted'}">selected</c:if> value="accepted">Chấp nhận</option>
                                                                        <option <c:if test="${filter=='rejected'}">selected</c:if> value="rejected">Từ chối</option>
                                                                        </select>
                                                                </form>
                                                        </div>
                                                        <div class="panel-body">
                                                                <table class="table">
                                                                        <thead>
                                                                                <tr>
                                                                                        <th class="col-xs-7 col-sm-6 col-md-5 col-lg-4">Tên sản phẩm</th>
                                                                                        <th class="hidden-xs col-sm-3 col-md-2 col-lg-2">Tên người bán</th>
                                                                                        <th class="hidden-xs hidden-sm col-md-2 col-lg-2">Tên người mua</th>
                                                                                        <th class="hidden-xs hidden-sm col-md-2 col-lg-2">Ngày tạo</th>
                                                                                        <th class="hidden-xs col-sm-3 col-md-2 col-lg-2">Trạng thái</th>
                                                                                        <th class="col-xs-5 col-sm-3 col-md-3 col-lg-3 text-center">Quản lý</th>
                                                                                </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <c:forEach items="${listReportDto}" var="r">
                                                                                <tr>
                                                                                        <td>
                                                                                                <a href="" title="">
                                                                                                        <b>${r.getProductName()}</b>
                                                                                                </a>
                                                                                        </td>
                                                                                        <td>${r.getSaler().getUsername()}</td>
                                                                                        <td>${r.getBuyer().getUsername()}</td>
                                                                                        <td>${r.getCreated_at()}</td>
                                                                                        <td>
                                                                                                <c:if test="${r.getStatusId() == '2'}">
                                                                                                        Đang xử lý
                                                                                                </c:if>
                                                                                                <c:if test="${r.getStatusId() == '4'}">
                                                                                                        Chấp nhận
                                                                                                </c:if>
                                                                                                <c:if test="${r.getStatusId() == '3'}">
                                                                                                        Từ chối
                                                                                                </c:if>
                                                                                        </td>
                                                                                        <td class="">
                                                                                                <div class="d-flex">
                                                                                                        <a href="/ElaTrading/admin/report?code=${r.code}">
                                                                                                                <button type="submit" class="btn btn-info mx-3">
                                                                                                                        <i class="fa-solid fa-circle-info"></i>
                                                                                                                </button>
                                                                                                        </a>
                                                                                                       <form action="/ElaTrading/admin/reports/process" method="post">
                                                                                                                <input type="hidden" name="status" value="4" />
                                                                                                                <input type="hidden" name="tradingId" value="${r.code}" />
                                                                                                                <button type="submit" class="btn btn-success mx-3">
                                                                                                                        <i class="fa-solid fa-check"></i>
                                                                                                                </button>
                                                                                                        </form>
                                                                                                        <form action="/ElaTrading/admin/reports/process" method="post">
                                                                                                                <input type="hidden" name="status" value="3" />
                                                                                                                <input type="hidden" name="tradingId" value="${r.code}" />
                                                                                                                <button type="submit" class="btn btn-danger mx-3">
                                                                                                                        <i class="fa-solid fa-xmark"></i>
                                                                                                                </button>
                                                                                                        </form>
                                                                                                </div>
                                                                                        </td>
                                                                                </tr>
                                                                        </c:forEach>
                                                                </tbody>
                                                        </table>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
                <jsp:include page="/views/base/system_base/footer.jsp"/>
                <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js" ></script>
                <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
                <script>
                                                                        document.getElementById("ft1").value = document.getElementById("filter").value;
                                                                        document.getElementById("ft2").value = document.getElementById("filter").value;
                                                                        function activeErrorToast(abc) {
                                                                                toastr.error(abc);
                                                                        }
                                                                        function activeSuccessToast(abc) {
                                                                                toastr.success(abc);
                                                                        }
                                                                        if ('${status}' === 'green')
                                                                                activeSuccessToast('${message}');
                                                                        if ('${status}' === 'red')
                                                                                activeErrorToast('${message}');
                </script>
        </body>
</html>

