<%-- 
    Document   : productList
    Created on : Feb 25, 2024, 9:58:07 PM
    Author     : admin
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="robots" content="NoIndex, NoFollow">
                <title>Yêu cầu rút tiền</title>
        </head>
        <body>
                <jsp:include page="/views/base/admin_base/header.jsp"/>
                <style>
                        tr:hover {
                                background-color: #f0f0f0;
                        }
                        tr .btn {
                                font-size: 10px;
                        }
                        td {
                                vertical-align: middle !important;
                        }

                </style>
                <div class="container">
                        <div class="row">             
                                <div class="col-md-12">
                                        <div class="panel panel-default">
                                                <div class="panel-heading">
                                                        Yêu cầu rút tiền
                                                </div>
                                                <div class="panel-body">
                                                        <table class="table table-striped table-hover"  id="productTable">
                                                                <thead>
                                                                        <tr style="font-size: 13px">
                                                                                <th>Người rút</th>
                                                                                <th>Số tiền</th>
                                                                                <th>Trạng thái</th>
                                                                                <th>Ngày tạo đơn</th>
                                                                        </tr>
                                                                </thead>
                                                                <tbody>
                                                                        <tr >
                                                                                <c:forEach items="${listWithdraw}" var="o">
                                                                                        <td>${o.user.username}</td>
                                                                                        <td>${o.getTotal()}</td>
                                                                                        <c:if test="${o.getStatusID() == 2}">
                                                                                                <td style="color: black"> 
                                                                                                        Đang duyệt
                                                                                                </td>
                                                                                        </c:if>
                                                                                        <c:if test="${o.getStatusID() == 3}">
                                                                                                <td style="color: red"> 
                                                                                                        Thất bại
                                                                                                </td>
                                                                                        </c:if>
                                                                                        <c:if test="${o.getStatusID() == 4}">
                                                                                                <td style="color: green"> 
                                                                                                        Thành công
                                                                                                </td>
                                                                                        </c:if>
                                                                                        <td>${o.created_at}</td>
                                                                                        <td >
                                                                                                <a href="/ElaTrading/admin/withdraw?code=${o.code}"><button type="button" class="btn btn-primary"><i class="material-icons" data-toggle="tooltip" title="Details" style="font-size: 10px">Chi tiết</i></button></a>
                                                                                        </td>
                                                                                </tr>
                                                                        </c:forEach>
                                                                </tbody>
                                                        </table>
                                                        <form action="/ElaTrading/action/withdraws" method="get" style="text-align: right; margin-top: 10px">
                                                                <select name="status" id="status">
                                                                        <option value="All">Tất cả</option>
                                                                        <option value="DangDuyet">Đang duyệt </option>
                                                                        <option value="ThatBai">Thất bại </option>
                                                                        <option value="ThanhCong">Thành công </option>
                                                                </select>
                                                                <input type="submit" value="Lọc">
                                                        </form>
                                                        <c:if test="${countList > 0}">
                                                                <ul class="pagination">
                                                                        <c:set var="currentPage" value="${param.index}" />
                                                                        <c:if test="${empty currentPage}">
                                                                                <c:set var="currentPage" value="1" />
                                                                        </c:if>
                                                                        <c:forEach begin="1" end="${endP}" var="i">     
                                                                                <li class="page-item text-white ${currentPage == i ? 'active' : ''}"></li>
                                                                                <li class="page-item text-white ${tag == i ? 'active': ''}"> <a href="/ElaTrading/action/withdraws?status=${status}&index=${i}" class="page-link">${i}</a></li>
                                                                        </c:forEach>
                                                                </ul>
                                                        </c:if>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </body>
        <jsp:include page="/views/base/system_base/footer.jsp"/>
</html>


