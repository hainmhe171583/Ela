<%-- 
    Document   : userList
    Created on : 05/03/2024, 8:46:49 PM
    Author     : Admin
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="robots" content="NoIndex, NoFollow">
                <title>Bảng điều khiển</title>
                <style>
                        table {
                                width: 100%;
                                border-collapse: collapse;
                        }
                        th, td {
                                text-align: center !important; /* Aligning both th and td content to the center */
                                padding: 8px;
                        }

                        tr:hover {
                                background-color: #f0f0f0;
                        }

                        tr .btn {
                                font-size: 10px;
                        }

                        td {
                                vertical-align: middle !important;
                        }

                        .btn-danger {
                                width: 35.3px;
                        }

                        .state {
                                width: 20px;
                                height: 20px;
                                border-radius: 50%;
                                display: inline-block;
                        }

                        .active {
                                background-color: red;
                        }

                        .inactive {
                                background-color: green;
                        }

                        .filter-contain{
                                display: flex;
                                justify-content: start;
                                width: 100%;
                        }

                        .filter-form {
                                background-color: #fff;
                                padding: 20px;
                                border-radius: 5px;
                                box-shadow: 0 2px 5px rgba(0,0,0,0.1);
                                width: 300px;
                        }

                        .filter-label {
                                font-weight: bold;
                                display: block;
                                margin-bottom: 10px;
                        }

                        .filter-select {
                                width: 100%;
                                padding: 10px;
                                border: 1px solid #ccc;
                                border-radius: 5px;
                                font-size: 16px;
                        }

                        .filter-submit {
                                display: block;
                                width: 100%;
                                padding: 10px;
                                margin-top: 10px;
                                background-color: #007bff;
                                color: #fff;
                                border: none;
                                border-radius: 5px;
                                cursor: pointer;
                        }

                        .filter-submit:hover {
                                background-color: #0056b3;
                        }
                </style>
        </head>
        <body>
                <jsp:include page="/views/base/admin_base/header.jsp"/>
                <div class="container">
                        <div class="row">
                                <div class="col-md-12">
                                        <div class="panel panel-default">
                                                <div class="panel-heading">
                                                        Danh sách quản lý người dùng
                                                </div>
                                                <div class="filter-contain">
                                                        <form id="filter-form" class="filter-form" action="/ElaTrading/admin/users/filter" method="post">
                                                                <label class="filter-label" for="filter">Tùy chọn: </label>
                                                                <select onchange="document.getElementById('filter-form').submit()" class="filter-select" name="filter" id="filter">
                                                                        <option <c:if test="${filter=='all'}">selected</c:if> value="all">Tất cả</option>
                                                                        <option <c:if test="${filter=='banned'}">selected</c:if> value="banned">Người dùng bị khóa</option>
                                                                        <option <c:if test="${filter=='deleted'}">selected</c:if> value="deleted">Người dùng bị xóa</option>
                                                                        </select>
                                                                </form>
                                                        </div>
                                                        <div class="panel-body">
                                                                <table class="table">
                                                                        <thead>
                                                                                <tr>
                                                                                        <th class="col-xs-7 col-sm-6 col-md-5 col-lg-3">Tên người dùng</th>
                                                                                        <th class="hidden-xs col-sm-3 col-md-2 col-lg-3">Email</th>
                                                                                        <th class="hidden-xs hidden-sm col-md-2 col-lg-2">Số điện thoại</th>
                                                                                        <th class="hidden-xs hidden-sm col-md-2 col-lg-2">Trạng thái hoạt động</th>
                                                                                        <c:if test="${filter != 'deleted'}">
                                                                                                <th class="col-xs-5 col-sm-3 col-md-3 col-lg-2">Quản lý</th>
                                                                                        </c:if>
                                                                        </tr>
                                                                </thead>
                                                                <tbody>
                                                                        <c:forEach items="${listUser}" var="u">
                                                                                <tr>
                                                                                        <td>
                                                                                                <a href="" title="">
                                                                                                        <b>${u.getUsername()}</b>
                                                                                                </a>
                                                                                        </td>
                                                                                        <td>${u.getEmail()}</td>
                                                                                        <td>${u.getPhone()}</td>
                                                                                        <c:if test="${filter == 'deleted'}">
                                                                                                <td style="color: red">Đã xóa</td>
                                                                                        </c:if>
                                                                                        <c:if test="${filter != 'deleted'}">
                                                                                                <td>
                                                                                                        <c:if test="${u.isBanned()}">
                                                                                                                <div style="color: red">Ðã khóa</div>
                                                                                                        </c:if>
                                                                                                        <c:if test="${!u.isBanned()}">
                                                                                                                <div style="color: green">Đang hoạt động</div>
                                                                                                        </c:if>
                                                                                                </td>
                                                                                        </c:if>
                                                                                        <c:if test="${filter != 'deleted'}">
                                                                                                <td  class="">
                                                                                                        <div style="justify-content: center" class="d-flex">
                                                                                                                <c:if test="${u.isBanned() == 'false'}">
                                                                                                                        <form action="/ElaTrading/admin/users/ban" method="post">
                                                                                                                                <input type="hidden" value="${u.getId()}" name="userId" />
                                                                                                                                <input type="hidden" value="ban" name="type" />
                                                                                                                                <button type="submit" class="btn btn-danger mx-3">
                                                                                                                                        <i class="fa-solid fa-ban"></i>
                                                                                                                                </button>
                                                                                                                        </form>
                                                                                                                </c:if>
                                                                                                                <c:if test="${filter == 'banned'}">
                                                                                                                        <form action="/ElaTrading/admin/users/ban" method="post">
                                                                                                                                <input type="hidden" value="${u.getId()}" name="userId" />
                                                                                                                                <input type="hidden" value="unban" name="type" />
                                                                                                                                <button type="submit" class="btn btn-success mx-3">
                                                                                                                                        <i class="fa-solid fa-check"></i>
                                                                                                                                </button>
                                                                                                                        </form>
                                                                                                                </c:if>  
                                                                                                                <c:if test="${filter != 'banned'}">
                                                                                                                        <form action="/ElaTrading/admin/users/delete" method="post">
                                                                                                                                <input type="hidden" value="${u.getId()}" name="userId" />
                                                                                                                                <button type="submit" class="btn btn-danger mx-3">
                                                                                                                                        <i class="fa-solid fa-xmark"></i>
                                                                                                                                </button>
                                                                                                                        </form>
                                                                                                                </c:if>
                                                                                                        </div>
                                                                                                </td>
                                                                                        </c:if>
                                                                                </tr>
                                                                        </c:forEach>
                                                                </tbody>
                                                        </table>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
                <jsp:include page="/views/base/system_base/footer.jsp"/>
                <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js" ></script>
                <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
                <script>
                                                function activeErrorToast(abc) {
                                                        toastr.error(abc);
                                                }
                                                function activeSuccessToast(abc) {
                                                        toastr.success(abc);
                                                }
                                                if ('${status}' === 'green')
                                                        activeSuccessToast('${message}');
                                                if ('${status}' === 'red')
                                                        activeErrorToast('${message}');
                </script>
        </body>
</html>

