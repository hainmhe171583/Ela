<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <style media="screen" type="text/css">
/*                        body {
                                overflow-x: hidden;
                                background-color: #f8f9fa;
                        }*/

                        #chat-box {
                                position: fixed;
                                margin-right: 50px;
                                bottom: 20px;
                                right: 20px;
                                width: 300px;
                                display: none;
                                flex-direction: column;
                                border: 2px solid #4caf50;
                                border-radius: 10px;
                                overflow: hidden;
                                box-shadow: 0 0 15px #4caf50;

                        }

                        #chat {
                                width: 100%;
                                min-height: 300px;
                                max-height: 300px;
                                overflow-y: auto;
                                background-color: #dee2e6;
                                padding: 10px;
                                box-sizing: border-box;
                        }

                        #input {
                                display: flex;
                                align-items: center;
                                border-top: 2px solid #4caf50;
                                background-color: #fff;
                                padding: 10px;
                        }

                        #textcontent {
                                flex: 1;
                                padding: 8px;
                                border: 1px solid #ced4da;
                                border-radius: 5px;
                                margin-right: 10px;
                        }

                        button {
                                background-color: #4caf50;
                                color: #fff;
                                border: none;
                                padding: 10px 15px;
                                border-radius: 5px;
                                cursor: pointer;
                        }

                        button:hover {
                                background-color: #6a9fd9;
                        }

                        #chat-icon {
                                position: fixed;
                                bottom: 20px;
                                right: 20px;
                                background-color: #4caf50;
                                color: #fff;
                                padding: 10px;
                                border-radius: 50%;
                                cursor: pointer;
                                box-shadow: 0 0 15px #4caf50;
                                z-index: 100;
                        }

                        #chat-content1 {
                                display: flex;
                                flex-direction: column;
                                margin-top: 5px;
                                margin-bottom: 10px;
                                padding: 10px;
                                border: 1px solid #ccc; /* Thêm border cho chat-content */
                                border-radius: 10px; /* Bo tròn góc của border */
                                width: 75%; /* Đặt chiều rộng là 50% */
                        }

                        #chat-content1 > div {
                                margin-bottom: 10px;
                                padding: 3px;
                                position: relative;
                                overflow-wrap: break-word;
                        }

                        #chat-content2 {
                                display: flex;
                                flex-direction: column;
                                margin-top: 5px;
                                margin-bottom: 10px;
                                padding: 10px;
                                border: 1px solid #ccc; /* Thêm border cho chat-content */
                                border-radius: 10px; /* Bo tròn góc của border */
                                width: 75%; /* Đặt chiều rộng là 50% */
                                margin-left: auto; /* Đẩy chat-content2 về bên phải */
                        }

                        #chat-content2 > div {
                                margin-bottom: 10px;
                                padding: 3px;
                                position: relative;
                                overflow-wrap: break-word;
                        }

                        .created_at {
                                font-size: 10px;
                        }
                </style>
        </head>
        <script type="text/javascript">
                var senderId = "${sender.id}";
                var wsUrl;
                if (window.location.protocol === 'http:') {
                        wsUrl = 'ws://';
                } else {
                        wsUrl = 'wss://';
                }
                var ws = new WebSocket(wsUrl + window.location.host + "/ElaTrading/chat");

                ws.onclose = function (event) {
                        console.log("WebSocket connection closed:", event);
                };

                ws.onmessage = function (event) {
                        var message = JSON.parse(event.data);
                        console.log("Received message:", message);
                        var mySpan = document.getElementById("chat");
                        var messageDiv = document.createElement("div");
                        var imagechat = document.createElement("img");
                        var isSender = message.senderId === ${sender.id};
                        if (isSender) {
                                messageDiv.id = "chat-content2";
                        } else {
                                messageDiv.id = "chat-content1";
                        }
                        var timeDiv = document.createElement("div");
                        timeDiv.setAttribute("class", "created_at");
                        timeDiv.textContent = message.created_at;
                        messageDiv.appendChild(timeDiv);

                        if (message.type === "Text") {
                                var contentDiv = document.createElement("div");
                                contentDiv.textContent = message.senderName + ' : ' + message.content;
                                messageDiv.appendChild(contentDiv);
                        } else if (message.type === "Image") {
                                var contentDiv = document.createElement("div");
                                contentDiv.textContent = message.senderName + ' : ';
                                messageDiv.appendChild(contentDiv);
                                var imagechat = document.createElement("img");
                                imagechat.className = "chat-image";
                                var fullImageUrl = message.content;
                                imagechat.style.borderRadius = "3px";
                                imagechat.style.height = "auto";
                                imagechat.style.width = "170px";
                                var linkElement = document.createElement("a");
                                linkElement.href = fullImageUrl;
                                linkElement.target = "_blank";
                                linkElement.appendChild(imagechat);
                                messageDiv.appendChild(linkElement);
                                imagechat.src = fullImageUrl;
                        }
                        mySpan.appendChild(messageDiv);
                };


                function sendMsg() {
                        try {
                                var currentDate = new Date();
                                var currentYear = currentDate.getFullYear();
                                var currentMonth = currentDate.getMonth() + 1;
                                var currentDay = currentDate.getDate();
                                var currentHour = currentDate.getHours();
                                var currentMinute = currentDate.getMinutes();
                                var currentSecond = currentDate.getSeconds();
                                var formattedDateTime = currentYear + '-' + currentMonth + '-' + currentDay +
                                        ' ' + currentHour + ':' + currentMinute + ':' + currentSecond;
                                var textcontent = document.getElementById("textcontent").value;
                                var imagecontent = document.getElementById("imagecontent");
                                var image = imagecontent.files[0];

                                var formData = new FormData();
                                if (image) {
                                        formData.append("imagecontent", image);
                                        formData.append("textcontent", "");
                                        formData.append("senderId", ${sender.id});


                                        var fullImageUrl = URL.createObjectURL(image);
                                        var ImageChatDetail = {
                                                type: "Image",
                                                content: fullImageUrl,
                                                senderId: ${sender.id},
                                                senderName: "${sender.username}",
                                                create_at: formattedDateTime
                                        };
                                        document.getElementById("textcontent").value = "";
                                        document.getElementById("imagecontent").value = "";
                                        var url = URL.createObjectURL(image);
                                        ws.send(JSON.stringify(ImageChatDetail));
                                        addChat(formData);
                                } else {
                                        formData.append("imagecontent", "");
                                        formData.append("textcontent", textcontent);
                                        formData.append("senderId", ${sender.id});

                                        var TextChatDetail = {
                                                type: "Text",
                                                content: textcontent,
                                                senderId: ${sender.id},
                                                senderName: "${sender.username}",
                                                create_at: formattedDateTime
                                        };
                                        ws.send(JSON.stringify(TextChatDetail));
                                        addChat(formData);
                                }
                                var chatBox = document.getElementById('chat-box');
                                chatBox.scrollTop = chatBox.scrollHeight;
                        } catch (error) {
                                console.error("Error parsing message:", error);
                        }
                }

                function addChat(formData) {
                        console.log("Sending data to server - senderId:", formData.get("senderId"), "textcontent:", formData.get("textcontent"), "imgcontent:", formData.get("imagecontent"));

                        $.ajax({
                                type: "POST",
                                url: "chat/add",
                                data: formData,
                                contentType: false,
                                processData: false,
                                success: function (response) {
                                        console.log("Status updated successfully");
                                        formData.forEach(function (value, key) {
                                                formData.delete(key);
                                        });
                                },
                                error: function (error) {
                                        console.error("Error updating status", error);
                                }
                        });
                        formData = new FormData();
                        $("#textcontent").val("");
                        $("#imagecontent").val("");
                        var preview = document.getElementById("image-preview");
                        if (preview) {
                                preview.parentNode.removeChild(preview);
                        }
                }

        </script>
        <body style="overflow-x: hidden; background-color: #f8f9fa;">
                <c:if test="${sender != null}">
                        <div id="chat-icon">
                                <i class="fas fa-comments"></i>
                        </div>
                        <div id="chat-box" style="display:none;">
                                <c:set var="sender" value="${sender}"/>
                                <div id="chat" class="chat">
                                        <div id="chat-header" style="padding: 10px; box-shadow: 0 0 15px #4caf50; border-radius: 5px;background-color: #46ab4a; color: white; text-align: center; position: sticky; top: 0; z-index: 100;">Cộng đồng</div>
                                        <c:forEach var="lc" items="${listchat}" varStatus="loop">
                                                <c:choose>
                                                        <c:when test="${sender.username != lc.sender.username}">
                                                                <div id="chat-content1">
                                                                        <div class="created_at">${lc.created_at}</div>
                                                                        <div>${lc.sender.username} :</br>
                                                                                <c:choose>
                                                                                        <c:when test="${not empty lc.imagecontent}">
                                                                                                <a href="http://localhost:9999/ElaTrading/assets/image/chat/${lc.imagecontent}" target="_blank">
                                                                                                        <img src="${lc.imagecontent}" alt="Image" style="width: 170px; height: auto; border-radius: 3px">
                                                                                                </a>
                                                                                        </c:when>
                                                                                        <c:otherwise>
                                                                                                ${lc.textcontent}
                                                                                        </c:otherwise>
                                                                                </c:choose>
                                                                        </div>
                                                                </div>
                                                        </c:when>
                                                        <c:otherwise>
                                                                <div id="chat-content2">
                                                                        <div class="created_at">${lc.created_at}</div>
                                                                        <div>${lc.sender.username} :</br>
                                                                                <c:choose>
                                                                                        <c:when test="${not empty lc.imagecontent}">
                                                                                                <a href="http://localhost:9999/ElaTrading/${lc.imagecontent}" target="_blank">
                                                                                                        <img src="${lc.imagecontent}" alt="Image" style="width: 170px; height: auto; border-radius: 3px">
                                                                                                </a>
                                                                                        </c:when>
                                                                                        <c:otherwise>
                                                                                                ${lc.textcontent}
                                                                                        </c:otherwise>
                                                                                </c:choose>
                                                                        </div>
                                                                </div>
                                                        </c:otherwise>
                                                </c:choose>
                                        </c:forEach>
                                </div>
                                <div id="input">
                                        <input type="hidden" id="senderid" value="${sender.id}"/>
                                        <label for="imagecontent" class="file-label">
                                                <i class="fas fa-file-image" style="padding: 5px"></i>
                                                <input type="file" accept="*/*" enctype="multipart/form-data"  id="imagecontent" onchange="displayFileName()" style="display: none"/>
                                        </label>
                                        <input type="text" id="textcontent"  placeholder="Nhập tin nhắn..."/>
                                        <button  onclick="sendMsg()">Gửi</button>
                                </div>
                        </div>
                </c:if>
                <script>
                        document.getElementById('chat-icon').addEventListener('click', function () {
                                var chatBox = document.getElementById('chat-box');
                                if (chatBox.style.display === 'none') {
                                        chatBox.style.display = 'flex';
                                } else {
                                        chatBox.style.display = 'none';
                                }


                        });

                        function handleFileUpload(event) {
                                var imagecontent = document.getElementById("imagecontent");
                                var files = imagecontent.files;

                                for (var i = 0; i < files.length; i++) {
                                        var file = files[i];
                                        console.log("Selected file:", file);
                                }
                        }

                        function displayFileName() {
                                var fileInput = document.getElementById("imagecontent");
                                var textcontent = document.getElementById("textcontent");

                                if (fileInput.files.length > 0) {
                                        var fileName = fileInput.files[0].name;
                                        textcontent.value = fileName;
                                }
                        }

                </script>

        </body>
</html>



