<%-- 
    Document   : report_detail
    Created on : Mar 3, 2024, 1:31:53 AM
    Author     : admin
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
        <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Report Form</title>
                <!-- Link to Bootstrap CSS -->
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
                <style>
                        body, html {
                                height: 100%;
                                margin: 0;
                        }

                        .container {
                                display: flex;
                                height: 100vh;
                        }

                        .notification, .form-container {
                                height: 800px;
                                flex: 1;
                                padding: 20px;
                                border: 1px solid #000;
                                border-radius: 5px;
                                box-sizing: border-box;
                        }

                        .notification {
                                background-color: #fb7457a3;
                                box-shadow: 0 5px 10px rgb(221 19 19);
                        }

                        .form-button{
                                display: block;
                                width: 100%;
                                padding: 10px;
                                margin-top: 10px;
                                background-color: #3bdb72;
                                color: #fff;
                                border: none;
                                border-radius: 5px;
                        }

                        .form-button:hover {
                                background-color: #fb7457a3;
                        }

                        .back-button:hover {
                                background-color: #fb7457a3;
                        }

                </style>
        </head>
        <body>
                <c:set var="trading" value="${trading}"/>
                <div style="padding-right: 5%">
                        <button class="back-button" style="background-color: #3bdb72; margin-left: 20px; margin-top: 20px; border-radius: 5px"><a style="color: white; text-decoration: none" href="/trading?proCode=${trading.product.code}&code=${trading.code}">Back</a></button>
                </div>
                <div class="container">
                        <div class="notification" style="height: 50%">
                                <p style="color: #3b392e">Sau khi gửi đơn khiếu nại, thời gian bên bán xử lí sẽ từ 1 - 2h. Bên bán sẽ chủ động liên lạc để đối chiếu sản phẩm với bạn thông qua thông tin liên hệ đã có sẵn.
                                        Nếu sản phẩm không đúng với mô tả và cam kết thì bên bán sẽ hủy đơn hàng và chịu trách nhiệm với bên mua.</p>
                        </div>
                        <div class="form-container" >
                                <form action="create" method="post" style="height: 90%; margin-bottom: 30px">
                                        <div class="form-group">
                                                <label for="titleReport">Tiêu đề:</label>
                                                <input type="text" class="form-control" name="titlereport" id="titleReport" placeholder="Nhập tiêu đề ..." required>
                                        </div>
                                        <div class="form-group">
                                                <label for="customerName">Tên khách hàng:</label>
                                                <input type="text" class="form-control"  name="customername" id="customerName" readonly value="${trading.user.username}" placeholder="Customer name">
                                        </div>
                                        <div class="form-group">
                                                <label for="sellerName">Tên người bán:</label>
                                                <input type="text" class="form-control" name="sellername"  id="sellerName" readonly value="${seller.username}" placeholder="Seller name">
                                        </div>
                                        <div class="form-group">
                                                <label for="codeProduct">Tên sản phẩm:</label>
                                                <input type="text" class="form-control" name="codeproduct"  readonly value="${trading.product.productName}" id="codeProduct" placeholder="Product code">
                                        </div>
                                        <div class="form-group">
                                                <label for="codeTrading">Mã giao dịch:</label>
                                                <input type="text" class="form-control" name="codetrading"  readonly value="${trading.code}" id="codeTrading" placeholder="Trading code">
                                        </div>
                                        <div class="form-group">
                                                <label for="contentReport">Nội dung:</label>
                                                <textarea class="form-control" style="min-height: 240px" name="contentreport"  id="contentReport" placeholder="Nhập nội dung ..." rows="3" required></textarea>
                                        </div>
                                        <button type="submit" class="form-button">Gửi</button>
                                </form>
                        </div>
                </div>
                <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>

        </body>
</html>



