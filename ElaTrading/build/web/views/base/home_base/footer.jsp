<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
        <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Document</title>
        </head>
        <body>
                <div class="row">
                        <div class="col-md-4">
                                <h5 class="fw-bold" style="font-size: 20px;">Liên hệ</h5>
                                <div style="font-size: 15px;">
                                        Liên hệ ngay nếu bạn có khó khăn khi sử dụng dịch vụ hoặc cần hợp tác.
                                </div>
                                <div class="py-2" style="font-size: 15px;">
                                        <div class="d-flex align-items-center py-1">
                                                <i class="fa-solid fa-paper-plane"></i>
                                                <div class="mx-1">Chat với hỗ trợ viên</div>
                                        </div>
                                        <div class="d-flex align-items-center py-1">
                                                <i class="fa-brands fa-facebook"></i>
                                                <div class="mx-1">ElaTrading</div>
                                        </div>
                                        <div class="d-flex align-items-center py-1">
                                                <i class="fa-regular fa-envelope"></i>
                                                <div class="mx-1">support@Elatrading.net</div>
                                        </div>
                                </div>
                        </div>
                        <div class="col-md-4">
                                <h5 class="fw-bold" style="font-size: 20px;">Thông tin</h5>
                                <div style="font-size: 15px;">
                                        Liên hệ ngay nếu bạn có khó khăn khi sử dụng dịch vụ hoặc cần hợp tác.
                                </div>
                                <div style="font-size: 15px;">
                                        Thanh toán tự động, nhận hàng ngay tức thì.
                                </div>
                                <div style="font-size: 15px;">
                                        Câu hỏi thường gặp
                                </div>
                                <div style="font-size: 15px;">
                                        Điều khoản sử dụng
                                </div>
                        </div>
                        <div class="col-md-4">
                                <div style="font-size: 15px;" class="fw-bold" >
                                        Theo dõi chúng tôi trên mạng xã hội
                                </div>
                                <div class="d-flex">
                                        <div class="mx-1" style="width: 30px; height: 30px">
                                                <img class="w-100 h-100" style="border-radius: 50%;" src="https://www.facebook.com/images/fb_icon_325x325.png"/>
                                        </div>
                                </div>
                        </div>
                </div>
        </body>
</html>