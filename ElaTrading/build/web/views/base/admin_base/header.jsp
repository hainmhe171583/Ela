<%-- 
    Document   : header
    Created on : Jan 24, 2024, 8:24:06 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
        <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="robots" content="NoIndex, NoFollow">
                <title>Bảng điều khiển</title>
                <link href="/ElaTrading/assets/css/notification.css" rel="stylesheet">
                <link href="/ElaTrading/assets/css/app.css" rel="stylesheet">
                <style type="text/css">

                        @font-face {
                                font-family: Roboto;
                                font-style: normal;
                                font-weight: 400;
                                src: url(/cf-fonts/s/roboto/5.0.11/greek/400/normal.woff2);
                                unicode-range: U+0370-03FF;
                                font-display: swap;
                        }

                        @font-face {
                                font-family: Roboto;
                                font-style: normal;
                                font-weight: 400;
                                src: url(/cf-fonts/s/roboto/5.0.11/cyrillic/400/normal.woff2);
                                unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
                                font-display: swap;
                        }

                        @font-face {
                                font-family: Roboto;
                                font-style: normal;
                                font-weight: 400;
                                src: url(/cf-fonts/s/roboto/5.0.11/cyrillic-ext/400/normal.woff2);
                                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
                                font-display: swap;
                        }

                        @font-face {
                                font-family: Roboto;
                                font-style: normal;
                                font-weight: 400;
                                src: url(/cf-fonts/s/roboto/5.0.11/latin-ext/400/normal.woff2);
                                unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
                                font-display: swap;
                        }

                        @font-face {
                                font-family: Roboto;
                                font-style: normal;
                                font-weight: 400;
                                src: url(/cf-fonts/s/roboto/5.0.11/latin/400/normal.woff2);
                                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
                                font-display: swap;
                        }

                        @font-face {
                                font-family: Roboto;
                                font-style: normal;
                                font-weight: 400;
                                src: url(/cf-fonts/s/roboto/5.0.11/vietnamese/400/normal.woff2);
                                unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
                                font-display: swap;
                        }

                        @font-face {
                                font-family: Roboto;
                                font-style: normal;
                                font-weight: 400;
                                src: url(/cf-fonts/s/roboto/5.0.11/greek-ext/400/normal.woff2);
                                unicode-range: U+1F00-1FFF;
                                font-display: swap;
                        }

                        @font-face {
                                font-family: Roboto;
                                font-style: normal;
                                font-weight: 300;
                                src: url(/cf-fonts/s/roboto/5.0.11/vietnamese/300/normal.woff2);
                                unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
                                font-display: swap;
                        }

                        @font-face {
                                font-family: Roboto;
                                font-style: normal;
                                font-weight: 300;
                                src: url(/cf-fonts/s/roboto/5.0.11/cyrillic/300/normal.woff2);
                                unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
                                font-display: swap;
                        }

                        @font-face {
                                font-family: Roboto;
                                font-style: normal;
                                font-weight: 300;
                                src: url(/cf-fonts/s/roboto/5.0.11/latin/300/normal.woff2);
                                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
                                font-display: swap;
                        }

                        @font-face {
                                font-family: Roboto;
                                font-style: normal;
                                font-weight: 300;
                                src: url(/cf-fonts/s/roboto/5.0.11/latin-ext/300/normal.woff2);
                                unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
                                font-display: swap;
                        }

                        @font-face {
                                font-family: Roboto;
                                font-style: normal;
                                font-weight: 300;
                                src: url(/cf-fonts/s/roboto/5.0.11/greek/300/normal.woff2);
                                unicode-range: U+0370-03FF;
                                font-display: swap;
                        }

                        @font-face {
                                font-family: Roboto;
                                font-style: normal;
                                font-weight: 300;
                                src: url(/cf-fonts/s/roboto/5.0.11/cyrillic-ext/300/normal.woff2);
                                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
                                font-display: swap;
                        }

                        @font-face {
                                font-family: Roboto;
                                font-style: normal;
                                font-weight: 300;
                                src: url(/cf-fonts/s/roboto/5.0.11/greek-ext/300/normal.woff2);
                                unicode-range: U+1F00-1FFF;
                                font-display: swap;
                        }
                </style>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"
                        integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g=="
                        crossorigin="anonymous" referrerpolicy="no-referrer">
                </script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css"
                      integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA=="
                      crossorigin="anonymous" referrerpolicy="no-referrer" />
                <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
                <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
                <link href="/ElaTrading/assets/css/action.css" rel="stylesheet">
        </head>
        <body>
                <nav class="navbar navbar-default" style="z-index: 999">
                        <div class="container-fluid">
                                <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                                <span class="sr-only">Toggle Navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                        </button>
                                        <a class="navbar-brand" href="/ElaTrading/action">Bảng điều khiển</a>
                                </div>
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                        <ul class="nav navbar-nav ">
                                                <li><a href="/ElaTrading" target="_blank"><i class="fas fa-home"></i><span class="hidden-md hidden-lg"> ElaTrading</span></a></li>
                                                <li><a href="/ElaTrading/admin/withdraws" style="color: red">Yêu cầu rút tiền</a></li>
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="color: #10b591">Quản Lý <span class="caret"></span></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                                <li><a href="/ElaTrading/admin/products">Quản lý đơn hàng</a></li>
                                                                <li><a href="/ElaTrading/admin/users">Quản lý người dùng</a></li>
                                                                <li><a href="/ElaTrading/admin/reports">Quản lý khiếu nại</a></li>
                                                        </ul>
                                                </li>
                                                <ul class="nav navbar-nav navbar-right">
                                                        <li class="dropdown">
                                                                <a href="/ElaTrading/logout">Thoát</a>
                                                        </li>
                                                </ul>
                                        </ul>
                                </div>
                        </div>                                
                </nav>
                <div id="toastY"></div>
                <script src="/ElaTrading/assets/script/notification.js"></script>
        </body>
</html>
