<%-- 
    Document   : product_list
    Created on : Jan 20, 2024, 11:27:23 AM
    Author     : Phong Vu
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>JSP Page</title>
        </head>
        <style>
                .active-product {
                        border-bottom: 2px solid #21BF73;
                }
        </style>
        <body>
                <%@include file="../base/home_base/header.jsp" %>
                <div class="container-fluid d-flex">
                        <div class="col-md-2">
                                <div class="w-100" style="border: 2px solid #f0f0f0;">
                                        <form action="products" method="get">
                                                <h4 class="fw-bold px-3 my-3" style="font-size: 20px">Bộ lọc</h4>
                                                <div class="w-100" style="height: 2px; background-color: #f0f0f0"></div>
                                                <div class="px-3 py-2">
                                                        <a class="text-decoration-none fw-bold text-black" data-bs-toggle="collapse" href="#filter" role="button" aria-expanded="false" aria-controls="filter">
                                                                Chọn 1 hoặc nhiều sản phẩm
                                                        </a>
                                                        <c:forEach items="${listCategory}" var="category">
                                                                <div class="d-flex align-items-center">
                                                                        <input type="checkbox" value="${category.id}" name="filterCategory"
                                                                               <c:if test="${listCategoryCheck.contains(category.id)}">
                                                                                       checked
                                                                               </c:if>
                                                                               />
                                                                        <label class="mx-2" style="font-size: 15px">${category.categoryName}</label>
                                                                </div>
                                                        </c:forEach>
                                                        <div class="collapse show" id="filter">
                                                                <div class="py-2" style="color: black;">
                                                                        <input type="submit" class="px-3 text-white mt-4" style="background-color: #67B740; border: none; outline: none; border-radius: 3px;"/>
                                                                </div>
                                                        </div>
                                                </div>
                                        </form>
                                </div>
                        </div>
                        <div class="col-md-10 px-4">
                                <div class="d-flex align-items-center">
                                        <h1 class="fw-bold" style="font-size: 28px;">Gian hàng</h1>
                                        <div class="mx-3">Tổng cộng ${listProduct.size()} gian hàng</div>
                                </div>
                                <div class="d-flex">
                                        <a  onclick="chooseActiveProduct(1, '')" class="text-decoration-none text-black" style="font-size: 1rem; font-weight: 500;" onclick="chooseActiveProduct(0)">
                                                <div class="py-2 px-3 type-product
                                                     <c:if test="${empty sortCheck}">
                                                             active-product
                                                     </c:if>
                                                     ">Phổ biến</div>
                                        </a>
                                        <a  class="text-decoration-none text-black" style="font-size: 1rem; font-weight: 500;" onclick="chooseActiveProduct(1, 'ASC')">
                                                <div class="py-2 px-3 type-product
                                                     <c:if test="${not empty sortCheck and sortCheck eq 'ASC'}">
                                                             active-product
                                                     </c:if>
                                                     ">Giá tăng dần
                                                </div>
                                        </a>

                                        <a  class="text-decoration-none text-black" style="font-size: 1rem; font-weight: 500;" onclick="chooseActiveProduct(2, 'DESC')">
                                                <div class="py-2 px-3 type-product
                                                     <c:if test="${not empty sortCheck and sortCheck eq 'DESC'}">
                                                             active-product
                                                     </c:if>
                                                     ">Giá giảm dần
                                                </div>
                                        </a>
                                </div>
                                <div class="px-2 py-3 mt-3" style="border: 1px solid #f0f0f0; color: #21BF73;">
                                        <i>
                                                Sản phẩm đã được chúng tôi kiểm tra kỹ lưỡng trước khi tới tay khách hàng!
                                        </i>
                                </div>
                                <div class="row">
                                        <c:forEach items="${listProduct}" var="p"> 
                                                <div class="d-flex mt-2 py-2 mx-2 px-2" style="border: 1px solid #f0f0f0; width: 48%;">
                                                        <div class="position-relative col-md-5" style="width: 180px;">
                                                                <c:if test="${p.getCategory().getId() == 1}">
                                                                        <div style="width: 150px; height: auto">
                                                                                <img src="/ElaTrading/assets/image/taikhoan.png" class="px-2 w-100 h-100 py-2"/>
                                                                        </div>

                                                                </c:if>
                                                                <c:if test="${p.getCategory().getId() == 2}">
                                                                        <div style="width: 150px; height: auto">
                                                                                <img src="/ElaTrading/assets/image/phanmem.png" class="px-2 w-100 py-2" style="width: 100px;"/>
                                                                        </div>

                                                                </c:if>
                                                                <c:if test="${p.getCategory().getId() == 3}">
                                                                        <div style="width: 150px; height: auto">
                                                                                <img src="/ElaTrading/assets/image/more.png" class="px-2 w-100 py-2" style="width: 100px;"/>
                                                                        </div>
                                                                </c:if>
                                                        </div>
                                                        <div class="px-3">
                                                                <div class="d-flex">
                                                                        <div class="text-white px-1" style="background-color: #21BF73; line-height: 20px; font-size: 11px; border-radius: 2px; width: 80px; height: 25px;">
                                                                                Sản phẩm
                                                                        </div>
                                                                        <a class="fw-bold mx-2" href="/ElaTrading/productdetail?code=${p.code}" style="text-decoration: none; color: black;">
                                                                                ${p.getProductName()}
                                                                        </a>
                                                                </div>
                                                                <div class="d-flex">
                                                                        <div>Giá: </div>
                                                                        <div style="color: #21BF73;" class="mx-1">${p.getPrice()}</div>
                                                                </div>
                                                                <div class="d-flex">
                                                                        <div>Người bán: </div>
                                                                        <div style="color: #21BF73;" class="mx-1">${p.getUser().getUsername()}</div>
                                                                </div>
                                                                <div class="d-flex">
                                                                        <div>Sản phẩm: </div>
                                                                        <div style="color: #21BF73;" class="mx-1">${p.getCategory().getCategoryName()}</div>
                                                                </div>
                                                        </div>
                                                </div>
                                        </c:forEach>
                                </div>
                        </div>

                </div>
                <script>
                        function chooseActiveProduct(index) {
                                var typeProduct = document.getElementsByClassName("type-product");
                                for (const x of typeProduct) {
                                        x.classList.remove("active-product");
                                }
                                typeProduct[index].classList.add("active-product");
                        }

                        function onLoadData() {

                        }
                </script>
        </body>
        <script>
                function chooseActiveProduct(productId, sort) {
                        // Get the current URL
                        var currentUrl = window.location.href;

                        // Check if the sort parameter already exists in the URL
                        var sortIndex = currentUrl.indexOf('sort=');

                        if (sortIndex !== -1) {
                                // If the sort parameter exists, replace its value
                                var endIndex = currentUrl.indexOf('&', sortIndex);
                                if (endIndex === -1) {
                                        // If there are no additional parameters after sort, replace until the end of the URL
                                        currentUrl = currentUrl.substring(0, sortIndex) + 'sort=' + sort;
                                } else {
                                        // If there are additional parameters after sort, replace until the next '&'
                                        currentUrl = currentUrl.substring(0, sortIndex) + 'sort=' + sort + currentUrl.substring(endIndex);
                                }
                        } else {
                                // If the sort parameter doesn't exist, append it to the URL
                                currentUrl += (currentUrl.includes('?') ? '&' : '?') + 'sort=' + sort;
                        }

                        window.location.href = currentUrl;
                }
        </script>
</html>
