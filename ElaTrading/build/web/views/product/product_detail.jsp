
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="vi" class="light">
        <head>
                <meta charset="utf-8">
                <title></title>
                <link rel="stylesheet" href="/ElaTrading/assets/css/tailwind.css">
                <link rel="stylesheet" href="/ElaTrading/assets/css/interface.css">
                <script src="/ElaTrading/assets/script/plugins.js"></script>
                <style>
                        .modal-content {
                                background-color: #fefefe;
                                padding: 20px;
                                border: 1px solid #888;
                                width: 100%;
                        }
                        .close {
                                color: #aaa;
                                float: right;
                                font-size: 28px;
                                font-weight: bold;
                        }

                        .close:hover,
                        .close:focus {
                                color: black;
                                text-decoration: none;
                                cursor: pointer;
                        }
                </style>
        </head>
        <body>
                <c:set var="u" value="${user}"/>
                <c:set value="${requestScope.product}" var="p"/>
                <jsp:include page="/views/base/home_base/header.jsp"/>
                <main id="mainpart" class="project-page">
                        <div class="container">
                                <div class="row d-block clearfix">
                                        <div class="col-12 col-lg-12 float-left">
                                                <section class="feature-section at-series clear">
                                                        <main class="section-body">
                                                                <div class="top-part">
                                                                        <div class="row">
                                                                                <div class="left-column col-12 col-md-4" style="height: 300px">
                                                                                        <div class="series-cover">
                                                                                                <div class="a6-ratio">
                                                                                                        <div class="content img-in-ratio">
                                                                                                                <img style="width: 70%; height: auto; border: solid 1px black; border-radius: 50%; box-shadow: #83cf86de;" class="product-image-feature" src="${p.category.describe}" style="border-radius: 30px" alt="${p.productName}">
                                                                                                        </div>
                                                                                                </div>
                                                                                        </div>
                                                                                </div>
                                                                                <div class="col-12 col-md-8 flex flex-col" style="height: 300px;">
                                                                                        <div class="flex-1">
                                                                                                <div class="series-name-group">
                                                                                                        <span class="series-name">
                                                                                                                ${p.productName}
                                                                                                        </span>
                                                                                                </div>
                                                                                                <div class="series-information mb-0">
                                                                                                        <div class="info-item">
                                                                                                                <span class="info-name">Người bán:</span>
                                                                                                                <span class="info-value ">${p.user.username}</span>
                                                                                                        </div>
                                                                                                        <div class="info-item">
                                                                                                                <span class="info-name">Liên Hệ:</span>
                                                                                                                <span class="info-value">
                                                                                                                        <a href="${p.contact}" target="_blank">${p.contact}</a>
                                                                                                                </span>
                                                                                                        </div>
                                                                                                        <div class="info-item">
                                                                                                                <span class="info-name">Giá:</span>
                                                                                                                <span class="info-value">
                                                                                                                        ${p.price}
                                                                                                                </span>
                                                                                                        </div>
                                                                                                        <div class="flex-X flex-row-X self-center-X md:self-start-X gap-2-X">
                                                                                                                <c:if test="${user == null}">
                                                                                                                        <c:if test="${p.statusID != 1}">
                                                                                                                                <div wire:snapshot="{&quot;data&quot;:{&quot;series&quot;:[null,{&quot;class&quot;:&quot;App\\Models\\Series&quot;,&quot;key&quot;:17144,&quot;s&quot;:&quot;mdl&quot;}],&quot;ignored&quot;:false},&quot;memo&quot;:{&quot;id&quot;:&quot;yAMnmd7i3FcLKxC6fJpN&quot;,&quot;name&quot;:&quot;pub.series.member.notification-ignore-button&quot;,&quot;path&quot;:&quot;truyen\/17144-apparently-my-childhood-friends-older-sister-who-is-called-a-gal-nowadays-is-a-yandere-who-pathologically-loves-me-a-degraded-version-of-my-twin-brother-who-can-do-everything-perfectly&quot;,&quot;method&quot;:&quot;GET&quot;,&quot;children&quot;:[],&quot;scripts&quot;:[],&quot;assets&quot;:[],&quot;errors&quot;:[],&quot;locale&quot;:&quot;vi&quot;},&quot;checksum&quot;:&quot;aa7f834d8af8e96d8fc71e971f392cc50e65636e96b775adbb640444f36dd887&quot;}"
                                                                                                                                     wire:effects="[]" wire:id="yAMnmd7i3FcLKxC6fJpN">
                                                                                                                                        <p style="color: red">Sản phẩm đã không còn khả dụng</p>
                                                                                                                                </div>
                                                                                                                        </c:if>
                                                                                                                        <c:if test="${p.statusID == 1}">
                                                                                                                                <div wire:snapshot="{&quot;data&quot;:{&quot;series&quot;:[null,{&quot;class&quot;:&quot;App\\Models\\Series&quot;,&quot;key&quot;:17144,&quot;s&quot;:&quot;mdl&quot;}],&quot;ignored&quot;:false},&quot;memo&quot;:{&quot;id&quot;:&quot;yAMnmd7i3FcLKxC6fJpN&quot;,&quot;name&quot;:&quot;pub.series.member.notification-ignore-button&quot;,&quot;path&quot;:&quot;truyen\/17144-apparently-my-childhood-friends-older-sister-who-is-called-a-gal-nowadays-is-a-yandere-who-pathologically-loves-me-a-degraded-version-of-my-twin-brother-who-can-do-everything-perfectly&quot;,&quot;method&quot;:&quot;GET&quot;,&quot;children&quot;:[],&quot;scripts&quot;:[],&quot;assets&quot;:[],&quot;errors&quot;:[],&quot;locale&quot;:&quot;vi&quot;},&quot;checksum&quot;:&quot;aa7f834d8af8e96d8fc71e971f392cc50e65636e96b775adbb640444f36dd887&quot;}"
                                                                                                                                     wire:effects="[]" wire:id="yAMnmd7i3FcLKxC6fJpN">
                                                                                                                                        <div class="row">
                                                                                                                                                <p style="color: #55cb57">Bạn cần đăng nhập để mua sản phẩm</p>
                                                                                                                                                <a href="/ElaTrading/login" style="width: 145px"> 
                                                                                                                                                        <button style="margin-left: 12px"
                                                                                                                                                                class="self-center-X md:self-start-X rounded-full-X bg-amber-600-X mt-3-X mb-3-X px-4-X py-2-X text-sm-X font-semibold-X text-white-X  shadow-sm-X hover:bg-amber-800-X focus-visible:outline-X focus-visible:outline-2-X focus-visible:outline-offset-2-X focus-visible:outline-[#d43f3a]-X justify-center-X"
                                                                                                                                                                wire:click="handle">
                                                                                                                                                                Đăng nhập
                                                                                                                                                        </button>                                                                    
                                                                                                                                                </a>
                                                                                                                                        </div>
                                                                                                                                </div>
                                                                                                                        </c:if>
                                                                                                                </c:if>
                                                                                                                <c:if test="${user != null}">
                                                                                                                        <c:if test="${p.statusID == 1}">
                                                                                                                                <div wire:snapshot="{&quot;data&quot;:{&quot;series&quot;:[null,{&quot;class&quot;:&quot;App\\Models\\Series&quot;,&quot;key&quot;:17144,&quot;s&quot;:&quot;mdl&quot;}],&quot;ignored&quot;:false},&quot;memo&quot;:{&quot;id&quot;:&quot;yAMnmd7i3FcLKxC6fJpN&quot;,&quot;name&quot;:&quot;pub.series.member.notification-ignore-button&quot;,&quot;path&quot;:&quot;truyen\/17144-apparently-my-childhood-friends-older-sister-who-is-called-a-gal-nowadays-is-a-yandere-who-pathologically-loves-me-a-degraded-version-of-my-twin-brother-who-can-do-everything-perfectly&quot;,&quot;method&quot;:&quot;GET&quot;,&quot;children&quot;:[],&quot;scripts&quot;:[],&quot;assets&quot;:[],&quot;errors&quot;:[],&quot;locale&quot;:&quot;vi&quot;},&quot;checksum&quot;:&quot;aa7f834d8af8e96d8fc71e971f392cc50e65636e96b775adbb640444f36dd887&quot;}"
                                                                                                                                     wire:effects="[]" wire:id="yAMnmd7i3FcLKxC6fJpN">
                                                                                                                                        <div class="row">
                                                                                                                                                <button id="buyNowButton" style="margin-left: 12px"
                                                                                                                                                        class="self-center md:self-start rounded-full bg-amber-600 mt-3 mb-3 px-4 py-2 text-sm font-semibold text-white  shadow-sm hover:bg-amber-800 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#d43f3a] justify-center"
                                                                                                                                                        wire:click="handle">
                                                                                                                                                        Mua ngay
                                                                                                                                                </button>
                                                                                                                                        </div>
                                                                                                                                </div>
                                                                                                                        </c:if>
                                                                                                                        <c:if test="${p.statusID != 1}">
                                                                                                                                <div wire:snapshot="{&quot;data&quot;:{&quot;series&quot;:[null,{&quot;class&quot;:&quot;App\\Models\\Series&quot;,&quot;key&quot;:17144,&quot;s&quot;:&quot;mdl&quot;}],&quot;ignored&quot;:false},&quot;memo&quot;:{&quot;id&quot;:&quot;yAMnmd7i3FcLKxC6fJpN&quot;,&quot;name&quot;:&quot;pub.series.member.notification-ignore-button&quot;,&quot;path&quot;:&quot;truyen\/17144-apparently-my-childhood-friends-older-sister-who-is-called-a-gal-nowadays-is-a-yandere-who-pathologically-loves-me-a-degraded-version-of-my-twin-brother-who-can-do-everything-perfectly&quot;,&quot;method&quot;:&quot;GET&quot;,&quot;children&quot;:[],&quot;scripts&quot;:[],&quot;assets&quot;:[],&quot;errors&quot;:[],&quot;locale&quot;:&quot;vi&quot;},&quot;checksum&quot;:&quot;aa7f834d8af8e96d8fc71e971f392cc50e65636e96b775adbb640444f36dd887&quot;}"
                                                                                                                                     wire:effects="[]" wire:id="yAMnmd7i3FcLKxC6fJpN">
                                                                                                                                        <p style="color: red">Sản phẩm đã không còn khả dụng</p>
                                                                                                                                </div>
                                                                                                                        </c:if>
                                                                                                                </c:if>
                                                                                                                <div id="confirmationModal" class="modal" style="display:none;">
                                                                                                                        <div class="modal-content">
                                                                                                                                <form id="trading" role="form" method="POST" action="action/trading/create">
                                                                                                                                        <span class="close" style="text-align: right">&times;</span>
                                                                                                                                        <input type="hidden" value="${p.code}" readonly="true" name="proCode" id="proCode">
                                                                                                                                        <input type="hidden" value="${codetrading}" readonly="true" name="code" id="code">
                                                                                                                                        <p>Xác nhận đơn hàng</p>
                                                                                                                                        <p style="color: #55cb57">Vui lòng xác nhận thông tin sau</p>
                                                                                                                                        <p>Mặt hàng:</p>
                                                                                                                                        <div style="background-color: #ffcc00; border-radius: 5px; width: 80%; font-size: 30px; padding: 5px">${p.productName}</div>

                                                                                                                                        <p>Người bán: ${p.user.username}</p>
                                                                                                                                        <p>Tổng tiền: ${p.price} VND</p>
                                                                                                                                        <div><p style="font-size: 30px">Tổng thanh toán: ${p.price} VND</p></div>
                                                                                                                                        <button style="background-color:#55cb57; color: white; border: solid 1px black; display: block; margin: auto; padding: 10px; border-radius: 5px" type="submit" >
                                                                                                                                                Mua hàng
                                                                                                                                        </button>
                                                                                                                                </form>
                                                                                                                        </div>
                                                                                                                </div>
                                                                                                                <script>
                                                                                                                        var modal = document.getElementById('confirmationModal');
                                                                                                                        var btn = document.getElementById("buyNowButton");
                                                                                                                        var span = document.getElementsByClassName("close")[0];
                                                                                                                        btn.onclick = function () {
                                                                                                                                modal.style.display = "block";
                                                                                                                        };
                                                                                                                        span.onclick = function () {
                                                                                                                                modal.style.display = "none";
                                                                                                                        };
                                                                                                                        window.onclick = function (event) {
                                                                                                                                if (event.target === modal) {
                                                                                                                                        modal.style.display = "none";
                                                                                                                                }
                                                                                                                        };
                                                                                                                </script>
                                                                                                        </div>
                                                                                                </div>
                                                                                        </div>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="bottom-part">
                                                                        <div class="row">
                                                                                <div class="summary-wrapper col-12">
                                                                                        <div class="series-summary">
                                                                                                <h4 class="font-bold">Mô tả sản phẩm</h4>
                                                                                                <div class="summary-content">
                                                                                                        <p>${p.describe}</p>
                                                                                                </div>
                                                                                                <div class="summary-more none more-state">
                                                                                                        <div class="see_more">Xem thêm</div>
                                                                                                </div>
                                                                                        </div>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </main>
                                                </section>
                                        </div>
                                </div>
                        </div>
                </main>
                <script>
                        $("#trading").on("submit", function (e)
                        {
                                e.preventDefault();
                                const data = {
                                        proCode: $("#proCode").val(),
                                        code: $("#code").val()
                                };
                                $.ajax({
                                        method: "POST",
                                        url: "/ElaTrading/action/trading/create",
                                        data: data
                                }).done(function (data)
                                {
                                        console.log(data);
                                        if (!data.success)
                                        {
                                                error_Notification(data.message);
                                        } else {
                                                const path = $("#code").val();
                                                window.location.href = "/ElaTrading/action/trading?code=" + path;
                                        }
                                });
                        });
                </script>
        </body>
</html>


