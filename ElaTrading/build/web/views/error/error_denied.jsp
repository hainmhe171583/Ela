<%-- 
    Document   : error_denied
    Created on : Jan 24, 2024, 2:20:57 AM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="vi" class="light">
        <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <title>ElaTrading-Cân mọi giao dịch số</title>
                <link rel="stylesheet" href="/ElaTrading/assets/css/interface.css"/>
                <link rel="stylesheet" href="/ElaTrading/assets/css/tailwindX.css"/>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css" integrity="sha256-BtbhCIbtfeVWGsqxk1vOHEYXS6qcvQvLMZqjtpWUEx8=" crossorigin="anonymous" />
                <script src="/ElaTrading/assets/script/plugins.js"></script>
        </head>
        <body>
                <style>
                        #footer {
                                display: none
                        }
                </style>
                <main id="mainpart" class="error-page">
                        <div class="error-black"></div>
                        <div class="error-center text-center">
                                <div class="error-name">404</div>
                                <div class="error-note">
                                        <c:choose>
                                                <c:when test="${error != \"1\"}">
                                                        ${error}
                                                </c:when>
                                                <c:when test="${error == \"1\"}">
                                                        Bạn thấy gì không?
                                                        </br>
                                                        Đây chính là màn hình đó!
                                                </c:when>
                                                <c:otherwise>
                                                        Bạn thấy gì không?
                                                        </br>
                                                        Đây chính là màn hình đó!
                                                </c:otherwise>
                                        </c:choose>
                                </div>
                                <a href="/ElaTrading" class="button error-button button-red">Về Trang Chủ</a>
                        </div>
                </main>
                <script src="/ElaTrading/assets/script/app.js"></script>
        </body>
</html>

