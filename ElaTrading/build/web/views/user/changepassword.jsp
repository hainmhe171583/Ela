<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
               <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>Bảng điều khiển</title>
        </head>
        <body>
                <jsp:include page="../base/system_base/header.jsp"/>
                <div class="container">
                        <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                        <div class="panel panel-default">
                                                <div class="panel-heading">
                                                        Đổi mật khẩu
                                                </div>
                                                <div class="panel-body">
                                                        <form action="/ElaTrading/action/changepassword" method="post">
                                                                <p class="text-success">${mess}</p>
                                                                <div class="form-group clearfix required">
                                                                        <label class="col-md-2 control-label pt-7 text-right">Tài khoản</label>
                                                                        <div class="col-md-8">

                                                                                <input name="username" type="text" id="username" class="form-control" value="${user.username}" readonly="" autofocus="">
                                                                        </div>  
                                                                </div>
                                                                <div class="form-group clearfix required">
                                                                        <label class="col-md-2 control-label pt-7 text-right">Mật khẩu cũ</label>
                                                                        <div class="col-md-8">
                                                                                <input name="oldpassword" type="password" id="oldpassword" class="form-control" required="">

                                                                                <div class="input-group-append">
                                                                                        <span class="input-group-text">
                                                                                                <input type="checkbox" id="showOldPassword"> Hiển thị
                                                                                        </span>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix required">
                                                                        <label class="col-md-2 control-label pt-7 text-right">Mật khẩu mới</label>
                                                                        <div class="col-md-8">
                                                                                <input name="newpassword" type="password" id="newpassword" class="form-control" required="">
                                                                                <div class="input-group-append">
                                                                                        <span class="input-group-text">
                                                                                                <input type="checkbox" id="showNewPassword"> Hiển thị
                                                                                        </span>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix required">
                                                                        <label class="col-md-2 control-label pt-7 text-right">Xác nhận mật khẩu</label>
                                                                        <div class="col-md-8">
                                                                                <input name="confirmpassword" type="password" id="confirmpassword" class="form-control" required="">
                                                                                <div class="input-group-append">
                                                                                        <span class="input-group-text">
                                                                                                <input type="checkbox" id="showConfirmPassword"> Hiển thị
                                                                                        </span>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                        <div class="col-md-10 col-md-offset-2">
                                                                                <button type="submit" class="btn btn-primary">
                                                                                        Đổi mật khẩu
                                                                                </button>
                                                                        </div>
                                                                </div>
                                                        </form>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
                <!-- Bootstrap JS and jQuery -->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
                        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
                crossorigin="anonymous"></script>
                <script>
                        $(document).ready(function () {
                                function togglePassword(inputId) {
                                        var passwordInput = $("#" + inputId);
                                        var type = passwordInput.attr("type");
                                        if (type === "password") {
                                                passwordInput.attr("type", "text");
                                        } else {
                                                passwordInput.attr("type", "password");
                                        }
                                }

                                $("#showOldPassword").change(function () {
                                        togglePassword("oldpassword");
                                });

                                $("#showNewPassword").change(function () {
                                        togglePassword("newpassword");
                                });

                                $("#showConfirmPassword").change(function () {
                                        togglePassword("confirmpassword");
                                });
                        });
                </script>
                <jsp:include page="../base/system_base/footer.jsp"/>
        </body>
</html>
