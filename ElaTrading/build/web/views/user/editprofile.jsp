<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>Bảng điều khiển</title>
        </head>
        <body>
                <jsp:include page="../base/system_base/header.jsp"/>
                <div class="container">
                        <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                        <div class="panel panel-default">
                                                <div class="panel-heading">
                                                        Hồ sơ
                                                </div>
                                                <div class="panel-body">
                                                        <form action="/ElaTrading/action/editprofile" method="post">
                                                                <p class="text-success">${mess}</p>
                                                                <div class="form-group clearfix">
                                                                        <label class="col-md-2 control-label pt-7 text-right">Tài khoản</label>
                                                                        <div class="col-md-8">
                                                                                <input name="username" type="text" id="username" class="form-control" value="${user.username}" readonly="" autofocus>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <label class="col-md-2 control-label pt-7 text-right">Email</label>
                                                                        <div class="col-md-8">
                                                                                <input name="email" type="text" id="email" class="form-control" value="${user.email}" readonly="" autofocus>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group clearfix">
                                                                        <label class="col-md-2 control-label pt-7 text-right">Số điện thoại</label>
                                                                        <div class="col-md-8">
                                                                                <input name="phone" type="text" id="phone" class="form-control" value="${user.phone}" required autofocus>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                        <div class="col-md-10 col-md-offset-2">
                                                                                <button type="submit" class="btn btn-primary">
                                                                                        Cập nhật
                                                                                </button>
                                                                        </div>
                                                                </div>
                                                        </form>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
                <jsp:include page="../base/system_base/footer.jsp"/>

</html>
