<%-- 
    Document   : profile
    Created on : Jan 18, 2024, 1:09:25 AM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>Profile</title>

        </head>
        <body style="background-color: white">
                <jsp:include page="../base/home_base/header.jsp"/>
                <div class="container rounded bg-white mt-5 mb-5">
                        <div class="row">

                                <div class="col-md-3 border-right">
                                        <div class="d-flex flex-column align-items-center text-center p-3 py-5"><img class="rounded-circle mt-5" width="150px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
                                                <span class="font-weight-bold"></span><span class="text-black-50"></span><span> </span></div>
                                </div>
                                <div class="col-md-5 border-right">
                                        <div class="p-3 py-5">

                                                <div class="d-flex justify-content-between align-items-center mb-3">
                                                        <h4 class="text-right">Profile Settings</h4>
                                                </div>
                                                <c:if test="${user.actived == false}">
                                                        <div >
                                                                <h3 style="font-size: 10px; color: red;text-align: center">Tài khoản chưa được kích hoạt, vui lòng kích hoạt tài khoản ở gmail để sửa dụng đầy đủ dịch vụ!</h3>
                                                        </div>
                                                </c:if>
                                                <form class="row mt-2" action="profile" method="post">

                                                        <div class="row mt-2">
                                                                <div class="col-md-6"><label class="labels" for="username">Tài khoản</label><input type="text" class="form-control"  value="${user.username}" readonly="" ></div>
                                                                <div class="col-md-6"><label class="labels" for="email">Email</label><input type="text" class="form-control" value="${user.email}" readonly=""></div>
                                                                <div class="col-md-6"><label class="labels" for="phone">Số điện thoại</label><input type="text" class="form-control" value="${user.phone}" readonly=""></div>
                                                                <div class="col-md-6"><label class="labels" for="balance">Số dư</label><input type="text" class="form-control"  value="${user.balance}" readonly=""></div>
                                                        </div>
                                                        <div class="row mt-3">

                                                                <div class="col-md-12"><label class="labels" for="created_at">Ngày đăng ký</label><input type="text" class="form-control" value="${user.created_at}" readonly=""></div>
                                                                <div class="col-md-12"><label class="labels" for="updated_at">Ngày Cập Nhật</label><input type="text" class="form-control" value="${user.updated_at}" readonly=""></div>
                                                                <div class="col-md-12 ">
                                                                        <div class="col-md-12">
                                                                                <label for="sosanpham">Đơn Hàng Của tôi</label>
                                                                                <div class="input-group">
                                                                                        <input type="text" class="form-control" name="sosanpham" value="${requestScope.countProductListSeller}" readonly="">
                                                                                        <div class="input-group-append">
                                                                                                <a href="/ElaTrading/products?username=${user.username}" class="btn btn-outline-secondary" type="button" id="button-addon2">
                                                                                                        <i class="fas fa-search"></i> 
                                                                                                </a>
                                                                                        </div>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                </form>
                                        </div>
                                        <p style="text-align: center; color: green; font-size: 12px">${mess}</p>
                                </div>
                        </div>
                </div>
        </body>
</html>
