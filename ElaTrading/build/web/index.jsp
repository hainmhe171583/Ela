<%-- 
    Document   : Home
    Created on : Jan 21, 2024, 1:19:21 PM
    Author     : Phong Vu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>JSP Page</title>
                <style>
                        .banner {
                                background-image: url('https://taphoammo.net/images/logo/finder.jpg');
                                background-repeat: no-repeat;
                                height: 300px;
                        }

                        mark {
                                background-color:pink;
                        }


                        .list-autocomplete {
                                padding:0;
                        }
                        .list-autocomplete em {
                                font-style:normal;
                                background-color:#e1f2f9;
                        }

                        .hasNoResults {
                                color:#aaa;
                        }
                        .hasNoResults,
                        .btn-extra {
                                display:block;
                                padding:10px;
                        }

                        .hasNoResults {
                                color:#aaa;
                        }

                        .btn-extra {
                                width:100%;
                                border-top:.5px solid #d2d2d2;
                        }
                </style>
        </head>
        <body>
                <%@include file="views/base/home_base/header.jsp" %>
                <div class="container">
                        <div class="col-12 position-relative">
                                <img  src="https://taphoammo.net/images/logo/finder.jpg" class="w-100" style="z-index: 0;"/>
                                <div class="col-12 d-flex justify-content-center position-absolute top-0" style="z-index: 50;">
                                        <div class="col-md-10">
                                                <form action="products" method="get">
                                                        <div class="row mt-5">
                                                                <div class="col-md-4 px-2">
                                                                        <select class="form-select" name="searchByName">
                                                                                <option value="username">Tìm kiếm theo tên người dùng</option>
                                                                                <option value="productName">Tìm kiếm theo tên sản phẩm</option>
                                                                        </select>
                                                                </div>
                                                        </div>
                                                        <div class="mt-3 row px-2">
                                                                <input type="text" name="searchContent" class="form-control" placeholder="Tìm gian hàng theo người bán"/>
                                                        </div>

                                                        <div class="d-flex justify-content-center mt-5">
                                                                <input type="submit" class="btn btn-success px-2 col-12" name="searchAction" value="Tìm kiếm"/>
                                                        </div>
                                                </form>
                                        </div>
                                </div>
                        </div>
                        <div class="my-4">
                                <div class="text-uppercase fw-bold text-center" style="color: #21BF73; font-size: 20px">-- Danh sách sản phẩm --</div>
                        </div>
                        <div class="row mb-5">
                                <div class="col-md-4 px-2 h-100" >
                                        <a href="products?catgory=1" style="text-decoration: none; color: inherit;">
                                                <div class="py-4 h-100" style="border: 1px solid #21BF73; border-radius: 3px; height: 245px !important;  background-color: #f7f7f7;">
                                                        <div class="d-flex justify-content-center">
                                                                <img src="/ElaTrading/assets/image/taikhoan.png" style="width: 100px; height: 100px"/>
                                                        </div>
                                                        <div class="text-center fw-bold my-2" style="color: #47991F; font-size: 20px;">
                                                                Tài khoản
                                                        </div>
                                                        <div class="text-center">
                                                                Gmail, Facebook, Youtube, Game và hơn thế nữa!
                                                        </div>
                                                </div>
                                        </a>
                                </div>
                                <div class="col-md-4 px-2 h-100" >
                                        <a href="products?catgory=2" style="text-decoration: none; color: inherit;">
                                                <div class="py-4 h-100" style="border: 1px solid #21BF73; border-radius: 3px; height: 245px;  background-color: #f7f7f7;">
                                                        <div class="d-flex justify-content-center">
                                                                <img src="/ElaTrading/assets/image/phanmem.png" style="width: 100px; height: 100px !important;"/>
                                                        </div>
                                                        <div class="text-center fw-bold my-2" style="color: #47991F; font-size: 20px;">
                                                                Phần mềm
                                                        </div>
                                                        <div class="text-center">
                                                                Các phần mềm chuyên dụng cho kiếm tiền online từ những coder uy tín!
                                                        </div>
                                                </div>
                                        </a>
                                </div>
                                <div class="col-md-4 px-2 h-100" >
                                        <a href="products?catgory=3" style="text-decoration: none; color: inherit;">
                                                <div class="py-4 h-100" style="border: 1px solid #21BF73; border-radius: 3px; height: 245px !important;  background-color: #f7f7f7;">
                                                        <div class="d-flex justify-content-center">
                                                                <img src="/ElaTrading/assets/image/more.png" style="width: 100px; height: 100px"/>
                                                        </div>
                                                        <div class="text-center fw-bold my-2" style="color: #47991F; font-size: 20px;">
                                                                Khác
                                                        </div>
                                                        <div class="text-center">
                                                                Các sản phẩm số khác!
                                                        </div>
                                                </div>
                                        </a>
                                </div>
                        </div>
                        <div class="py-1 px-1 mb-5 position-relative" style="border: 1px solid #21BF73; border-radius: 3px; background-color: #f7f7f7;" id="expand-text">
                                <div style="overflow: hidden;">
                                        <div class="fw-bold text-center" style="font-size: 15px;">Tạp hóa MMO - Chuyên trang thương mại điện tử sản phẩm số - Phục vụ cộng đồng MMO (Kiếm tiền online)</div>
                                        <p class="w-100 text-center" style="font-size: 13px;">MO - Chuyên trang thương mại điện tử sản phẩm số - Phục vụ cộng đồng MMO (Kiếm tiền online) Một sản phẩm ra đời với mục đích thuận tiện và an toàn hơn trong các giao dịch mua bán sản phẩm số.</p>
                                        <p class="w-100 text-start" style="font-size: 13px;">
                                                Như các bạn đã biết, tình trạng lừ.a đảo trên mạng xã hội kéo dài bao nhiêu năm nay, mặc dù đã có rất nhiều giải pháp từ cộng đồng như là trung gian hay bảo hiểm, nhưng vẫn rất nhiều người dùng lựa chọn mua bán nhanh gọn mà bỏ qua các bước kiểm tra, hay trung gian, từ đó tạo cơ hội cho s.c.a.m.m.e.r hoạt động. Ở Taphoammo, bạn sẽ có 1 trải nghiệm mua hàng yên tâm hơn rất nhiều, chúng tôi sẽ giữ tiền người bán 3 ngày, kiểm tra toàn bộ sản phẩm bán ra có trùng với người khác hay không, nhắm mục đích tạo ra một nơi giao dịch mà người dùng có thể tin tưởng, một trang mà người bán có thể yên tâm đặt kho hàng, và cạnh tranh sòng phẳng.
                                        </p>
                                        <div class="fw-bold text-center" style="font-size: 15px;">Các tính năng trên trang:</div>
                                        <ul>
                                                <li style="font-size: 13px;">Check trùng sản phẩm bán ra: toàn bộ gian hàng cam kết không bán trùng, hệ thống của chúng tôi sẽ kiểm tra từng sản phẩm một, để đảm bảo hàng đến tay người dùng chưa từng được bán cho ai khác trên trang, và hàng bạn đã mua, cũng không thể bán cho ai khác nữa.</li>
                                                <li style="font-size: 13px;">Nạp tiền và thanh toán tự động: Bạn chỉ cần nạp tiền đúng cú pháp, số dư của bạn sẽ đc cập nhật sau 1-5 phút. Mọi bước thanh toán và giao hàng đều được thực hiện ngay tức thì.</li>
                                                <li style="font-size: 13px;">Giữ tiền đơn hàng 3 ngày: Sau khi bạn mua hàng, đơn hàng đó sẽ ở trạng thái Tạm giữ tiền 3 ngày, đủ thời gian để bạn kiểm tra, đổi pass sản phẩm. Nếu có vấn đề gì, hãy nhanh chóng dùng tính năng "Khiếu nại" nhé.</li>
                                                <li style="font-size: 13px;">Tính năng dành cho cộng tác viên (Reseller): Các bạn đọc thêm ở mục "FAQs - Câu hỏi thường gặp" nhé.</li>
                                        </ul>
                                        <div class="fw-bold text-center" style="font-size: 15px;">Các tính năng trên trang:</div>
                                        <ul>
                                                <li style="font-size: 13px;">Mua bán email: Mua bán gmail, mail outlook, domain... tất cả đều có thể được tự do mua bán trên trang.</li>
                                                <li style="font-size: 13px;">Mua bán phần mềm MMO: các phần mềm phục vụ cho kiếm tiền online, như phần mềm youtube, phần mềm chạy facebook, phần mềm PTC, PTU, phần mềm gmail....</li>
                                                <li style="font-size: 13px;">Mua bán tài khoản: mua bán facebook, mua bán twitter, mua bán zalo, mua bán instagram.</li>
                                                <li style="font-size: 13px;">Các sản phẩm số khác: VPS, key window, key diệt virus, tất cả sản phẩm số không vi phạm chính sách của chúng tôi đều được phép kinh doanh trên trang.</li>
                                                <li style="font-size: 13px;">Các dịch vụ tăng tương tác (like, comment, share...), dịch vụ phần mềm, blockchain và các dịch vụ số khác.</li>
                                        </ul>
                                </div>
                        </div>
                        <%@include file="views/base/home_base/footer.jsp" %>
                </div>
        </body>
</html>
