/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.status;

import java.sql.Timestamp;
import model.BaseModel;

/**
 *
 * @author admin
 */
public class Status extends BaseModel
{
        private String statusName;

        public Status()
        {
        }

        public Status(String statusName, int id, Timestamp created_at, int created_by, Timestamp updated_at, int updated_by, Timestamp deleted_at, int deleted_by, boolean isDeleted)
        {
                super(id, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by, isDeleted);
                this.statusName = statusName;
        }

        public String getStatusName()
        {
                return statusName;
        }

        public void setStatusName(String statusName)
        {
                this.statusName = statusName;
        }
        
}
