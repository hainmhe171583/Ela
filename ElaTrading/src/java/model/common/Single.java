/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.common;

import model.payment.Pay;
import model.product.Product;
import model.trading.Trading;
import model.withdraw.Withdraw;

/**
 *
 * @author hai20
 */
public class Single
{

        private int userId;
        private double amount;
        private Product product;
        private Trading trading;
        private Pay payment;
        private Withdraw withdraw;
        private int type;

        //type=1 nap tien
        //type=2 Phi GD(when add product)
        //type=3 mua hang
        //type=4 ban hang
        //type=5 rut tien
        public Single(int userId, double amount, int type)
        {
                this.userId = userId;
                this.amount = amount;
                this.type = type;
        }

        public Single(int userId, double amount, Withdraw withdraw, int type)
        {
                this.userId = userId;
                this.amount = amount;
                this.withdraw = withdraw;
                this.type = type;
        }

        public Single(int userId, double amount, Trading trading, int type)
        {
                this.userId = userId;
                this.amount = amount;
                this.trading = trading;
                this.type = type;
        }

        public int getType()
        {
                return type;
        }

        public void setType(int type)
        {
                this.type = type;
        }

        public Single(int userId, double amount, Pay payment, int type)
        {
                this.userId = userId;
                this.amount = amount;
                this.payment = payment;
                this.type = type;
        }

        public Pay getPayment()
        {
                return payment;
        }

        public void setPayment(Pay payment)
        {
                this.payment = payment;
        }

        public Single(int userId, double amount, Product product, int type)
        {
                this.userId = userId;
                this.amount = amount;
                this.product = product;
                this.type = type;
        }

        public Single()
        {
        }

        public Trading getTrading()
        {
                return trading;
        }

        public void setTrading(Trading trading)
        {
                this.trading = trading;
        }

        public int getUserId()
        {
                return userId;
        }

        public Product getProduct()
        {
                return product;
        }

        public void setProduct(Product product)
        {
                this.product = product;
        }

        public void setUserId(int userId)
        {
                this.userId = userId;
        }

        public double getAmount()
        {
                return amount;
        }

        public void setAmount(int amount)
        {
                this.amount = amount;
        }

        public Withdraw getWithdraw()
        {
                return withdraw;
        }

        public void setWithdraw(Withdraw withdraw)
        {
                this.withdraw = withdraw;
        }

}
