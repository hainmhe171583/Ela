/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.common;

import java.sql.Timestamp;
import model.BaseModel;

/**
 *
 * @author admin
 */
public class Token extends BaseModel
{
        private String token;
        private String email;
        private Timestamp expiryDate;

        public Token()
        {
        }

        public String getToken()
        {
                return token;
        }

        public void setToken(String token)
        {
                this.token = token;
        }

        public String getEmail()
        {
                return email;
        }

        public void setEmail(String email)
        {
                this.email = email;
        }

        public Timestamp getExpiryDate()
        {
                return expiryDate;
        }

        public void setExpiryDate(Timestamp expiryDate)
        {
                this.expiryDate = expiryDate;
        }
}
