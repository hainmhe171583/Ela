/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.common;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import dal.auth.UserDBContext;
import java.io.IOException;
import java.sql.Timestamp;
import model.auth.User;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;

/**
 *
 * @author admin
 */
public class GoogleUtils
{

        public static String getToken(final String code) throws ClientProtocolException, IOException
        {
                String response = Request.Post(Constants.GOOGLE_LINK_GET_TOKEN)
                        .bodyForm(Form.form().add("client_id", Constants.GOOGLE_CLIENT_ID)
                                .add("client_secret", Constants.GOOGLE_CLIENT_SECRET)
                                .add("redirect_uri", Constants.GOOGLE_REDIRECT_URI).add("code", code)
                                .add("grant_type", Constants.GOOGLE_GRANT_TYPE).build())
                        .execute().returnContent().asString();

                JsonObject jobj = new Gson().fromJson(response, JsonObject.class);
                String accessToken = jobj.get("access_token").toString().replaceAll("\"", "");
                return accessToken;
        }

        public static User getUserInfo(final String accessToken) throws ClientProtocolException, IOException
        {
                String link = Constants.GOOGLE_LINK_GET_USER_INFO + accessToken;
                String response = Request.Get(link).execute().returnContent().asString();

                GooglePojo googlePojo = new Gson().fromJson(response, GooglePojo.class);

                UserDBContext userDB = new UserDBContext();
                User user = userDB.get("email", googlePojo.getEmail());
                System.out.println(googlePojo.getEmail());
                if (user == null)
                {
                        User new_user = new User();
                        new_user.setEmail(googlePojo.getEmail());
                        if (googlePojo.getGiven_name() != null)
                        {
                                new_user.setUsername(googlePojo.getGiven_name());
                        } else
                        {
                                String email = googlePojo.getEmail();
                                int atIndex = email.indexOf("@");
                                String username = email.substring(0, atIndex);
                                new_user.setUsername(username);
                        }
                        Timestamp created_at = new Timestamp(System.currentTimeMillis());
                        Timestamp updated_at = new Timestamp(System.currentTimeMillis());
                        new_user.setCreated_at(created_at);
                        new_user.setUpdated_at(updated_at);

                        user = userDB.insert(new_user);
                }
                System.out.println(user.getEmail());
                return user;
        }
}
