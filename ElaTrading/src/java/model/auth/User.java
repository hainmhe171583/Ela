/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.auth;

import java.sql.Timestamp;
import model.BaseModel;

/**
 *
 * @author admin
 */
public class User extends BaseModel
{

        private String username;
        private String password;
        private String email;
        private double balance;
        private String phone;
        private boolean actived;
        private boolean banned;
        private String notification_code;
        private String role;

        public User()
        {
        }

        public User(String username, String password, String email, double balance, String phone, boolean actived, boolean banned, String notification_code, String role, int id, Timestamp created_at, int created_by, Timestamp updated_at, int updated_by, Timestamp deleted_at, int deleted_by, boolean isDeleted)
        {
                super(id, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by, isDeleted);
                this.username = username;
                this.password = password;
                this.email = email;
                this.balance = balance;
                this.phone = phone;
                this.actived = actived;
                this.banned = banned;
                this.notification_code = notification_code;
                this.role = role;
        }
        
        public String getUsername()
        {
                return username;
        }

        public void setUsername(String username)
        {
                this.username = username;
        }

        public String getPassword()
        {
                return password;
        }

        public void setPassword(String password)
        {
                this.password = password;
        }

        public String getEmail()
        {
                return email;
        }

        public void setEmail(String email)
        {
                this.email = email;
        }

        public double getBalance()
        {
                return balance;
        }

        public void setBalance(double balance)
        {
                this.balance = balance;
        }

        public String getPhone()
        {
                return phone;
        }

        public void setPhone(String phone)
        {
                this.phone = phone;
        }

        public boolean isActived()
        {
                return actived;
        }

        public void setActived(boolean actived)
        {
                this.actived = actived;
        }

        public boolean isBanned()
        {
                return banned;
        }

        public void setBanned(boolean banned)
        {
                this.banned = banned;
        }

        public String getNotification_code()
        {
                return notification_code;
        }

        public void setNotification_code(String notification_code)
        {
                this.notification_code = notification_code;
        }

        public String getRole()
        {
                return role;
        }

        public void setRole(String role)
        {
                this.role = role;
        }

}
