/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.auth;

import java.sql.Timestamp;
import model.BaseModel;

/**
 *
 * @author admin
 */
public class Rating extends BaseModel
{

        private String code;
        private int rate;
        private String comment;
        private int userID;
        private User user;

        public Rating()
        {
        }

        public Rating(String code, int rate, String comment, int userID, User user, int id, Timestamp created_at, int created_by, Timestamp updated_at, int updated_by, Timestamp deleted_at, int deleted_by, boolean isDeleted)
        {
                super(id, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by, isDeleted);
                this.code = code;
                this.rate = rate;
                this.comment = comment;
                this.userID = userID;
                this.user = user;
        }

        public String getCode()
        {
                return code;
        }

        public void setCode(String code)
        {
                this.code = code;
        }

        public int getRate()
        {
                return rate;
        }

        public void setRate(int rate)
        {
                this.rate = rate;
        }

        public String getComment()
        {
                return comment;
        }

        public void setComment(String comment)
        {
                this.comment = comment;
        }

        public int getUserID()
        {
                return userID;
        }

        public void setUserID(int userID)
        {
                this.userID = userID;
        }

        public User getUser()
        {
                return user;
        }

        public void setUser(User user)
        {
                this.user = user;
        }

        @Override
        public String toString()
        {
                return "Rating{" + "code=" + code + ", rate=" + rate + ", comment=" + comment + ", created_at=" + this.getCreated_at() + ", user=" + user + '}';
        }

}
