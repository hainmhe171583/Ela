/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.product;

import java.sql.Timestamp;
import model.BaseModel;

/**
 *
 * @author admin
 */
public class Category extends BaseModel
{

        private String categoryName;
        private String describe;

        public Category()
        {
        }

        public Category(String categoryName, String describe, int id, Timestamp created_at, int created_by, Timestamp updated_at, int updated_by, Timestamp deleted_at, int deleted_by, boolean isDeleted)
        {
                super(id, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by, isDeleted);
                this.categoryName = categoryName;
                this.describe = describe;
        }
        
        public String getCategoryName()
        {
                return categoryName;
        }

        public void setCategoryName(String categoryName)
        {
                this.categoryName = categoryName;
        }

        public String getDescribe()
        {
                return describe;
        }

        public void setDescribe(String describe)
        {
                this.describe = describe;
        }

}
