/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.product;

import dal.auth.UserDBContext;
import dal.product.CategoryDBContext;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.util.Locale;
import model.BaseModel;
import model.auth.User;
import model.status.Status;

/**
 *
 * @author admin
 */
public class Product extends BaseModel
{

        private String code;
        private String productName;
        private String describe;
        private double price;
        private int stock;
        private String contact;
        private boolean isPrivate;
        private String dataproduct;
        private String link;
        private int statusID;
        private Status status;
        private int categoryID;
        private Category category;
        private int userID;
        private User user;

        public Product()
        {
        }

        public Product(String code, String productName, String describe, double price, int stock, String contact, boolean isPrivate, String dataproduct, String link, int statusID, Status status, int categoryID, Category category, int userID, User user, int id, Timestamp created_at, int created_by, Timestamp updated_at, int updated_by, Timestamp deleted_at, int deleted_by, boolean isDeleted)
        {
                super(id, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by, isDeleted);
                this.code = code;
                this.productName = productName;
                this.describe = describe;
                this.price = price;
                this.stock = stock;
                this.contact = contact;
                this.isPrivate = isPrivate;
                this.dataproduct = dataproduct;
                this.link = link;
                this.statusID = statusID;
                this.status = status;
                this.categoryID = categoryID;
                this.category = category;
                this.userID = userID;
                this.user = user;
        }

        public String getCode()
        {
                return code;
        }

        public void setCode(String code)
        {
                this.code = code;
        }

        public String getProductName()
        {
                return productName;
        }

        public void setProductName(String productName)
        {
                this.productName = productName;
        }

        public String getDescribe()
        {
                return describe;
        }

        public void setDescribe(String describe)
        {
                this.describe = describe;
        }

        public double _getPrice()
        {
                return price;
        }
        
        public String getPrice()
        {
                Locale localeVN = new Locale("vi", "VN");
                NumberFormat currencyVN = NumberFormat.getCurrencyInstance(localeVN);
                return currencyVN.format(price);
        }

        public void setPrice(double price)
        {
                this.price = price;
        }

        public int getStock()
        {
                return stock;
        }

        public void setStock(int stock)
        {
                this.stock = stock;
        }

        public String getContact()
        {
                return contact;
        }

        public void setContact(String contact)
        {
                this.contact = contact;
        }

        public boolean isIsPrivate()
        {
                return isPrivate;
        }

        public void setIsPrivate(boolean isPrivate)
        {
                this.isPrivate = isPrivate;
        }

        public String getDataproduct()
        {
                return dataproduct;
        }

        public void setDataproduct(String dataproduct)
        {
                this.dataproduct = dataproduct;
        }

        public String getLink()
        {
                return link;
        }

        public void setLink(String link)
        {
                this.link = link;
        }

        public int getStatusID()
        {
                return statusID;
        }

        public void setStatusID(int statusID)
        {
                this.statusID = statusID;
        }

        public Status getStatus()
        {
                return status;
        }

        public void setStatus(Status status)
        {
                this.status = status;
        }

        public int getCategoryID()
        {
                return categoryID;
        }

        public void setCategoryID(int categoryID)
        {
                this.categoryID = categoryID;
        }

        public Category getCategory()
        {
                CategoryDBContext cdb = new CategoryDBContext();
                return cdb.get("id", categoryID + "");
        }
        
        public Category _getCategory()
        {
                return category;
        }
        
        public void setCategory(Category category)
        {
                this.category = category;
        }

        public int getUserID()
        {
                return userID;
        }

        public void setUserID(int userID)
        {
                this.userID = userID;
        }

        public User getUser()
        {
                UserDBContext udb = new UserDBContext();
                return udb.get("id", userID + "");
        }
        
        public User _getUser()
        {
                return user;
        }

        public void setUser(User user)
        {
                this.user = user;
        }

}
