/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.chat;

import java.sql.Timestamp;
import model.BaseModel;
import model.auth.User;

/**
 *
 * @author admin
 */
public class Chat extends BaseModel
{

        private String code;
        private String textcontent;
        private String imagecontent;
        private User sender;
        private int senderid;

        public Chat()
        {
        }

        public Chat(String code, String textcontent, String imagecontent, User sender, int senderid, int id, Timestamp created_at, int created_by, Timestamp updated_at, int updated_by, Timestamp deleted_at, int deleted_by, boolean isDeleted)
        {
                super(id, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by, isDeleted);
                this.code = code;
                this.textcontent = textcontent;
                this.imagecontent = imagecontent;
                this.sender = sender;
                this.senderid = senderid;
        }

        public String getCode()
        {
                return code;
        }

        public void setCode(String code)
        {
                this.code = code;
        }

        public String getTextcontent()
        {
                return textcontent;
        }

        public void setTextcontent(String textcontent)
        {
                this.textcontent = textcontent;
        }

        public String getImagecontent()
        {
                return imagecontent;
        }

        public void setImagecontent(String imagecontent)
        {
                this.imagecontent = imagecontent;
        }

        public User getSender()
        {
                return sender;
        }

        public void setSender(User sender)
        {
                this.sender = sender;
        }

        public int getSenderid()
        {
                return senderid;
        }

        public void setSenderid(int senderid)
        {
                this.senderid = senderid;
        }

}
