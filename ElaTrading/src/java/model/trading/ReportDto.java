/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.trading;

import model.auth.User;

/**
 *
 * @author admin
 */
public class ReportDto
{

        private String reportId;
        private String code;
        private String productId;
        private String statusId;
        private String tradingId;
        private String productName;
        private String salerId;
        private String buyerId;
        private String created_at;

        private User saler;
        private User buyer;

        public ReportDto()
        {
        }

        public ReportDto(String reportId, String code, String productId, String statusId, String tradingId, String productName, String salerId, String buyerId, String created_at)
        {
                this.reportId = reportId;
                this.code = code;
                this.productId = productId;
                this.statusId = statusId;
                this.tradingId = tradingId;
                this.productName = productName;
                this.salerId = salerId;
                this.buyerId = buyerId;
                this.created_at = created_at;
        }

        public User getSaler()
        {
                return saler;
        }

        public void setSaler(User saler)
        {
                this.saler = saler;
        }

        public User getBuyer()
        {
                return buyer;
        }

        public void setBuyer(User buyer)
        {
                this.buyer = buyer;
        }

        public String getReportId()
        {
                return reportId;
        }

        public void setReportId(String reportId)
        {
                this.reportId = reportId;
        }

        public String getCode()
        {
                return code;
        }

        public void setCode(String code)
        {
                this.code = code;
        }

        public String getProductId()
        {
                return productId;
        }

        public void setProductId(String productId)
        {
                this.productId = productId;
        }

        public String getStatusId()
        {
                return statusId;
        }

        public void setStatusId(String statusId)
        {
                this.statusId = statusId;
        }

        public String getTradingId()
        {
                return tradingId;
        }

        public void setTradingId(String tradingId)
        {
                this.tradingId = tradingId;
        }

        public String getProductName()
        {
                return productName;
        }

        public void setProductName(String productName)
        {
                this.productName = productName;
        }

        public String getSalerId()
        {
                return salerId;
        }

        public void setSalerId(String salerId)
        {
                this.salerId = salerId;
        }

        public String getBuyerId()
        {
                return buyerId;
        }

        public void setBuyerId(String buyerId)
        {
                this.buyerId = buyerId;
        }

        public String getCreated_at()
        {
                return created_at;
        }

        public void setCreated_at(String created_at)
        {
                this.created_at = created_at;
        }

        @Override
        public String toString()
        {
                return "ReportDto{" + "reportId=" + reportId + ", productId=" + productId + ", statusId=" + statusId + ", tradingId=" + tradingId + ", productName=" + productName + ", salerId=" + salerId + ", buyerId=" + buyerId + ", created_at=" + created_at + '}';
        }

}
