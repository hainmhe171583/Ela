/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.trading;

import java.sql.Timestamp;
import model.BaseModel;
import model.auth.User;
import model.product.Product;
import model.status.Status;

/**
 *
 * @author admin
 */
public class Trading extends BaseModel
{
        private String code;
        private double total;
        private int statusID;
        private Status status;
        private int productID;
        private Product product;
        private int userID;
        private User user;

        public Trading()
        {
        }

        public Trading(String code, double total, int statusID, Status status, int productID, Product product, int userID, User user, int id, Timestamp created_at, int created_by, Timestamp updated_at, int updated_by, Timestamp deleted_at, int deleted_by, boolean isDeleted)
        {
                super(id, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by, isDeleted);
                this.code = code;
                this.total = total;
                this.statusID = statusID;
                this.status = status;
                this.productID = productID;
                this.product = product;
                this.userID = userID;
                this.user = user;
        }

        public String getCode()
        {
                return code;
        }

        public void setCode(String code)
        {
                this.code = code;
        }

        public double getTotal()
        {
                return total;
        }

        public void setTotal(double total)
        {
                this.total = total;
        }

        public int getStatusID()
        {
                return statusID;
        }

        public void setStatusID(int statusID)
        {
                this.statusID = statusID;
        }

        public Status getStatus()
        {
                return status;
        }

        public void setStatus(Status status)
        {
                this.status = status;
        }

        public int getProductID()
        {
                return productID;
        }

        public void setProductID(int productID)
        {
                this.productID = productID;
        }

        public Product getProduct()
        {
                return product;
        }

        public void setProduct(Product product)
        {
                this.product = product;
        }

        public int getUserID()
        {
                return userID;
        }

        public void setUserID(int userID)
        {
                this.userID = userID;
        }

        public User getUser()
        {
                return user;
        }

        public void setUser(User user)
        {
                this.user = user;
        }
        
}
