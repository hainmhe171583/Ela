/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.trading;

import java.sql.Timestamp;
import model.BaseModel;
import model.auth.User;
import model.status.Status;

/**
 *
 * @author admin
 */
public class Report extends BaseModel
{
        private String code;
        private String title;
        private String describe;
        private boolean callAdmin;
        private int statusID;
        private Status status;
        private int tradingID;
        private Trading trading;
        private int userID;
        private User user;

        public Report()
        {
        }

        public Report(String code, String title, String describe, boolean callAdmin, int statusID, Status status, int tradingID, Trading trading, int userID, User user, int id, Timestamp created_at, int created_by, Timestamp updated_at, int updated_by, Timestamp deleted_at, int deleted_by, boolean isDeleted)
        {
                super(id, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by, isDeleted);
                this.code = code;
                this.title = title;
                this.describe = describe;
                this.callAdmin = callAdmin;
                this.statusID = statusID;
                this.status = status;
                this.tradingID = tradingID;
                this.trading = trading;
                this.userID = userID;
                this.user = user;
        }

        public String getCode()
        {
                return code;
        }

        public void setCode(String code)
        {
                this.code = code;
        }

        public String getTitle()
        {
                return title;
        }

        public void setTitle(String title)
        {
                this.title = title;
        }

        public String getDescribe()
        {
                return describe;
        }

        public void setDescribe(String describe)
        {
                this.describe = describe;
        }

        public boolean isCallAdmin()
        {
                return callAdmin;
        }

        public void setCallAdmin(boolean callAdmin)
        {
                this.callAdmin = callAdmin;
        }

        public int getStatusID()
        {
                return statusID;
        }

        public void setStatusID(int statusID)
        {
                this.statusID = statusID;
        }

        public Status getStatus()
        {
                return status;
        }

        public void setStatus(Status status)
        {
                this.status = status;
        }

        public int getTradingID()
        {
                return tradingID;
        }

        public void setTradingID(int tradingID)
        {
                this.tradingID = tradingID;
        }

        public Trading getTrading()
        {
                return trading;
        }

        public void setTrading(Trading trading)
        {
                this.trading = trading;
        }

        public int getUserID()
        {
                return userID;
        }

        public void setUserID(int userID)
        {
                this.userID = userID;
        }

        public User getUser()
        {
                return user;
        }

        public void setUser(User user)
        {
                this.user = user;
        }

}
