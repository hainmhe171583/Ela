/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.notification;

import java.sql.Timestamp;
import model.BaseModel;

/**
 *
 * @author admin
 */
public class Di_subject extends BaseModel
{
        private int productId;
        private int notificationsId;

        public Di_subject()
        {
        }

        public Di_subject(int productId, int notificationsId, int id, Timestamp created_at, int created_by, Timestamp updated_at, int updated_by, Timestamp deleted_at, int deleted_by, boolean isDeleted)
        {
                super(id, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by, isDeleted);
                this.productId = productId;
                this.notificationsId = notificationsId;
        }
        
        public int getProductId()
        {
                return productId;
        }

        public void setProductId(int productId)
        {
                this.productId = productId;
        }

        public int getNotificationsId()
        {
                return notificationsId;
        }

        public void setNotificationsId(int notificationsId)
        {
                this.notificationsId = notificationsId;
        }
        
}
