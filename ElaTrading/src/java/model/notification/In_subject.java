/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.notification;

import java.sql.Timestamp;
import model.BaseModel;

/**
 *
 * @author admin
 */
public class In_subject extends BaseModel
{
        
        private int receiverId;
        private int notificationsId;

        public In_subject()
        {
        }

        public In_subject(int receiverId, int notificationsId, int id, Timestamp created_at, int created_by, Timestamp updated_at, int updated_by, Timestamp deleted_at, int deleted_by, boolean isDeleted)
        {
                super(id, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by, isDeleted);
                this.receiverId = receiverId;
                this.notificationsId = notificationsId;
        }

        public int getReceiverId()
        {
                return receiverId;
        }

        public void setReceiverId(int receiverId)
        {
                this.receiverId = receiverId;
        }

        public int getNotificationsId()
        {
                return notificationsId;
        }

        public void setNotificationsId(int notificationsId)
        {
                this.notificationsId = notificationsId;
        }
        
}
