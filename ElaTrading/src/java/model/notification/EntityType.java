/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.notification;

import java.sql.Timestamp;
import model.BaseModel;

/**
 *
 * @author admin
 */
public class EntityType extends BaseModel
{
        
        private String entity;
        private String description;

        public EntityType()
        {
        }

        public EntityType(String entity, String description, int id, Timestamp created_at, int created_by, Timestamp updated_at, int updated_by, Timestamp deleted_at, int deleted_by, boolean isDeleted)
        {
                super(id, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by, isDeleted);
                this.entity = entity;
                this.description = description;
        }
        
        public String getEntity()
        {
                return entity;
        }

        public void setEntity(String entity)
        {
                this.entity = entity;
        }

        public String getDescription()
        {
                return description;
        }

        public void setDescription(String description)
        {
                this.description = description;
        }
        
}
