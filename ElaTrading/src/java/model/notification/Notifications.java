/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.notification;

import java.sql.Timestamp;
import model.BaseModel;

/**
 *
 * @author admin
 */
public class Notifications extends BaseModel
{
        
        private String code;
        private String url;
        private boolean status;
        private int entityTypeId;

        public Notifications()
        {
        }

        public Notifications(String code, String url, boolean status, int entityTypeId, int id, Timestamp created_at, int created_by, Timestamp updated_at, int updated_by, Timestamp deleted_at, int deleted_by, boolean isDeleted)
        {
                super(id, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by, isDeleted);
                this.code = code;
                this.url = url;
                this.status = status;
                this.entityTypeId = entityTypeId;
        }

        public String getCode()
        {
                return code;
        }

        public void setCode(String code)
        {
                this.code = code;
        }

        public String getUrl()
        {
                return url;
        }

        public void setUrl(String url)
        {
                this.url = url;
        }

        public boolean isStatus()
        {
                return status;
        }

        public void setStatus(boolean status)
        {
                this.status = status;
        }

        public int getEntityTypeId()
        {
                return entityTypeId;
        }

        public void setEntityTypeId(int entityTypeId)
        {
                this.entityTypeId = entityTypeId;
        }
        
}
