/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.notification;

import java.sql.Timestamp;
import model.BaseModel;

/**
 *
 * @author admin
 */
public class Subject extends BaseModel
{
        private int notificationsId;
        private int actorId;

        public Subject()
        {
        }

        public Subject(int notificationsId, int actorId, int id, Timestamp created_at, int created_by, Timestamp updated_at, int updated_by, Timestamp deleted_at, int deleted_by, boolean isDeleted)
        {
                super(id, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by, isDeleted);
                this.notificationsId = notificationsId;
                this.actorId = actorId;
        }

        public int getNotificationsId()
        {
                return notificationsId;
        }

        public void setNotificationsId(int notificationsId)
        {
                this.notificationsId = notificationsId;
        }

        public int getActorId()
        {
                return actorId;
        }

        public void setActorId(int actorId)
        {
                this.actorId = actorId;
        }
        
}
