/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author admin
 */
public class BaseModel
{

        private int id;
        private Timestamp created_at;
        private int created_by;
        private Timestamp updated_at;
        private int updated_by;
        private Timestamp deleted_at;
        private int deleted_by;
        private boolean isDeleted;

        public BaseModel()
        {
        }

        public BaseModel(int id, Timestamp created_at, int created_by, Timestamp updated_at, int updated_by, Timestamp deleted_at, int deleted_by, boolean isDeleted)
        {
                this.id = id;
                this.created_at = created_at;
                this.created_by = created_by;
                this.updated_at = updated_at;
                this.updated_by = updated_by;
                this.deleted_at = deleted_at;
                this.deleted_by = deleted_by;
                this.isDeleted = isDeleted;
        }

        public int getId()
        {
                return id;
        }

        public void setId(int id)
        {
                this.id = id;
        }

        public Timestamp getCreated_at()
        {
                return created_at;
        }

        public void setCreated_at(Timestamp created_at)
        {
                this.created_at = created_at;
        }

        public int getCreated_by()
        {
                return created_by;
        }

        public void setCreated_by(int created_by)
        {
                this.created_by = created_by;
        }

        public Timestamp getUpdated_at()
        {
                return updated_at;
        }

        public void setUpdated_at(Timestamp updated_at)
        {
                this.updated_at = updated_at;
        }

        public int getUpdated_by()
        {
                return updated_by;
        }

        public void setUpdated_by(int updated_by)
        {
                this.updated_by = updated_by;
        }

        public Timestamp getDeleted_at()
        {
                return deleted_at;
        }

        public void setDeleted_at(Timestamp deleted_at)
        {
                this.deleted_at = deleted_at;
        }

        public int getDeleted_by()
        {
                return deleted_by;
        }

        public void setDeleted_by(int deleted_by)
        {
                this.deleted_by = deleted_by;
        }

        public boolean isIsDeleted()
        {
                return isDeleted;
        }

        public void setIsDeleted(boolean isDeleted)
        {
                this.isDeleted = isDeleted;
        }
        
}
