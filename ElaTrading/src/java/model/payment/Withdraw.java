/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.withdraw;

import java.sql.Timestamp;
import model.BaseModel;
import model.auth.User;
import model.product.Product;
import model.status.Status;

/**
 *
 * @author hai20
 */
public class Withdraw extends BaseModel
{

        private String code;
        private double total;
        private int statusID;
        private Status status;
        private int userID;
        private User user;
        private String userInfo;
        private String respond;

        public Withdraw()
        {
        }

        public Withdraw(String code, double total, int statusID, Status status, int userID, User user, String userInfo, String respond, int id, Timestamp created_at, int created_by, Timestamp updated_at, int updated_by, Timestamp deleted_at, int deleted_by, boolean isDeleted)
        {
                super(id, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by, isDeleted);
                this.code = code;
                this.total = total;
                this.statusID = statusID;
                this.status = status;
                this.userID = userID;
                this.user = user;
                this.userInfo = userInfo;
                this.respond = respond;
        }

        public String getCode()
        {
                return code;
        }

        public void setCode(String code)
        {
                this.code = code;
        }

        public double getTotal()
        {
                return total;
        }

        public void setTotal(double total)
        {
                this.total = total;
        }

        public int getStatusID()
        {
                return statusID;
        }

        public void setStatusID(int statusID)
        {
                this.statusID = statusID;
        }

        public Status getStatus()
        {
                return status;
        }

        public void setStatus(Status status)
        {
                this.status = status;
        }

        public int getUserID()
        {
                return userID;
        }

        public void setUserID(int userID)
        {
                this.userID = userID;
        }

        public User getUser()
        {
                return user;
        }

        public void setUser(User user)
        {
                this.user = user;
        }

        public String getUserInfo()
        {
                return userInfo;
        }

        public void setUserInfo(String userInfo)
        {
                this.userInfo = userInfo;
        }

        public String getRespond()
        {
                return respond;
        }

        public void setRespond(String respond)
        {
                this.respond = respond;
        }

}
