/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.payment;

import java.sql.Timestamp;
import model.BaseModel;

/**
 *
 * @author hai20
 */
public class Pay extends BaseModel
{
        private String vnp_TxnRef;
        private String payment_code;
        private double amount;
        private String link;
        private boolean status;
        private int transId;
        private int userId;

        public Pay()
        {
        }

        public Pay(String vnp_TxnRef, String payment_code, double amount, int transId, String link, boolean status, int userId, int id, Timestamp created_at, int created_by, Timestamp updated_at, int updated_by, Timestamp deleted_at, int deleted_by, boolean isDeleted)
        {
                super(id, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by, isDeleted);
                this.vnp_TxnRef = vnp_TxnRef;
                this.payment_code = payment_code;
                this.amount = amount;
                this.transId = transId;
                this.link = link;
                this.status = status;
                this.userId = userId;
        }

        public String getVnp_TxnRef()
        {
                return vnp_TxnRef;
        }

        public void setVnp_TxnRef(String vnp_TxnRef)
        {
                this.vnp_TxnRef = vnp_TxnRef;
        }

        public String getPayment_code()
        {
                return payment_code;
        }

        public void setPayment_code(String payment_code)
        {
                this.payment_code = payment_code;
        }

        public double getAmount()
        {
                return amount;
        }

        public void setAmount(double amount)
        {
                this.amount = amount;
        }

        public int getTransId()
        {
                return transId;
        }

        public void setTransId(int transId)
        {
                this.transId = transId;
        }

        public String getLink()
        {
                return link;
        }

        public void setLink(String link)
        {
                this.link = link;
        }

        public boolean isStatus()
        {
                return status;
        }

        public void setStatus(boolean status)
        {
                this.status = status;
        }

        public int getUserId()
        {
                return userId;
        }

        public void setUserId(int userId)
        {
                this.userId = userId;
        }

        @Override
        public String toString()
        {
                return "Pay{" + "userId=" + userId + ", vnp_TxnRef=" + vnp_TxnRef + ", payment_code=" + payment_code + ", amount=" + amount + ", transId=" + transId + ", status=" + status + ", link=" + link + '}';
        }
}



