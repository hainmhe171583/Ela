/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model.payment;

import java.sql.Timestamp;
import model.BaseModel;

/**
 *
 * @author hai20
 */
public class Transac extends BaseModel
{

        private String type;

        public Transac()
        {
        }

        public Transac(String type, int id, Timestamp created_at, int created_by, Timestamp updated_at, int updated_by, Timestamp deleted_at, int deleted_by, boolean isDeleted)
        {
                super(id, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by, isDeleted);
                this.type = type;
        }

        public String getType()
        {
                return type;
        }

        public void setType(String type)
        {
                this.type = type;
        }

}
