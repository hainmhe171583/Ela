package controller.admin.withdraw;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
import dal.auth.UserDBContext;
import dal.payment.WithdrawDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.Date;
import model.auth.User;
import model.common.Single;
import model.common._Deque;
import model.withdraw.Withdraw;
import utils.MultiProcess;

/**
 *
 * @author hai20
 */
public class A_WIthdrawController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet WithdrawDetailController</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet WithdrawDetailController at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                HttpSession ses = request.getSession();
                User u = (User) ses.getAttribute("user");
                UserDBContext newu = new UserDBContext();
                //u = newu.get("id", "1");//fix cung

                String code = request.getParameter("code");

                WithdrawDBContext wdb = new WithdrawDBContext();
                Withdraw withdraw = wdb.get("code", code);

                if (withdraw == null)
                {
                        request.setAttribute("error", "KHONG TIM THAY YEU CAU RUT TIEN");
                        request.getRequestDispatcher("/views/error/error_denied.jsp").forward(request, response);
                        return;
                }

                if (u == null)
                {
                        response.sendRedirect("/ElaTrading");
                        return;
                } else if (u.getId() != withdraw.getUserID() && !u.getRole().equals("admin"))
                {
                        response.sendRedirect("/ElaTrading");
                        return;
                }

                //System.out.println(code);
                request.setAttribute("withdraw", withdraw);
                if (u.getRole().equals("user"))
                {
                        request.setAttribute("role", 1);
                }
                if (u.getRole().equals("admin"))
                {
                        request.setAttribute("role", 2);
                }

                DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
                String formattedNumber = decimalFormat.format(withdraw.getTotal());
                request.setAttribute("total", formattedNumber.substring(0, formattedNumber.length() - 3));
                request.getRequestDispatcher("/views/payment/withdrawdetail.jsp").forward(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                HttpSession ses = request.getSession();
                User u = (User) ses.getAttribute("user");
                UserDBContext newu = new UserDBContext();
                //u = newu.get("id", "1");//fix cung

                long milis = System.currentTimeMillis();
                Date date = new Date();
                milis = date.getTime();
                Timestamp now = new Timestamp(milis);

                String code = request.getParameter("code");
                String respond = request.getParameter("respond");
                WithdrawDBContext wdb = new WithdrawDBContext();
                Withdraw withdraw = wdb.get("code", code);
                if (withdraw == null)
                {
                        request.setAttribute("error", "KHONG TIM THAY YEU CAU RUT TIEN");
                        request.getRequestDispatcher("/views/error/error_denied.jsp").forward(request, response);
                        return;
                }

                if (u == null)
                {
                        response.sendRedirect("/ElaTrading");
                        return;
                } else
                {
//            if (u.getId() == withdraw.getUserID()) {
//                withdraw.setStatusID(2);
//                withdraw.setUpdated_at(now);
//                withdraw.setRespond("CANCEL BY USER!!");
//                //wdb.update(withdraw);
//                deque.getMyrequest().add(new Single(withdraw.getUserID(), withdraw.getTotal(), withdraw, 6));
//            } else
                        if (!u.getRole().equals("admin") && u.getId() != withdraw.getUserID())
                        {
                                response.sendRedirect("/ElaTrading");
                                return;
                        }
                }

                int status = -1;
                if (u.getId() == withdraw.getUserID())
                {
                        status = 0;
                        respond = "CANCELED BY USER!!";
                } else
                {
                        status = Integer.parseInt(request.getParameter("status"));
                }

                //dung thong tin thanh toan
                if (status == 1)
                {
                        withdraw.setStatusID(4);
                        withdraw.setUpdated_at(now);
                        withdraw.setRespond(respond);
                        wdb.update(withdraw);
                }

                //sai thong tin thanh toan
                if (status == 0)
                {
                        withdraw.setStatusID(3);
                        withdraw.setUpdated_at(now);
                        withdraw.setRespond(respond);
                        _Deque.getMyrequest().add(new Single(withdraw.getUserID(), withdraw.getTotal(), withdraw, 6));
                }
                MultiProcess mt = new MultiProcess();
                mt.multiPro();

                doGet(request, response);

        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
