/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin.report;

import dal.trading.TradingDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
public class A_ProcessReportController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet UpdateReportController</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet UpdateReportController at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                processRequest(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
//        processRequest(request, response);
                TradingDBContext tradingDBContext = new TradingDBContext();
                String filter = request.getParameter("filter");
                String type = request.getParameter("type");
                String tradingId = request.getParameter("tradingId");

                try
                {
                        switch (type)
                        {
                                case "accept":
                                        tradingDBContext.UpdateTradingById(tradingId, 4 + "");
                                        request.setAttribute("filter", filter);
                                        request.setAttribute("status", "green");
                                        request.setAttribute("message", "Report has been accepted.");
                                        request.getRequestDispatcher("/admin/reports").forward(request, response);
                                        break;

                                case "reject":
                                        tradingDBContext.UpdateTradingById(tradingId, 3 + "");
                                        request.setAttribute("filter", "filter");
                                        request.setAttribute("status", "green");
                                        request.setAttribute("message", "Report has been rejected.");
                                        request.getRequestDispatcher("/admin/reports").forward(request, response);
                                        break;

                                default:
                                        break;
                        }
                } catch (Exception ex)
                {
                        request.setAttribute("filter", filter);
                        request.setAttribute("status", "red");
                        request.setAttribute("message", "Update trading failed.");
                        request.getRequestDispatcher("/admin/reports").forward(request, response);
                }

        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
