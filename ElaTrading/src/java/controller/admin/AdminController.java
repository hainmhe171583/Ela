/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.admin;

import dal.payment.PaymentDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.auth.User;
import model.payment.Pay;

/**
 *
 * @author admin
 */
public class AdminController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet AdminController</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet AdminController at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                //processRequest(request, response);
                request.setCharacterEncoding("UTF-8");
                response.setContentType("text/html; charset=UTF-8");
                response.setCharacterEncoding("UTF-8");
                HttpSession session = request.getSession();
                User u = (User) session.getAttribute("user");
                if (u == null)
                {
                        response.sendRedirect("/ElaTrading/login");
                        return;
                }
                String index = request.getParameter("index");
                if (index == null)
                {
                        index = "1";
                }
                String status = request.getParameter("status");
                int indexPage = Integer.parseInt(index);
                PaymentDBContext paydb = new PaymentDBContext();
                int id = 0;
                if (status == null)
                {
                        status = "All";
                }
                if ("NapTien".equals(status))
                {
                        status = "NapTien";
                }
                if ("TaoHang".equals(status))
                {
                        status = "TaoHang";
                }
                if ("MuaHang".equals(status))
                {
                        status = "MuaHang";
                }
                if ("RutTien".equals(status))
                {
                        status = "RutTien";
                }
                if ("BanHang".equals(status))
                {
                        status = "BanHang";
                }
                if ("HoanTien".equals(status))
                {
                        status = "HoanTien";
                }
                if ("GiaoDich".equals(status))
                {
                        status = "GiaoDich";
                }
                String messPay = request.getParameter("messPay");
                request.setAttribute("messPay", messPay);
                if ("NapTien".equals(status))
                {
                        int transID = 1;
                        int allPaymentID = paydb.countAllPaymentByStatus(id);
                        int endPage = allPaymentID / 5;
                        if (allPaymentID % 5 != 0)
                        {
                                endPage++;
                        }
                        ArrayList<Pay> listPay = paydb.filterListPayment(indexPage, id, transID);
                        request.setAttribute("tag", indexPage);
                        request.setAttribute("endP", endPage);
                        request.setAttribute("listPay", listPay);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.setAttribute("countList", allPaymentID);
                        request.getRequestDispatcher("/views/admin/admin.jsp").forward(request, response);
                        return;
                }
                if ("TaoHang".equals(status))
                {
                        int transID = 2;
                        int allPaymentID = paydb.countAllPaymentByStatus(id);
                        int endPage = allPaymentID / 5;
                        if (allPaymentID % 5 != 0)
                        {
                                endPage++;
                        }
                        ArrayList<Pay> listPay = paydb.filterListPayment(indexPage, id, transID);
                        request.setAttribute("tag", indexPage);
                        request.setAttribute("endP", endPage);
                        request.setAttribute("listPay", listPay);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.setAttribute("countList", allPaymentID);
                        request.getRequestDispatcher("/views/admin/admin.jsp").forward(request, response);
                        return;
                }
                if ("MuaHang".equals(status))
                {
                        int transID = 3;
                        int allPaymentID = paydb.countAllPaymentByStatus(id);
                        int endPage = allPaymentID / 5;
                        if (allPaymentID % 5 != 0)
                        {
                                endPage++;
                        }
                        ArrayList<Pay> listPay = paydb.filterListPayment(indexPage, id, transID);
                        request.setAttribute("tag", indexPage);
                        request.setAttribute("endP", endPage);
                        request.setAttribute("listPay", listPay);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.setAttribute("countList", allPaymentID);
                        request.getRequestDispatcher("/views/admin/admin.jsp").forward(request, response);
                        return;
                }
                if ("RutTien".equals(status))
                {
                        int transID = 5;
                        int allPaymentID = paydb.countAllPaymentByStatus(id);
                        int endPage = allPaymentID / 5;
                        if (allPaymentID % 5 != 0)
                        {
                                endPage++;
                        }
                        ArrayList<Pay> listPay = paydb.filterListPayment(indexPage, id, transID);
                        request.setAttribute("tag", indexPage);
                        request.setAttribute("endP", endPage);
                        request.setAttribute("listPay", listPay);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.setAttribute("countList", allPaymentID);
                        request.getRequestDispatcher("/views/admin/admin.jsp").forward(request, response);
                        return;
                }
                if ("BanHang".equals(status))
                {
                        int transID = 4;
                        int allPaymentID = paydb.countAllPaymentByStatus(id);
                        int endPage = allPaymentID / 5;
                        if (allPaymentID % 5 != 0)
                        {
                                endPage++;
                        }
                        ArrayList<Pay> listPay = paydb.filterListPayment(indexPage, id, transID);
                        request.setAttribute("tag", indexPage);
                        request.setAttribute("endP", endPage);
                        request.setAttribute("listPay", listPay);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.setAttribute("countList", allPaymentID);
                        request.getRequestDispatcher("/views/admin/admin.jsp").forward(request, response);
                        return;
                }
                if ("HoanTien".equals(status))
                {
                        int transID = 6;
                        int allPaymentID = paydb.countAllPaymentByStatus(id);
                        int endPage = allPaymentID / 5;
                        if (allPaymentID % 5 != 0)
                        {
                                endPage++;
                        }
                        ArrayList<Pay> listPay = paydb.filterListPayment(indexPage, id, transID);
                        request.setAttribute("tag", indexPage);
                        request.setAttribute("endP", endPage);
                        request.setAttribute("listPay", listPay);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.setAttribute("countList", allPaymentID);
                        request.getRequestDispatcher("/views/admin/admin.jsp").forward(request, response);
                        return;
                }
                if ("GiaoDich".equals(status))
                {
                        int transID = 7;
                        int allPaymentID = paydb.countAllPaymentByStatus(id);
                        int endPage = allPaymentID / 5;
                        if (allPaymentID % 5 != 0)
                        {
                                endPage++;
                        }
                        ArrayList<Pay> listPay = paydb.filterListPayment(indexPage, id, transID);
                        request.setAttribute("tag", indexPage);
                        request.setAttribute("endP", endPage);
                        request.setAttribute("listPay", listPay);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.setAttribute("countList", allPaymentID);
                        request.getRequestDispatcher("/views/admin/admin.jsp").forward(request, response);
                        return;
                }
                if ("All".equals(status) || "".equals(status))
                {
                        int allPaymentID = paydb.countAllPayment(id);
                        int endPage = allPaymentID / 5;
                        if (allPaymentID % 5 != 0)
                        {
                                endPage++;
                        }
                        ArrayList<Pay> listPay = paydb.filterAllPayment(indexPage, id);
                        request.setAttribute("tag", indexPage);
                        request.setAttribute("endP", endPage);
                        request.setAttribute("listPay", listPay);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.setAttribute("countList", allPaymentID);
                        request.getRequestDispatcher("/views/admin/admin.jsp").forward(request, response);
                        return;
                }
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
