/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.auth;

import dal.auth.UserDBContext;
import jakarta.servlet.ServletContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import model.auth.User;
import model.common.EmailUtility;
import utils.HashPass;
import utils.SendMail;
import utils.Validate;

/**
 *
 * @author admin
 */
public class SignupController extends HttpServlet
{

        private static final long serialVersionUID = 1L;

        private String host;
        private String port;
        private String email;
        private String name;
        private String pass;

        public void init()
        {
                // reads SMTP server setting from web.xml file
                ServletContext context = getServletContext();
                host = context.getInitParameter("host");
                port = context.getInitParameter("port");
                email = context.getInitParameter("email");
                name = context.getInitParameter("name");
                pass = context.getInitParameter("pass");
        }

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet SignupController</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet SignupController at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                request.getRequestDispatcher("/views/auth/signup.jsp").forward(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                Validate validate = new Validate();

                try
                {
                        String _name = validate.getFieldAjax(request, "name", true, "Mục tên đăng nhập không thể bỏ trống.");
                        _name = validate.fieldString(_name, "^.{4,}$", "Tên đăng nhập cần có từ 4 ký tự trở lên.");
                        String _email = validate.getFieldAjax(request, "email", true, "Mục Email không thể bỏ trống.");
                        _email = validate.fieldString(_email, "[A-Za-z0-9]+([_.-][A-Za-z0-9]+)*@[A-Za-z0-9]+([_.-][A-Za-z0-9]+)*\\.[A-Za-z]+$", "Email được nhập không đúng định dạng");
                        String password = validate.getFieldAjax(request, "password", true, "Mục mật khẩu không thể bỏ trống.");
                        password = validate.fieldString(password, "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).{6,}$", "Mật khẩu cần có ký tự thường, in hoa, đặc biệt, số và dài hơn 5 ký tự");
                        String password_confirm = validate.getFieldAjax(request, "password_confirm", true, "Mục xác nhận mật khẩu không thể bỏ trống.");
                        String captcha = validate.getFieldAjax(request, "captcha", true, "Mục mã captcha không thể bỏ trống.");

                        if (!password.equals(password_confirm))
                        {
                                throw new Exception("Mật khẩu xác nhận không khớp với mật khẩu");
                        } else
                        {
                                HashPass hashPass = new HashPass();
                                UserDBContext userDB = new UserDBContext();

                                if (userDB.get("username", _name) != null)
                                {
                                        throw new Exception("Tên đăng nhập đã được sử dụng");
                                }

                                if (userDB.get("email", _email) != null)
                                {
                                        throw new Exception("Email đã được sử dụng");
                                }

                                String captcha_txt = request.getSession().getAttribute("captcha").toString();
                                if (!captcha_txt.equalsIgnoreCase(captcha))
                                {
                                        throw new Exception("Mã captcha không khớp với mẫu");
                                }

                                String field_pass = hashPass.hashPassword(password);

                                User user = new User();
                                user.setUsername(_name);
                                user.setPassword(field_pass);
                                user.setEmail(_email);
                                Timestamp created_at = new Timestamp(System.currentTimeMillis());
                                Timestamp updated_at = new Timestamp(System.currentTimeMillis());
                                user.setCreated_at(created_at);
                                user.setUpdated_at(updated_at);

                                User new_user = userDB.insert(user);
                                request.getSession().setAttribute("user", new_user);
//
                                String json = "{\"success\": true, \"message\": \"Vui lòng xác thực tài khoản!\"}";
                                response.setContentType("application/json");
                                response.setCharacterEncoding("UTF-8");
                                response.getWriter().write(json);

                                SendMail send = new SendMail();
                                String content = send.ActiveAccount(_email);
                                String subject = "Xác Minh Địa Chỉ Email";
                                final String X_email = _email;
                                EmailUtility.sendEmail(host, port, email, name, pass, X_email, subject, content);
                        }
                } catch (Exception er)
                {
                        String json = "{\"success\": false, \"message\": \"" + er.getMessage() + "\"}";
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        response.getWriter().write(json);
                }
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>
}
