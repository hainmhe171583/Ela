/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.auth;

import com.google.gson.Gson;
import dal.auth.UserDBContext;
import dal.common.TokenDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import model.auth.User;
import model.common.Token;
import utils.HashPass;
import utils.Validate;

/**
 *
 * @author admin
 */
public class ResetController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet ResetController</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet ResetController at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                //processRequest(request, response);
                        try
                        {
                                Validate validate = new Validate();

                                String email = validate.getFieldAjax(request, "email", true, "URL không hợp lệ!");
                                String token = validate.getFieldAjax(request, "token", true, "URL không hợp lệ!");

                                request.setAttribute("email", email);
                                request.setAttribute("token", token);
                                request.getRequestDispatcher("/views/auth/reset.jsp").forward(request, response);
                        } catch (Exception er)
                        {
                                request.setAttribute("error", er.getMessage());
                                request.getRequestDispatcher("/views/error/error_denied.jsp").forward(request, response);
                        }
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                try
                {
                        Validate validate = new Validate();
                        String token = validate.getFieldAjax(request, "token", true, "");
                        String email = validate.getFieldAjax(request, "email", true, "");
                        String password = validate.getFieldAjax(request, "password", true, "Mục mật khẩu không thể bỏ trống");
                        String password_confirm = validate.getFieldAjax(request, "password_confirmation", true, "Mục xác nhận mật khẩu không thể bỏ trống");
                        String captcha = validate.getFieldAjax(request, "captcha", true, "Mục mã captcha không thể bỏ trống");
                        String captcha_txt = request.getSession().getAttribute("captcha").toString();
                        if (!captcha_txt.equals(captcha))
                        {
                                throw new Exception("Mã captcha không khớp với mẫu");
                        }

                        TokenDBContext tokenDB = new TokenDBContext();
                        Token _token = tokenDB.checkValid(token, email);
                        if (_token == null)
                        {
                                throw new Exception("Link không hợp lệ!");
                        }
                        long check = _token.getExpiryDate().getTime() - System.currentTimeMillis();
                        boolean check_timeExpiry = check > 0 && check < 30 * 60 * 1000;
                        if (check_timeExpiry)
                        {
                                if (!password.equals(password_confirm))
                                {
                                        throw new Exception("Mật khẩu xác nhận không khớp với mật khẩu");
                                }

                                UserDBContext userDB = new UserDBContext();
                                User user = userDB.get("email", email);
                                HashPass hashPass = new HashPass();
                                String _password = hashPass.hashPassword(password);
                                user.setPassword(_password);
                                Timestamp updated_at = new Timestamp(System.currentTimeMillis());
                                user.setUpdated_at(updated_at);

                                userDB.update(user);
                                tokenDB.delete(token);
                                request.getSession().setAttribute("user", user);
                                String json = new Gson().toJson("succeed");
                                response.setContentType("application/json");
                                response.setCharacterEncoding("UTF-8");
                                response.getWriter().write(json);
                        } else
                        {
                                throw new Exception("Đường Link đã hết hạn sử dụng!");
                        }
                } catch (Exception er)
                {
                        String json = new Gson().toJson(er.getMessage());
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        response.getWriter().write(json);
                }
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
