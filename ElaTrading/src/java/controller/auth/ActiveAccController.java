/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.auth;

import dal.auth.UserDBContext;
import dal.common.TokenDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.auth.User;
import model.common.Token;
import utils.Validate;

/**
 *
 * @author admin
 */
@WebServlet(name = "ActiveAccController", urlPatterns =
{
        "/active"
})
public class ActiveAccController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet ActiveAccController</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet ActiveAccController at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                try
                {
                        Validate validate = new Validate();
                        String email = validate.getFieldAjax(request, "email", true, "Đường chỉ link không hợp lệ!");
                        String token = validate.getFieldAjax(request, "token", true, "Đường chỉ Link không hợp lệ!");
                        TokenDBContext tokenDB = new TokenDBContext();
                        Token _token = tokenDB.checkValid(token, email);
                        if (_token == null)
                        {
                                throw new Exception("Đường chỉ Link không hợp lệ!");
                        }
                        long check = _token.getExpiryDate().getTime() - System.currentTimeMillis();
                        boolean check_timeExpiry = check > 0 && check < 30 * 60 * 1000;
                        if (check_timeExpiry)
                        {
                                UserDBContext userDB = new UserDBContext();
                                User user = userDB.get("email", email);
                                user.setActived(true);
                                userDB.update(user);
                                HttpSession session = request.getSession(false);
                                boolean loggedIn = (session != null) && (session.getAttribute("user") != null);
                                if (loggedIn)
                                {
                                        session.removeAttribute("user");
                                        session.setAttribute("user", user);
                                }
                                tokenDB.delete(token);

                                response.sendRedirect("/ElaTrading");

                        } else
                        {
                                throw new Exception("Đường chỉ Link đã quá hạn sử dụng!");
                        }
                } catch (Exception er)
                {
                        request.setAttribute("error", er.getMessage());
                        request.getRequestDispatcher("/views/error/error_denied.jsp").forward(request, response);
                }

        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
