/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.auth;

import dal.auth.UserDBContext;
import jakarta.servlet.ServletContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.auth.User;
import model.common.EmailUtility;
import utils.SendMail;
import utils.Validate;

/**
 *
 * @author admin
 */
public class ResetPasswordController extends HttpServlet
{

        private static final long serialVersionUID = 1L;

        private String host;
        private String port;
        private String email;
        private String name;
        private String pass;

        public void init()
        {
                // reads SMTP server setting from web.xml file
                ServletContext context = getServletContext();
                host = context.getInitParameter("host");
                port = context.getInitParameter("port");
                email = context.getInitParameter("email");
                name = context.getInitParameter("name");
                pass = context.getInitParameter("pass");
        }

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet ResetPasswordController</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet ResetPasswordController at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                request.getRequestDispatcher("/views/auth/reset_password.jsp").forward(request, response);
        }

        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                try
                {
                        Validate validate = new Validate();
                        String recipient = validate.getFieldAjax(request, "email", true, "Mục Email không thể để trống");
                        String captcha = validate.getFieldAjax(request, "captcha", true, "Mục Captcha không thể để trống");
                        String captcha_txt = request.getSession().getAttribute("captcha").toString();
                        if (!captcha_txt.equalsIgnoreCase(captcha))
                        {
                                throw new Exception("Mã captcha không khớp với mẫu");
                        }

                        UserDBContext userDB = new UserDBContext();
                        User user = userDB.get("email", recipient);
                        if (user == null || user.isIsDeleted())
                        {
                                throw new Exception("Không tìm thấy Email trong hệ thống");
                        } else
                        {
                                String json = "{\"success\": true, \"message\": \"Vui lòng kiểm tra Email của bạn!\"}";
                                response.setContentType("application/json");
                                response.setCharacterEncoding("UTF-8");
                                response.getWriter().write(json);

                                SendMail send = new SendMail();
                                String content = send.ResetPassword(recipient);
                                String subject = "Thông báo đặt lại mật khẩu";
                                final String _recipient = recipient;
                                EmailUtility.sendEmail(host, port, email, name, pass, _recipient, subject, content);
                        }
                } catch (Exception er)
                {
                        String json = "{\"success\": false, \"message\": \"" + er.getMessage() + "\"}";
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        response.getWriter().write(json);
                }
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
