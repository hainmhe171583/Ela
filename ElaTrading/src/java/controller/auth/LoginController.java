/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.auth;

import dal.auth.UserDBContext;
import dal.chat.ChatDBContext;
import dal.notification.NotificationsDBContext;
import dal.product.CategoryDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import model.auth.User;
import model.chat.Chat;
import model.notification.Send_Notification;
import model.product.Category;
import utils.HashPass;
import utils.Validate;

/**
 *
 * @author admin
 */
public class LoginController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet LoginController</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet LoginController at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                request.getRequestDispatcher("/views/auth/login.jsp").forward(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                Validate validate = new Validate();
                HashPass hashPass = new HashPass();
                try
                {
                        String name = validate.getFieldAjax(request, "name", true, "Tên đăng nhập và Email không thể bỏ trống");
                        String password = validate.getFieldAjax(request, "password", true, "Mật khẩu không thể bỏ trống");
                        String captcha = validate.getFieldAjax(request, "captcha", true, "Mã captcha không thể bỏ trống");

                        UserDBContext userDB = new UserDBContext();

                        String field_pass = hashPass.hashPassword(password);
                        User user = userDB.getLogin(name, field_pass);

                        if (user != null)
                        {
                                if (!user.isIsDeleted())
                                {
                                        String captcha_txt = request.getSession().getAttribute("captcha").toString();
                                        if (captcha_txt.equalsIgnoreCase(captcha))
                                        {
                                                NotificationsDBContext notificationDB = new NotificationsDBContext();
                                                List<Send_Notification> notifications = notificationDB.load(user.getId());

                                                String unread = user.getNotification_code();
                                                int count = 0;
                                                for (Send_Notification send_Notification : notifications)
                                                {
                                                        if (!send_Notification.getCode().equalsIgnoreCase(unread))
                                                        {
                                                                count++;
                                                        } else
                                                        {
                                                                break;
                                                        }
                                                }
                                                HttpSession session = request.getSession();

                                                CategoryDBContext categoryDB = new CategoryDBContext();
                                                List<Category> nCate = categoryDB.getList("", "");
                                                session.setAttribute("clist", nCate);

                                                session.setAttribute("user", user);
                                                //notification
                                                session.setAttribute("count", count);
                                                session.setAttribute("notifications", notifications);
                                                //chat community
                                                ChatDBContext cdb = new ChatDBContext();
                                                session.setAttribute("sender", user);
                                                ArrayList<Chat> listchat = cdb.getChat();
                                                session.setAttribute("listchat", listchat);

                                                String json = "{\"success\": true, \"message\": \"null\"}";
                                                response.setContentType("application/json");
                                                response.setCharacterEncoding("UTF-8");
                                                response.getWriter().write(json);
                                        } else
                                        {
                                                throw new Exception("Mã captcha không khớp với mẫu");
                                        }
                                } else
                                {
                                        throw new Exception("Tên đăng nhập hoặc mật khẩu không đúng");
                                }

                        } else
                        {
                                throw new Exception("Tên đăng nhập hoặc mật khẩu không đúng");
                        }
                } catch (Exception er)
                {
                        String json = "{\"success\": false, \"message\": \"" + er.getMessage() + "\"}";
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        response.getWriter().write(json);
                }
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
