/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.chat;

import com.google.gson.Gson;
import dal.chat.ChatDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import static java.lang.System.out;
import java.util.UUID;
import model.chat.Chat;

/**
 *
 * @author admin
 */
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 1,
        maxFileSize = 1024 * 1024 * 10,
        maxRequestSize = 1024 * 1024 * 100
)
public class AddChatController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet AddChatController</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet AddChatController at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                request.setAttribute("error", "Không thể thực hiện hành động!");
                request.getRequestDispatcher("/views/error/error_denied.jsp").forward(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                ChatDBContext cdb = new ChatDBContext();
                
                int senderId = Integer.parseInt(request.getParameter("senderId"));
                String textcontent = request.getParameter("textcontent");
                Part imagecontent = request.getPart("imagecontent");
                
                String fileName = imagecontent.getSubmittedFileName();
                String filePath, realPath = "";
                String relativePath = "assets/image/chat/";
                
                UUID uuid = UUID.randomUUID();
                String id = uuid.toString();
                String code = uuid.toString();
                
                Chat chat = new Chat();

                Gson gson = new Gson();
                try
                {
                        if (imagecontent.getSize() != 0)
                        {
                                fileName = id + ".jpg";
                                filePath = relativePath + fileName;
                                realPath = request.getServletContext().getRealPath(filePath);
                                imagecontent.write(realPath);
                        }
                        chat.setTextcontent(textcontent);
                        if (fileName != null)
                        {
                                chat.setImagecontent("assets/image/chat/" + fileName);
                        } else
                        {
                                chat.setImagecontent(fileName);
                        }
                        
                        chat.setSenderid(senderId);
                        chat.setCode(code);
                        cdb.AddChat(chat);
                        String json = gson.toJson(chat);
                        out.println(json);
                        
                } catch (Exception e)
                {
                        e.printStackTrace();
                        String errorMessage = "Có lỗi xảy ra khi thêm tin nhắn";
                        String jsonError = gson.toJson(errorMessage);
                        out.println(jsonError);
                } finally
                {
                        out.close();
                }
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
