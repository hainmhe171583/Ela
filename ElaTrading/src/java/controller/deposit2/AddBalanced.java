/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.deposit2;

import utils.MultiProcess;
import dal.payment.PaymentDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import model.common.Single;
import model.common._Deque;
import model.payment.Pay;
import utils.Config;
import utils.Validate;

/**
 *
 * @author hai20
 */
public class AddBalanced extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet AddBalance</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet AddBalance at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                Validate validate = new Validate();
                try
                {
                        String vnp_TxnRef = validate.getFieldAjax(request, "vnp_TxnRef", true, "");

                        PaymentDBContext payDB = new PaymentDBContext();
                        Pay payment = payDB.get("vnp_TxnRef", vnp_TxnRef);

                        if (payment == null)
                        {
                                throw new Exception();
                        }

                        try
                        {

                                Map fields = new HashMap();
                                for (Enumeration params = request.getParameterNames(); params.hasMoreElements();)
                                {
                                        String fieldName = (String) params.nextElement();
                                        String fieldValue = request.getParameter(fieldName);
                                        if ((fieldValue != null) && (fieldValue.length() > 0))
                                        {
                                                fields.put(fieldName, fieldValue);
                                        }
                                }

                                String vnp_SecureHash = request.getParameter("vnp_SecureHash");
                                if (fields.containsKey("vnp_SecureHashType"))
                                {
                                        fields.remove("vnp_SecureHashType");
                                }
                                if (fields.containsKey("vnp_SecureHash"))
                                {
                                        fields.remove("vnp_SecureHash");
                                }

                                // Check checksum
                                boolean checkOrderId = true; // vnp_TxnRef exists in your database
                                boolean checkAmount = true; // vnp_Amount is valid (Check vnp_Amount VNPAY returns compared to the amount of the code (vnp_TxnRef) in the Your database).
                                boolean checkOrderStatus = true; // PaymnentStatus = 0 (pending)

                                try
                                {
                                        if (Double.parseDouble(request.getParameter("vnp_Amount")) != (payment.getAmount() * 100))
                                        {
                                                checkAmount = false;
                                        }
                                        if (payment.isStatus() == true)
                                        {
                                                checkOrderStatus = false;//tien da duoc nap
                                        }
                                } catch (Exception e)
                                {
                                        throw new Exception();
                                }
                                if (checkAmount == false)
                                {
                                        throw new Exception();
                                }

                                String signValue = Config.hashAllFields(fields);
                                if (!signValue.equals(vnp_SecureHash))
                                {
                                        if (checkOrderId)
                                        {
                                                if (checkAmount)
                                                {
                                                        if (checkOrderStatus)
                                                        {
                                                                String vnp_ResponseCode = validate.getFieldAjax(request, "vnp_ResponseCode", true, "");
                                                                if ("00".equals(vnp_ResponseCode))
                                                                {
                                                                        payment.setStatus(true);
                                                                        _Deque.getMyrequest().add(new Single(payment.getUserId(), payment.getAmount(), payment, 1));
                                                                        //Here Code update PaymnentStatus = 1 into your Database
                                                                } else
                                                                {
                                                                        System.out.print("lose");
                                                                        // Here Code update PaymnentStatus = 2 into your Database
                                                                }
                                                                System.out.print("{\"RspCode\":\"00\",\"Message\":\"Confirm Success\"}");
                                                        } else
                                                        {

                                                                System.out.print("{\"RspCode\":\"02\",\"Message\":\"Order already confirmed\"}");
                                                        }
                                                } else
                                                {
                                                        System.out.print("{\"RspCode\":\"04\",\"Message\":\"Invalid Amount\"}");
                                                }
                                        } else
                                        {
                                                System.out.print("{\"RspCode\":\"01\",\"Message\":\"Order not Found\"}");
                                        }
                                } else
                                {
                                        System.out.print("{\"RspCode\":\"97\",\"Message\":\"Invalid Checksum\"}");
                                }
                        } catch (Exception e)
                        {
                                System.out.print("{\"RspCode\":\"99\",\"Message\":\"Unknow error\"}");
                        }
                        
                        MultiProcess mt = new MultiProcess();
                        mt.multiPro();

                        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
                        String formattedNumber = decimalFormat.format(payment.getAmount());
                        
                        request.setAttribute("amount", formattedNumber.substring(0, formattedNumber.length() - 3));
                        request.setAttribute("pay", payment);

                        request.getRequestDispatcher("views/payment/depositdetail.jsp").forward(request, response);

                } catch (Exception er)
                {
//                        request.setAttribute("error", "Không thể thực hiện hành động!");
//                        request.getRequestDispatcher("/views/error/error_denied.jsp").forward(request, response);
                        er.printStackTrace();
                }

        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                //processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
