/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.deposit2;

import dal.payment.PaymentDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import model.auth.User;
import model.payment.Pay;
import utils.Config;

/**
 *
 * @author hai20
 */
public class DepositController2 extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet Payment</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet Payment at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse res)
                throws ServletException, IOException
        {
                //processRequest(request, response);
                req.getRequestDispatcher("views/payment/deposit.jsp").forward(req, res);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse res)
                throws ServletException, IOException
        {
                //processRequest(request, response);
                int amount = 0;
                HttpSession ses = req.getSession();
                User u = (User) ses.getAttribute("user");

                String vnp_Version = "2.1.0";
                String vnp_Command = "pay";
                String orderType = "other";
                
                try
                {
                        amount = Integer.parseInt(req.getParameter("amount").replaceAll(",", "")) * 100;
                        if (amount > 50000000)
                        {
                                req.setAttribute("mss", "Số tiền muốn nạp quá lớn!!");
                                doGet(req, res);
                        }
                        if (amount < 10000)
                        {
                                req.setAttribute("mss", "Số tiền muốn nạp quá nhỏ!!");
                                doGet(req, res);
                        }
                } catch (Exception e)
                {
                        req.setAttribute("mss", "Nhập sai kiểu!!");
                        doGet(req, res);
                        return;
                }

                String bankCode = null; //"NCB";//req.getParameter("bankCode");

                String vnp_TxnRef = Config.getRandomNumber(8);
                String vnp_IpAddr = Config.getIpAddress(req);

                String vnp_TmnCode = Config.vnp_TmnCode;

                Map<String, String> vnp_Params = new HashMap<>();
                vnp_Params.put("vnp_Version", vnp_Version);
                vnp_Params.put("vnp_Command", vnp_Command);
                vnp_Params.put("vnp_TmnCode", vnp_TmnCode);
                vnp_Params.put("vnp_Amount", String.valueOf(amount));
                vnp_Params.put("vnp_CurrCode", "VND");

                if (bankCode != null && !bankCode.isEmpty())
                {
                        vnp_Params.put("vnp_BankCode", bankCode);
                }
                
                vnp_Params.put("vnp_TxnRef", vnp_TxnRef);
                vnp_Params.put("vnp_OrderInfo", "Thanh toan don hang:" + vnp_TxnRef);
                vnp_Params.put("vnp_OrderType", orderType);
                String locate = req.getParameter("language");
                
                if (locate != null && !locate.isEmpty())
                {
                        vnp_Params.put("vnp_Locale", locate);
                } else
                {
                        vnp_Params.put("vnp_Locale", "vn");
                }
                
                vnp_Params.put("vnp_ReturnUrl", Config.vnp_ReturnUrl);
                vnp_Params.put("vnp_IpAddr", vnp_IpAddr);

                Calendar cld = Calendar.getInstance(TimeZone.getTimeZone("Etc/GMT+7"));
                SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
                String vnp_CreateDate = formatter.format(cld.getTime());
                vnp_Params.put("vnp_CreateDate", vnp_CreateDate);

                cld.add(Calendar.MINUTE, 15);
                String vnp_ExpireDate = formatter.format(cld.getTime());
                vnp_Params.put("vnp_ExpireDate", vnp_ExpireDate);

                List fieldNames = new ArrayList(vnp_Params.keySet());
                Collections.sort(fieldNames);
                StringBuilder hashData = new StringBuilder();
                StringBuilder query = new StringBuilder();
                Iterator itr = fieldNames.iterator();
                
                while (itr.hasNext())
                {
                        String fieldName = (String) itr.next();
                        String fieldValue = (String) vnp_Params.get(fieldName);
                        if ((fieldValue != null) && (fieldValue.length() > 0))
                        {
                                //Build hash data
                                hashData.append(fieldName);
                                hashData.append('=');
                                hashData.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                                //Build query
                                query.append(URLEncoder.encode(fieldName, StandardCharsets.US_ASCII.toString()));
                                query.append('=');
                                query.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                                if (itr.hasNext())
                                {
                                        query.append('&');
                                        hashData.append('&');
                                }
                        }
                }
                String queryUrl = query.toString();
                String vnp_SecureHash = Config.hmacSHA512(Config.secretKey, hashData.toString());
                queryUrl += "&vnp_SecureHash=" + vnp_SecureHash;
                String paymentUrl = Config.vnp_PayUrl + "?" + queryUrl;
                insertPayment(u.getId(), vnp_TxnRef, amount, paymentUrl);
                System.out.println(paymentUrl);
                res.sendRedirect(paymentUrl);
        }

        private void insertPayment(int userId, String vnp_TxnRef, double amount, String link)
        {
                Pay pay = new Pay();
                PaymentDBContext paydbc = new PaymentDBContext();
                
                Timestamp now = new Timestamp(System.currentTimeMillis());
                
                UUID uuid = UUID.randomUUID();
                String code = uuid.toString();
                
                pay.setVnp_TxnRef(vnp_TxnRef);
                pay.setPayment_code(code);
                pay.setAmount(amount / 100);
                pay.setLink(link);
                pay.setStatus(false);
                pay.setCreated_at(now);
                pay.setUpdated_at(now);
                pay.setIsDeleted(false);
                pay.setTransId(1);
                pay.setUserId(userId);
                
                paydbc.insert(pay);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
