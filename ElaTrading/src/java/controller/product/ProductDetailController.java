/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.product;

import dal.auth.UserDBContext;
import dal.product.CategoryDBContext;
import dal.product.ProductDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.UUID;
import model.product.Product;
import utils.Validate;

/**
 *
 * @author admin
 */
public class ProductDetailController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet ProductDetailController</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet ProductDetailController at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                UserDBContext udb = new UserDBContext();
                ProductDBContext pdb = new ProductDBContext();
                CategoryDBContext cdb = new CategoryDBContext();
                
                Validate validate = new Validate();
                try
                {
                        String productcode = validate.getFieldAjax(request, "code", true, "");

                        UUID uuid = UUID.randomUUID();
                        String codetrading = uuid.toString();

                        Product p = pdb.get("code", productcode);
                        p.setUser(udb.get("id", String.valueOf(p.getUserID())));
                        p.setCategory(cdb.get("id", String.valueOf(p.getCategoryID())));

                        request.setAttribute("product", p);
                        request.setAttribute("codetrading", codetrading);
                        request.getRequestDispatcher("/views/product/product_detail.jsp").forward(request, response);
                } catch (Exception ex)
                {
                        request.setAttribute("error", "Không thể thực hiện hành động!");
                        request.getRequestDispatcher("/views/error/error_denied.jsp").forward(request, response);
                }
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
