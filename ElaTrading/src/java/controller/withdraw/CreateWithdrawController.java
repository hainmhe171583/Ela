/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.withdraw;

import utils.MultiProcess;
import dal.auth.UserDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;
import model.auth.User;
import model.common.Single;
import model.common._Deque;
import model.withdraw.Withdraw;

/**
 *
 * @author hai20
 */
public class CreateWithdrawController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet NewWithdrawController</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet NewWithdrawController at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                request.getRequestDispatcher("/views/payment/withdraw.jsp").forward(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                HttpSession ses = request.getSession();
                User u = (User) ses.getAttribute("user");

                UUID uuid = UUID.randomUUID();
                String code = uuid.toString();
                String info = request.getParameter("info");
                double total = 0;

                if (info == null || info.equals(""))
                {
                        request.setAttribute("mss", "Nhập hết các trường!");
                        doGet(request, response);
                        return;
                }

                try
                {
                        total = Double.parseDouble(request.getParameter("total").replace(",", ""));

                } catch (Exception e)
                {
                        request.setAttribute("mss", "Nhập sai kiểu");
                        doGet(request, response);
                        return;
                }
                long milis = System.currentTimeMillis();
                Date date = new Date();
                milis = date.getTime();
                Timestamp now = new Timestamp(milis);

                Withdraw withdraw = new Withdraw();
                withdraw.setCode(code);
                withdraw.setTotal(total);
                withdraw.setUserInfo(info);
                withdraw.setUserID(u.getId());
                withdraw.setStatusID(1);
                withdraw.setCreated_at(now);
                withdraw.setUpdated_at(now);
                if (u.getBalance() <= total)
                {
                        request.setAttribute("mss", "so du khong du de rut tien!!");
                        request.getRequestDispatcher("/views/payment/withdraw.jsp").forward(request, response);
                        return;
                } else
                {
                        _Deque.getMyrequest().add(new Single(u.getId(), total * (-1), withdraw, 5));
                }

                //xu ly da luong
                MultiProcess mt = new MultiProcess();
                mt.multiPro();

                //response.sendRedirect("withdrawdetail?code=" + code);
                request.getRequestDispatcher("/views/payment/withdraw.jsp").forward(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
