/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.action.product;

import dal.product.ProductDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import model.auth.User;
import model.product.Product;
import utils.Validate;

/**
 *
 * @author hai20
 */
public class UpdateProductController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet UpdateProductController</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet UpdateProductController at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                Validate validate = new Validate();
                try
                {
                        HttpSession ses = request.getSession();
                        User u = (User) ses.getAttribute("user");

                        String code = validate.getFieldAjax(request, "code", true, "");

                        ProductDBContext pdb = new ProductDBContext();
                        Product pro = pdb.get("code", code);

                        if (pro == null || pro.getUserID() != u.getId() || pro.getStatusID() != 1 || pro.isIsDeleted() == true)
                        {
                                throw new Exception("Không thể thực hiện chỉnh sửa trên đơn hàng này!");
                        }
                        
                        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
                        String formattedNumber = decimalFormat.format(pro._getPrice());

                        request.setAttribute("id", pro.getId());
                        request.setAttribute("code", code);
                        request.setAttribute("pro", pro);
                        request.setAttribute("price", formattedNumber.substring(0, formattedNumber.length() - 3));
                        request.setAttribute("cont", pro.getContact());
                        request.setAttribute("link", pro.getLink());

                        request.getRequestDispatcher("/views/saler/product/updateProduct.jsp").forward(request, response);

                } catch (Exception er)
                {
                        request.setAttribute("error", er.getMessage());
                        request.getRequestDispatcher("/views/error/error_denied.jsp").forward(request, response);
                }
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                Validate validate = new Validate();
                try
                {
                        String code = validate.getFieldAjax(request, "code", true, "Bạn chưa nhập đủ thông tin!");
                        String name = validate.getFieldAjax(request, "pTitle", true, "Bạn chưa nhập đủ thông tin!");
                        String des = validate.getFieldAjax(request, "pDes", true, "Bạn chưa nhập đủ thông tin!");
                        String cont = validate.getFieldAjax(request, "cont", true, "Bạn chưa nhập đủ thông tin!");
                        String data = validate.getFieldAjax(request, "data", true, "Bạn chưa nhập đủ thông tin!");
                        String _priv = validate.getFieldAjax(request, "priv", true, "Bạn chưa nhập đủ thông tin!");
                        String _price = validate.getFieldAjax(request, "price", true, "Bạn chưa nhập đủ thông tin!");
                        String _cid = validate.getFieldAjax(request, "category", true, "Bạn chưa nhập đủ thông tin!");

                        ProductDBContext pdb = new ProductDBContext();
                        Product pro = pdb.get("code", code);

                        if (pro.getStatusID() != 1)
                        {
                                throw new Exception("Không thể thực hiện hành động!");
                        }

                        boolean priv = Integer.parseInt(_priv) == 1;
                        
                        String convert  = _price.replace(",", "");
                        double price = validate.fieldDouble(convert, "Giá sản phẩm bạn nhập không đúng định dạng!");

                        int cId = validate.fieldInt(_cid, "Bạn vẫn chưa chọn mục Phân loại!");

                        Timestamp updated_at = new Timestamp(System.currentTimeMillis());

                        pro.setProductName(name);
                        pro.setPrice(price);
                        pro.setDescribe(des);
                        pro.setContact(cont);
                        pro.setIsPrivate(priv);
                        pro.setDataproduct(data);
                        pro.setUpdated_at(updated_at);
                        pro.setCategoryID(cId);

                        pdb.update(pro);

                        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
                        String formattedNumber = decimalFormat.format(pro._getPrice());
                        request.setAttribute("price", formattedNumber.substring(0, formattedNumber.length() - 3));

                        request.setAttribute("id", pro.getId());
                        request.setAttribute("code", code);
                        request.setAttribute("link", pro.getLink());
                        request.setAttribute("pro", pro);

                        String json = "{\"success\": true, \"message\": \"Sản phẩm đã được cập nhật thành công!\"}";
                        request.setAttribute("jsonData", json);
                        request.getRequestDispatcher("/views/saler/product/updateProduct.jsp").forward(request, response);
                        
                } catch (Exception er)
                {
                        String json = "{\"success\": false, \"message\": \"" + er.getMessage() + "\"}";
                        request.setAttribute("jsonData", json);
                        request.getRequestDispatcher("/views/saler/product/updateProduct.jsp").forward(request, response);
                }
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
