package controller.action.product;

import dal.product.ProductDBContext;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.PrintWriter;
import model.auth.User;
import model.product.Product;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Admin
 */
public class ExcelProductsController extends HttpServlet
{

        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                request.setCharacterEncoding("UTF-8");
                response.setContentType("text/html; charset=UTF-8");

                ProductDBContext pdb = new ProductDBContext();
                HttpSession session = request.getSession();
                User u = (User) session.getAttribute("user");
                int id = u.getId();
                List<Product> list = pdb.execlProductSeller(id);
                List<Product> filteredList = new ArrayList<>();
                for (Product product : list)
                {
                        String statusName = "";
                        switch (product.getStatus().getId())
                        {
                                case 1:
                                        statusName = "Chưa bán";
                                        break;
                                case 2:
                                        statusName = "Đang bán";
                                        break;
                                case 3:
                                        statusName = "Bị huỷ";
                                        break;
                                case 4:
                                        statusName = "Đã bán";
                                        break;
                                case 5:
                                        statusName = "Bị khiếu nại";
                                        break;
                                case 6:
                                        statusName = "Other";
                                        break;
                                default:
                                        break;
                        }
                        if (!statusName.isEmpty())
                        {
                                product.getStatus().setStatusName(statusName);
                                filteredList.add(product);
                        }
                }

                String fileType = request.getParameter("fileType");
                if ("excel".equals(fileType))
                {
                        exportToExcel(list, response);
                } else if ("txt".equals(fileType))
                {
                        exportToText(list, response);
                }
        }

        private void exportToText(List<Product> productList, HttpServletResponse response) throws IOException
        {
                String filename = "Danh sach san pham dang ban.txt";
                response.setContentType("text/plain");
                response.setHeader("Content-Disposition", "attachment; filename=" + filename);
                try ( PrintWriter out = response.getWriter())
                {
                        for (Product product : productList)
                        {
                                out.println("STT: " + product.getId());
                                out.println("Tên sản phẩm: " + product.getProductName());
                                out.println("Mô tả: " + product.getDescribe());
                                out.println("Giá: " + product.getPrice());
                                out.println("Số lượng: " + product.getStock());
                                out.println("Link: " + product.getLink());
                                out.println("Liên hệ: " + product.getContact());
                                out.println("Thông tin ẩn: " + product.getDataproduct());
                                out.println("Trạng thái: " + product.getStatus().getStatusName());
                                out.println("Thể loại: " + product._getCategory().getCategoryName());
                                out.println("Ngày tạo: " + product.getCreated_at());
                                out.println("Ngày cập nhật: " + product.getUpdated_at());
                                out.println("--------------------------------------");
                        }
                }

        }

        private void exportToExcel(List<Product> productList, HttpServletResponse response) throws IOException
        {

                String fileName = "Danh sach san pham dang ban.xlsx";
                response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                XSSFWorkbook workbook = new XSSFWorkbook();
                XSSFSheet workSheet = workbook.createSheet("1");

                XSSFRow row = workSheet.createRow(0);
                String[] headers =
                {
                        "STT", "Code", "Tên sản phẩm", "Mô tả", "Giá", "Số lượng", "Link", "Liên hệ", "Thông tin ẩn", "Trạng thái", "Thể loại", "Ngày tạo ", "Ngày cập nhật"
                };
                for (int i = 0; i < headers.length; i++)
                {
                        XSSFCell cell = row.createCell(i);
                        cell.setCellValue(headers[i]);
                }

                int rowNum = 1;
                for (Product product : productList)
                {
                        System.out.println("Elaina-chan");
                        row = workSheet.createRow(rowNum++);
                        row.createCell(0).setCellValue(product.getId());
                        row.createCell(1).setCellValue(product.getCode());
                        row.createCell(2).setCellValue(product.getProductName());
                        row.createCell(3).setCellValue(product.getDescribe());
                        row.createCell(4).setCellValue(product.getPrice());
                        row.createCell(5).setCellValue(product.getStock());
                        row.createCell(6).setCellValue(product.getLink());
                        row.createCell(7).setCellValue(product.getContact());
                        row.createCell(8).setCellValue(product.getDataproduct());
                        row.createCell(9).setCellValue(product.getStatus().getStatusName());
                        row.createCell(10).setCellValue(product._getCategory().getCategoryName());
                        row.createCell(11).setCellValue(product.getCreated_at().toString());
                        row.createCell(12).setCellValue(product.getUpdated_at().toString());
                }

                try ( ServletOutputStream outputStream = response.getOutputStream())
                {
                        workbook.write(outputStream);
                } finally
                {
                        workbook.close();
                }
        }

        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                processRequest(request, response);
        }

        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                processRequest(request, response);
        }

        @Override
        public String getServletInfo()
        {
                return "Short description";
        }
}
