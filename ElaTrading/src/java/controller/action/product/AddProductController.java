/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.action.product;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.UUID;
import model.auth.User;
import model.common.Single;
import model.common._Deque;
import model.product.Product;
import utils.MultiProcess;
import utils.Validate;

/**
 *
 * @author hai20
 */
public class AddProductController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet Create</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet Create at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                //processRequest(request, response);
                request.getRequestDispatcher("/views/saler/product/addProduct.jsp").forward(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                //processRequest(request, response);
                Validate validate = new Validate();
                try
                {
                        HttpSession ses = request.getSession();
                        User u = (User) ses.getAttribute("user");

                        UUID uuid = UUID.randomUUID();
                        String code = uuid.toString();

                        String name = validate.getFieldAjax(request, "pTitle", true, "Bạn chưa nhập đủ thông tin!");
                        String des = validate.getFieldAjax(request, "pDes", true, "Bạn chưa nhập đủ thông tin!");
                        String cont = validate.getFieldAjax(request, "cont", true, "Bạn chưa nhập đủ thông tin!");
                        String data = validate.getFieldAjax(request, "data", true, "Bạn chưa nhập đủ thông tin!");
                        String _priv = validate.getFieldAjax(request, "priv", true, "Bạn chưa nhập đủ thông tin!");
                        String _price = validate.getFieldAjax(request, "price", true, "Bạn chưa nhập đủ thông tin!");
                        String _cid = validate.getFieldAjax(request, "category", true, "Bạn chưa nhập đủ thông tin!");

                        boolean priv = Integer.parseInt(_priv) == 1;
                        
                        String convert  = _price.replace(",", "");
                        double price = validate.fieldDouble(convert, "Giá sản phẩm bạn nhập không đúng định dạng!");

                        int cId = validate.fieldInt(_cid, "Bạn vẫn chưa chọn mục Phân loại!");

                        if (u.getBalance() < 5000)
                        {
                                throw new Exception("Số dư không đủ để tạo đơn hàng!");
                                
                        } else
                        {
                                Timestamp created_at = new Timestamp(System.currentTimeMillis());

                                Product p = new Product();
                                p.setProductName(name);
                                p.setCode(code);
                                p.setPrice(price);
                                p.setStock(1);
                                p.setDescribe(des);
                                p.setContact(cont);
                                //fix
                                p.setLink("http://localhost:9999/ElaTrading/productdetail?code=" + code);
                                p.setIsPrivate(priv);
                                p.setDataproduct(data);
                                p.setStatusID(1);
                                p.setCategoryID(cId);
                                p.setUserID(u.getId());
                                p.setCreated_at(created_at);
                                p.setCreated_by(u.getId());
                                p.setUpdated_at(created_at);
                                p.setUpdated_by(u.getId());

                                _Deque.getMyrequest().add(new Single(u.getId(), -5000, p, 2));
                                //xu ly da luong
                                MultiProcess mt = new MultiProcess();
                                mt.multiPro();

                                response.sendRedirect("update?code=" + code);
                        }
                } catch (Exception er)
                {
                        String json = "{\"success\": false, \"message\": \"" + er.getMessage() + "\"}";
                        request.setAttribute("jsonData", json);
                        request.getRequestDispatcher("/views/saler/product/addProduct.jsp").forward(request, response);
                }

        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
