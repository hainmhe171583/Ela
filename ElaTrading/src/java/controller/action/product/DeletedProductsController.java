/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.action.product;

import dal.product.CategoryDBContext;
import dal.product.ProductDBContext;
import dal.status.StatusDBContext;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.auth.User;
import model.product.Category;
import model.product.Product;
import model.status.Status;

/**
 *
 * @author Admin
 */
public class DeletedProductsController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                CategoryDBContext cdb = new CategoryDBContext();
                List<Category> listC = cdb.getAll();
                request.setAttribute("listCC", listC);
                request.getRequestDispatcher("/views/saler/product/productList.jsp").forward(request, response);
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                HttpSession session = request.getSession();
                User u = (User) session.getAttribute("user");
                if (u == null)
                {
                        response.sendRedirect("login");
                        return;
                }
                String index = request.getParameter("index");
                if (index == null)
                {
                        index = "1";
                }
                int indexPage = Integer.parseInt(index);
                ProductDBContext pdb = new ProductDBContext();
                CategoryDBContext cdb = new CategoryDBContext();
                StatusDBContext sdb = new StatusDBContext();
                int id = u.getId();
                List<Category> listC = cdb.getAll();
                List<Status> listS = sdb.getAll();
                int countListProduct = pdb.countAllProductByStockDeleted(id);

                int allProductBySellerID = pdb.countAllProductByStockDeleted(id);
                int endPage = allProductBySellerID / 5;
                if (allProductBySellerID % 5 != 0)
                {
                        endPage++;
                }
                List<Product> listFilter = pdb.filterListProductDeleted(indexPage, id);
                request.setAttribute("tagRestore", indexPage);
                request.setAttribute("endPageRestore", endPage);
                request.setAttribute("listD", listFilter);
                request.setAttribute("listS", listS);
                request.setAttribute("listCC", listC);
                request.setAttribute("index", index);
                request.setAttribute("countListProductDeleted", countListProduct);
                request.getRequestDispatcher("/views/saler/product/productList.jsp").forward(request, response);

        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
