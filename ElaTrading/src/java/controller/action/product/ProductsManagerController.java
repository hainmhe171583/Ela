/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.action.product;

import dal.product.CategoryDBContext;
import dal.product.ProductDBContext;
import dal.status.StatusDBContext;
import dal.trading.TradingDBContext;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.auth.User;
import model.product.Category;
import model.product.Product;
import model.status.Status;
import model.trading.Trading;

/**
 *
 * @author Admin
 */
public class ProductsManagerController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                request.getRequestDispatcher("/views/saler/product/productList.jsp").forward(request, response);
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                request.setCharacterEncoding("UTF-8");
                response.setContentType("text/html; charset=UTF-8");

                HttpSession session = request.getSession();
                User u = (User) session.getAttribute("user");
                if (u == null)
                {
                        response.sendRedirect("/ElaTrading/login");
                        return;
                }
                String index = request.getParameter("index");
                if (index == null)
                {
                        index = "1";
                }
                String status = request.getParameter("status");
                int indexPage = Integer.parseInt(index);
                ProductDBContext pdb = new ProductDBContext();
                CategoryDBContext cdb = new CategoryDBContext();
                StatusDBContext sdb = new StatusDBContext();
                TradingDBContext tdb = new TradingDBContext();
                int id = u.getId();
                List<Category> listC = cdb.getAll();
                List<Status> listS = sdb.getAll();
                List<Trading> listT = tdb.getAll(id);
                int countListProduct = pdb.countProductListSeller(id);

                if (status == null)
                {
                        status = "All";
                }
                if ("DaBan".equals(status))
                {
                        status = "DaBan";
                }
                if ("ChuaBan".equals(status))
                {
                        status = "ChuaBan";
                }
                if ("DaXoa".equals(status))
                {
                        status = "DaXoa";
                }
                if ("BiHuy".equals(status))
                {
                        status = "BiHuy";
                }
                if ("DangBan".equals(status))
                {
                        status = "DangBan";
                }
                String mess = request.getParameter("mess");
                request.setAttribute("mess", mess);
                if ("BiHuy".equals(status))
                {
                        int sID = 3;
                        int allProductBySellerID = pdb.countAllProductByStatus(id, sID);
                        int endPage = allProductBySellerID / 5;
                        if (allProductBySellerID % 5 != 0)
                        {
                                endPage++;
                        }
                        List<Product> listFilter = pdb.filterListProduct(indexPage, id, sID);
                        
                        request.setAttribute("tag", indexPage);
                        request.setAttribute("endP", endPage);
                        request.setAttribute("listP", listFilter);
                        request.setAttribute("listS", listS);
                        request.setAttribute("listCC", listC);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.setAttribute("countListProduct", countListProduct);
                        request.getRequestDispatcher("/views/saler/product/productList.jsp").forward(request, response);
                        return;
                }

                if ("DaBan".equals(status))
                {
                        int sID = 4;
                        int allProductBySellerID = pdb.countAllProductByStatus(id, sID);
                        int endPage = allProductBySellerID / 5;
                        if (allProductBySellerID % 5 != 0)
                        {
                                endPage++;
                        }
                        List<Product> listFilter = pdb.filterListProduct(indexPage, id, sID);
                        request.setAttribute("tag", indexPage);
                        request.setAttribute("endP", endPage);
                        request.setAttribute("listP", listFilter);
                        request.setAttribute("listS", listS);
                        request.setAttribute("listCC", listC);
                        request.setAttribute("listT", listT);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.setAttribute("countListProduct", countListProduct);
                        request.getRequestDispatcher("/views/saler/product/productList.jsp").forward(request, response);
                        return;
                }
                if ("ChuaBan".equals(status))
                {
                        int sID = 1;
                        int allProductBySellerID = pdb.countAllProductByStatus(id, sID);
                        int endPage = allProductBySellerID / 5;
                        if (allProductBySellerID % 5 != 0)
                        {
                                endPage++;
                        }
                        List<Product> listFilter = pdb.filterListProduct(indexPage, id, sID);
                        request.setAttribute("tag", indexPage);
                        request.setAttribute("endP", endPage);
                        request.setAttribute("listP", listFilter);
                        request.setAttribute("listS", listS);
                        request.setAttribute("listCC", listC);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.setAttribute("countListProduct", countListProduct);
                        request.getRequestDispatcher("/views/saler/product/productList.jsp").forward(request, response);
                        return;
                }
                if ("KhieuNai".equals(status))
                {
                        int sID = 5;
                        int allProductBySellerID = pdb.countAllProductByStatus(id, sID);
                        int endPage = allProductBySellerID / 5;
                        if (allProductBySellerID % 5 != 0)
                        {
                                endPage++;
                        }
                        List<Product> listFilter = pdb.filterListProduct(indexPage, id, sID);
                        request.setAttribute("tag", indexPage);
                        request.setAttribute("endP", endPage);
                        request.setAttribute("listP", listFilter);
                        request.setAttribute("listS", listS);
                        request.setAttribute("listCC", listC);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.setAttribute("countListProduct", countListProduct);
                        request.getRequestDispatcher("/views/saler/product/productList.jsp").forward(request, response);
                        return;
                }
                if ("DangBan".equals(status))
                {
                        int sID = 2;
                        int allProductBySellerID = pdb.countAllProductByStatus(id, sID);
                        int endPage = allProductBySellerID / 5;
                        if (allProductBySellerID % 5 != 0)
                        {
                                endPage++;
                        }
                        List<Product> listFilter = pdb.filterListProduct(indexPage, id, sID);
                        request.setAttribute("tag", indexPage);
                        request.setAttribute("endP", endPage);
                        request.setAttribute("listP", listFilter);
                        request.setAttribute("listS", listS);
                        request.setAttribute("listCC", listC);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.setAttribute("countListProduct", countListProduct);
                        request.getRequestDispatcher("/views/saler/product/productList.jsp").forward(request, response);
                        return;
                }

                if ("All".equals(status) || "".equals(status))
                {
                        int all = pdb.countAllProductBySellerID(id);
                        int end = all / 5;
                        if (all % 5 != 0)
                        {
                                end++;
                        }
                        List<Product> list = pdb.pagingProduct(indexPage, id);
                        request.setAttribute("endP", end);
                        request.setAttribute("listP", list);
                        request.setAttribute("listS", listS);
                        request.setAttribute("listCC", listC);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.setAttribute("countListProduct", countListProduct);
                        request.getRequestDispatcher("/views/saler/product/productList.jsp").forward(request, response);
                        return;
                }

        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
