package controller.action.product;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
import dal.product.ProductDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Timestamp;
import model.auth.User;
import model.product.Product;
import utils.Validate;

/**
 *
 * @author hai20
 */
public class DeleteProductController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet DeleteProductController</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet DeleteProductController at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                //processRequest(request, response);
                Validate validate = new Validate();
                try
                {
                        HttpSession ses = request.getSession();
                        User u = (User) ses.getAttribute("user");
                        
                        String code = validate.getFieldAjax(request, "code", true, "");
                        ProductDBContext pdb = new ProductDBContext();
                        Product pro = pdb.get("code", code);

                        if (pro.getStatusID() != 1 || pro.getUserID() != u.getId())
                        {
                                throw new Exception();
                        }
                        
                        Timestamp deleted_at = new Timestamp(System.currentTimeMillis());

                        pro.setIsDeleted(true);
                        pro.setDeleted_at(deleted_at);
                        pdb.update(pro);

                        response.sendRedirect("products");
                        
                } catch (Exception er)
                {
                        request.setAttribute("error", "Không thể thực hiện hành động!");
                        request.getRequestDispatcher("/views/error/error_denied.jsp").forward(request, response);               
                }

        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
