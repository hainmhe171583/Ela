/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.action.user;

import com.google.gson.Gson;
import dal.auth.RatingDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.UUID;
import model.auth.Rating;
import model.auth.User;

/**
 *
 * @author admin
 */
public class AddFeadbackController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet RatingController</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet RatingController at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                //processRequest(request, response);
                request.setAttribute("error", "1");
                request.getRequestDispatcher("/views/error/error_denied.jsp").forward(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                //processRequest(request, response);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                PrintWriter out = response.getWriter();
                
                LocalDateTime currentDateTime = LocalDateTime.now();
                Timestamp createdAt = Timestamp.valueOf(currentDateTime);
                try
                {
                        
                        HttpSession session = request.getSession();
                        User u = (User) session.getAttribute("user");
                        
                        String rating_raw = request.getParameter("rating");
                        if (rating_raw == null)
                        {
                                out.print("{\"success\": false, \"message\": \"Invalid parameters ratingvalue\"}");
                                return;
                        }
                        int rating = Integer.parseInt(rating_raw);
                        String comment = request.getParameter("comment");
                        if (comment == null)
                        {
                                out.print("{\"success\": false, \"message\": \"Invalid user\"}");
                                return;
                        }
                                
                        Rating newRating = new Rating();
                        newRating.setCode(UUID.randomUUID().toString());
                        newRating.setRate(rating);
                        newRating.setComment(comment);
                        newRating.setUserID(u.getId());
                        newRating.setUser(u);
                        newRating.setCreated_at(createdAt);
                        newRating.setIsDeleted(false);
                        newRating.setCreated_by(u.getId());
                        
                        RatingDBContext rdb = new RatingDBContext();
                        rdb.insert(newRating);
                        
                        Gson gson = new Gson();
                        String json = gson.toJson(newRating);
                        out.print("{\"success\": true, \"data\": " + json + "}");
                } catch (NumberFormatException e)
                {
                        out.print("{\"success\": false, \"message\": \"Invalid parameter format\"}");
                } catch (Exception e)
                {
                        e.printStackTrace();
                        out.print("{\"success\": false, \"message\": \"Error inserting rating into the database.\"}");
                }
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
