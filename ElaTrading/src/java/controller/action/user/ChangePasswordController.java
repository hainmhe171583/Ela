package controller.action.user;

import dal.auth.UserDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.auth.User;
import utils.HashPass;
import utils.Validate;

public class ChangePasswordController extends HttpServlet
{

        Validate check = new Validate();
        HashPass hash = new HashPass();

        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet ActionController</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet ActionController at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                request.getRequestDispatcher("/views/user/changepassword.jsp").forward(request, response);
        }

        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");

                HttpSession session = request.getSession();
                User u = (User) session.getAttribute("user");
                int id = u.getId();

                String oldpassword = request.getParameter("oldpassword");
                String newpassword = request.getParameter("newpassword");
                String confirmpassword = request.getParameter("confirmpassword");
                
                UserDBContext userDao = new UserDBContext();
                String getPassFromDatabase = userDao.getById(id).getPassword();
                String hashedOldPassword = hash.hashPassword(oldpassword);

                if (oldpassword.trim().isEmpty() || newpassword.trim().isEmpty() || confirmpassword.trim().isEmpty())
                {
                        request.setAttribute("mess", "Mật khẩu không được chứa khoảng trắng");
                        request.getRequestDispatcher("views/user/changepassword.jsp").forward(request, response);

                } else if (!hashedOldPassword.equals(getPassFromDatabase))
                {
                        request.setAttribute("mess", "Mật khẩu cũ không đúng");
                        request.getRequestDispatcher("views/user/changepassword.jsp").forward(request, response);

                } else if (!newpassword.trim().equals(confirmpassword.trim()))
                {
                        request.setAttribute("mess", "Mật khẩu xác nhận không đúng");
                        request.getRequestDispatcher("views/user/changepassword.jsp").forward(request, response);
                } else if (!check.isValidPassword(newpassword))
                {
                        request.setAttribute("mess", "Mật khẩu phải từ 8-20 ký tự, chứa ít nhất 1 ký tự chữ thường, in hoa, đặc biệt(@,#,$,%,^,&,+,=)");
                        request.getRequestDispatcher("views/user/changepassword.jsp").forward(request, response);
                } else
                {
                        userDao.changePassword(hash.hashPassword(confirmpassword), id);
                        session.removeAttribute("user");

                        response.sendRedirect("/ElaTrading/login");
                }
        }

        @Override
        public String getServletInfo()
        {
                return "ChangePasswordController Servlet";
        }
}
