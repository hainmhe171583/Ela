/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.action.user;

import dal.auth.UserDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.auth.User;
import utils.Validate;

/**
 *
 * @author Admin
 */
public class EditProfileController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet ActionController</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet ActionController at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                request.getRequestDispatcher("/views/user/editprofile.jsp").forward(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                Validate validate = new Validate();
                try
                {
                        HttpSession session = request.getSession();
                        User u = (User) session.getAttribute("user");
                        int id = u.getId();

                        String username = validate.getFieldAjax(request, "username", false, "");
                        String email = validate.getFieldAjax(request, "email", false, "");
                        String phone = validate.getFieldAjax(request, "phone", false, "");
                        UserDBContext userDao = new UserDBContext();

                        if (!validate.checkPhone(phone) && !u.getPhone().equals(phone))
                        {
                                throw new Exception("Số điện thoại không hợp lệ hoặc đã tồn tại");
                        }

                        userDao.editProfile(username, email, phone, id);
                        User updatedUser = userDao.getUserInfor(id);
                        //fix
                        String json = "{\"success\": true, \"message\": \"Thay đổi thông tin thành công\"}";
                
                        request.setAttribute("jsonData", json);
                        session.setAttribute("user", updatedUser);
                        request.getRequestDispatcher("/views/user/editprofile.jsp").forward(request, response);

                } catch (Exception er)
                {
                        String json = "{\"success\": false, \"message\": \"" + er.getMessage() + "\"}";
                        request.setAttribute("jsonData", json);
                        request.getRequestDispatcher("/views/saler/product/addProduct.jsp").forward(request, response);
                }
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
