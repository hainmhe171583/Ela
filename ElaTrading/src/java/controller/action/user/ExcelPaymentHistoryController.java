/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.action.user;

import dal.payment.PaymentDBContext;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.util.ArrayList;
import model.auth.User;
import model.payment.Pay;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Admin
 */
public class ExcelPaymentHistoryController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                request.setCharacterEncoding("UTF-8");
                response.setContentType("text/html; charset=UTF-8");
                response.setCharacterEncoding("UTF-8");
                PaymentDBContext pdb = new PaymentDBContext();
                HttpSession session = request.getSession();
                User u = (User) session.getAttribute("user");
                int id = u.getId();
                ArrayList<Pay> list = pdb.excelPayment(id);
                String fileType = request.getParameter("fileType");
                if ("excel".equals(fileType))
                {
                        exportToExcel(list, response);
                } else if ("txt".equals(fileType))
                {
                        exportToText(list, response);
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                processRequest(request, response);
        }

        private void exportToExcel(ArrayList<Pay> payList, HttpServletResponse response) throws IOException
        {

                String fileName = "Danh sach lich su giao dich.xlsx";
                response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                XSSFWorkbook workbook = new XSSFWorkbook();
                XSSFSheet workSheet = workbook.createSheet("1");

                XSSFRow row = workSheet.createRow(0);
                String[] headers =
                {
                        "STT", "Tên giao dịch", "Tiền", "Link", "Ngày tạo", "Trạng thái"
                };
                for (int i = 0; i < headers.length; i++)
                {
                        XSSFCell cell = row.createCell(i);
                        cell.setCellValue(headers[i]);
                }
                int rowNum = 1;
                for (Pay payment : payList)
                {
                        row = workSheet.createRow(rowNum++);
                        row.createCell(0).setCellValue(payment.getId());
                        String transactionType = null;
                        switch (payment.getTransId())
                        {
                                case 1:
                                        transactionType = "Nạp tiền";
                                        break;
                                case 2:
                                        transactionType = "Phí tạo đơn hàng";
                                        break;
                                case 3:
                                        transactionType = "Mua hàng";
                                        break;
                                case 4:
                                        transactionType = "Bán hàng";
                                        break;
                                case 5:
                                        transactionType = "Rút tiền";
                                        break;
                                case 6:
                                        transactionType = "Hoàn trả";
                                        break;
                                case 7:
                                        transactionType = "Phí giao dịch";
                                        break;
                        }
                        String status = null;
                        if (payment.isStatus() == true)
                        {
                                status = "Thành công";
                        } else if (payment.isStatus() == false)
                        {
                                status = "Thất bại";
                        }
                        row.createCell(1).setCellValue(transactionType);
                        row.createCell(2).setCellValue(payment.getAmount());
                        row.createCell(3).setCellValue(payment.getLink());
                        row.createCell(4).setCellValue(payment.getCreated_at());
                        row.createCell(5).setCellValue(status);
                        try ( ServletOutputStream outputStream = response.getOutputStream())
                        {
                                workbook.write(outputStream);
                        } finally
                        {
                                workbook.close();
                        }
                }
        }

        private void exportToText(ArrayList<Pay> payList, HttpServletResponse response) throws IOException
        {
                String filename = "Danh sach lich su giao dich.txt";
                response.setContentType("text/plain");
                response.setHeader("Content-Disposition", "attachment; filename=" + filename);

                try ( PrintWriter out = response.getWriter())
                {
                        for (Pay payment : payList)
                        {
                                String transactionType = null;
                                switch (payment.getTransId())
                                {
                                        case 1:
                                                transactionType = "Nạp tiền";
                                                break;
                                        case 2:
                                                transactionType = "Phí tạo đơn hàng";
                                                break;
                                        case 3:
                                                transactionType = "Mua hàng";
                                                break;
                                        case 4:
                                                transactionType = "Bán hàng";
                                                break;
                                        case 5:
                                                transactionType = "Rút tiền";
                                                break;
                                        case 6:
                                                transactionType = "Hoàn trả";
                                                break;
                                        case 7:
                                                transactionType = "Phí giao dịch";
                                                break;
                                }
                                String status = null;
                                if (payment.isStatus() == true)
                                {
                                        status = "Thành công";
                                } else if (payment.isStatus() == false)
                                {
                                        status = "Thất bại";
                                }
                                out.println("STT: " + payment.getId());
                                out.println("Tên giao dịch: " + transactionType);
                                out.println("Tiền: " + payment.getAmount());
                                out.println("Người bán: " + payment.getLink());
                                out.println("Trạng thái: " + status);
                                out.println("--------------------------------------");

                        }

                }
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
