/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.trading;

import dal.auth.UserDBContext;
import dal.product.ProductDBContext;
import dal.trading.ReportDBContext;
import dal.trading.TradingDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.UUID;
import model.auth.User;
import model.product.Product;
import model.trading.Report;
import model.trading.Trading;
import utils.Notification;

/**
 *
 * @author admin
 */
public class CreateReportController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet ReportCreateController</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet ReportCreateController at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                PrintWriter out = response.getWriter();
                String tradingCode = request.getParameter("code");
                
                TradingDBContext tdb = new TradingDBContext();
                UserDBContext udb = new UserDBContext();
                Trading trading = tdb.get("code", tradingCode);
                if (trading.getStatusID() == 2)
                {
                        User seller = udb.get("id", String.valueOf(trading.getProduct().getUserID()));
                        
                        request.setAttribute("trading", trading);
                        request.setAttribute("seller", seller);
                        request.getRequestDispatcher("/views/trading/report_create.jsp").forward(request, response);
                } else
                {
                        out.print("Đơn hàng đã được xử lí, không thể tạo báo cáo.");
                }

        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                HttpSession session = request.getSession();
//                PrintWriter out = response.getWriter();
                User u = (User) session.getAttribute("user");
                String titlereport = request.getParameter("titlereport");
//                String customername = request.getParameter("customername");
//                String sellername = request.getParameter("sellername");
//                String codeproduct = request.getParameter("codeproduct");
                String codetrading = request.getParameter("codetrading");
                String contentreport = request.getParameter("contentreport");
                
                UUID uuid = UUID.randomUUID();
                String codereport = uuid.toString();
                
                TradingDBContext tdb = new TradingDBContext();
                Trading t = tdb.get("code", codetrading);
                t.setStatusID(5);
                
                tdb.update(t);
                
                ProductDBContext productDB = new ProductDBContext();
                Product product = t.getProduct();
                product.setStatusID(5);
                
                productDB.update(product);

                Report r = new Report();
                r.setCode(codereport);
                r.setTitle(titlereport);
                r.setDescribe(contentreport);
                r.setStatusID(2);
                r.setTradingID(t.getId());
                r.setUser(u);
                r.setUserID(u.getId());
                r.setCreated_by(u.getId());
                ReportDBContext rdb = new ReportDBContext();
                rdb.createReport(r);
//                Trading trading = tdb.get("code", codetrading);
//                request.setAttribute("trading", trading);
//                request.setAttribute("report", r);
//                String url = "/tradings?proCode=" + t.getProduct().getCode() + "&code=" + t.getCode();

                //notification
                Timestamp created_at = new Timestamp(System.currentTimeMillis());

                String sendFrom = u.getUsername();
                int sendFrom_id = u.getId();
                String subject = product.getProductName();
                String id = String.valueOf(product.getId());
                int sendTo = product.getUserID();

                Notification notification = new Notification();
                notification.Create(sendFrom, sendFrom_id, subject, id, sendTo, created_at, 3);
                
                response.sendRedirect("/ElaTrading/action/tradings");
                
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
