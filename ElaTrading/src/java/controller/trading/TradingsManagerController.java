/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.trading;

import dal.trading.TradingDBContext;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.auth.User;
import model.trading.Trading;

/**
 *
 * @author Admin
 */
public class TradingsManagerController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                request.getRequestDispatcher("/views/user/myorder.jsp").forward(request, response);
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {

                HttpSession session = request.getSession();
                User u = (User) session.getAttribute("user");
                if (u == null)
                {
                        response.sendRedirect("/ElaTrading/login");
                        return;
                }
                String index = request.getParameter("index");
                if (index == null)
                {
                        index = "1";
                }
                int id = u.getId();
                int indexPage = Integer.parseInt(index);
                String status = request.getParameter("status");
                TradingDBContext tdb = new TradingDBContext();
                if (status == null)
                {
                        status = "All";
                }
                if ("DaMua".equals(status))
                {
                        status = "DaMua";
                }
                if ("BiHuy".equals(status))
                {
                        status = "BiHuy";
                }
                if ("DangKhieuNai".equals(status))
                {
                        status = "DangKhieuNai";
                }
                if ("DangMua".equals(status))
                {
                        status = "DangMua";
                }
                String messBuy = request.getParameter("messBuy");
                request.setAttribute("messBuy", messBuy);
                if ("All".equals(status) || "".equals(status))
                {
                        int allProductBySellerID = tdb.countAllProductByTrading(id);
                        int endPage = allProductBySellerID / 5;
                        if (allProductBySellerID % 5 != 0)
                        {
                                endPage++;
                        }
                        List<Trading> listFilter = tdb.pagingProductTrading(indexPage, id);
                        request.setAttribute("tag", indexPage);
                        request.setAttribute("endP", endPage);
                        request.setAttribute("listP", listFilter);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.setAttribute("countList", allProductBySellerID);
                        request.getRequestDispatcher("/views/trading/trading_list.jsp").forward(request, response);
                        return;
                }
                if ("DaMua".equals(status))
                {
                        int sID = 4;
                        int allProductBySellerID = tdb.countAllProductByStatusIDTrading(id, sID);
                        int endPage = allProductBySellerID / 5;
                        if (allProductBySellerID % 5 != 0)
                        {
                                endPage++;
                        }
                        List<Trading> listFilter = tdb.filterListProductTrading(indexPage, id, sID);

                        request.setAttribute("tag", indexPage);
                        request.setAttribute("endP", endPage);
                        request.setAttribute("listP", listFilter);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.setAttribute("countList", allProductBySellerID);
                        request.getRequestDispatcher("/views/trading/trading_list.jsp").forward(request, response);
                        return;
                }
                if ("BiHuy".equals(status))
                {
                        int sID = 3;
                        int allProductBySellerID = tdb.countAllProductByStatusIDTrading(id, sID);
                        int endPage = allProductBySellerID / 5;
                        if (allProductBySellerID % 5 != 0)
                        {
                                endPage++;
                        }
                        List<Trading> listFilter = tdb.filterListProductTrading(indexPage, id, sID);
                        request.setAttribute("tag", indexPage);
                        request.setAttribute("endP", endPage);
                        request.setAttribute("listP", listFilter);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.getRequestDispatcher("/views/trading/trading_list.jsp").forward(request, response);
                        return;
                }
                if ("DangKhieuNai".equals(status))
                {
                        int sID = 5;
                        int allProductBySellerID = tdb.countAllProductByStatusIDTrading(id, sID);
                        int endPage = allProductBySellerID / 5;
                        if (allProductBySellerID % 5 != 0)
                        {
                                endPage++;
                        }
                        List<Trading> listFilter = tdb.filterListProductTrading(indexPage, id, sID);
                        request.setAttribute("tag", indexPage);
                        request.setAttribute("endP", endPage);
                        request.setAttribute("listP", listFilter);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.getRequestDispatcher("/views/trading/trading_list.jsp").forward(request, response);
                        return;
                }
                if ("DangMua".equals(status))
                {
                        int sID = 2;
                        int allProductBySellerID = tdb.countAllProductByStatusIDTrading(id, sID);
                        int endPage = allProductBySellerID / 5;
                        if (allProductBySellerID % 5 != 0)
                        {
                                endPage++;
                        }
                        List<Trading> listFilter = tdb.filterListProductTrading(indexPage, id, sID);
                        request.setAttribute("tag", indexPage);
                        request.setAttribute("endP", endPage);
                        request.setAttribute("listP", listFilter);
                        request.setAttribute("status", status);
                        request.setAttribute("index", index);
                        request.getRequestDispatcher("/views/trading/trading_list.jsp").forward(request, response);
                        return;
                }

        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
