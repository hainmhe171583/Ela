package controller.trading;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
import dal.product.ProductDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Timestamp;
import model.auth.User;
import model.common.Single;
import model.common._Deque;
import model.product.Product;
import model.trading.Trading;
import utils.MultiProcess;
import utils.Notification;
import utils.Validate;

/**
 *
 * @author hai20
 */
public class CreateTradingController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet Trading</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet Trading at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                //processRequest(request, response);
                request.setAttribute("error", "1");
                request.getRequestDispatcher("/views/error/error_denied.jsp").forward(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                //processRequest(request, response);
                Validate validate = new Validate();
                try
                {
                        HttpSession ses = request.getSession();
                        User u = (User) ses.getAttribute("user");
                        String proCode = validate.getFieldAjax(request, "proCode", true, "");
                        String code = validate.getFieldAjax(request, "code", true, "");
                        ProductDBContext pdb = new ProductDBContext();
                        Product pro = pdb.get("code", proCode);

                        if (pro == null || pro.getStatusID() != 1)
                        {
                                throw new Exception();
                                //huy sua
                        }

                        if (pro._getPrice() > u.getBalance())
                        {
                                //fix
                                String json = "{\"success\": false, \"message\": \"Số dư của bạn không đủ để thực hiện giao dịch!\"}";
                                response.setContentType("application/json");
                                response.setCharacterEncoding("UTF-8");
                                response.getWriter().write(json);

                                return;
                        }

                        Timestamp created_at = new Timestamp(System.currentTimeMillis());

                        //Notification
                        String sendFrom = u.getUsername();
                        int sendFrom_id = u.getId();
                        String subject = pro.getProductName();
                        String id = String.valueOf(pro.getId());
                        int sendTo = pro.getUserID();

                        Notification notification = new Notification();
                        notification.Create(sendFrom, sendFrom_id, subject, id, sendTo, created_at, 1);
                        
                        Trading trade = new Trading();

                        trade.setCode(code);
                        trade.setTotal(pro._getPrice());
                        trade.setStatusID(2);
                        trade.setProductID(pro.getId());
                        trade.setUserID(u.getId());
                        trade.setCreated_at(created_at);
                        trade.setUpdated_at(created_at);
                        trade.setIsDeleted(false);

                        _Deque.getMyrequest().add(new Single(u.getId(), pro._getPrice() * (-1), trade, 3));

                        //xu ly da luong
                        MultiProcess mt = new MultiProcess();
                        mt.multiPro();

                        String json = "{\"success\": true, \"message\": \"null\"}";
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        response.getWriter().write(json);
                } catch (Exception er)
                {
                        er.printStackTrace();
                        request.setAttribute("error", "Không thể thực hiện hành động!");
                        request.getRequestDispatcher("/views/error/error_denied.jsp").forward(request, response);
                }
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
