/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.trading;

import dal.product.ProductDBContext;
import dal.trading.TradingDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import model.auth.User;
import model.common.Single;
import model.common._Deque;
import model.product.Product;
import model.trading.Trading;
import utils.MultiProcess;
import utils.Notification;
import utils.Validate;

/**
 *
 * @author hai20
 */
public class TradingController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet TradingDetail</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet TradingDetail at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                //processRequest(request, response);
                Validate validate = new Validate();
                try
                {
                        HttpSession ses = request.getSession();
                        User u = (User) ses.getAttribute("user");
                        
                        String code = validate.getFieldAjax(request, "code", true, "");

                        TradingDBContext tdb = new TradingDBContext();
                        Trading trade = tdb.get("code", code);

                        if (u.getId() != trade.getUserID())
                        {
                                throw new Exception();
                        }

                        ProductDBContext pdb = new ProductDBContext();
                        Product pro = pdb.get("id", String.valueOf(trade.getProductID()));

                        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
                        String formattedNumber = decimalFormat.format(pro._getPrice());

                        request.setAttribute("code", code);
                        request.setAttribute("proName", pro.getProductName());
                        request.setAttribute("total", formattedNumber.substring(0, formattedNumber.length() - 3));
                        request.setAttribute("data", pro.getDataproduct());
                        request.setAttribute("trade", trade);

                        request.getRequestDispatcher("/views/trading/trading_detail.jsp").forward(request, response);

                } catch (Exception er)
                {
                        request.setAttribute("error", "Không thể thực hiện hành động!");
                        request.getRequestDispatcher("/views/error/error_denied.jsp").forward(request, response);
                }
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                //processRequest(request, response);
                Validate validate = new Validate();
                try
                {
                        HttpSession ses = request.getSession();
                        User u = (User) ses.getAttribute("user");

                        String code = validate.getFieldAjax(request, "code", true, "");

                        TradingDBContext tdb = new TradingDBContext();
                        Trading trade = tdb.get("code", code);

                        ProductDBContext pdb = new ProductDBContext();
                        Product pro = pdb.get("id", String.valueOf(trade.getProductID()));
                        
                        //notification
                        String sendFrom = u.getUsername();
                        int sendFrom_id = u.getId();
                        String subject = pro.getProductName();
                        String id = String.valueOf(pro.getId());
                        int sendTo = pro.getUserID();
                        Timestamp created_at = new Timestamp(System.currentTimeMillis());

                        Notification notification = new Notification();
                        notification.Create(sendFrom, sendFrom_id, subject, id, sendTo, created_at, 2);

                        _Deque.getMyrequest().add(new Single(pro.getUserID(), pro._getPrice(), trade, 4));

                        //xu ly da luong
                        MultiProcess mt = new MultiProcess();
                        mt.multiPro();

                        response.sendRedirect("trading?code=" + code);
                } catch (Exception er)
                {
                        String json = "{\"success\": false, \"message\": \"Không thể thực hiện hành động!\"}";
                        request.setAttribute("jsonData", json);
                        request.getRequestDispatcher("/views/trading/trading_detail.jsp").forward(request, response);
                }
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
