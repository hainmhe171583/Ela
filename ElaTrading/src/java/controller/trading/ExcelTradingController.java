/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.trading;

import dal.trading.TradingDBContext;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.util.ArrayList;
import model.auth.User;
import model.trading.Trading;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Admin
 */
public class ExcelTradingController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                request.setCharacterEncoding("UTF-8");
                TradingDBContext tdb = new TradingDBContext();
                HttpSession session = request.getSession();
                User u = (User) session.getAttribute("user");
                int id = u.getId();
                ArrayList<Trading> list = tdb.excelTrading(id);
                String fileType = request.getParameter("fileType");
                if ("excel".equals(fileType))
                {
                        exportToExcel(list, response);
                } else if ("txt".equals(fileType))
                {
                        exportToText(list, response);
                }
        }

        private void exportToExcel(ArrayList<Trading> tradingList, HttpServletResponse response) throws IOException
        {
                XSSFWorkbook workbook = new XSSFWorkbook();
                XSSFSheet workSheet = workbook.createSheet("1");
                XSSFRow row = workSheet.createRow(0);
                String[] headers =
                {
                        "STT", "Tên sản phẩm", "Giá", "Người bán", "Trạng thái"
                };
                for (int i = 0; i < headers.length; i++)
                {
                        XSSFCell cell = row.createCell(i);
                        cell.setCellValue(headers[i]);
                }
                int rowNum = 1;
                for (Trading trading : tradingList)
                {
                        row = workSheet.createRow(rowNum++);
                        row.createCell(0).setCellValue(trading.getId());
                        row.createCell(1).setCellValue(trading.getProduct().getProductName());
                        row.createCell(2).setCellValue(trading.getProduct().getPrice());
                        row.createCell(3).setCellValue(trading.getUser().getUsername());
                        String statusName = null;
                        switch (trading.getStatusID())
                        {
                                case 2:
                                        statusName = "Đang xử lí";
                                        break;
                                case 4:
                                        statusName = "Đã mua";
                                        break;
                                case 3:
                                        statusName = "Bị huỷ";
                                        break;
                                case 5:
                                        statusName = "Đang khiếu nại";
                                        break;
                        }
                        row.createCell(4).setCellValue(statusName);
                }
                String fileName = "Danh sach mua san pham.xlsx";
                response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
                try ( ServletOutputStream outputStream = response.getOutputStream())
                {
                        workbook.write(outputStream);
                } finally
                {
                        workbook.close();
                }
        }

        private void exportToText(ArrayList<Trading> tradingList, HttpServletResponse response) throws IOException
        {
                String filename = "Danh sach mua san pham.txt";
                response.setContentType("text/plain");
                response.setHeader("Content-Disposition", "attachment; filename=" + filename);

                try ( PrintWriter out = response.getWriter())
                {
                        for (Trading trading : tradingList)
                        {
                                String statusName = null;
                                switch (trading.getStatusID())
                                {
                                        case 2:
                                                statusName = "Đang xử lí";
                                                break;
                                        case 4:
                                                statusName = "Đã mua";
                                                break;
                                        case 3:
                                                statusName = "Bị huỷ";
                                                break;
                                        case 5:
                                                statusName = "Đang khiếu nại";
                                                break;
                                }
                                out.println("STT: " + trading.getId());
                                out.println("Tên sản phẩm: " + trading.getProduct().getProductName());
                                out.println("Giá: " + trading.getProduct().getPrice());
                                out.println("Người bán: " + trading.getUser().getUsername());
                                out.println("Trạng thái: " + statusName);
                                out.println("--------------------------------------");
                        }
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                processRequest(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
