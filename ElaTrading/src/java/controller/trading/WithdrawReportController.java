/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.trading;

import dal.product.ProductDBContext;
import dal.trading.ReportDBContext;
import dal.trading.TradingDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Timestamp;
import model.auth.User;
import model.common.Single;
import model.common._Deque;
import model.product.Product;
import model.trading.Report;
import model.trading.Trading;
import utils.MultiProcess;
import utils.Notification;

/**
 *
 * @author admin
 */
public class WithdrawReportController extends HttpServlet
{

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                response.setContentType("text/html;charset=UTF-8");
                try ( PrintWriter out = response.getWriter())
                {
                        /* TODO output your page here. You may use following sample code. */
                        out.println("<!DOCTYPE html>");
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet WithdrawReportController</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<h1>Servlet WithdrawReportController at " + request.getContextPath() + "</h1>");
                        out.println("</body>");
                        out.println("</html>");
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                HttpSession session = request.getSession();
                User u = (User) session.getAttribute("user");

                String codereport = request.getParameter("code");

                ReportDBContext rdb = new ReportDBContext();
                Report r = rdb.getByID("code", codereport);
                r.setStatusID(3);
                rdb.update(r);

                TradingDBContext tdb = new TradingDBContext();
                String tradingId = String.valueOf(r.getTradingID());
                Trading trading = tdb.get("id", tradingId);

                trading.setStatusID(4);
                _Deque.getMyrequest().add(new Single(u.getId(), trading.getTotal(), trading, 4));

                //xu ly da luong
                MultiProcess mt = new MultiProcess();
                mt.multiPro();
                
                //notification
                Timestamp created_at = new Timestamp(System.currentTimeMillis());
                Product pro = trading.getProduct();

                String sendFrom = u.getUsername();
                int sendFrom_id = u.getId();
                String subject = pro.getProductName();
                String id = String.valueOf(pro.getId());
                int sendTo = pro.getUserID();

                Notification notification = new Notification();
                notification.Create(sendFrom, sendFrom_id, subject, id, sendTo, created_at, 4);

                String url = "/ElaTrading/action/trading?code=" + trading.getCode();
                response.sendRedirect(url);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException
        {
                processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo()
        {
                return "Short description";
        }// </editor-fold>

}
