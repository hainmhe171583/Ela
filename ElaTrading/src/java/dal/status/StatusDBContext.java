/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal.status;

import dal.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.auth.User;
import model.status.Status;

/**
 *
 * @author admin
 */
public class StatusDBContext extends DBContext<Status>
{

        @Override
        public ArrayList<Status> getList(String field, String value)
        {
                String sql = "Select * from status\n";
                if (!field.equals(""))
                {
                        sql += "where " + field + " = " + value;
                }
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        ResultSet result = statement.executeQuery();
                        ArrayList<Status> statues = new ArrayList<>();
                        while (result.next())
                        {
                                Status status = new Status();
                                status.setId(result.getInt("id"));
                                status.setStatusName(result.getString("statusName"));

                                statues.add(status);
                        }
                        return statues;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public Status get(String field, String value)
        {
                String sql = "Select * from status\n"
                        + "where " + field + " = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        ResultSet result = statement.executeQuery();
                        while (result.next())
                        {
                                Status status = new Status();
                                status.setId(result.getInt("id"));
                                status.setStatusName(result.getString("statusName"));

                                return status;
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public Status insert(Status model)
        {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        @Override
        public void update(Status model)
        {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        @Override
        public void delete(int id, User user)
        {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }
        
          public ArrayList<Status> getAll()
        {
                String sql = "Select * from Status;\n";
                
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        ResultSet result = statement.executeQuery();
                        ArrayList<Status> list = new ArrayList<>();
                        while (result.next())
                        {
                                Status status = new Status();
                                status.setId(result.getInt("id"));
                                status.setStatusName(result.getString("statusName"));;
                                list.add(status);
                        }
                        return list;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }
          
//          public static void main(String[] args) {
//        StatusDBContext s = new StatusDBContext();
//              System.out.println(s.getAll().get(0).getStatusName());
//    }
          
          

}
