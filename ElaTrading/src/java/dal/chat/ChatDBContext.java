/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal.chat;

import dal.DBContext;
import dal.auth.UserDBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.auth.User;
import model.chat.Chat;

/**
 *
 * @author admin
 */
public class ChatDBContext extends DBContext<Chat>
{

        public void AddChat(Chat chat)
        {
                String sql = "INSERT INTO chat (`code`,textcontent, imagecontent, senderid, created_at)\n"
                        + "VALUES  (?,?,?,?, now());";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        statement.setString(1, chat.getCode());
                        statement.setString(2, chat.getTextcontent());
                        statement.setString(3, chat.getImagecontent());
                        statement.setInt(4, chat.getSenderid());
                        statement.executeUpdate();
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
        }

        public ArrayList<Chat> getChat()
        {
                ArrayList<Chat> list = new ArrayList<>();
                UserDBContext udb = new UserDBContext();
                String sql = "select * \n"
                        + "from chat\n";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        ResultSet rs = statement.executeQuery();
                        while (rs.next())
                        {
                                Chat c = new Chat();
                                c.setId(rs.getInt("id"));
                                c.setSenderid(rs.getInt("senderid"));
                                c.setSender(udb.get("id", String.valueOf(rs.getInt("senderid"))));
                                c.setTextcontent(rs.getString("textcontent"));
                                c.setImagecontent(rs.getString("imagecontent"));
                                c.setCreated_at(rs.getTimestamp("created_at"));
                                c.setUpdated_at(rs.getTimestamp("updated_at"));
                                c.setDeleted_at(rs.getTimestamp("deleted_at"));
                                c.setIsDeleted(rs.getBoolean("isDeleted"));
                                list.add(c);
                        }
                        return list;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public ArrayList<Chat> getList(String field, String value)
        {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        @Override
        public Chat get(String field, String value)
        {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        @Override
        public Chat insert(Chat model)
        {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        @Override
        public void update(Chat model)
        {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        @Override
        public void delete(int id, User user)
        {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }
}
