/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal.product;

import dal.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import model.auth.User;
import model.product.Category;

/**
 *
 * @author admin
 */
public class CategoryDBContext extends DBContext<Category>
{

        public CategoryDBContext()
        {
                super();
        }

        @Override
        public ArrayList<Category> getList(String field, String value)
        {
                String sql = "Select * from category\n";
                if (!field.equals(""))
                {
                        sql += "where " + field + " = " + value;
                }
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        
                        ResultSet result = statement.executeQuery();
                        ArrayList<Category> categories = new ArrayList<>();
                        while (result.next())
                        {
                                Category category = new Category();
                                category.setId(result.getInt("id"));
                                category.setCategoryName(result.getString("categoryName"));
                                category.setDescribe(result.getString("describe"));
                                category.setCreated_at(result.getTimestamp("created_at"));
                                category.setUpdated_at(result.getTimestamp("updated_at"));
                                category.setDeleted_at(result.getTimestamp("deleted_at"));
                                category.setIsDeleted(result.getBoolean("isDeleted"));

                                categories.add(category);
                        }
                        return categories;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public Category get(String field, String value)
        {
                String sql = "Select * from category\n"
                        + "where " + field + " = " + value;
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        
                        ResultSet result = statement.executeQuery();
                        while (result.next())
                        {
                                Category category = new Category();
                                category.setId(result.getInt("id"));
                                category.setCategoryName(result.getString("categoryName"));
                                category.setDescribe(result.getString("describe"));
                                category.setCreated_at(result.getTimestamp("created_at"));
                                category.setUpdated_at(result.getTimestamp("updated_at"));
                                category.setDeleted_at(result.getTimestamp("deleted_at"));
                                category.setIsDeleted(result.getBoolean("isDeleted"));

                                return category;
                        }
                } catch (SQLException er)
                {
                        System.out.println("get category error: " + er.getMessage());
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public Category insert(Category category)
        {
                String sql = "INSERT INTO swp_new.category\n"
                        + "(categoryName,\n"
                        + "describe,\n"
                        + "created_at,\n"
                        + "updated_at,\n"
                        + "deleted_at,\n"
                        + "isDeleted)\n"
                        + "VALUES(?, ?, ?, ?, null, 0);";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        
                        statement.setString(1, category.getCategoryName());
                        statement.setString(2, category.getDescribe());
                        statement.setTimestamp(3, category.getCreated_at());
                        statement.setTimestamp(4, category.getUpdated_at());
                        statement.executeUpdate();
                        Category new_category = get("categoryName", category.getCategoryName());

                        return new_category;
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public void update(Category category)
        {
                String sql = "Update swp_new.category\n"
                        + "Set categoryName = ?,\n"
                        + "describe = ?,\n"
                        + "created_at = ?,\n"
                        + "updated_at = ?,\n"
                        + "deleted_at = ?,\n"
                        + "isDeleted = ?\n"
                        + "Where id = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        
                        statement.setString(1, category.getCategoryName());
                        statement.setString(2, category.getDescribe());
                        statement.setTimestamp(3, category.getCreated_at());
                        statement.setTimestamp(4, category.getUpdated_at());
                        statement.setTimestamp(5, category.getDeleted_at());
                        statement.setBoolean(6, category.isIsDeleted());
                        statement.setInt(7, category.getId());

                        statement.executeUpdate();
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
        }

        @Override
        public void delete(int id, User user)
        {
                String sql = "Update swp_new.category\n"
                        + "Set deleted_at = ?,\n"
                        + "isDeleted = 1,\n"
                        + "deleted_by = ?\n"
                        + "Where id = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        
                        Timestamp deleted_at = new Timestamp(System.currentTimeMillis());
                        statement.setTimestamp(1, deleted_at);
                        statement.setInt(2, user.getId());
                        statement.setInt(3, id);

                        statement.executeUpdate();
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
        }
        
         public ArrayList<Category> getAll()
        {
                String sql = "Select * from category;\n";
                
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        ResultSet result = statement.executeQuery();
                        ArrayList<Category> categories = new ArrayList<>();
                        while (result.next())
                        {
                                Category category = new Category();
                                category.setId(result.getInt("id"));
                                category.setCategoryName(result.getString("categoryName"));
                                category.setDescribe(result.getString("describe"));
                                category.setCreated_at(result.getTimestamp("created_at"));
                                category.setUpdated_at(result.getTimestamp("updated_at"));
                                category.setDeleted_at(result.getTimestamp("deleted_at"));
                                category.setIsDeleted(result.getBoolean("isDeleted"));

                                categories.add(category);
                        }
                        return categories;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

}
