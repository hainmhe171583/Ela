/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal.product;

import dal.DBContext;
import dal.auth.UserDBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.auth.User;
import model.product.Category;
import model.product.Product;
import model.status.Status;

/**
 *
 * @author admin
 */
public class ProductDBContext extends DBContext<Product>
{

        ResultSet rs = null;
        PreparedStatement ps = null;

        public ProductDBContext()
        {
        }

        @Override
        public ArrayList<Product> getList(String field, String value)
        {
                String sql = "SELECT * FROM product\n";
                if (!field.equals(""))
                {
                        sql += "where " + field + " = \"" + value + "\"";
                }
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        ResultSet result = statement.executeQuery();
                        ArrayList<Product> products = new ArrayList<>();
                        while (result.next())
                        {
                                Product product = new Product();
                                product.setId(result.getInt("id"));
                                product.setCode(result.getString("code"));
                                product.setProductName(result.getString("productName"));
                                product.setDescribe(result.getString("describe"));
                                product.setPrice(result.getDouble("price"));
                                product.setStock(result.getInt("stock"));
                                product.setContact(result.getString("contact"));
                                product.setIsPrivate(result.getBoolean("is_Private"));
                                product.setDataproduct(result.getString("dataProduct"));
                                product.setLink(result.getString("link"));
                                product.setStatusID(result.getInt("statusID"));
                                product.setCategoryID(result.getInt("categoryID"));
                                product.setUserID(result.getInt("userID"));
                                product.setCreated_at(result.getTimestamp("created_at"));
                                product.setUpdated_at(result.getTimestamp("updated_at"));
                                product.setDeleted_at(result.getTimestamp("deleted_at"));
                                product.setIsDeleted(result.getBoolean("isDeleted"));

                                products.add(product);
                        }
                        return products;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public Product get(String field, String value)
        {
                String sql = "Select * from product\n";
                if (!field.equals(""))
                {
                        sql += "where " + field + " = \"" + value + "\"";
                } // SQL Injection
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        ResultSet result = statement.executeQuery();
                        while (result.next())
                        {
                                Product product = new Product();
                                product.setId(result.getInt("id"));
                                product.setCode(result.getString("code"));
                                product.setProductName(result.getString("productName"));
                                product.setDescribe(result.getString("describe"));
                                product.setPrice(result.getDouble("price"));
                                product.setStock(result.getInt("stock"));
                                product.setContact(result.getString("contact"));
                                product.setIsPrivate(result.getBoolean("is_Private"));
                                product.setDataproduct(result.getString("dataProduct"));
                                product.setLink(result.getString("link"));
                                product.setStatusID(result.getInt("statusID"));
                                product.setCategoryID(result.getInt("categoryID"));
                                product.setUserID(result.getInt("userID"));
                                product.setCreated_at(result.getTimestamp("created_at"));
                                product.setUpdated_at(result.getTimestamp("updated_at"));
                                product.setDeleted_at(result.getTimestamp("deleted_at"));
                                product.setIsDeleted(result.getBoolean("isDeleted"));

                                return product;
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public Product insert(Product product)
        {
                String sql = "INSERT INTO `swp_new`.`product`\n"
                        + "(`code`,\n"
                        + "`productName`,\n"
                        + "`describe`,\n"
                        + "`price`,\n"
                        + "`stock`,\n"
                        + "`contact`,\n"
                        + "`is_Private`,\n"
                        + "`dataProduct`,\n"
                        + "`link`,\n"
                        + "`statusID`,\n"
                        + "`categoryID`,\n"
                        + "`userID`,\n"
                        + "`created_at`,\n"
                        + "`updated_at`,\n"
                        + "`deleted_at`,\n"
                        + "`isDeleted`)\n"
                        + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, null, 0);";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        statement.setString(1, product.getCode());
                        statement.setString(2, product.getProductName());
                        statement.setString(3, product.getDescribe());
                        statement.setDouble(4, product._getPrice());
                        statement.setInt(5, product.getStock());
                        statement.setString(6, product.getContact());
                        statement.setBoolean(7, product.isIsPrivate());
                        statement.setString(8, product.getDataproduct());
                        statement.setString(9, product.getLink());
                        statement.setInt(10, product.getStatusID());
                        statement.setInt(11, product.getCategoryID());
                        statement.setInt(12, product.getUserID());
                        statement.setTimestamp(13, product.getCreated_at());
                        statement.setTimestamp(14, product.getUpdated_at());
                        statement.executeUpdate();
                        Product new_product = get("code", product.getCode());

                        return new_product;
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public void update(Product product)
        {
                String sql = "Update swp_new.product\n"
                        + "Set code = ?,"
                        + " productName = ?,"
                        + " `describe` = ?,"
                        + " price = ?,"
                        + " stock = ?,"
                        + " contact = ?,"
                        + " is_Private = ?,"
                        + " dataProduct = ?,"
                        + " link = ?,"
                        + " statusID = ?,"
                        + " categoryID = ?,"
                        + " userID = ?,"
                        + " created_at = ?,"
                        + " updated_at = ?,"
                        + " deleted_at = ?,"
                        + " isDeleted = ?\n"
                        + "Where id = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        statement.setString(1, product.getCode());
                        statement.setString(2, product.getProductName());
                        statement.setString(3, product.getDescribe());
                        statement.setDouble(4, product._getPrice());
                        statement.setInt(5, product.getStock());
                        statement.setString(6, product.getContact());
                        statement.setBoolean(7, product.isIsPrivate());
                        statement.setString(8, product.getDataproduct());
                        statement.setString(9, product.getLink());
                        statement.setInt(10, product.getStatusID());
                        statement.setInt(11, product.getCategoryID());
                        statement.setInt(12, product.getUserID());
                        statement.setTimestamp(13, product.getCreated_at());
                        statement.setTimestamp(14, product.getUpdated_at());
                        statement.setTimestamp(15, product.getDeleted_at());
                        statement.setBoolean(16, product.isIsDeleted());
                        statement.setInt(17, product.getId());
                        statement.executeUpdate();

                        statement.executeUpdate();
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
        }

        @Override
        public void delete(int id, User user)
        {
                String sql = "Update swp_new.product\n"
                        + "Set deleted_at = ?,\n"
                        + "isDeleted = 1,\n"
                        + "deleted_by = ?\n"
                        + " Where id = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        Timestamp deleted_at = new Timestamp(System.currentTimeMillis());
                        statement.setTimestamp(1, deleted_at);
                        statement.setInt(2, user.getId());
                        statement.setInt(3, id);
                        
                        statement.executeUpdate();
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
        }

        public ArrayList<Product> filterByGroup(String sort, String username, String[] categorylst)
        {
                ArrayList<Product> list = new ArrayList();

                try
                {
                        String SQL = "SELECT * FROM product as p " + "INNER JOIN category as c ON p.categoryId = c.id WHERE 1=1\n";
                        if (categorylst != null && categorylst.length > 0)
                        {
                                String categoryIds = String.join(", ", categorylst);
                                if (categoryIds != null && !categoryIds.equals("null"))
                                {
                                        SQL += "AND c.id IN (" + categoryIds + ") ";
                                }
                        }
                        SQL += "AND p.isDeleted = 0 AND p.is_Private = 0\n";
                        if (sort != null && !"".equals(sort.trim()))
                        {
                                SQL += "ORDER BY p.price " + sort;
                        }
                        ps = connection.prepareStatement(SQL);
                        rs = ps.executeQuery();
                        while (rs.next())
                        {
                                Product product = new Product();
                                product.setId(rs.getInt("id"));
                                product.setCode(rs.getString("code"));
                                product.setProductName(rs.getString("productName"));
                                product.setDescribe(rs.getString("describe"));
                                product.setPrice(rs.getDouble("price"));
                                product.setStock(rs.getInt("stock"));
                                product.setContact(rs.getString("contact"));
                                product.setIsPrivate(rs.getBoolean("is_Private"));
                                product.setDataproduct(rs.getString("dataProduct"));
                                product.setLink(rs.getString("link"));
                                product.setStatusID(rs.getInt("statusID"));
                                product.setCategoryID(rs.getInt("categoryID"));
                                product.setUserID(rs.getInt("userID"));
                                product.setCreated_at(rs.getTimestamp("created_at"));
                                product.setUpdated_at(rs.getTimestamp("updated_at"));
                                product.setDeleted_at(rs.getTimestamp("deleted_at"));
                                product.setIsDeleted(rs.getBoolean("isDeleted"));

                                Category c = new Category();
                                c.setCategoryName(rs.getString("categoryName"));
                                c.setDescribe(rs.getString("describe"));
                                product.setCategory(c);
                                list.add(product);
                        }
                        System.out.println("SQL Query = " + SQL);
                } catch (Exception e)
                {
                        System.err.println("Get list product error: " + e.getMessage());
                        e.printStackTrace();
                }
                return list;
        }

        public ArrayList<Product> searchByField(String field, String value)
        {
                String sql = "SELECT * FROM product\n";
                if (!field.equals(""))
                {
                        sql += "WHERE " + field + " LIKE '%" + value + "%' AND p.isDeleted = 0 AND p.is_Private = 0\n";
                }
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        ResultSet result = statement.executeQuery();
                        ArrayList<Product> products = new ArrayList<>();
                        while (result.next())
                        {
                                Product product = new Product();
                                product.setId(result.getInt("id"));
                                product.setCode(result.getString("code"));
                                product.setProductName(result.getString("productName"));
                                product.setDescribe(result.getString("describe"));
                                product.setPrice(result.getDouble("price"));
                                product.setStock(result.getInt("stock"));
                                product.setContact(result.getString("contact"));
                                product.setIsPrivate(result.getBoolean("is_Private"));
                                product.setDataproduct(result.getString("dataProduct"));
                                product.setLink(result.getString("link"));
                                product.setStatusID(result.getInt("statusID"));
                                product.setCategoryID(result.getInt("categoryID"));
                                product.setUserID(result.getInt("userID"));
                                product.setCreated_at(result.getTimestamp("created_at"));
                                product.setUpdated_at(result.getTimestamp("updated_at"));
                                product.setDeleted_at(result.getTimestamp("deleted_at"));
                                product.setIsDeleted(result.getBoolean("isDeleted"));

                                products.add(product);
                        }
                        return products;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        public ArrayList<Product> getListByUserIds(List<Integer> ids)
        {
                String sql = "SELECT * FROM product\n";

                String listIds = "";
                for (int i = 0; i < ids.size(); i++)
                {
                        listIds += ids.get(i);
                        if (i < ids.size() - 1)
                        {
                                listIds += ", ";
                        }
                }

                if (!ids.isEmpty())
                {
                        sql += "WHERE userID IN (" + listIds + ") AND isDeleted = 0 AND is_Private = 0\n";
                }
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        ResultSet result = statement.executeQuery();
                        ArrayList<Product> products = new ArrayList<>();
                        while (result.next())
                        {
                                Product product = new Product();
                                product.setId(result.getInt("id"));
                                product.setCode(result.getString("code"));
                                product.setProductName(result.getString("productName"));
                                product.setDescribe(result.getString("describe"));
                                product.setPrice(result.getDouble("price"));
                                product.setStock(result.getInt("stock"));
                                product.setContact(result.getString("contact"));
                                product.setIsPrivate(result.getBoolean("is_Private"));
                                product.setDataproduct(result.getString("dataProduct"));
                                product.setLink(result.getString("link"));
                                product.setStatusID(result.getInt("statusID"));
                                product.setCategoryID(result.getInt("categoryID"));
                                product.setUserID(result.getInt("userID"));
                                product.setCreated_at(result.getTimestamp("created_at"));
                                product.setUpdated_at(result.getTimestamp("updated_at"));
                                product.setDeleted_at(result.getTimestamp("deleted_at"));
                                product.setIsDeleted(result.getBoolean("isDeleted"));

                                products.add(product);
                        }
                        return products;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        public int countProductListSeller(int id)
        {
                String sql = "SELECT COUNT(*) FROM Product AS p\n"
                        + "WHERE p.userID = ? and p.isDeleted = 0";
                try
                {
                        PreparedStatement statement = connection.prepareStatement(sql);
                        statement.setInt(1, id); // Set the user ID parameter
                        ResultSet result = statement.executeQuery();
                        if (result.next())
                        {
                                return result.getInt(1); // Return the count
                        }
                } catch (SQLException e)
                {
                        e.printStackTrace(); // Handle the exception according to your requirement
                }
                return 0;
        }

        public int countAllProductByStatus(int id, int statusID)
        {
                String sql = "select count(*) from Product as p\n"
                        + "join Status as s On p.statusID = s.id\n"
                        + "Join User as u On p.userId = u.id\n"
                        + "Where u.id =? and  p.isDeleted = 0 and p.statusID  = ?";
                try
                {
                        PreparedStatement statement = connection.prepareStatement(sql);
                        statement.setInt(1, id);
                        statement.setInt(2, statusID);// Set the user ID parameter
                        ResultSet result = statement.executeQuery();
                        if (result.next())
                        {
                                return result.getInt(1); // Return the count
                        }
                } catch (SQLException e)
                {
                        e.printStackTrace(); // Handle the exception according to your requirement
                }
                return 0;
        }

        public ArrayList<Product> filterListProduct(int pageNumber, int userId, int sID)
        {
                String sql = "   SELECT \n"
                        + "    p.*, \n"
                        + "    s.statusName, \n"
                        + "    c.categoryName,\n"
                        + "    bu.username AS buyer_username\n"
                        + "FROM \n"
                        + "    Product AS p\n"
                        + "JOIN \n"
                        + "    Status AS s ON p.statusID = s.id\n"
                        + "JOIN \n"
                        + "    User AS u ON p.userId = u.id\n"
                        + "JOIN \n"
                        + "    Category AS c ON p.categoryID = c.id\n"
                        + "LEFT JOIN \n"
                        + "    Trading AS t ON p.id = t.productID\n"
                        + "LEFT JOIN \n"
                        + "    User AS bu ON t.userID = bu.id\n"
                        + "WHERE \n"
                        + "    u.id = ? \n"
                        + "    AND p.isDeleted = 0   \n"
                        + "    AND p.statusID = ?   \n"
                        + "ORDER BY \n"
                        + "    p.statusID\n"
                        + "LIMIT \n"
                        + "    5 OFFSET ?;";
                try ( PreparedStatement statement = connection.prepareStatement(sql))
                {
                        int offset = (pageNumber - 1) * 5; // Calculate offset
                        statement.setInt(1, userId);
                        statement.setInt(2, sID);
                        statement.setInt(3, offset);
                        try ( ResultSet result = statement.executeQuery())
                        {
                                ArrayList<Product> products = new ArrayList<>();
                                while (result.next())
                                {
                                        Product product = new Product();
                                        product.setId(result.getInt("id"));
                                        product.setCode(result.getString("code"));
                                        product.setProductName(result.getString("productName"));
                                        product.setDescribe(result.getString("describe"));
                                        product.setPrice(result.getDouble("price"));
                                        product.setStock(result.getInt("stock"));
                                        product.setContact(result.getString("contact"));
                                        product.setDataproduct(result.getString("dataProduct"));
                                        product.setLink(result.getString("link"));
                                        product.setCreated_at(result.getTimestamp("created_at"));
                                        product.setUpdated_at(result.getTimestamp("updated_at"));
                                        product.setIsDeleted(result.getBoolean("isDeleted"));
                                        product.setStatusID(result.getInt("statusID"));

                                        // Create and set Status object
                                        Status status = new Status();
                                        status.setStatusName(result.getString("statusName"));
                                        product.setStatus(status);

                                        // Create and set Category object
                                        Category category = new Category();
                                        category.setCategoryName(result.getString("categoryName"));
                                        product.setCategory(category);

                                        // Create and set Buyer's information
                                        if (result.getString("buyer_username") != null)
                                        {
                                                User buyer = new User();
                                                buyer.setUsername(result.getString("buyer_username"));
                                                product.setUser(buyer);
                                        }

                                        products.add(product);
                                }
                                return products;
                        }
                } catch (SQLException e)
                {
                        e.printStackTrace();
                        return null;
                }
        }

        public int countAllProductBySellerID(int id)
        {
                String sql = "SELECT COUNT(*) FROM Product AS p\n"
                        + "JOIN User AS u ON p.userId = u.id\n"
                        + "WHERE u.id = ?; ";
                try
                {
                        PreparedStatement statement = connection.prepareStatement(sql);
                        statement.setInt(1, id); // Set the user ID parameter
                        ResultSet result = statement.executeQuery();
                        if (result.next())
                        {
                                return result.getInt(1); // Return the count
                        }
                } catch (SQLException e)
                {
                        e.printStackTrace(); // Handle the exception according to your requirement
                }
                return 0;
        }

        public List<Product> pagingProduct(int pageNumber, int userId)
        {
                List<Product> list = new ArrayList<>();
                String sql = "SELECT \n"
                        + "    p.*, \n"
                        + "    u.username AS seller_username, \n"
                        + "    c.categoryName, \n"
                        + "    s.statusName,\n"
                        + "    bu.username AS buyer_username\n"
                        + "FROM \n"
                        + "    Product AS p\n"
                        + "JOIN \n"
                        + "    User AS u ON p.userId = u.id\n"
                        + "JOIN \n"
                        + "    Category AS c ON p.categoryID = c.id\n"
                        + "JOIN \n"
                        + "    Status AS s ON p.statusID = s.id\n"
                        + "LEFT JOIN \n"
                        + "    Trading AS t ON p.id = t.productID\n"
                        + "LEFT JOIN \n"
                        + "    User AS bu ON t.userID = bu.id\n"
                        + "WHERE \n"
                        + "    u.id = ? \n"
                        + "    AND p.isDeleted = 0\n"
                        + "ORDER BY \n"
                        + "    p.statusID\n"
                        + "LIMIT \n"
                        + "    5 OFFSET ?;";
                try ( PreparedStatement statement = connection.prepareStatement(sql))
                {
                        int offset = (pageNumber - 1) * 5; // Calculate offset
                        statement.setInt(1, userId);
                        statement.setInt(2, offset);
                        try ( ResultSet result = statement.executeQuery())
                        {
                                while (result.next())
                                {
                                        Product product = new Product();
                                        product.setId(result.getInt("id"));
                                        product.setCode(result.getString("code"));
                                        product.setProductName(result.getString("productName"));
                                        product.setDescribe(result.getString("describe"));
                                        product.setPrice(result.getDouble("price"));
                                        product.setStock(result.getInt("stock"));
                                        product.setContact(result.getString("contact"));
                                        product.setDataproduct(result.getString("dataProduct"));
                                        product.setLink(result.getString("link"));
                                        product.setCreated_at(result.getTimestamp("created_at"));
                                        product.setUpdated_at(result.getTimestamp("updated_at"));
                                        product.setIsDeleted(result.getBoolean("isDeleted"));
                                        product.setStatusID(result.getInt("statusID"));

                                        // Set Seller's information
                                        User seller = new User();
                                        seller.setUsername(result.getString("seller_username"));
                                        product.setUser(seller);

                                        // Set Category information
                                        Category category = new Category();
                                        category.setCategoryName(result.getString("categoryName"));
                                        product.setCategory(category);

                                        // Set Status information
                                        Status status = new Status();
                                        status.setStatusName(result.getString("statusName"));
                                        product.setStatus(status);

                                        // Set Buyer's information
                                        if (result.getString("buyer_username") != null)
                                        {
                                                User buyer = new User();
                                                buyer.setUsername(result.getString("buyer_username"));
                                                product.setUser(buyer);
                                        }

                                        // Add the product to the list
                                        list.add(product);
                                }
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return list;
        }

        public int countAllProductByStockDeleted(int id)
        {
                String sql = "select count(*) from Product as p\n"
                        + "join Status as s On p.statusID = s.id\n"
                        + "Join User as u On p.userId = u.id\n"
                        + "Where u.id =? and  p.isDeleted = 1";
                try
                {
                        PreparedStatement statement = connection.prepareStatement(sql);
                        statement.setInt(1, id);

                        ResultSet result = statement.executeQuery();
                        if (result.next())
                        {
                                return result.getInt(1); // Return the count
                        }
                } catch (SQLException e)
                {
                        e.printStackTrace(); // Handle the exception according to your requirement
                }
                return 0;
        }

        public ArrayList<Product> filterListProductDeleted(int pageNumber, int userId)
        {
                String sql = "select * from Product as p\n"
                        + "join Status as s On p.statusID = s.id\n"
                        + "Join User as u On p.userId = u.id\n"
                        + "Join Category as c On p.categoryID = c.id\n"
                        + "Where u.id =? and p.isDeleted = 1\n"
                        + "ORDER BY p.id\n"
                        + "LIMIT 5 OFFSET ?;";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        // Set the statusName parameter
                        int offset = (pageNumber - 1) * 5; // Calculate offset
                        statement.setInt(1, userId);

                        statement.setInt(2, offset); // Set the product ID parameter
                        ResultSet result = statement.executeQuery();

                        ArrayList<Product> products = new ArrayList<>();
                        while (result.next())
                        {
                                Product product = new Product();
                                product.setId(result.getInt("id"));
                                product.setCode(result.getString("code"));
                                product.setProductName(result.getString("productName"));
                                product.setDescribe(result.getString("describe"));
                                product.setPrice(result.getDouble("price"));
                                product.setStock(result.getInt("stock"));
                                product.setContact(result.getString("contact"));
                                product.setDataproduct(result.getString("dataProduct"));
                                product.setLink(result.getString("link"));
                                product.setCreated_at(result.getTimestamp("created_at"));
                                product.setUpdated_at(result.getTimestamp("updated_at"));
                                product.setIsDeleted(result.getBoolean("isDeleted"));
                                product.setStatusID(result.getInt("statusID"));
                                Status status = new Status();
                                status.setStatusName(result.getString("statusName"));
                                product.setStatus(status);

                                Category category = new Category();
                                category.setCategoryName(result.getString("categoryName"));
                                product.setCategory(category);

                                products.add(product);
                        }
                        return products;
                } catch (SQLException e)
                {
                        e.printStackTrace();

                        return null;
                }
        }

        public ArrayList<Product> execlProductSeller(int uid)
        {
                String sql = "  SELECT \n"
                        + "    p.*, \n"
                        + "    s.statusName, \n"
                        + "    u.username AS seller_username, \n"
                        + "    c.categoryName,\n"
                        + "    bu.username AS buyer_username\n"
                        + "FROM \n"
                        + "    Product AS p\n"
                        + "JOIN \n"
                        + "    Status AS s ON p.statusID = s.id\n"
                        + "JOIN \n"
                        + "    User AS u ON p.userId = u.id\n"
                        + "JOIN \n"
                        + "    Category AS c ON p.categoryID = c.id\n"
                        + "LEFT JOIN \n"
                        + "    Trading AS t ON p.id = t.productID\n"
                        + "LEFT JOIN \n"
                        + "    User AS bu ON t.userID = bu.id\n"
                        + "WHERE \n"
                        + "    u.id = ? \n";

                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        statement.setInt(1, uid);
                        ResultSet result = statement.executeQuery();

                        ArrayList<Product> products = new ArrayList<>();
                        while (result.next())
                        {
                                Product product = new Product();
                                product.setId(result.getInt("id"));
                                product.setCode(result.getString("code"));
                                product.setProductName(result.getString("productName"));
                                product.setDescribe(result.getString("describe"));
                                product.setPrice(result.getDouble("price"));
                                product.setStock(result.getInt("stock"));
                                product.setContact(result.getString("contact"));
                                product.setIsPrivate(result.getBoolean("is_Private"));
                                product.setDataproduct(result.getString("dataProduct"));
                                product.setLink(result.getString("link"));

                                Status status = new Status();
                                status.setId(result.getInt("statusID"));
                                status.setStatusName(result.getString("statusName"));
                                product.setStatus(status);

                                // Create and set Category object
                                Category category = new Category();
                                category.setCategoryName(result.getString("categoryName"));
                                product.setCategory(category);
                                product.setCreated_at(result.getTimestamp("created_at"));
                                product.setUpdated_at(result.getTimestamp("updated_at"));

                                products.add(product);

                        }
                        return products;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        public Product getProductBySellerID(int id, int uID)
        {
                String sql = "select\n"
                        + "    p.*, \n"
                        + "    s.statusName, \n"
                        + "    u.username AS seller_username, \n"
                        + "    c.categoryName,\n"
                        + "    bu.username AS buyer_username\n"
                        + "FROM \n"
                        + "    Product AS p\n"
                        + "JOIN \n"
                        + "    Status AS s ON p.statusID = s.id\n"
                        + "JOIN \n"
                        + "    User AS u ON p.userId = u.id\n"
                        + "JOIN \n"
                        + "    Category AS c ON p.categoryID = c.id\n"
                        + "LEFT JOIN \n"
                        + "    Trading AS t ON p.id = t.productID\n"
                        + "LEFT JOIN \n"
                        + "    User AS bu ON t.userID = bu.id\n"
                        + "Where  p.id = ? and u.id = ?"; // Corrected SQL syntax

                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        // Set the statusName parameter
                        statement.setInt(1, id);
                        statement.setInt(2, uID); // Set the product ID parameter
                        ResultSet result = statement.executeQuery();

                        while (result.next())
                        {
                                Product product = new Product();
                                product.setId(result.getInt("id"));
                                product.setCode(result.getString("code"));
                                product.setProductName(result.getString("productName"));
                                product.setDescribe(result.getString("describe"));
                                product.setPrice(result.getDouble("price"));
                                product.setStock(result.getInt("stock"));
                                product.setContact(result.getString("contact"));
                                product.setDataproduct(result.getString("dataProduct"));
                                product.setLink(result.getString("link"));
                                product.setCreated_at(result.getTimestamp("created_at"));
                                product.setUpdated_at(result.getTimestamp("updated_at"));
                                product.setIsDeleted(result.getBoolean("isDeleted"));
                                product.setStatusID(result.getInt("statusID"));
                                product.setIsPrivate(result.getBoolean("is_Private"));

                                // Create and set Status object
                                Status status = new Status();
                                status.setStatusName(result.getString("statusName"));
                                product.setStatus(status);

                                // Create and set Category object
                                Category category = new Category();
                                category.setCategoryName(result.getString("categoryName"));
                                product.setCategory(category);

                                // Create and set Seller's information
                                User seller = new User();
                                seller.setUsername(result.getString("seller_username"));
                                product.setUser(seller);

                                // Create and set Buyer's information
                                if (result.getString("buyer_username") != null)
                                {
                                        User buyer = new User();
                                        buyer.setUsername(result.getString("buyer_username"));
                                        product.setUser(buyer);
                                }

                                return product;
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        public List<Product> filterByStatus(int status, int perPage, int pageNumber)
        {
                List<Product> list = new ArrayList<>();
                // 1 : chua ban
                // 2 da ban
                // 3 da xoa
                // 0 la tat ca
                try
                {
                        String SQL = "SELECT * FROM product WHERE 1=1 ";
                        if (status == 1)
                        {
                                SQL += " AND stock > 0 AND (isDeleted = 0 OR isDeleted IS NULL)";
                        } else if (status == 2)
                        {
                                SQL += " AND stock = 0 AND (isDeleted = 0 OR isDeleted IS NULL)";
                        } else if (status == 3)
                        {
                                SQL += " AND isDeleted = 1";
                        }

                        if (pageNumber <= 0)
                        {
                                pageNumber = 1; // tránh truyen so trang la so am
                        }
                        int startIndex = (pageNumber - 1) * perPage;
                        SQL += " LIMIT " + perPage + " OFFSET " + startIndex;
                        ps = connection.prepareStatement(SQL);

                        System.out.println("filter SQL = " + SQL);
                        rs = ps.executeQuery();
                        while (rs.next())
                        {
                                Product product = new Product();
                                product.setId(rs.getInt("id"));
                                product.setCode(rs.getString("code"));
                                product.setProductName(rs.getString("productName"));
                                product.setDescribe(rs.getString("describe"));
                                product.setPrice(rs.getDouble("price"));
                                product.setStock(rs.getInt("stock"));
                                product.setContact(rs.getString("contact"));
                                product.setIsPrivate(rs.getBoolean("is_Private"));
                                product.setDataproduct(rs.getString("dataProduct"));
                                product.setLink(rs.getString("link"));
                                product.setStatusID(rs.getInt("statusID"));
                                product.setCategoryID(rs.getInt("categoryID"));
                                product.setUserID(rs.getInt("userID"));
                                product.setCreated_at(rs.getTimestamp("created_at"));
                                product.setUpdated_at(rs.getTimestamp("updated_at"));
                                product.setDeleted_at(rs.getTimestamp("deleted_at"));
                                product.setIsDeleted(rs.getBoolean("isDeleted"));
                                
                                list.add(product);
                        }
                } catch (Exception e)
                {
                        System.out.println("filter product by status error: " + e.getMessage());
                        e.printStackTrace();
                }
                return list;
        }

        public int getTotalProduct()
        {
                try
                {
                        String SQL = "SELECT COUNT(*) FROM product";
                        ps = connection.prepareStatement(SQL);
                        rs = ps.executeQuery();
                        while (rs.next())
                        {
                                return rs.getInt(1);
                        }
                } catch (Exception e)
                {
                        System.out.println("Get total product error");
                }
                return 0;
        }

}
