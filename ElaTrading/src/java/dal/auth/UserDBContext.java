/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal.auth;

import dal.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import model.auth.User;

/**
 *
 * @author admin
 */
public class UserDBContext extends DBContext<User>
{

        ResultSet rs = null;
        PreparedStatement ps = null;

        @Override
        public ArrayList<User> getList(String field, String value)
        {
                String sql = "Select * from user\n";
                if (!field.equals(""))
                {
                        sql += "where " + field + " = " + value;
                }
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        ResultSet result = statement.executeQuery();
                        ArrayList<User> users = new ArrayList<>();
                        while (result.next())
                        {
                                User user = new User();
                                user.setId(result.getInt("id"));
                                user.setUsername(result.getString("username"));
                                user.setPassword(result.getString("password"));
                                user.setEmail(result.getString("email"));
                                user.setBalance(result.getDouble("balance"));
                                user.setPhone(result.getString("phone"));
                                user.setActived(result.getBoolean("actived"));
                                user.setBanned(result.getBoolean("Banned"));
                                user.setNotification_code(result.getString("notification_code"));
                                user.setRole(result.getString("role"));
                                user.setCreated_at(result.getTimestamp("created_at"));
                                user.setUpdated_at(result.getTimestamp("updated_at"));
                                user.setDeleted_at(result.getTimestamp("deleted_at"));
                                user.setIsDeleted(result.getBoolean("isDeleted"));

                                users.add(user);
                        }
                        return users;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public User get(String field, String value)
        {
                String sql = "Select * from user\n"
                        + "where " + field + " = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        statement.setString(1, value);
                        ResultSet result = statement.executeQuery();
                        while (result.next())
                        {
                                User user = new User();
                                user.setId(result.getInt("id"));
                                user.setUsername(result.getString("username"));
                                user.setPassword(result.getString("password"));
                                user.setEmail(result.getString("email"));
                                user.setBalance(result.getDouble("balance"));
                                user.setBanned(result.getBoolean("Banned"));
                                user.setPhone(result.getString("phone"));
                                user.setActived(result.getBoolean("actived"));
                                user.setNotification_code(result.getString("notification_code"));
                                user.setRole(result.getString("role"));
                                user.setCreated_at(result.getTimestamp("created_at"));
                                user.setUpdated_at(result.getTimestamp("updated_at"));
                                user.setDeleted_at(result.getTimestamp("deleted_at"));
                                user.setIsDeleted(result.getBoolean("isDeleted"));

                                return user;
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public User insert(User user)
        {
                String sql = "INSERT INTO swp_new.user\n"
                        + "(username,\n"
                        + "password,\n"
                        + "email,\n"
                        + "balance,\n"
                        + "phone,\n"
                        + "actived,\n"
                        + "banned,\n"
                        + "role,\n"
                        + "created_at,\n"
                        + "updated_at,\n"
                        + "deleted_at,\n"
                        + "isDeleted)\n"
                        + "VALUES(?, ?, ?, 0, null, 0, 0, 'user', ?, ?, null, 0);";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        statement.setString(1, user.getUsername());
                        statement.setString(2, user.getPassword());
                        statement.setString(3, user.getEmail());
                        statement.setTimestamp(4, user.getCreated_at());
                        statement.setTimestamp(5, user.getUpdated_at());
                        System.out.println(statement);
                        statement.executeUpdate();
                        User new_user = get("username", user.getUsername());

                        return new_user;
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public void update(User user)
        {
                String sql = "Update swp_new.user\n"
                        + "Set username = ?,\n"
                        + "password = ?,\n"
                        + "email = ?,\n"
                        + "balance = ?,\n"
                        + "phone = ?,\n"
                        + "actived = ?,\n"
                        + "banned = ?,\n"
                        + "role = ?,\n"
                        + "created_at = ?,\n"
                        + "updated_at = ?,\n"
                        + "deleted_at = ?,\n"
                        + "isDeleted = ?\n"
                        + "Where id = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        statement.setString(1, user.getUsername());
                        statement.setString(2, user.getPassword());
                        statement.setString(3, user.getEmail());
                        statement.setDouble(4, user.getBalance());
                        statement.setString(5, user.getPhone());
                        statement.setBoolean(6, user.isActived());
                        statement.setBoolean(7, user.isBanned());
                        statement.setString(8, user.getRole());
                        statement.setTimestamp(9, user.getCreated_at());
                        statement.setTimestamp(10, user.getUpdated_at());
                        statement.setTimestamp(11, user.getDeleted_at());
                        statement.setBoolean(12, user.isIsDeleted());
                        statement.setInt(13, user.getId());

                        statement.executeUpdate();
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
        }

        @Override
        public void delete(int id, User user)
        {
                String sql = "Update swp_new.user\n"
                        + "Set deleted_at = ?,\n"
                        + "isDeleted = 1,\n"
                        + "deleted_by = ?\n"
                        + " Where id = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        Timestamp deleted_at = new Timestamp(System.currentTimeMillis());
                        statement.setTimestamp(1, deleted_at);
                        statement.setInt(2, user.getId());
                        statement.setInt(3, id);

                        statement.executeUpdate();
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
        }

//        public List<User> getUsernameByName(String username)
//        {
//                List<User> list = new ArrayList<>();
//                try
//                {
//                        String SQL = "SELECT id, username FROM `user` WHERE username LIKE '%" + username + "%'";
//                        ps = connection.prepareStatement(SQL);
//                        rs = ps.executeQuery();
//
//                        while (rs.next())
//                        {
//                                User u = new User();
//
//                                u.setId(rs.getInt(1));
//                                u.setUsername(rs.getString(2));
//                                list.add(u);
//                        }
//                } catch (Exception e)
//                {
//                        System.out.println("Get user by username error: " + e.getMessage());
//                }
//                return list;
//        }
        public User getLogin(String username, String password)
        {
                String sql = "select * from user"
                        + " where username = ? and password = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        statement.setString(1, username);
                        statement.setString(2, password);
                        ResultSet result = statement.executeQuery();
                        while (result.next())
                        {
                                User user = new User();
                                user.setId(result.getInt("id"));
                                user.setUsername(result.getString("username"));
                                user.setPassword(result.getString("password"));
                                user.setEmail(result.getString("email"));
                                user.setBalance(result.getDouble("balance"));
                                user.setPhone(result.getString("phone"));
                                user.setActived(result.getBoolean("actived"));
                                user.setBanned(result.getBoolean("banned"));
                                user.setNotification_code(result.getString("notification_code"));
                                user.setRole(result.getString("role"));
                                user.setCreated_at(result.getTimestamp("created_at"));
                                user.setUpdated_at(result.getTimestamp("updated_at"));
                                user.setDeleted_at(result.getTimestamp("deleted_at"));
                                user.setIsDeleted(result.getBoolean("isDeleted"));

                                return user;
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        public User resetNotification_code(String id, String code)
        {
                String sql = "UPDATE `SWP_NEW`.`user`\n"
                        + "SET `notification_code` = ?\n"
                        + "WHERE `id` = ?;";
                try
                {
                        PreparedStatement statement = connection.prepareStatement(sql);
                        statement.setString(1, code);
                        statement.setString(2, id);

                        statement.executeUpdate();

                        User user = get("id", id);
                        return user;
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        public User getUserInfor(int id)
        {
                String sql = "select * From User\n"
                        + " where id = ?";
                try
                {
                        PreparedStatement pre = connection.prepareStatement(sql);
                        pre.setInt(1, id);
                        ResultSet result = pre.executeQuery();
                        while (result.next())
                        {
                                User user = new User();
                                user.setId(result.getInt("id"));
                                user.setUsername(result.getString("username"));
                                user.setPassword("password");
                                user.setEmail(result.getString("email"));
                                user.setBalance(result.getDouble("balance"));
                                user.setBanned(result.getBoolean("banned"));
                                user.setPhone(result.getString("phone"));
                                user.setActived(result.getBoolean("actived"));
                                user.setRole(result.getString("role"));
                                user.setCreated_at(result.getTimestamp("created_at"));
                                user.setCreated_by(result.getInt("created_by"));
                                user.setUpdated_at(result.getTimestamp("updated_at"));
                                user.setUpdated_by(result.getInt("updated_by"));
                                user.setDeleted_at(result.getTimestamp("deleted_at"));
                                user.setDeleted_by(result.getInt("deleted_by"));
                                user.setIsDeleted(result.getBoolean("isDeleted"));

                                return user;
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        public User changePassword(String pass, int id)
        {
                String sql = "Update User\n"
                        + "Set password = ?\n"
                        + "where id = ?";
                try
                {
                        PreparedStatement pre = connection.prepareStatement(sql);

                        pre.setString(1, pass);
                        pre.setInt(2, id);
                        pre.executeUpdate();

                } catch (SQLException e)
                {
                        System.out.println(e);
                }
                return null;
        }

        public User editProfile(String username, String email, String phone, int userid)
        {
                String sql = "Update user\n"
                        + "Set username = ?,\n"
                        + "email = ?,\n"
                        + "phone = ?\n"
                        + "where id = ?";
                try
                {
                        PreparedStatement pre = connection.prepareStatement(sql);

                        pre.setString(1, username);
                        pre.setString(2, email);
                        pre.setString(3, phone);
                        pre.setInt(4, userid);
                        pre.executeUpdate();

                } catch (SQLException e)
                {
                        System.out.println(e);
                }
                return null;
        }

        public User getById(int value)
        {
                String sql = "Select * from user\n"
                        + "where id = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        statement.setInt(1, value);
                        ResultSet result = statement.executeQuery();
                        while (result.next())
                        {
                                User user = new User();
                                user.setId(result.getInt("id"));
                                user.setUsername(result.getString("username"));
                                user.setPassword(result.getString("password"));
                                user.setEmail(result.getString("email"));
                                user.setBalance(result.getDouble("balance"));
                                user.setBanned(result.getBoolean("banned"));
                                user.setPhone(result.getString("phone"));
                                user.setActived(result.getBoolean("actived"));
                                user.setRole(result.getString("role"));
                                user.setCreated_at(result.getTimestamp("created_at"));
                                user.setUpdated_at(result.getTimestamp("updated_at"));
                                user.setDeleted_at(result.getTimestamp("deleted_at"));
                                user.setIsDeleted(result.getBoolean("isDeleted"));

                                return user;
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        public boolean changePassword(String oldPassword, String newPassword, int userId)
        {
                // Chuỗi SQL để cập nhật mật khẩu mới
                String sql = "UPDATE User SET password = ? WHERE id = ? AND password = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        // Thiết lập các tham số cho truy vấn
                        statement.setString(1, newPassword);
                        statement.setInt(2, userId);
                        statement.setString(3, oldPassword);

                        // Thực thi truy vấn
                        int rowsAffected = statement.executeUpdate();

                        // Trả về true nếu có ít nhất một dòng được cập nhật
                        return rowsAffected > 0;

                } catch (SQLException e)
                {
                        e.printStackTrace();
                        // Xử lý các ngoại lệ SQL (ví dụ: log, gửi email thông báo lỗi, ...)
                        return false; // Trả về false nếu có lỗi xảy ra
                }
        }

        public ArrayList<User> searchByField(String field, String value)
        {
                String sql = "Select * from user\n";
                if (!field.equals(""))
                {
                        sql += "where " + field + " LIKE '%" + value + "%'";
                }
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        ResultSet result = statement.executeQuery();
                        ArrayList<User> users = new ArrayList<>();
                        while (result.next())
                        {
                                User user = new User();
                                user.setId(result.getInt("id"));
                                user.setUsername(result.getString("username"));
                                user.setPassword(result.getString("password"));
                                user.setEmail(result.getString("email"));
                                user.setBalance(result.getDouble("balance"));
                                user.setPhone(result.getString("phone"));
                                user.setActived(result.getBoolean("actived"));
                                user.setBanned(result.getBoolean("Banned"));
                                user.setRole(result.getString("role"));
                                user.setCreated_at(result.getTimestamp("created_at"));
                                user.setUpdated_at(result.getTimestamp("updated_at"));
                                user.setDeleted_at(result.getTimestamp("deleted_at"));
                                user.setIsDeleted(result.getBoolean("isDeleted"));

                                users.add(user);
                        }
                        return users;

                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        public ArrayList<User> getListStatusFilter(String filter)
        {
                String sql = "SELECT * FROM User  where isDeleted <> 1 or isDeleted is null";
                if (filter.equalsIgnoreCase("banned"))
                {
                        sql = "SELECT * FROM User where banned = 1";
                } else if (filter.equalsIgnoreCase("deleted"))
                {
                        sql = "SELECT * FROM User where isDeleted = 1";
                }

                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        ResultSet result = statement.executeQuery();
                        ArrayList<User> users = new ArrayList<>();
                        while (result.next())
                        {
                                User user = new User();
                                user.setId(result.getInt("id"));
                                user.setUsername(result.getString("username"));
                                user.setPassword(result.getString("password"));
                                user.setEmail(result.getString("email"));
                                user.setBalance(result.getDouble("balance"));
                                user.setPhone(result.getString("phone"));
                                user.setActived(result.getBoolean("actived"));
                                user.setBanned(result.getBoolean("banned"));
                                user.setRole(result.getString("role"));
                                user.setCreated_at(result.getTimestamp("created_at"));
                                user.setUpdated_at(result.getTimestamp("updated_at"));
                                user.setDeleted_at(result.getTimestamp("deleted_at"));
                                user.setIsDeleted(result.getBoolean("isDeleted"));

                                users.add(user);
                        }
                        return users;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        public ArrayList<User> getListByCondition(String condition)
        {
                String sql = "Select * from user " + condition;

                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        ResultSet result = statement.executeQuery();
                        ArrayList<User> users = new ArrayList<>();
                        while (result.next())
                        {
                                User user = new User();
                                user.setId(result.getInt("id"));
                                user.setUsername(result.getString("username"));
                                user.setPassword(result.getString("password"));
                                user.setEmail(result.getString("email"));
                                user.setBalance(result.getDouble("balance"));
                                user.setPhone(result.getString("phone"));
                                user.setActived(result.getBoolean("actived"));
                                user.setBanned(result.getBoolean("Banned"));
                                user.setRole(result.getString("role"));
                                user.setCreated_at(result.getTimestamp("created_at"));
                                user.setUpdated_at(result.getTimestamp("updated_at"));
                                user.setDeleted_at(result.getTimestamp("deleted_at"));
                                user.setIsDeleted(result.getBoolean("isDeleted"));

                                users.add(user);
                        }
                        return users;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        public void deleteThrowException(int id, model.auth.User user) throws Exception
        {
                if (user.getId() == id)
                {
                        throw new Exception();
                }
                String sql = "Update user set isDeleted = 1 where id = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        statement.setInt(1, id);

                        statement.executeUpdate();
                } catch (Exception er)
                {
                        throw new Exception(er);
                }
        }
}
