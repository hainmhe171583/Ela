/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal.auth;

import dal.DBContext;
import dal.auth.UserDBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import model.auth.User;
import model.auth.Rating;

/**
 *
 * @author admin
 */
public class RatingDBContext extends DBContext<Rating>
{

        public ArrayList<Rating> getListRating(String field, String value)
        {
                UserDBContext udb = new UserDBContext();
                String sql = "Select * from rating\n";
                if (!field.equals(""))
                {
                        sql += "where " + field + " = " + value + " ORDER BY created_at DESC;";
                }
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        ResultSet result = statement.executeQuery();
                        ArrayList<Rating> ratings = new ArrayList<>();
                        while (result.next())
                        {
                                Rating rating = new Rating();
                                rating.setId(result.getInt("id"));
                                rating.setCode(result.getString("code"));
                                rating.setRate(result.getInt("rate"));
                                rating.setComment(result.getString("comment"));
                                rating.setUserID(result.getInt("userID"));
                                rating.setUser(udb.get("id", String.valueOf(result.getInt("userID"))));
                                rating.setCreated_at(result.getTimestamp("created_at"));
                                rating.setCreated_by(result.getInt("created_by"));
                                rating.setUpdated_at(result.getTimestamp("updated_at"));
                                rating.setDeleted_at(result.getTimestamp("deleted_at"));
                                rating.setIsDeleted(result.getBoolean("isDeleted"));
                                ratings.add(rating);
                        }
                        return ratings;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public Rating get(String field, String value)
        {
                String sql = "Select * from rating\n"
                        + "where " + field + " = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        statement.setString(1, value);
                        ResultSet result = statement.executeQuery();
                        while (result.next())
                        {
                                Rating rating = new Rating();
                                rating.setId(result.getInt("id"));
                                rating.setCode(result.getString("code"));
                                rating.setRate(result.getInt("rate"));
                                rating.setComment(result.getString("comment"));
                                rating.setUserID(result.getInt("userID"));
                                rating.setCreated_at(result.getTimestamp("created_at"));
                                rating.setUpdated_at(result.getTimestamp("updated_at"));
                                rating.setDeleted_at(result.getTimestamp("deleted_at"));
                                rating.setIsDeleted(result.getBoolean("isDeleted"));
                                return rating;
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public Rating insert(Rating rating)
        {
                String sql = "INSERT INTO swp_new.rating\n"
                        + "(code,"
                        + " rate,"
                        + " comment,"
                        + " userID,"
                        + " created_at,"
                        + " updated_at,"
                        + " deleted_at,"
                        + " isDeleted)\n"
                        + "VALUES(?, ?, ?, ?, ?, ?, null, 0);";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        statement.setString(1, rating.getCode());
                        statement.setInt(2, rating.getRate());
                        statement.setString(3, rating.getComment());
                        statement.setInt(4, rating.getUserID());
                        statement.setTimestamp(5, rating.getCreated_at());
                        statement.setTimestamp(6, rating.getUpdated_at());
                        statement.executeUpdate();
                        Rating new_rating = get("code", rating.getCode());

                        return new_rating;
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public void update(Rating rating)
        {
                String sql = "Update swp_new.rating\n"
                        + "Set code = ?,"
                        + " rate = ?,"
                        + " comment = ?,"
                        + " productID = ?,"
                        + " userID = ?,"
                        + " created_at = ?,"
                        + " updated_at = ?,"
                        + " deleted_at = ?,"
                        + " isDeleted = ?\n"
                        + "Where id = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        statement.setString(1, rating.getCode());
                        statement.setInt(2, rating.getRate());
                        statement.setString(3, rating.getComment());
                        statement.setInt(5, rating.getUserID());
                        statement.setTimestamp(6, rating.getCreated_at());
                        statement.setTimestamp(7, rating.getUpdated_at());
                        statement.setTimestamp(8, rating.getDeleted_at());
                        statement.setBoolean(9, rating.isIsDeleted());
                        statement.setInt(10, rating.getId());

                        statement.executeUpdate();
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
        }

        @Override
        public void delete(int id, User user)
        {
                String sql = "Update swp_new.rating\n"
                        + "Set deleted_at = ?,"
                        + " isDeleted = 1\n"
                        + "deleted_by = ?\n"
                        + " Where id = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        Timestamp deleted_at = new Timestamp(System.currentTimeMillis());
                        statement.setTimestamp(1, deleted_at);
                        statement.setInt(2, user.getId());
                        statement.setInt(3, id);
                        statement.executeUpdate();
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
        }

        public void deleteByCode(String code, User user)
        {
                String sql = "Update swp_new.rating\n"
                        + "Set deleted_at = ?,"
                        + " isDeleted = 1,\n"
                        + "deleted_by = ?\n"
                        + " Where code = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        Timestamp deleted_at = new Timestamp(System.currentTimeMillis());
                        statement.setTimestamp(1, deleted_at);
                        statement.setInt(2, user.getId());
                        statement.setString(3, code);
                        statement.executeUpdate();
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
        }

        @Override
        public ArrayList<Rating> getList(String field, String value)
        {
                throw new UnsupportedOperationException("Not supported yet.");
        }
}
