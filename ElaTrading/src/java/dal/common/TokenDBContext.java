/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal.common;

import dal.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.auth.User;
import model.common.Token;

/**
 *
 * @author admin
 */
public class TokenDBContext extends DBContext<Token>
{
        
        @Override
        public ArrayList<Token> getList(String field, String value)
        {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        @Override
        public Token get(String field, String value)
        {
                String sql = "SELECT * FROM swp_new.token"
                        + " where " + field + " = ?";
                try
                {
                        PreparedStatement statement = connection.prepareStatement(sql);
                        
                        statement.setString(1, value);
                        ResultSet result = statement.executeQuery();
                        while(result.next())
                        {
                                Token token = new Token();
                                token.setId(result.getInt("id"));
                                token.setToken(result.getString("token"));
                                token.setEmail(result.getString("email"));
                                token.setExpiryDate(result.getTimestamp("expiryDate"));
                                
                                return token;
                        }
                }
                catch(SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }
        
        @Override
        public Token insert(Token token)
        {
                String sql = "INSERT INTO swp_new.token\n"
                        + "(token,\n"
                        + "email,\n"
                        + "expiryDate)\n"
                        + "VALUES(?, ?, ?)";
                try
                {
                        PreparedStatement statement = connection.prepareStatement(sql);
                        
                        statement.setString(1, token.getToken());
                        statement.setString(2, token.getEmail());
                        statement.setTimestamp(3, token.getExpiryDate());
                        statement.executeUpdate();
                        
                        Token new_token = get("token", token.getToken());
                        return new_token;
                }
                catch(SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public void update(Token model)
        {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        @Override
        public void delete(int id, User user)
        {
                
        }
        
        public void delete(String token)
        {
                String sql = "DELETE FROM swp_new.token "
                        + " WHERE token = ?";
                
                try
                {
                        PreparedStatement statement = connection.prepareStatement(sql);
                        statement.setString(1, token);
                        
                        statement.executeUpdate();
                }
                catch(Exception er)
                {
                        er.printStackTrace();
                }                
        }
        
        public Token checkValid(String _token, String email)
        {
                String sql = "SELECT * FROM swp_new.token"
                        + " where token =  ? and email = ?";
                try
                {
                        PreparedStatement statement = connection.prepareStatement(sql);
                        statement.setString(1, _token);
                        statement.setString(2, email);
                        
                        ResultSet result = statement.executeQuery();
                        while(result.next())
                        {
                                Token token = new Token();
                                token.setId(result.getInt("id"));
                                token.setToken(result.getString("token"));
                                token.setEmail(result.getString("email"));
                                token.setExpiryDate(result.getTimestamp("expiryDate"));
                                
                                return token;
                        }
                }
                catch(Exception e)
                {
                        e.printStackTrace();
                }
                return null;
        }

}