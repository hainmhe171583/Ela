/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal.payment;

import dal.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.auth.User;
import model.status.Status;
import model.payment.Transac;

/**
 *
 * @author hai20
 */
public class TransacDBContext extends DBContext<Transac>
{

        @Override
        public ArrayList<Transac> getList(String field, String value)
        {
                String sql = "Select * from transac\n";
                if (!field.equals(""))
                {
                        sql += "where " + field + " = " + value;
                }
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        ResultSet result = statement.executeQuery();
                        ArrayList<Transac> transac = new ArrayList<>();
                        while (result.next())
                        {
                                Transac trans = new Transac();
                                trans.setId(result.getInt("id"));
                                trans.setType(result.getString("type"));

                                transac.add(trans);
                        }
                        return transac;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public Transac get(String field, String value)
        {
                //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
                String sql = "Select * from transac\n"
                        + "where " + field + " = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        statement.setString(1, value);
                        ResultSet result = statement.executeQuery();
                        while (result.next())
                        {
                                Transac trans = new Transac();
                                trans.setId(result.getInt("id"));
                                trans.setType(result.getString("type"));

                                return trans;
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;

        }

        @Override
        public Transac insert(Transac model)
        {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        @Override
        public void update(Transac model)
        {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        @Override
        public void delete(int id, User user)
        {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

}
