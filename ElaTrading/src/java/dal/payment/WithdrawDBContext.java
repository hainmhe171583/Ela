/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal.payment;

import dal.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.auth.User;
import model.withdraw.Withdraw;

/**
 *
 * @author hai20
 */
public class WithdrawDBContext extends DBContext<Withdraw>
{

        @Override
        public ArrayList<Withdraw> getList(String field, String value)
        {
                String sql = "Select * from trading\n";
                if (!field.equals(""))
                {
                        sql += "where " + field + " = " + value;
                }
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        ResultSet result = statement.executeQuery();
                        ArrayList<Withdraw> withdraws = new ArrayList<>();
                        while (result.next())
                        {
                                Withdraw withdraw = new Withdraw();
                                withdraw.setId(result.getInt("id"));
                                withdraw.setCode(result.getString("code"));
                                withdraw.setTotal(result.getDouble("total"));
                                withdraw.setUserInfo(result.getString("userinfo"));
                                withdraw.setRespond(result.getString("respond"));
                                withdraw.setStatusID(result.getInt("statusID"));
                                withdraw.setUserID(result.getInt("userID"));
                                withdraw.setCreated_at(result.getTimestamp("created_at"));
                                withdraw.setUpdated_at(result.getTimestamp("updated_at"));
                                withdraws.add(withdraw);
                        }
                        return withdraws;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public Withdraw get(String field, String value)
        {
                String sql = "Select * from withdraw\n"
                        + "where " + field + " = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        statement.setString(1, value);
                        ResultSet result = statement.executeQuery();
                        while (result.next())
                        {
                                Withdraw withdraw = new Withdraw();
                                withdraw.setId(result.getInt("id"));
                                withdraw.setCode(result.getString("code"));
                                withdraw.setTotal(result.getDouble("total"));
                                withdraw.setUserInfo(result.getString("userinfo"));
                                withdraw.setRespond(result.getString("respond"));
                                withdraw.setStatusID(result.getInt("statusID"));
                                withdraw.setUserID(result.getInt("userID"));
                                withdraw.setCreated_at(result.getTimestamp("created_at"));
                                withdraw.setUpdated_at(result.getTimestamp("updated_at"));
                                return withdraw;
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public Withdraw insert(Withdraw withdraw)
        {
                String sql = "INSERT INTO `swp_new`.`withdraw`\n"
                        + "(`code`,`total`,`userinfo`,`respond`,`statusID`,`userId`,`created_at`,`updated_at`)\n"
                        + "VALUES\n"
                        + "(?,?,?,?,?,?,?,?);";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        statement.setString(1, withdraw.getCode());
                        statement.setDouble(2, withdraw.getTotal());
                        statement.setString(3, withdraw.getUserInfo());
                        statement.setString(4, withdraw.getRespond());
                        statement.setInt(5, withdraw.getStatusID());
                        statement.setInt(6, withdraw.getUserID());
                        statement.setTimestamp(7, withdraw.getCreated_at());
                        statement.setTimestamp(8, withdraw.getUpdated_at());
                        statement.executeUpdate();
                        Withdraw new_withdraw = get("code", withdraw.getCode());

                        return new_withdraw;
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public void update(Withdraw withdraw)
        {
                String sql = "UPDATE `swp_new`.`withdraw`\n"
                        + "SET\n"
                        + "`code` = ?,\n"
                        + "`total` = ?,\n"
                        + "`userinfo` = ?,\n"
                        + "`respond` = ?,\n"
                        + "`statusID` = ?,\n"
                        + "`userId` = ?,\n"
                        + "`created_at` = ?,\n"
                        + "`updated_at` = ?\n"
                        + "WHERE `code` = ?;";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        statement.setString(1, withdraw.getCode());
                        statement.setDouble(2, withdraw.getTotal());
                        statement.setString(3, withdraw.getUserInfo());
                        statement.setString(4, withdraw.getRespond());
                        statement.setInt(5, withdraw.getStatusID());
                        statement.setInt(6, withdraw.getUserID());
                        statement.setTimestamp(7, withdraw.getCreated_at());
                        statement.setTimestamp(8, withdraw.getUpdated_at());
                        statement.setString(9, withdraw.getCode());
                        statement.executeUpdate();

                        statement.executeUpdate();
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
        }

        @Override
        public void delete(int id, User user)
        {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        public ArrayList<Withdraw> getByID(int id, int pageNumber)
        {
                String sql = "SELECT w.*,u.username FROM withdraw as w\n"
                        + "Join User as u on w.userId = u.id\n"
                        + "where w.userId = ?\n"
                        + "Limit 5 offset ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        int offset = (pageNumber - 1) * 5; // Calculate offset
                        statement.setInt(1, id);
                        statement.setInt(2, offset);
                        ResultSet result = statement.executeQuery();
                        ArrayList<Withdraw> withdraws = new ArrayList<>();
                        while (result.next())
                        {
                                Withdraw withdraw = new Withdraw();
                                withdraw.setId(result.getInt("id"));
                                withdraw.setCode(result.getString("code"));
                                withdraw.setTotal(result.getDouble("total"));
                                withdraw.setUserInfo(result.getString("userinfo"));
                                withdraw.setRespond(result.getString("respond"));
                                withdraw.setStatusID(result.getInt("statusID"));
                                withdraw.setUserID(result.getInt("userID"));
                                withdraw.setCreated_at(result.getTimestamp("created_at"));
                                withdraw.setUpdated_at(result.getTimestamp("updated_at"));
                                User user = new User();
                                user.setUsername(result.getString("username"));
                                withdraw.setUser(user);
                                withdraws.add(withdraw);
                        }
                        return withdraws;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        public int countAllWithdraw(int id)
        {
                String sql = "SELECT count(*) FROM swp_new.withdraw as w\n"
                        + "where w.userId = ?";
                try
                {
                        PreparedStatement statement = connection.prepareStatement(sql);
                        statement.setInt(1, id);
                        ResultSet result = statement.executeQuery();
                        if (result.next())
                        {
                                return result.getInt(1); // Return the count
                        }
                } catch (SQLException e)
                {
                        e.printStackTrace(); // Handle the exception according to your requirement
                }
                return 0;
        }

        public int countAllWithdrawByStatus(int id, int sid)
        {
                String sql = "SELECT count(*) FROM swp_new.withdraw as w\n"
                        + "where w.userId = ? and w.statusID = ?";
                try
                {
                        PreparedStatement statement = connection.prepareStatement(sql);
                        statement.setInt(1, id);
                        statement.setInt(2, sid);
                        ResultSet result = statement.executeQuery();
                        if (result.next())
                        {
                                return result.getInt(1); // Return the count
                        }
                } catch (SQLException e)
                {
                        e.printStackTrace(); // Handle the exception according to your requirement
                }
                return 0;
        }

        public ArrayList<Withdraw> getByStatus(int id, int pageNumber, int sid)
        {
                String sql = "SELECT w.*,u.username FROM withdraw as w\n"
                        + "Join User as u on w.userId = u.id\n"
                        + "where w.userId = ? and w.statusID = ?\n"
                        + "Limit 5 offset ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        int offset = (pageNumber - 1) * 5; // Calculate offset
                        statement.setInt(1, id);
                        statement.setInt(2, sid);
                        statement.setInt(3, offset);
                        ResultSet result = statement.executeQuery();
                        ArrayList<Withdraw> withdraws = new ArrayList<>();
                        while (result.next())
                        {
                                Withdraw withdraw = new Withdraw();
                                withdraw.setId(result.getInt("id"));
                                withdraw.setCode(result.getString("code"));
                                withdraw.setTotal(result.getDouble("total"));
                                withdraw.setUserInfo(result.getString("userinfo"));
                                withdraw.setRespond(result.getString("respond"));
                                withdraw.setStatusID(result.getInt("statusID"));
                                withdraw.setUserID(result.getInt("userID"));
                                withdraw.setCreated_at(result.getTimestamp("created_at"));
                                withdraw.setUpdated_at(result.getTimestamp("updated_at"));

                                User user = new User();
                                user.setUsername(result.getString("username"));
                                withdraw.setUser(user);
                                withdraws.add(withdraw);
                        }
                        return withdraws;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

}
