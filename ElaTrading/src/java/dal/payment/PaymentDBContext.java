/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal.payment;

import dal.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.auth.User;
import model.payment.Pay;
import model.payment.Transac;

/**
 *
 * @author hai20
 */
public class PaymentDBContext extends DBContext<Pay>
{

        @Override
        public ArrayList<Pay> getList(String field, String value)
        {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        @Override
        public Pay get(String field, String value)
        {
                String sql = "Select * from payment\n"
                        + "where " + field + " = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        statement.setString(1, value);
                        ResultSet result = statement.executeQuery();
                        while (result.next())
                        {
                                Pay payment = new Pay();
                                payment.setVnp_TxnRef(result.getString("vnp_TxnRef"));
                                payment.setPayment_code(result.getString("payment_code"));
                                payment.setAmount(result.getDouble("amount"));
                                payment.setLink(result.getString("link"));
                                payment.setStatus(result.getBoolean("status"));
                                payment.setTransId(result.getInt("transId"));
                                payment.setUserId(result.getInt("userID"));
                                payment.setCreated_at(result.getTimestamp("created_at"));
                                //payment.setIsDeleted(result.getBoolean("isDeleted"));

                                return payment;
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public Pay insert(Pay Payment)
        {
                //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
                String sql = "INSERT INTO `swp_new`.`payment`\n"
                        + "(`vnp_TxnRef`, `payment_code`, `amount`, link, `status`, `transId`, `userID`, `created_at`)\n"
                        + "VALUES\n"
                        + "(?, ?, ?, ?, ?, ?, ?, ?);";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        
                        statement.setString(1, Payment.getVnp_TxnRef());
                        statement.setString(2, Payment.getPayment_code());
                        statement.setDouble(3, Payment.getAmount());
                        statement.setString(4, Payment.getLink());
                        statement.setBoolean(5, Payment.isStatus());
                        statement.setInt(6, Payment.getTransId());
                        statement.setInt(7, Payment.getUserId());
                        statement.setTimestamp(8, Payment.getCreated_at());
                        //statement.setInt(9, Payment.getCreated_by());

                        statement.executeUpdate();

                        return Payment;
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public void update(Pay Payment)
        {
                String sql = "UPDATE `swp_new`.`payment`\n"
                        + "SET\n"
                        + "`vnp_TxnRef` = ?,\n"
                        + "`payment_code` = ?,\n"
                        + "`amount` = ?,\n"
                        + "`link` = ?,\n"
                        + "`status` = ?,\n"
                        + "`transId` = ?\n"
                        + "`userID` = ?,\n"
                        + "WHERE `payment_code` = ?;";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        statement.setString(1, Payment.getVnp_TxnRef());
                        statement.setString(2, Payment.getPayment_code());
                        statement.setDouble(3, Payment.getAmount());
                        statement.setString(4, Payment.getLink());
                        statement.setBoolean(5, Payment.isStatus());
                        statement.setInt(6, Payment.getUserId());
                        statement.setInt(7, Payment.getTransId());
                        statement.setString(8, Payment.getPayment_code());

                        statement.executeUpdate();
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
        }

        @Override
        public void delete(int id, User user)
        {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        public ArrayList<Pay> filterListPayment(int pageNumber, int userId, int transId)
        {
                String sql = "SELECT pay.*, tran.type "
                        + "FROM swp_new.payment as pay\n"
                        + "JOIN user as u on pay.userID = u.id\n"
                        + "JOIN transac as tran on pay.transId = tran.id\n"
                        + "WHERE pay.userID = ? AND pay.transId = ?\n"
                        + "LIMIT 5 OFFSET ?";

                try ( PreparedStatement statement = connection.prepareStatement(sql))
                {
                        int offset = (pageNumber - 1) * 5; // Calculate offset
                        statement.setInt(1, userId);
                        statement.setInt(2, transId);
                        statement.setInt(3, offset);
                        try ( ResultSet result = statement.executeQuery())
                        {
                                ArrayList<Pay> payments = new ArrayList<>();
                                while (result.next())
                                {
                                        Pay payment = new Pay();
                                        payment.setId(result.getInt("id"));
                                        payment.setVnp_TxnRef(result.getString("vnp_TxnRef"));
                                        payment.setPayment_code(result.getString("payment_code"));
                                        payment.setAmount(result.getInt("amount"));
                                        payment.setLink(result.getString("link"));
                                        payment.setStatus(result.getBoolean("status"));
                                        payment.setTransId(result.getInt("transId"));
                                        payment.setUserId(result.getInt("userID"));
                                        payment.setCreated_at(result.getTimestamp("created_at"));

                                        payments.add(payment);
                                }
                                return payments;
                        }
                } catch (SQLException e)
                {
                        e.printStackTrace();
                        return null;
                }
        }

        public ArrayList<Pay> filterAllPayment(int pageNumber, int userId)
        {
                String sql = "SELECT pay.*,tran.* "
                        + "FROM swp_new.payment as pay\n"
                        + "Join user as u on pay.userID = u.id\n"
                        + "Join transac as tran on pay.transId = tran.id\n"
                        + "where pay.userID = ?\n"
                        + "Limit 5 offset ?;";
                try ( PreparedStatement statement = connection.prepareStatement(sql))
                {
                        int offset = (pageNumber - 1) * 5; // Calculate offset
                        statement.setInt(1, userId);
                        statement.setInt(2, offset);
                        try ( ResultSet result = statement.executeQuery())
                        {
                                ArrayList<Pay> payments = new ArrayList<>();
                                while (result.next())
                                {
                                        Pay payment = new Pay();
                                        payment.setId(result.getInt("id"));
                                        payment.setVnp_TxnRef(result.getString("vnp_TxnRef"));
                                        payment.setPayment_code(result.getString("payment_code"));
                                        payment.setAmount(result.getInt("amount"));
                                        payment.setLink(result.getString("link"));
                                        payment.setStatus(result.getBoolean("status"));
                                        payment.setTransId(result.getInt("transId"));
                                        payment.setUserId(result.getInt("userID"));
                                        payment.setCreated_at(result.getTimestamp("created_at"));
                                        payment.setCreated_by(result.getInt("created_by"));

                                        Transac tran = new Transac();
                                        tran.setType(result.getString("type"));

                                        payments.add(payment);
                                }
                                return payments;
                        }
                } catch (SQLException e)
                {
                        e.printStackTrace();
                        return null;
                }
        }

        public int countAllPaymentByStatus(int id)
        {
                String sql = "SELECT count(*)\n"
                        + "FROM swp_new.payment as pay\n"
                        + "Join user as u on pay.userID = u.id\n"
                        + "Join transac as tran on pay.transId = tran.id\n"
                        + "where pay.userID = ? and pay.status = 1;";
                try
                {
                        PreparedStatement statement = connection.prepareStatement(sql);
                        statement.setInt(1, id);
                        ResultSet result = statement.executeQuery();
                        if (result.next())
                        {
                                return result.getInt(1); // Return the count
                        }
                } catch (SQLException e)
                {
                        e.printStackTrace(); // Handle the exception according to your requirement
                }
                return 0;
        }

        public int countAllPayment(int id)
        {
                String sql = "SELECT count(*)\n"
                        + "FROM swp_new.payment as pay\n"
                        + "Join user as u on pay.userID = u.id\n"
                        + "Join transac as tran on pay.transId = tran.id\n"
                        + "where pay.userID = ?";
                try
                {
                        PreparedStatement statement = connection.prepareStatement(sql);
                        statement.setInt(1, id);
                        ResultSet result = statement.executeQuery();
                        if (result.next())
                        {
                                return result.getInt(1); // Return the count
                        }
                } catch (SQLException e)
                {
                        e.printStackTrace(); // Handle the exception according to your requirement
                }
                return 0;
        }

        public ArrayList<Pay> excelPayment(int userId)
        {
                String sql = "SELECT pay.*, tran.type "
                        + "FROM swp_new.payment as pay\n"
                        + "JOIN user as u on pay.userID = u.id\n"
                        + "JOIN transac as tran on pay.transId = tran.id\n"
                        + "WHERE pay.userID = ?\n";

                try ( PreparedStatement statement = connection.prepareStatement(sql))
                {
                        statement.setInt(1, userId);
                        try ( ResultSet result = statement.executeQuery())
                        {
                                ArrayList<Pay> payments = new ArrayList<>();
                                while (result.next())
                                {
                                        Pay payment = new Pay();
                                        payment.setId(result.getInt("id"));
                                        payment.setVnp_TxnRef(result.getString("vnp_TxnRef"));
                                        payment.setPayment_code(result.getString("payment_code"));
                                        payment.setAmount(result.getInt("amount"));
                                        payment.setLink(result.getString("link"));
                                        payment.setStatus(result.getBoolean("status"));
                                        payment.setTransId(result.getInt("transId"));
                                        payment.setUserId(result.getInt("userID"));
                                        payment.setCreated_at(result.getTimestamp("created_at"));

                                        payments.add(payment);
                                }
                                return payments;
                        }
                } catch (SQLException e)
                {
                        e.printStackTrace();
                        return null;
                }
        }

}
