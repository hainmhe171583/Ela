/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal.trading;

import dal.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import model.auth.User;
import model.trading.Report;
import model.trading.ReportDto;

/**
 *
 * @author admin
 */
public class ReportDBContext extends DBContext<Report>
{

        @Override
        public ArrayList<Report> getList(String field, String value)
        {
                String sql = "Select * from report\n";
                if (!field.equals(""))
                {
                        sql += "where " + field + " = " + value;
                }
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        ResultSet result = statement.executeQuery();
                        ArrayList<Report> reports = new ArrayList<>();
                        while (result.next())
                        {
                                Report report = new Report();
                                report.setId(result.getInt("id"));
                                report.setCode(result.getString("code"));
                                report.setTitle(result.getString("title"));
                                report.setDescribe(result.getString("describe"));
                                report.setStatusID(result.getInt("statusID"));
                                report.setTradingID(result.getInt("tradingID"));
                                report.setUserID(result.getInt("userID"));
                                report.setCallAdmin(result.getBoolean("call_admin"));
                                report.setCreated_at(result.getTimestamp("created_at"));
                                report.setUpdated_at(result.getTimestamp("updated_at"));
                                report.setDeleted_at(result.getTimestamp("deleted_at"));
                                report.setIsDeleted(result.getBoolean("isDeleted"));

                                reports.add(report);
                        }
                        return reports;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public Report get(String field, String value)
        {
                String sql = "SELECT r.*\n"
                        + "FROM Report AS r\n"
                        + "JOIN Trading AS t ON r.tradingID = t.id\n"
                        + "WHERE " + field + " = ?";

                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        statement.setString(1, value);
                        ResultSet result = statement.executeQuery();
                        while (result.next())
                        {
                                Report report = new Report();
                                report.setId(result.getInt("id"));
                                report.setCode(result.getString("code"));
                                report.setTitle(result.getString("title"));
                                report.setDescribe(result.getString("describe"));
                                report.setStatusID(result.getInt("statusID"));
                                report.setTradingID(result.getInt("tradingID"));
                                report.setUserID(result.getInt("userID"));
                                report.setCallAdmin(result.getBoolean("call_admin"));
                                report.setCreated_at(result.getTimestamp("created_at"));
                                report.setUpdated_at(result.getTimestamp("updated_at"));
                                report.setDeleted_at(result.getTimestamp("deleted_at"));
                                report.setIsDeleted(result.getBoolean("isDeleted"));

                                return report;
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        public Report getByID(String field, String value)
        {
                String sql = "Select * from report\n"
                        + "where " + field + " = ?";

                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        statement.setString(1, value);
                        ResultSet result = statement.executeQuery();
                        while (result.next())
                        {
                                Report report = new Report();
                                report.setId(result.getInt("id"));
                                report.setCode(result.getString("code"));
                                report.setTitle(result.getString("title"));
                                report.setDescribe(result.getString("describe"));
                                report.setStatusID(result.getInt("statusID"));
                                report.setTradingID(result.getInt("tradingID"));
                                report.setUserID(result.getInt("userID"));
                                report.setCallAdmin(result.getBoolean("call_admin"));
                                report.setCreated_at(result.getTimestamp("created_at"));
                                report.setUpdated_at(result.getTimestamp("updated_at"));
                                report.setDeleted_at(result.getTimestamp("deleted_at"));
                                report.setIsDeleted(result.getBoolean("isDeleted"));

                                return report;
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public Report insert(Report report)
        {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        public void createReport(Report report)
        {
                String sql = "INSERT INTO swp_new.report\n"
                        + "(code,"
                        + " `title`,"
                        + " `describe`,"
                        + " statusID,"
                        + " tradingID,"
                        + " call_admin,"
                        + " userID,"
                        + " created_at,"
                        + " created_by,"
                        + " updated_at,"
                        + " deleted_at,"
                        + " isDeleted)\n"
                        + "VALUES(?, ?, ?, ?, ?, 0, ?, now(), ?, null, null, 0);";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        statement.setString(1, report.getCode());
                        statement.setString(2, report.getTitle());
                        statement.setString(3, report.getDescribe());
                        statement.setInt(4, report.getStatusID());
                        statement.setInt(5, report.getTradingID());
                        statement.setInt(6, report.getUserID());
                        statement.setInt(7, report.getCreated_by());
                        statement.executeUpdate();
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
        }

        @Override
        public void update(Report model)
        {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        @Override
        public void delete(int id, User user)
        {
                String sql = "Update swp_new.report\n"
                        + "Set deleted_at = ?,"
                        + " isDeleted = 1\n"
                        + "deleted_by = ?\n"
                        + " Where id = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        Timestamp deleted_at = new Timestamp(System.currentTimeMillis());
                        statement.setTimestamp(1, deleted_at);
                        statement.setInt(2, user.getId());
                        statement.setInt(3, id);
                        statement.executeUpdate();
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
        }

        public boolean updateReport(Report report)
        {
                String sql = "update swp_new.report\n"
                        + "SET code = ?,"
                        + " `title` = ?,"
                        + " `describe` = ?,"
                        + " statusID = ?,"
                        + " tradingID = ?,"
                        + " call_admin = ?,"
                        + " updated_at = now(),"
                        + " updated_by = ?,"
                        + " isDeleted = 0\n"
                        + "Where id = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        statement.setString(1, report.getCode());
                        statement.setString(2, report.getTitle());
                        statement.setString(3, report.getDescribe());
                        statement.setInt(4, report.getStatusID());
                        statement.setInt(5, report.getTradingID());
                        statement.setBoolean(6, report.isCallAdmin());
                        statement.setInt(7, report.getUserID());
                        statement.setInt(8, report.getId());
                        int result = statement.executeUpdate();
                        if (result != 0)
                        {
                                return true;
                        }
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
                return false;
        }

        public void updateReportStatus(int reportid, int statusid)
        {
                String sql = "update swp_new.report\n"
                        + "SET statusID = " + statusid + "\n"
                        + "Where id = " + reportid + ";";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        int result = statement.executeUpdate();
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
        }
        
        public ArrayList<ReportDto> GetAllReportDto(String whereCondition)
        {
                String sql = "select r.Id as reportId, t.code as code, p.Id as productId,"
                        + " t.statusId as statusId, t.Id as tradingId, p.productName,"
                        + " p.userId as salerId, t.userId as buyerId,"
                        + " r.created_at from report r join status s"
                        + " on r.statusId = s.Id join trading t"
                        + " on r.tradingId = t.Id join product p"
                        + " on p.Id = t.productId " + whereCondition;

                PreparedStatement statement = null;
                ArrayList<ReportDto> listReportDto = new ArrayList<ReportDto>();
                try
                {
                        statement = connection.prepareStatement(sql);
                        ResultSet result = statement.executeQuery();

                        while (result.next())
                        {
                                ReportDto reportDto = new ReportDto();
                                reportDto.setReportId(result.getInt("reportId") + "");
                                reportDto.setCode(result.getString("code") + "");
                                reportDto.setProductId(result.getInt("productId") + "");
                                reportDto.setStatusId(result.getInt("statusId") + "");
                                reportDto.setTradingId(result.getInt("tradingId") + "");
                                reportDto.setProductName(result.getString("productName"));
                                reportDto.setSalerId(result.getInt("salerId") + "");
                                reportDto.setBuyerId(result.getInt("buyerId") + "");
                                reportDto.setCreated_at(result.getTimestamp("created_at") + "");

                                listReportDto.add(reportDto);
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return listReportDto;
        }
}
