/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal.trading;

import dal.DBContext;
import dal.auth.UserDBContext;
import dal.product.ProductDBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import model.auth.User;
import model.product.Product;
import model.status.Status;
import model.trading.Trading;

/**
 *
 * @author admin
 */
public class TradingDBContext extends DBContext<Trading>
{

        @Override
        public ArrayList<Trading> getList(String field, String value)
        {
                String sql = "Select * from trading\n";
                if (!field.equals(""))
                {
                        sql += "where " + field + " = " + value;
                }
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        ResultSet result = statement.executeQuery();
                        ArrayList<Trading> tradings = new ArrayList<>();
                        while (result.next())
                        {
                                Trading trading = new Trading();
                                trading.setId(result.getInt("id"));
                                trading.setTotal(result.getDouble("total"));
                                trading.setStatusID(result.getInt("statusID"));
                                trading.setProductID(result.getInt("productID"));
                                trading.setUserID(result.getInt("userID"));
                                trading.setCreated_at(result.getTimestamp("created_at"));
                                trading.setUpdated_at(result.getTimestamp("updated_at"));
                                trading.setDeleted_at(result.getTimestamp("deleted_at"));
                                trading.setIsDeleted(result.getBoolean("isDeleted"));

                                tradings.add(trading);
                        }
                        return tradings;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public Trading get(String field, String value)
        {
                String sql = "Select * from trading\n"
                        + "where " + field + " = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        statement.setString(1, value);
                        ResultSet result = statement.executeQuery();
                        while (result.next())
                        {
                                Trading trading = new Trading();

                                trading.setId(result.getInt("id"));
                                trading.setCode(result.getString("code"));
                                trading.setTotal(result.getDouble("total"));
                                trading.setStatusID(result.getInt("statusID"));
                                trading.setProductID(result.getInt("productID"));
                                trading.setUserID(result.getInt("userID"));
                                trading.setCreated_at(result.getTimestamp("created_at"));
                                trading.setUpdated_at(result.getTimestamp("updated_at"));
                                trading.setDeleted_at(result.getTimestamp("deleted_at"));
                                trading.setIsDeleted(result.getBoolean("isDeleted"));

                                UserDBContext udb = new UserDBContext();
                                trading.setUser(udb.get("id", String.valueOf(result.getInt("userID"))));
                                ProductDBContext pdb = new ProductDBContext();
                                trading.setProduct(pdb.get("id", String.valueOf(result.getInt("productID"))));

                                return trading;
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public Trading insert(Trading trading)
        {
                String sql = "INSERT INTO swp_new.trading\n"
                        + "(code,"
                        + " total,"
                        + " statusID,"
                        + " productID,"
                        + " userID,"
                        + " created_at,"
                        + " updated_at,"
                        + " deleted_at,"
                        + " isDeleted)\n"
                        + "VALUES(?, ?, ?, ?, ?, ?, ?, null, 0);";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        statement.setString(1, trading.getCode());
                        statement.setDouble(2, trading.getTotal());
                        statement.setInt(3, trading.getStatusID());
                        statement.setInt(4, trading.getProductID());
                        statement.setInt(5, trading.getUserID());
                        statement.setTimestamp(6, trading.getCreated_at());
                        statement.setTimestamp(7, trading.getUpdated_at());
                        statement.executeUpdate();
                        Trading new_trading = get("code", trading.getCode());

                        return new_trading;
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        @Override
        public void update(Trading trading)
        {
                String sql = "Update swp_new.trading\n"
                        + "Set code = ?,"
                        + " total = ?,"
                        + " statusID = ?,"
                        + " productID = ?,"
                        + " userID = ?,"
                        + " created_at = ?,"
                        + " updated_at = ?,"
                        + " deleted_at = ?,"
                        + " isDeleted = ?\n"
                        + "Where id = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        statement.setString(1, trading.getCode());
                        statement.setDouble(2, trading.getTotal());
                        statement.setInt(3, trading.getStatusID());
                        statement.setInt(4, trading.getProductID());
                        statement.setInt(5, trading.getUserID());
                        statement.setTimestamp(6, trading.getCreated_at());
                        statement.setTimestamp(7, trading.getUpdated_at());
                        statement.setTimestamp(8, trading.getDeleted_at());
                        statement.setBoolean(9, trading.isIsDeleted());
                        statement.setInt(10, trading.getId());
                        statement.executeUpdate();

                        statement.executeUpdate();
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
        }

        @Override
        public void delete(int id, User user)
        {
                String sql = "Update swp_new.trading\n"
                        + "Set deleted_at = ?,"
                        + " isDeleted = 1\n"
                        + "deleted_by = ?\n"
                        + " Where id = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);

                        Timestamp deleted_at = new Timestamp(System.currentTimeMillis());
                        statement.setTimestamp(1, deleted_at);
                        statement.setInt(2, user.getId());
                        statement.setInt(3, id);

                        statement.executeUpdate();
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
        }

        public ArrayList<Trading> getAll(int id)
        {
                String sql = "SELECT \n"
                        + "    p.*, \n"
                        + "    s.statusName, \n"
                        + "    u.username, \n"
                        + "    c.categoryName, \n"
                        + "    bu.username \n"
                        + "FROM \n"
                        + "    Trading AS t\n"
                        + "JOIN \n"
                        + "    Product AS p ON t.productID = p.id\n"
                        + "JOIN \n"
                        + "    Status AS s ON p.statusID = s.id\n"
                        + "JOIN \n"
                        + "    `User` AS u ON p.userID = u.id\n"
                        + "JOIN \n"
                        + "    Category AS c ON p.categoryID = c.id\n"
                        + "LEFT JOIN \n"
                        + "    `User` AS bu ON t.userID = bu.id\n"
                        + "WHERE \n"
                        + "    p.userID = ? ";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        statement.setInt(1, id);
                        ResultSet result = statement.executeQuery();
                        ArrayList<Trading> tradings = new ArrayList<>();
                        while (result.next())
                        {
                                Trading trading = new Trading();
                                trading.setId(result.getInt("id"));
                                trading.setCode(result.getString("code"));
                                trading.setTotal(result.getDouble("total"));
                                trading.setStatusID(result.getInt("statusID"));
                                trading.setProductID(result.getInt("productID"));
                                trading.setUserID(result.getInt("userID"));
                                trading.setCreated_at(result.getTimestamp("created_at"));
                                trading.setUpdated_at(result.getTimestamp("updated_at"));
                                trading.setDeleted_at(result.getTimestamp("deleted_at"));
                                trading.setCreated_by(result.getInt("created_by"));
                                trading.setUpdated_by(result.getInt("updated_by"));
                                trading.setDeleted_by(result.getInt("deleted_by"));
                                trading.setIsDeleted(result.getBoolean("isDeleted"));
                                tradings.add(trading);
                        }
                        return tradings;
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        public ArrayList<Trading> pagingProductTrading(int pageNumber, int userId)
        {
                ArrayList<Trading> list = new ArrayList<>();
                String sql = "SELECT t.*, u.username as seller_username, ul.username as buyer_username,p.*, s.*\n"
                        + "FROM Trading as t\n"
                        + "Join product as p On t.productID = p.id\n"
                        + "JOIN Status AS s ON t.statusID = s.id\n"
                        + "JOIN User AS u ON t.userId = u.id\n"
                        + "Left Join User as ul on p.userId = ul.id\n"
                        + "where u.id = ? \n"
                        + "order by t.id\n"
                        + "LIMIT 5 OFFSET ?;";
                try ( PreparedStatement statement = connection.prepareStatement(sql))
                {
                        int offset = (pageNumber - 1) * 5; // Calculate offset
                        statement.setInt(1, userId);
                        statement.setInt(2, offset);
                        try ( ResultSet result = statement.executeQuery())
                        {
                                while (result.next())
                                {
                                        Trading trading = new Trading();
                                        trading.setId(result.getInt("id"));
                                        trading.setCode(result.getString("code"));
                                        trading.setCreated_at(result.getTimestamp("created_at"));
                                        trading.setUpdated_at(result.getTimestamp("updated_at"));
                                        trading.setIsDeleted(result.getBoolean("isDeleted"));
                                        trading.setStatusID(result.getInt("statusID"));

                                        // Set Product information
                                        Product product = new Product();
                                        product.setId(result.getInt("id"));
                                        product.setCode(result.getString("code"));
                                        product.setProductName(result.getString("productName"));
                                        product.setDescribe(result.getString("describe"));
                                        product.setPrice(result.getDouble("price"));
                                        product.setStock(result.getInt("stock"));
                                        product.setContact(result.getString("contact"));
                                        product.setDataproduct(result.getString("dataProduct"));
                                        product.setLink(result.getString("link"));
                                        trading.setProduct(product);
                                        // Set Seller's information
                                        User seller = new User();
                                        seller.setUsername(result.getString("seller_username"));
                                        trading.setUser(seller);

                                        // Set Status information
                                        Status status = new Status();
                                        status.setStatusName(result.getString("statusName"));
                                        trading.setStatus(status);

                                        // Set Buyer's information
                                        if (result.getString("buyer_username") != null)
                                        {
                                                User buyer = new User();
                                                buyer.setUsername(result.getString("buyer_username"));
                                                trading.setUser(buyer);
                                        }

                                        // Add the product to the list
                                        list.add(trading);
                                }
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return list;
        }

        public int countAllProductByTrading(int id)
        {
                String sql = "select count(*) \n"
                        + "  From  Trading AS t\n"
                        + "JOIN \n"
                        + "    Product AS p ON t.productID = p.id\n"
                        + "JOIN \n"
                        + "    Status AS s ON p.statusID = s.id\n"
                        + "JOIN \n"
                        + "    `User` AS u ON p.userID = u.id\n"
                        + "JOIN \n"
                        + "    Category AS c ON p.categoryID = c.id\n"
                        + "LEFT JOIN \n"
                        + "    `User` AS bu ON t.userID = bu.id\n"
                        + "WHERE \n"
                        + "    p.userID = ?";
                try
                {
                        PreparedStatement statement = connection.prepareStatement(sql);
                        statement.setInt(1, id);
                        ResultSet result = statement.executeQuery();
                        if (result.next())
                        {
                                return result.getInt(1);
                        }
                } catch (SQLException e)
                {
                        e.printStackTrace();
                }
                return 0;
        }

        public int countAllProductByStatusIDTrading(int id, int sid)
        {
                String sql = "select count(*) \n"
                        + "  From  Trading AS t\n"
                        + "JOIN \n"
                        + "    Product AS p ON t.productID = p.id\n"
                        + "JOIN \n"
                        + "    Status AS s ON p.statusID = s.id\n"
                        + "JOIN \n"
                        + "    `User` AS u ON p.userID = u.id\n"
                        + "JOIN \n"
                        + "    Category AS c ON p.categoryID = c.id\n"
                        + "LEFT JOIN \n"
                        + "    `User` AS bu ON t.userID = bu.id\n"
                        + "WHERE \n"
                        + "    p.userID = ? and t.statusID = ?";
                try
                {
                        PreparedStatement statement = connection.prepareStatement(sql);
                        statement.setInt(1, id);
                        statement.setInt(2, sid);
                        ResultSet result = statement.executeQuery();
                        if (result.next())
                        {
                                return result.getInt(1);
                        }
                } catch (SQLException e)
                {
                        e.printStackTrace();
                }
                return 0;
        }

        public ArrayList<Trading> filterListProductTrading(int pageNumber, int userId, int sid)
        {
                String sql = "SELECT t.*, u.username as seller_username, ul.username as buyer_username,p.*, s.*\n"
                        + "FROM Trading as t\n"
                        + "Join product as p On t.productID = p.id\n"
                        + "JOIN Status AS s ON t.statusID = s.id\n"
                        + "JOIN User AS u ON t.userId = u.id\n"
                        + "Left Join User as ul on p.userId = ul.id\n"
                        + "where u.id = ? and t.statusID = ? \n"
                        + "order by t.id\n"
                        + "LIMIT 5 OFFSET ?;";
                try ( PreparedStatement statement = connection.prepareStatement(sql))
                {
                        int offset = (pageNumber - 1) * 5;
                        statement.setInt(1, userId);
                        statement.setInt(2, sid);
                        statement.setInt(3, offset);
                        try ( ResultSet result = statement.executeQuery())
                        {
                                ArrayList<Trading> list = new ArrayList<>();
                                while (result.next())
                                {
                                        Trading trading = new Trading();
                                        trading.setId(result.getInt("id"));
                                        trading.setCode(result.getString("code"));
                                        trading.setCreated_at(result.getTimestamp("created_at"));
                                        trading.setUpdated_at(result.getTimestamp("updated_at"));
                                        trading.setIsDeleted(result.getBoolean("isDeleted"));
                                        trading.setStatusID(result.getInt("statusID"));

                                        // Set Product information
                                        Product product = new Product();
                                        product.setId(result.getInt("id"));
                                        product.setCode(result.getString("code"));
                                        product.setProductName(result.getString("productName"));
                                        product.setDescribe(result.getString("describe"));
                                        product.setPrice(result.getDouble("price"));
                                        product.setStock(result.getInt("stock"));
                                        product.setContact(result.getString("contact"));
                                        product.setDataproduct(result.getString("dataProduct"));
                                        product.setLink(result.getString("link"));
                                        trading.setProduct(product);
                                        // Set Seller's information
                                        User seller = new User();
                                        seller.setUsername(result.getString("seller_username"));
                                        trading.setUser(seller);

                                        // Set Status information
                                        Status status = new Status();
                                        status.setStatusName(result.getString("statusName"));
                                        trading.setStatus(status);

                                        // Set Buyer's information
                                        if (result.getString("buyer_username") != null)
                                        {
                                                User buyer = new User();
                                                buyer.setUsername(result.getString("buyer_username"));
                                                trading.setUser(buyer);
                                        }

                                        // Add the product to the list
                                        list.add(trading);
                                }
                                return list;
                        }
                } catch (SQLException e)
                {
                        e.printStackTrace();
                        return null;
                }
        }

        public Trading getByID(int id)
        {
                String sql = "SELECT t.*, u.username as seller_username, ul.username as buyer_username,p.*, s.*\n"
                        + "FROM Trading as t\n"
                        + "Join product as p On t.productID = p.id\n"
                        + "JOIN Status AS s ON t.statusID = s.id\n"
                        + "JOIN User AS u ON t.userId = u.id\n"
                        + "Left Join User as ul on p.userId = ul.id\n"
                        + "WHERE \n"
                        + "    t.id = ? ";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        statement.setInt(1, id);
                        ResultSet result = statement.executeQuery();

                        while (result.next())
                        {
                                Trading trading = new Trading();
                                trading.setId(result.getInt("id"));
                                trading.setCreated_at(result.getTimestamp("created_at"));
                                trading.setUpdated_at(result.getTimestamp("updated_at"));
                                trading.setIsDeleted(result.getBoolean("isDeleted"));
                                trading.setStatusID(result.getInt("statusID")); // Get statusID from Trading table

                                // Set Product information
                                Product product = new Product();
                                product.setId(result.getInt("id"));
                                product.setCode(result.getString("code"));
                                product.setProductName(result.getString("productName"));
                                product.setDescribe(result.getString("describe"));
                                product.setPrice(result.getDouble("price"));
                                product.setStock(result.getInt("stock"));
                                product.setContact(result.getString("contact"));
                                product.setDataproduct(result.getString("dataProduct"));
                                product.setLink(result.getString("link"));
                                product.setCreated_at(result.getTimestamp("created_at"));
                                product.setUpdated_at(result.getTimestamp("updated_at"));
                                product.setIsDeleted(result.getBoolean("isDeleted"));
                                product.setStatusID(result.getInt("statusID"));
                                product.setIsPrivate(result.getBoolean("is_Private"));

                                trading.setProduct(product);
                                // Set Seller's information
                                User seller = new User();
                                seller.setUsername(result.getString("seller_username"));
                                trading.setUser(seller);

                                // Set Status information
                                Status status = new Status();
                                status.setId(result.getInt("id")); // Change to statusID
                                status.setStatusName(result.getString("statusName"));
                                trading.setStatus(status);

                                // Set Buyer's information
                                if (result.getString("buyer_username") != null)
                                {
                                        User buyer = new User();
                                        buyer.setUsername(result.getString("buyer_username"));
                                        trading.setUser(buyer);
                                }
                                // Add the product to the list
                                return trading;
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return null;
        }

        public ArrayList<Trading> excelTrading(int userId)
        {
                ArrayList<Trading> list = new ArrayList<>();
                String sql = "SELECT t.*, u.username as seller_username, ul.username as buyer_username,p.*, s.*\n"
                        + "FROM Trading as t\n"
                        + "Join product as p On t.productID = p.id\n"
                        + "JOIN Status AS s ON t.statusID = s.id\n"
                        + "JOIN User AS u ON t.userId = u.id\n"
                        + "Left Join User as ul on p.userId = ul.id\n"
                        + "where u.id = ? \n";
                try ( PreparedStatement statement = connection.prepareStatement(sql))
                {
                        statement.setInt(1, userId);
                        try ( ResultSet result = statement.executeQuery())
                        {
                                while (result.next())
                                {
                                        Trading trading = new Trading();
                                        trading.setId(result.getInt("id"));
                                        trading.setCreated_at(result.getTimestamp("created_at"));
                                        trading.setUpdated_at(result.getTimestamp("updated_at"));
                                        trading.setIsDeleted(result.getBoolean("isDeleted"));
                                        trading.setStatusID(result.getInt("statusID"));

                                        // Set Product information
                                        Product product = new Product();
                                        product.setId(result.getInt("id"));
                                        product.setCode(result.getString("code"));
                                        product.setProductName(result.getString("productName"));
                                        product.setDescribe(result.getString("describe"));
                                        product.setPrice(result.getDouble("price"));
                                        product.setStock(result.getInt("stock"));
                                        product.setContact(result.getString("contact"));
                                        product.setDataproduct(result.getString("dataProduct"));
                                        product.setLink(result.getString("link"));
                                        trading.setProduct(product);
                                        // Set Seller's information
                                        User seller = new User();
                                        seller.setUsername(result.getString("seller_username"));
                                        trading.setUser(seller);

                                        // Set Status information
                                        Status status = new Status();
                                        status.setStatusName(result.getString("statusName"));
                                        trading.setStatus(status);

                                        // Set Buyer's information
                                        if (result.getString("buyer_username") != null)
                                        {
                                                User buyer = new User();
                                                buyer.setUsername(result.getString("buyer_username"));
                                                trading.setUser(buyer);
                                        }

                                        // Add the product to the list
                                        list.add(trading);
                                }
                        }
                } catch (SQLException er)
                {
                        er.printStackTrace();
                }
                return list;
        }

        public void UpdateTradingById(String id, String status)
        {
                String sql = "Update trading set statusId = ? where Id = ?";
                PreparedStatement statement = null;
                try
                {
                        statement = connection.prepareStatement(sql);
                        statement.setString(1, status);
                        statement.setString(2, id);
                        statement.executeUpdate();

                        statement.executeUpdate();
                } catch (Exception er)
                {
                        er.printStackTrace();
                }
        }

}
