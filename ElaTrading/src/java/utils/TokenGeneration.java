/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import dal.common.TokenDBContext;
import java.sql.Timestamp;
import model.common.Token;
import org.apache.commons.lang3.RandomStringUtils;

/**
 *
 * @author admin
 */
public class TokenGeneration
{
        public String getToken(String email)
        {
                String _token = RandomStringUtils.randomAlphanumeric(17);
                Timestamp expiryDate = new Timestamp(System.currentTimeMillis() + 30*60*1000);
                
                Token token = new Token();
                token.setToken(_token);
                token.setEmail(email);
                token.setExpiryDate(expiryDate);
                
                TokenDBContext tokenDB = new TokenDBContext();
                tokenDB.insert(token);
                
                return _token;
        }
}
