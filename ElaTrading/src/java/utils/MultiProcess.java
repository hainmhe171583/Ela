/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import dal.auth.UserDBContext;
import dal.payment.PaymentDBContext;
import dal.payment.WithdrawDBContext;
import dal.product.ProductDBContext;
import dal.trading.TradingDBContext;
import java.sql.Timestamp;
import java.util.UUID;
import model.auth.User;
import model.common.Single;
import model.common._Deque;
import model.payment.Pay;
import model.product.Product;
import model.trading.Trading;
import model.withdraw.Withdraw;

/**
 *
 * @author hai20
 */
public class MultiProcess
{

        public void multiPro()
        {

                UserDBContext newu = new UserDBContext();
                ProductDBContext pdb = new ProductDBContext();
                PaymentDBContext payDB = new PaymentDBContext();
                TradingDBContext tdb = new TradingDBContext();
                WithdrawDBContext wdb = new WithdrawDBContext();

                while (_Deque.getMyrequest().size() != 0)
                {
                        Timestamp now = new Timestamp(System.currentTimeMillis());

                        String id = UUID.randomUUID().toString();
                        String id2 = UUID.randomUUID().toString();

                        Single temp;
                        temp = _Deque.getMyrequest().getFirst();

                        Pay pay = new Pay();
                        Pay payad = new Pay();

                        User admin = newu.get("id", "0");
                        User u2 = newu.get("id", String.valueOf(temp.getUserId()));

                        pay.setPayment_code(id);
                        pay.setAmount(temp.getAmount());
                        pay.setCreated_at(now);
                        pay.setCreated_by(u2.getId());
                        pay.setUserId(u2.getId());

                        //them
                        payad.setPayment_code(id2);
                        payad.setAmount(temp.getAmount());
                        payad.setCreated_at(now);
                        payad.setCreated_by(u2.getId());
                        payad.setUserId(admin.getId());
                        //them

                        Trading trade = null;
                        //for nap tien
                        if (temp.getType() == 1)
                        {
                                if (payDB.get("vnp_TxnRef", temp.getPayment().getVnp_TxnRef()).isStatus() == false)
                                {
                                        u2.setBalance(u2.getBalance() + temp.getAmount());
                                        newu.update(u2);

                                        payDB.update(temp.getPayment());
                                }

                        }
                        //need explain
                        //for add product
                        if (temp.getType() == 2)
                        {
                                if (u2.getBalance() + temp.getAmount() >= 0)
                                {
                                        pay.setStatus(true);
                                        pay.setTransId(2);
                                        payDB.insert(pay);
                                        //
                                        u2.setBalance(u2.getBalance() + temp.getAmount());
                                        newu.update(u2);

                                        payad.setStatus(true);
                                        payad.setTransId(2);
                                        payad.setAmount(temp.getAmount() * (-1));
                                        payDB.insert(payad);
                                        //
                                        admin.setBalance(admin.getBalance() + (-1) * temp.getAmount());
                                        newu.update(admin);

                                        pdb.insert(temp.getProduct());

                                } else
                                {
                                        temp.getProduct().setStatusID(3);
                                        pdb.insert(temp.getProduct());
                                }
                        }

                        //for add trading, decrease money of user when buy
                        //if (temp.getTrading() != null&&temp.getProduct() == null) {
                        if (temp.getType() == 3)
                        {
                                Product pro = pdb.get("id", String.valueOf(temp.getTrading().getProductID()));
                                if (pro.getStatusID() == 1)
                                {
                                        if (u2.getBalance() + temp.getAmount() > 0)
                                        {
                                                pay.setStatus(true);
                                                pay.setTransId(3);
                                                pay.setPayment_code(temp.getTrading().getCode());
                                                payDB.insert(pay);

                                                u2.setBalance(u2.getBalance() + temp.getAmount());
                                                newu.update(u2);

//                                                pro.setStock(0);
                                                pro.setStatusID(2);
                                                pdb.update(pro);

                                                tdb.insert(temp.getTrading());
                                        }
                                }
                        }

                        //add money for who sell
                        //if(temp.getProduct()==null&&temp.getTrading()==null)
                        if (temp.getType() == 4)
                        {
                                Product pro = pdb.get("id", String.valueOf(temp.getTrading().getProductID()));
                                if (pro.getStatusID() == 2 || pro.getStatusID() == 5)
                                {
                                        pay.setStatus(true);
                                        pay.setTransId(4);
                                        pay.setAmount(temp.getAmount() - Math.round(temp.getAmount() / 10));
                                        payDB.insert(pay);

                                        u2.setBalance(u2.getBalance() + temp.getAmount() - Math.round(temp.getAmount() / 10));
                                        newu.update(u2);
                                        //
                                        payad.setStatus(true);
                                        payad.setTransId(7);
                                        payad.setAmount(Math.round(temp.getAmount() / 10));
                                        payDB.insert(payad);

                                        admin.setBalance(admin.getBalance() + Math.round(temp.getAmount() / 10));
                                        newu.update(admin);
                                        //
                                        pro.setStock(0);
                                        pro.setStatusID(4);
                                        pdb.update(pro);

                                        Trading trading = temp.getTrading();
                                        trading.setStatusID(4);
                                        tdb.update(trading);
                                }

                        }

                        //decrease money for who with draw
                        if (temp.getType() == 5)
                        {
                                if (u2.getBalance() + temp.getAmount() >= 0)
                                {
                                        pay.setStatus(true);
                                        pay.setTransId(5);
                                        pay.setPayment_code(temp.getWithdraw().getCode());//
                                        payDB.insert(pay);

                                        u2.setBalance(u2.getBalance() + temp.getAmount());
                                        newu.update(u2);
                                        wdb.insert(temp.getWithdraw());
                                } else
                                {
                                        temp.getWithdraw().setStatusID(2);

                                        wdb.insert(temp.getWithdraw());
                                }
                        }

                        //cac giao dich hoan tra tien
                        if (temp.getType() == 6)
                        {
                                //hoan tra khi yeu cau rut tien that bai
                                if (temp.getWithdraw() != null)
                                {
                                        Withdraw temp_withdraw = wdb.get("code", temp.getWithdraw().getCode());
                                        if (temp_withdraw.getStatusID() == 1)
                                        {
                                                pay.setStatus(true);
                                                pay.setTransId(6);
                                                payDB.insert(pay);

                                                u2.setBalance(u2.getBalance() + temp.getAmount());
                                                newu.update(u2);

                                                wdb.update(temp.getWithdraw());
                                        }
                                }
                                //hoan tra cho user khi report thanh cong
                                if (temp.getTrading() != null)
                                {
                                        trade = tdb.get("code", temp.getTrading().getCode());
                                        Product pro = trade.getProduct();

                                        if (trade.getStatusID() == 5)
                                        {
                                                pay.setStatus(true);
                                                pay.setTransId(6);
                                                payDB.insert(pay);

                                                u2.setBalance(u2.getBalance() + temp.getAmount());
                                                newu.update(u2);

                                                pro.setStatusID(3);
                                                pdb.update(pro);

                                                trade.setStatusID(3);
                                                tdb.update(trade);
                                        }
                                }

                        }
                        _Deque.getMyrequest().pop();
                }
        }
}
