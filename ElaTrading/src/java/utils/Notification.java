/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import Websocket.WebSocketServer;
import dal.notification.Di_subjectDBContext;
import dal.notification.In_subjectDBContext;
import dal.notification.NotificationsDBContext;
import dal.notification.SubjectDBContext;
import dal.product.ProductDBContext;
import java.sql.Timestamp;
import java.util.UUID;
import model.notification.Di_subject;
import model.notification.In_subject;
import model.notification.Notifications;
import model.notification.Send_Notification;
import model.notification.Subject;
import model.product.Product;

/**
 *
 * @author admin
 */
public class Notification
{

        public void Save(String code, String url, Timestamp created_at, int entity, int actor, int receiver, String id)
        {
                NotificationsDBContext notificationDB = new NotificationsDBContext();

                Notifications notification = new Notifications();
                notification.setCode(code);
                notification.setUrl(url);
                notification.setStatus(false);
                notification.setCreated_at(created_at);
                notification.setUpdated_at(created_at);
                notification.setEntityTypeId(entity);
                Notifications new_notification = notificationDB.insert(notification);

                SubjectDBContext subjectDB = new SubjectDBContext();

                Subject subject = new Subject();
                subject.setActorId(actor);
                subject.setNotificationsId(new_notification.getId());
                subjectDB.insert(subject);

                In_subjectDBContext in_subjectDB = new In_subjectDBContext();

                In_subject in_subject = new In_subject();
                in_subject.setReceiverId(receiver);
                in_subject.setNotificationsId(new_notification.getId());
                in_subjectDB.insert(in_subject);

                Di_subjectDBContext di_subjectDB = new Di_subjectDBContext();

                Di_subject di_subject = new Di_subject();
                try
                {
                        int _id = Integer.parseInt(id);
                        di_subject.setProductId(_id);
                } 
                catch (NumberFormatException e)
                {
                        ProductDBContext productDB = new ProductDBContext();
                        Product product = productDB.get("code", id);
                        di_subject.setProductId(product.getId());
                }
//                di_subject.setProductId(id);
                di_subject.setNotificationsId(new_notification.getId());
                di_subjectDB.insert(di_subject);

        }

        public void Create(String sendFrom, int sendFrom_id, String subject, String id, int sendTo, Timestamp created_at, int entity)
        {
                UUID uuid = UUID.randomUUID();

                String code = uuid.toString();
                String message = null;
                String url = null;
                switch (entity)
                {
                        case 1:
                                message = sendFrom + " đã thực hiện giao dịch cho sản phẩm " + subject + " của bạn.";
                                url = "/ElaTrading/action/product?id=" + id;
                                break;
                        // code block
                        case 2:
                                message = sendFrom + " đã chấp nhận giao dịch cho sản phẩm " + subject + " của bạn.";
                                url = "/ElaTrading/action/product?id=" + id;
                                break;
                        // code block
                        case 3:
                                message = sendFrom + " đã khiếu nại về sản phẩm " + subject + " của bạn.";
                                url = "/ElaTrading/action/product?id=" + id;
                                break;
                        // code block
                        case 4:
                                message = sendFrom + " đã rút khiếu nại về sản phẩm " + subject + " của bạn.";
                                url = "/ElaTrading/action/product?id=" + id;
                                break;
                        // code block
                        case 5:
                                message = sendFrom + " đã chấp nhận khiếu nại về sản phẩm " + subject + " của bạn.";
                                url = "/ElaTrading/action/trading?code=" + id;
                                break;
                        // code block
                        case 6:
                                message = "Yêu cầu khiếu nại về sản phẩm " + subject + "đã được chấp nhận.";
                                url = "/ElaTrading/action/product?id=" + id;
                                break;
                        // code block
                        case 7:
                                message = "Yêu cầu khiếu nại về sản phẩm " + subject + "đã được chấp nhận.";
                                url = "/ElaTrading/action/trading?code=" + id;
                                break;
                        // code block
                        case 8:
                                message = "Yêu cầu khiếu nại về sản phẩm " + subject + "đã bị từ chối.";
                                url = "/ElaTrading/action/product?id=" + id;
                                break;
                        // code block
                        case 9:
                                message = "Yêu cầu khiếu nại về sản phẩm " + subject + "đã bị từ chối.";
                                url = "/ElaTrading/action/trading?code=" + id;
                                break;
                        // code block
                        case 10:
                                message = "Yêu cầu nạp tiền của bạn đã được chấp nhận.";
                                break;
                        // code block          
                }
                Send_Notification notification = new Send_Notification();
                notification.setCode(code);
                notification.setMessage(message);
                notification.setUrl(url);
                notification.setCreated_at(created_at);

                WebSocketServer.broadcast(notification, sendTo);
                Save(code, url, created_at, entity, sendFrom_id, sendTo, id);
        }

        public String fetch(int entity, String actor, String description, String pro)
        {
                String[] arr = description.split("_", 2);

                String message = null;
                if (1 <= entity && entity <= 3)
                {
                        message = actor + " " + arr[0] + " " + pro + " " + arr[1];
                } else if (4 <= entity && entity <= 5)
                {
                        message = arr[0] + " " + pro + " " + arr[1];
                } else
                {
                        message = description;
                }
                return message;
        }
}
