/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.Part;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author admin
 */
public class Validate
{

        public String getFieldAjax(HttpServletRequest request, String fieldName, boolean required, String error) throws Exception
        {
                String value = null;
                value = request.getParameter(fieldName);
                if (value == null || value.trim().isEmpty())
                {
                        if (required)
                        {
                                throw new Exception(error);
                        } else
                        {
                                value = null; // Make empty string null so that you don't need to hassle with equals("") afterwards.
                        }
                }
                return value;
        }

        public Part getFieldAjaxFile(HttpServletRequest request, String fieldName, boolean required) throws Exception
        {
                Part value = null;
                value = request.getPart(fieldName);
                if (value == null)
                {
                        if (required)
                        {
                                String error = "Field " + fieldName + " is required";
                                throw new Exception(error);
                        } else
                        {
                                value = null; // Make empty string null so that you don't need to hassle with equals("") afterwards.
                        }
                }
                return value;
        }

        public int fieldInt(String value, String message) throws Exception
        {
                int number = 0;
                try
                {
                        number = Integer.parseInt(value);
                } catch (NumberFormatException e)
                {
                        throw new Exception(message);
                }
                return number;
        }

        public double fieldDouble(String value, String message) throws Exception
        {
                double number = 0;
                try
                {
                        number = Double.parseDouble(value);
                } catch (NumberFormatException e)
                {
                        throw new Exception(message);
                }
                return number;
        }

        public String fieldString(String value, String regex, String message) throws Exception
        {
                if (!value.matches(regex))
                {
                        throw new Exception(message);
                }
                return value;
        }

        public boolean fieldBoolean(String value, String message) throws Exception
        {
                boolean bool = false;
                try
                {
                        bool = Boolean.parseBoolean(value);
                } catch (Exception e)
                {
                        throw new Exception(message);
                }
                return bool;
        }

        public Date fieldDate(String value, String message) throws Exception
        {
                Date date = null;
                try
                {
                        date = Date.valueOf(value);
                } catch (Exception e)
                {
                        throw new Exception(message);
                }
                return date;
        }

        public Timestamp fieldTimestamp(String value, String message) throws Exception
        {
                Timestamp timestamp = null;
                try
                {
                        timestamp = new Timestamp(new java.util.Date(Long.parseLong(value)).getTime());
                } catch (Exception e)
                {
                        throw new Exception(message);
                }
                return timestamp;
        }

        public  boolean isValidPassword(String password)
        {

//                int minLength = 8;
//                int maxLength = 20;
                String regex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).{6,}$";

                Pattern pattern = Pattern.compile(regex);

                Matcher matcher = pattern.matcher(password);

                return matcher.matches();
        }

        public boolean checkPhone(String phone)
        {
                String regex = "^0[984375]{1}\\d{8}$";

                Pattern pattern = Pattern.compile(regex);

                Matcher matcher = pattern.matcher(phone);

                return matcher.matches();

        }

        public boolean checkEmail(String email)
        {
                String regex = "[A-Za-z0-9]+([_.-][A-Za-z0-9]+)*@[A-Za-z0-9]+([_.-][A-Za-z0-9]+)*\\.[A-Za-z]+$";

                Pattern pattern = Pattern.compile(regex);

                Matcher matcher = pattern.matcher(email);

                return matcher.matches();

        }

        public boolean checkName(String name)
        {
                String regex = "^.{4,}$";
                Pattern pattern = Pattern.compile(regex);

                Matcher matcher = pattern.matcher(name);

                return matcher.matches();
        }

}
